//
//  PLServiceBookDateVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 25/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
var selectDatePost = String()
class PLServiceBookDateVC: UIViewController {
    var dateFormatter = DateFormatter()
   
    @IBOutlet var bookDateView: UIView!
    

    @IBOutlet var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        bookDateView.isHidden = false
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.setValue(UIColor.init(red: 11/255, green: 222/255, blue: 224/255, alpha: 1), forKeyPath: "textColor")
        datePicker.backgroundColor = UIColor.clear
        //datePicker.isHighlighted = true
        //datePicker.subviews[0].subviews[1].backgroundColor = UIColor.green
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate = dateFormatter.string(from: datePicker.date)
        //print(selectedDate)
        selectDatePost = selectedDate
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setCurrentDate(){
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let selectedDate = dateFormatter.string(from: datePicker.date)
        selectDatePost = selectedDate
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        guard valiDate()else{return}
        bookDateView.isHidden  = true
        setCurrentDate()
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceLocationPicker") as! PLServiceLocationPicker
        self.present(vc, animated: true)

    }
    func valiDate()-> Bool{
        if selectDatePost == ""{
            showBanner(message: "Please Select Date")
            return false
        }
        else{
            return true
        }
        
    }
    
}
