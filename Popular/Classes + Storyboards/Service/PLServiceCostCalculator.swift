//
//  PLServiceCostCalculator.swift
//  Popular
//
//  Created by Appzoc-Macmini on 19/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLServiceCostCalculator: UIViewController,SideOptionsDelegate,UITextFieldDelegate {
    
    @IBOutlet var modelLBL: UILabel!
    
    @IBOutlet var varientLBL: UILabel!
    
    @IBOutlet var kilometersLBL: UITextField!
    
    @IBOutlet var variantBTNRef: UIButton!
    
    
    var selectedDropDown = Int()
    /// variables
    var carModel = [PLSideOptionsModel]()
    var carVarient = [PLSideOptionsModel]()
    var carModelValue = String()
    var carModelID = String()
    var modelIndex = IndexPath()
    var varientIndex = IndexPath()
    var carVarientValue = String()
    var carVarientID = Int()
    var servicePrice = String()
    var selectedModleId = Int()
    var selectedVarientID = Int()
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        if selectedDropDown == 1{
            carModelValue = data.name
            carModelID = data.id.description
            modelLBL.text = data.name
            modelIndex = indexPath
            self.getCarVarients()
            varientLBL.text = "Variant"
            carVarientID = 0
        }
        else if selectedDropDown == 2{
            carVarientValue = data.name
            varientLBL.text = data.name
            carVarientID = data.id
            varientIndex = indexPath
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kilometersLBL.keyboardType = .numberPad
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getCarModels()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let txt = textField.text, txt == "Kilometers"{
          textField.text = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let txt = textField.text, txt == ""{
            textField.text = "Kilometers"
        }
    }
    
    ////button action
    @IBAction func backAction(_ sender: Any) {
       
        self.dismiss(animated:true, completion: nil)
    }
    
    @IBAction func modelAction(_ sender: Any) {
        selectedDropDown = 1
        //print(carModel)
        showSideOption(presentOn: self, delegate: self, source: carModel,indexPath: modelIndex)
    }
    
    @IBAction func varientAction(_ sender: Any) {
        if carVarient.count > 0{
            selectedDropDown = 2
           // print(carVarient)
            showSideOption(presentOn: self, delegate: self, source: self.carVarient)
        }else{
            showBanner(message: "Please Select Model")
        }
    }
    
    
    @IBAction func calculateAction(_ sender: Any) {
//        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "PLServiveRequestedPrice") as! PLServiveRequestedPrice
//
//
//        secondViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.present(secondViewController, animated: true)
          self.serviceCost()
    }
    
    
}






////web
extension PLServiceCostCalculator{
    
    private func getCarModels(){
        BaseThread.asyncMain {
            let id = PLAppState.session.userID.description
            var parameterModelList = [String:Any]()
            parameterModelList["user_id"] = id
            
            print(parameterModelList)
            
            WebServices.getMethodWith(url:"getmodels" ,
                                   parameter: [:],
                                            CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        
                       // print(parameterModelList)
                        
                        
                        let data = result["data"] as! [[String:Any]]
                        //print(data)
                       
                        let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                        self.carModel = modeldata
                        self.variantBTNRef.isUserInteractionEnabled = true
                        //                        for model in data {
                        //                            print(model)
                        //
                        //                            let value = model["carmodelname"] as! String
                        //                            let id = model["carmodelid"] as! String
                        //
                        //                            self.carModelValue.append(value)
                        //                            self.carModelID.append(Int(id)!)
                        //
                        //                        }
                        //print(self.carModelValue)
                        //print(self.carModelID)
                        
                    }
                    else
                    {
                       // showBanner(message: "Oops error..!")
                    }
            })
            
            
            
        }
    }
    
    private func getCarVarients(){
        BaseThread.asyncMain {
            guard self.validateData() else { return }
            _ = PLAppState.session.userID.description
            var parameterVarientList = [String:Any]()
            //parameterVarientList["user_id"] = id
            print("Selected Car Model ID : ",self.carModelID)
            parameterVarientList["modelid"] = Int(self.carModelID)
            
            WebServices.postMethod(url:"getvariants" ,
                                            parameter: parameterVarientList,
                                            CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        let data = result["data"] as! [[String:Any]]
                        let modeldata = PLSideOptionsModel.getData(fromArray: data, idKey: "id", nameKey: "name", isStringId: false)
                        self.carVarient = modeldata
                        BaseThread.asyncMain {
                           
                        }
                        //                        print(data)
                        
                        //                        for model in data {
                        //                            print(model)
                        //
                        //                            let value = model["name"] as! String
                        //                            let id = model["id"] as! Int
                        //
                        //                            self.carVarientValue.append(value)
                        //                            self.carVarientID.append(id)
                        //
                        //                        }
                        //                        print(self.carVarientValue)
                        //                        print(self.carVarientID)
                        
                    }
                    else
                    {
                        //showBanner(message: "Oops error..!")
                    }
            })
            
            
        }
    }
    func validateData()-> Bool{
        if (carModelID == ""){
             showBanner(message: "Please Select Model")
            return false
        }
        else {
            return true
        }
    
    }
    private func serviceCost(){
        guard self.validateData2()else {return}
        var parameter = [String:Any]()
        parameter["carmodelid"] = self.carModelID
        parameter["variantid"] = self.carVarientID
        parameter["kilometer"] = kilometersLBL.text!
        
        WebServices.postMethod(url:"serviceCost" ,
                                        parameter: parameter,
                                        CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    
                    let data = result["data"] as! [Json]
                    print(data)
                    for model in data{
                           let value = model["price"] as? Int ?? 0
                           self.servicePrice = value.description
                    }
                    
                    BaseThread.asyncMain {
                        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiveRequestedPrice") as! PLServiveRequestedPrice
                        print(self.servicePrice)
                        vc.approxPrice = self.servicePrice
                        vc.carModelID = self.carModelID
                        vc.variantID = self.carVarientID.description
                        vc.kilometer = self.kilometersLBL.text!
                        // vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                else
                {
                   // showBanner(message: "Oops error..!")
                }
        })
        
    }
    
    func validateData2()-> Bool{
        if (carVarientID != 0 && kilometersLBL.text != ""){
            return true
        }
        else  {
            if(carVarientID == 0){
                 showBanner(title: "", message: "Please select Car Variant")
            }
            else{
                showBanner(title: "", message: "Please Enter Distance")
            }
           // showBanner(title: "", message: "Plz fill data")
            return false
        }
    }
    
}

