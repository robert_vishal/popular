//
//  PLServiceBookType.swift
//  Popular
//
//  Created by Appzoc-Macmini on 26/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class serviceTypeCell : UICollectionViewCell{
    @IBOutlet var typeLabel: UILabel!
    
}

var postTypeId = 0
var bookType = 1

class PLServiceBookType: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet var chooseServiceType: UIView!
    var serviceId = [Int]()
    var serviceName = [String]()
    
    
    var currentIndex:IndexPath?
    var previousIndex:IndexPath?
    
    func setDefaultSelection(){
        let initialIndex = IndexPath(item: 0, section: 0)
        currentIndex = initialIndex
        postTypeId = serviceId[0]
        DispatchQueue.main.async {
            self.typelistCV.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceId.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! serviceTypeCell
         cell.typeLabel.text = serviceName[indexPath.row]
        if let current = currentIndex, current == indexPath {
             cell.typeLabel.textColor = UIColor.selectionLightBlue
           
          //  cell.customLabel.textColor = UIColor.redColor()
        } else {
             cell.typeLabel.textColor = UIColor.gray
            // change color back to whatever it was
            //cell.customLabel.textColor = UIColor.blackColor()
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentIndex = indexPath
        typelistCV.reloadData()
        postTypeId = serviceId[indexPath.row]
//        if let prev = previousIndex{
//            let cell = collectionView.cellForItem(at: prev) as! serviceTypeCell
//            DispatchQueue.main.async {
//                cell.typeLabel.textColor = UIColor.gray
//            }
//        }
//        previousIndex = currentIndex
//        currentIndex = indexPath
//        if let current = currentIndex{
//            let cell = collectionView.cellForItem(at: current)  as! serviceTypeCell
//            DispatchQueue.main.async {
//                cell.typeLabel.textColor = UIColor.selectionLightBlue
//            }
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if serviceName.count > 0{
            let label = UILabel()
            label.font = UIFont(name: "Roboto-Bold", size: 26)
            label.text = serviceName[indexPath.row]
            let size = label.intrinsicContentSize
            let returnSize = CGSize(width: size.width + 10 , height: size.height )
            //print(returnSize)
            return returnSize
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    @IBOutlet var typelistCV: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseServiceType.isHidden = false
        postTypeId = 0
        self.getServiceType()
        // Do any additional setup after loading the view.
    }

   
    @IBAction func NextButtonAction(_ sender: Any) {
        guard valiDate()else{return}
        self.bookService()

    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func valiDate() ->Bool{
        if postTypeId == 0{
            showBanner(message: "Please Choose Any Service Type")
            return false
        }
        else{
            return true
        }
    }
    
}

extension PLServiceBookType{
    func getServiceType(){
        WebServices.getMethodWith(url: "getServiceType", parameter: ["":""],CompletionHandler: { (isFetched, result) in
            if isFetched
            {
                print(result)
                let data = result["data"] as! [[String:Any]]
               // print(data)
                BaseThread.asyncMain {
                    for item in data{
                        let Id = item["id"] as! Int
                        let name = item["type"] as! String
                        self.serviceId.append(Id)
                        self.serviceName.append(name)
                    }
                    self.setDefaultSelection()
                    self.typelistCV.reloadData()
                    self.typelistCV.collectionViewLayout.invalidateLayout()
                }
            }
            else
            {
                // showBanner(message: "Oops error..!")
            }
        })
    }
    
    private func bookService(){
        var parameter = [String:Any]()
        parameter["userid"] = PLAppState.session.userID.description
        parameter["carmodelid"] = carModelPostId
        parameter["variant_id"] = carVarientPostId
        parameter["regno"] = regPostNo
        parameter["date"] = selectDatePost
        parameter["locationid"] = locationPostId
        parameter["ishome"] = ishome//id
        parameter["typeid"] = postTypeId
        
        print("Book Service Parameters : ",parameter.description)
        
        WebServices.postMethod(url:"bookService" ,
                                         parameter: parameter,
                                         CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    //print(data)
                    BaseThread.asyncMain{
                        self.chooseServiceType.isHidden = true
                        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceOriginalPrice") as! PLServiceOriginalPrice
                        self.present(vc, animated: true)
                    }
                }
                else
                {
                   // showBanner(message: "Oops error..!")
                }
        })
        
    }

}
