//
//  PLServiceHistory.swift
//  Popular
//
//  Created by Appzoc-Macmini on 19/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
class PLServiceHistoryTVC: UITableViewCell{
    @IBOutlet var bckColorView: UIView!
    @IBOutlet var serviceDistance: UILabel!
    @IBOutlet var serviceStatus: UILabel!
    @IBOutlet var carName: UILabel!
    @IBOutlet var date: UILabel!
}

class PLServiceHistory: UIViewController {
    @IBOutlet var historyTV: UITableView!
    
    var historyModel : [PLServiceHistoryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.serviceHistory()
    }

    @IBAction func backAction(_ sender: Any) {
       self.dismiss(animated:true, completion: nil)
    }
    
}

extension PLServiceHistory : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PLServiceHistoryTVC") as! PLServiceHistoryTVC
        
        let source = historyModel[indexPath.row]
        cell.date.text = source.date.description
        cell.carName.text =  source.carmodel_id
        cell.serviceStatus.text = source.service_current_status.capitalized
        cell.serviceDistance.text = "\(source.servicename) \(source.kilometer) Kms"
        if(indexPath.row % 2 == 0){
             cell.bckColorView.backgroundColor = UIColor.from(hex: "EC4C2E")
        }else {
             cell.bckColorView.backgroundColor = UIColor(red: 82/255.0, green: 174/255.0, blue: 179/255.0, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let source = historyModel[indexPath.row]
        let vc = UIStoryboard(name: "Services", bundle: nil).instantiateViewController(withIdentifier: "PLServiceHistoryDetails") as! PLServiceHistoryDetails
        vc.date = source.date
        vc.kilometers = source.kilometer.description
        vc.workNature = source.work_nature
        vc.status = source.status.description
        vc.serviceName = source.servicename
        vc.serviceStatus = source.service_current_status
        vc.serviceID = source.id.description
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
extension PLServiceHistory{
    private func serviceHistory(){
        var parameter = [String:Any]()
        
        let id = PLAppState.session.userID.description
        parameter["userid"] = id
        
        WebServices.postMethod(url:"getserviceHistory",
                                        parameter: parameter,
                                        CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    _ = result["data"] as? [Json]
                    guard let data = result["data"] as? [Json], !data.isEmpty else { return }
                    let source = PLServiceHistoryModel.getValue(from: data)
                   // print(source[0].id)
                    
                    self.historyModel = source.reversed()
                    BaseThread.asyncMain {
                        self.historyTV.reloadData()
                    }
                }
                else
                {
                   // showBanner(message: "Oops error..!")
                }
        })
        
    }
}

class PLServiceHistoryModel{
    var id: Int = 0
    var user_id: Int = 0
    var branch_id: Int = 0
    var carmodel_id: String = ""
    var servicename: String = ""
    var type_id: Int = 0
    var regno: String = ""
    var date: String = ""
    var ishome: Int = 0
    var kilometer: Int = 0
    var phone: String =  ""
    var feedback: String =  ""
    var status: Int =  0
    var work_nature: String =  ""
    var service_current_status: String =  ""
        class func getValue(from object: Json?) -> PLServiceHistoryModel? {
            guard let object = object else { return PLServiceHistoryModel()}
            return PLServiceHistoryModel(object)
        }
    
        class func getValue(from array: JsonArray?) -> [PLServiceHistoryModel] {
            var modelArray : [PLServiceHistoryModel] = []
            guard let array = array else { return modelArray }
            for item in array{
                let object = PLServiceHistoryModel.getValue(from: item)
                modelArray.append(object!)
            }
            return modelArray
        }
        required public init(_ object: Json) {
         id = object["id"] as? Int ?? 0
         user_id = object["user_id"] as? Int ?? 0
         carmodel_id = object["carmodel_id"] as? String ?? ""
         branch_id = object["branch_id"] as? Int ?? 0
         servicename = object["servicename"] as? String ?? ""
         type_id = object["type_id"] as? Int ?? 0
         regno = object["regno"] as? String ?? ""
         date = object["date"] as? String ?? ""
         ishome = object["ishome"] as? Int ?? 0
         kilometer = object["kilometer"] as? Int ?? 0
         phone = object["phone"] as? String ?? ""
         feedback = object["feedback"] as? String ?? ""
         status = object["status"] as? Int ?? 0
         work_nature = object["work_nature"] as? String ?? ""
         service_current_status = object["service_current_status"] as? String ?? ""
        }
    
        private init(){}
    
    }


