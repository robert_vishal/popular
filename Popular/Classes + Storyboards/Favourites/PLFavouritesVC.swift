//
//  PLFavouritesVC.swift
//  Popular
//
//  Created by Appzoc on 26/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit


class favCell: UITableViewCell {
    
    @IBOutlet var carImage: UIImageView!
    @IBOutlet var deleteBTN: UIButton!
    @IBOutlet var carName: UILabel!
    @IBOutlet var tittleOne: UILabel!
    @IBOutlet var tittleTwo: UILabel!
    
}

//protocol FavouritesUsedCarDelegate {
//    func didClearAll()
//    func didDelete(deletedUsedCarId: Int)
//}
//
class PLFavouritesVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
        
        @IBOutlet weak var headerCV: UICollectionView!
        @IBOutlet weak var favTV: UITableView!
        
        
        var isVideo = false
        
        var selectedHeader = 0
        
        var indX = 0
    
    var newCarIndex:Int?
    
    
    @IBOutlet var listEmptyIndicatorLBL: UILabel!
    
    let favSource = [String:Any]()
    var newcar = [[String:Any]](){
        didSet{
            if selectedHeader == 0{
                DispatchQueue.main.async {
                    if self.newcar.isEmpty{
                        self.listEmptyIndicatorLBL.isHidden = false
                    }else{
                        self.listEmptyIndicatorLBL.isHidden = true
                    }
                }
            }
        }
    }
    
    var usedCar = [[String:Any]](){
        didSet{
            if selectedHeader == 1{
                DispatchQueue.main.async {
                    if self.usedCar.isEmpty{
                        self.listEmptyIndicatorLBL.isHidden = false
                    }else{
                        self.listEmptyIndicatorLBL.isHidden = true
                    }
                }
            }
        }
    }
    
    var Access = [[String:Any]](){
        didSet{
            if selectedHeader == 2{
                DispatchQueue.main.async {
                    if self.Access.isEmpty{
                        self.listEmptyIndicatorLBL.isHidden = false
                    }else{
                        self.listEmptyIndicatorLBL.isHidden = true
                    }
                }
            }
        }
    }
    
    var listType:FavouritesListSection = .newCars
    //final var usedCarDelegate: FavouritesUsedCarDelegate? //  this is for reload the used car list vc if the user clear all pressed
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerCV.dataSource = self
        headerCV.delegate = self
        listType = favouritesListType
        setListType()
       // headerCV.reloadData()
        addSwipeToTable()
        // Do any additional setup after loading the view.
    }
    
    func addSwipeToTable(){
        let swipeGestureRight = UISwipeGestureRecognizer()
        swipeGestureRight.direction = .right
        swipeGestureRight.addTarget(self, action: #selector(swipeRight))
        self.favTV.addGestureRecognizer(swipeGestureRight)
        let swipeGestureLeft = UISwipeGestureRecognizer()
        swipeGestureLeft.direction = .left
        swipeGestureLeft.addTarget(self, action: #selector(swipeLeft))
        self.favTV.addGestureRecognizer(swipeGestureLeft)
    }
    
    @objc func swipeLeft(){
        //print("Swiped Left - Progressing Right")
        if selectedHeader < 2{
            selectedHeader += 1
            if selectedHeader == 2{
                isVideo = true
            }else{
                isVideo = false
            }
            favTV.reloadData()
            headerCV.reloadData()
            checkIfListEmpty()
        }
    }
    
    @objc func swipeRight(){
        //print("Swiped Right - Progressing Left")
        if selectedHeader > 0{
            selectedHeader -= 1
            if selectedHeader == 2{
                isVideo = true
            }else{
                isVideo = false
            }
            favTV.reloadData()
            headerCV.reloadData()
            checkIfListEmpty()
        }
    }
    
    func checkIfListEmpty(){
        switch selectedHeader {
        case 0:
            DispatchQueue.main.async {
                if self.newcar.isEmpty{
                    self.listEmptyIndicatorLBL.isHidden = false
                }else{
                    self.listEmptyIndicatorLBL.isHidden = true
                }
            }
        case 1:
            DispatchQueue.main.async {
                if self.usedCar.isEmpty{
                    self.listEmptyIndicatorLBL.isHidden = false
                }else{
                    self.listEmptyIndicatorLBL.isHidden = true
                }
            }
        case 2:
            DispatchQueue.main.async {
                if self.Access.isEmpty{
                    self.listEmptyIndicatorLBL.isHidden = false
                }else{
                    self.listEmptyIndicatorLBL.isHidden = true
                }
            }
        default:
            print("Invalid Section")
        }
    }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
        func setListType(){
            switch listType {
                case .newCars:
                break
                    //print("New Car Section")
                case .usedCars:
                    //print("Used Cars Section")
                    selectedHeader = 1
                    headerCV.reloadData()
                    favTV.reloadData()
                case .accessories:
                break
                    //print("Accessories Section")
//                    headerCV.selectItem(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .centeredHorizontally)
//                    headerCV.delegate?.collectionView!(headerCV, didSelectItemAt: IndexPath(row: 2, section: 0))
            }
        }
        
        
        @IBAction func but_BackTapped(_ sender: UIButton) {
            
            self.dismiss(animated: true, completion: nil)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            getFavCount()
        }
    
    func clearAllDataFromList(){
        
        
        let id = PLAppState.session.userID
        
        var param = [String:Any]()
        
        var url1 = ""
        
        if selectedHeader == 0
        {
            
            url1 = "clearnewcarfavlist"
            param["userid"] = id
        }
        else if selectedHeader == 1
        {
            url1 = "clearfavlist"
            param["user_id"] = id
        }
        else if selectedHeader == 2
        {
            url1 = "delete/lifestyle_fav"
            param["user_id"] = id
            
        }
        
        WebServices.postMethod(url:url1, parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    showBanner(title: "", message: "Favourite list cleared")
                    self.getFavCount()
                    /// used carlist
                    if self.selectedHeader == 1 {
                        //self.usedCarDelegate?.didClearAll()
                        NotificationCenter.default.post(Notification(name: .usedCarFavouriteAll, object: nil, userInfo: nil))
                        
                    }
                }
                else
                {
                    //showBanner(title: "", message: "Favourite list cleared", color: .green)
                }
                
                
        })
        
        
    }
    
    @IBAction func clearAllBTM(_ sender: UIButton) {
        let confirmationVC = UIAlertController(title: "Clear Favourites", message: "Do you want to delete all items in list?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.clearAllDataFromList()
            confirmationVC.dismiss(animated: false, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "No", style: .default) { (action) in
            confirmationVC.dismiss(animated: true, completion: nil)
        }
        confirmationVC.addAction(okAction)
        confirmationVC.addAction(cancelAction)
        self.present(confirmationVC)
    }
    

    func getFavCount(){
            
            let id = PLAppState.session.userID
            
            WebServices.postMethod(url:"getfavlist", parameter: ["user_id":id] ,
                                   CompletionHandler:
                { (isFetched, result) in
                    
                    let dt = result["data"] as? [String:Any] ?? [:]
                    
                    self.newcar.removeAll()
                     self.usedCar.removeAll()
                     self.Access.removeAll()
                    
                    if isFetched
                    {
                        
                        self.newcar = dt["newcarfav"] as! [[String:Any]]
                        self.usedCar = dt["usedcarfav"] as! [[String:Any]]
                        self.Access = dt["accessories"] as! [[String:Any]]
                        //print(dt["accessories"])
                    }
                    self.favTV.reloadData()
                    
            })
            
        }
        
    }
    
    extension PLFavouritesVC {
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            //        NSInteger viewWidth = COLLECTIONVIEW_WIDTH;
            //        NSInteger totalCellWidth = CELL_WIDTH * _numberOfCells;
            //        NSInteger totalSpacingWidth = CELL_SPACING * (_numberOfCells -1);
            //
            //        NSInteger leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2;
            //        NSInteger rightInset = leftInset;
            
            if collectionView == headerCV {
                
//                let viewWidth = Int(collectionView.frame.width)
//                let totalCellWidth = 265
//                let totalSpacingWidth = 0
//                let leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
//                return UIEdgeInsetsMake(0, CGFloat(leftInset), 0, CGFloat(leftInset))
                return UIEdgeInsetsMake(0, 15, 0, 15)
                
            } else {
                return UIEdgeInsetsMake(5, 10, 5, 10)
            }
        }
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
                let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
               // layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 0
                layout.invalidateLayout()
                //return CGSize(width: 95, height: self.headerCV.frame.height)
                return CGSize(width: (collectionView.frame.width/3) - 10, height: collectionView.frame.height)
            
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
                return 3
            
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryHeaderCVC", for: indexPath) as! galleryHeaderCVC
                //set title
                if indexPath.row == 0 {
                    cell.titleLBL.text = "New Cars"
                    
                } else if indexPath.row == 1 {
                    cell.titleLBL.text = "Used Cars"
                } else {
                    cell.titleLBL.text = "Lifestyle"
                }
                
                //set color for font
                if indexPath.row == selectedHeader {
                    cell.titleLBL.textColor = UIColor.white
                } else {
                    cell.titleLBL.textColor = UIColor.white.withAlphaComponent(0.5)
                }
                
                return cell
            
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            _ = indexPath.row
            
            if collectionView == headerCV {
                
                
                if indexPath.row == 0 {
                    isVideo = false
                    selectedHeader = 0
                     favTV.reloadData()
                    headerCV.reloadData()
                    checkIfListEmpty()
                }
                else if indexPath.row == 1 {
                    isVideo = false
                    selectedHeader = 1
                    favTV.reloadData()
                    headerCV.reloadData()
                    checkIfListEmpty()
                }
                else if indexPath.row == 2
                {
                    isVideo = true
                    selectedHeader = 2
                    favTV.reloadData()
                    headerCV.reloadData()
                    checkIfListEmpty()
                }
                
            }

        }
        
}

extension PLFavouritesVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedHeader == 0
        {
          return self.newcar.count
        }
        else if selectedHeader == 1
        {
           return self.usedCar.count
        }
        else if selectedHeader == 2
        {
           return self.Access.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let section = indexPath.section
//        let row = indexPath.row
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "favCell", for: indexPath) as? favCell else { return UITableViewCell() }
        
       // self.newcar = dt["newcarfav"] as! [[String:Any]]
        //self.usedCar = dt["usedcarfav"] as! [[String:Any]]
        //self.Access
        
        cell.deleteBTN.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        cell.deleteBTN.tag = 0
        
         if selectedHeader == 0
         {
            let list = self.newcar[indexPath.row]
            //print("New Car Image : ",newcar[indexPath.row]["images"] as Any)
            let mURL = list["images"] as? String ?? ""
            let mod = list["model"] as? String ?? ""
            let yr = list["year"] as? String ?? ""
            
            //print("Image link : \(mURL)")
            cell.carImage.kf.setImage(with: URL(string: mURL), options: [.requestModifier(getImageAuthorization())])
            cell.tittleOne.text = "Model :" + mod
             cell.tittleTwo.text = "Year :" + yr
            
            cell.carName.text = mod.capitalized
            
            cell.deleteBTN.tag = list["newcarid"] as? Int ?? 0
            cell.tittleOne.isHidden = true
            cell.tittleTwo.isHidden = true
         }
        else if selectedHeader == 1
         {
            
            let list = self.usedCar[indexPath.row]
            let mURL = list["images"] as? String ?? ""
            let mod = list["model"] as? String ?? ""
            let yr = list["year"] as? Int ?? 0
            
            
            cell.carImage.kf.setImage(with: URL(string: mURL), options: [.requestModifier(getImageAuthorization())])
            cell.tittleOne.text = "Model :" + mod
            cell.tittleTwo.text = "Year :" + "\(yr)"
            
            cell.carName.text = mod.capitalized
            
            cell.deleteBTN.tag = list["usedcarid"] as? Int ?? 0
            
            cell.tittleOne.isHidden = false
            cell.tittleTwo.isHidden = false
        }
         else if selectedHeader == 2
         {
            
            let list = self.Access[indexPath.row]
            let mURL = list["images"] as? String ?? ""
            let mod = list["model"] as? String ?? ""
            let name = list["name"] as? String ?? ""
            _ = list["year"] as? Int ?? 0
            
            cell.carName.text = name.capitalized
            
            
            cell.carImage.kf.setImage(with: URL(string:  mURL), options: [.requestModifier(getImageAuthorization())])
            cell.tittleOne.text = "Model : " + mod
            cell.tittleTwo.text = ""//"Year :" + "\(yr)"
            
            cell.deleteBTN.tag = list["productid"] as? Int ?? 0
            
            cell.tittleOne.isHidden = false
            cell.tittleTwo.isHidden = false
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 147
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      //  tableView.deselectRow(at: indexPath, animated: false)
        
        if selectedHeader == 0
        {
            let list = self.newcar[indexPath.row]
            let idForSelection = list["newcarid"] as? Int ?? 0
            WebServices.getMethodWith(url: "get/new_cars/\(idForSelection)", parameter: ["user_id":PLAppState.session.userID.description], CompletionHandler: { (isComplete, json) in
                if isComplete{
                    
                    guard let data = json["data"] as? [String:Any] else { return }
                    let dataSourceForSelection = NewCarListDataModel()
                    //print("Selected Data ",dataSourceForSelection.carListData.map({$0.name}))
                    dataSource = dataSourceForSelection.getNewCarList(dataDict: data)
                    let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLNewCarDetailVC") as! PLNewCarDetailVC
                    if dataSourceForSelection.carListData.count > 0 ,dataSourceForSelection.carListData[0].colorImageas.count != 0
                    {
                        nextVC.ImageString = dataSourceForSelection.carListData[0].colorImageas[0].url
                    }
                    
                    nextVC.CarName = dataSourceForSelection.carListData[0].name
                    nextVC.carPrice = dataSourceForSelection.carListData[0].price
                    nextVC.isNex = dataSourceForSelection.carListData[0].isnexa
                    nextVC.ind = 0
                    //print("Is Favourite ",dataSourceForSelection.carListData[0].isFav, "as Bool ",dataSourceForSelection.carListData[0].isfavourite)
                    nextVC.isFav = dataSourceForSelection.carListData[0].isFav
                    self.present(nextVC, animated: true, completion: nil)
                }
            })
        }
        else if selectedHeader == 1
        {
            
            let list = self.usedCar[indexPath.row]
            let idForSelection = list["usedcarid"] as? Int ?? 0
            let segueVC = self.storyBoardUsedCars?.instantiateViewController(withIdentifier: "PLUsedCarsDetailsVC") as! PLUsedCarsDetailsVC
            segueVC.usedCarId = idForSelection
            self.present(segueVC, animated: true, completion: nil)
            
        }
        else if selectedHeader == 2
        {
            
            let list = self.Access[indexPath.row]
            _ = list["images"] as? String ?? ""
            _ = list["model"] as? String ?? ""
            _ = list["year"] as? Int ?? 0
            
           // print(list.description)
            
        }
    }
    
    func updateFavouritesDataForNewCar(id:Int){
        if let index = newCarIndex{
            let newCarID = dataSource.carListData[index].id
            if newCarID == id{
                dataSource.carListData[index].isfavourite = false
                dataSource.carListData[index].isFav = 0
            }
        }
    }
    

    func deleteItemFromList(sender: UIButton){
        //print("Button Clicked")
        let id = PLAppState.session.userID
        
        var param = [String:Any]()
        
        var url1 = ""
        
        
        if selectedHeader == 0
        {
            param["newcarid"] = sender.tag
            url1 = "deletenewcarfavlist"
            param["userid"] = id
        }
        else if selectedHeader == 1
        {
            param["usedcarid"] = sender.tag
            url1 = "deletefavlist"
            param["user_id"] = id
        }
        else if selectedHeader == 2
        {
            url1 = "delete/lifestyle_fav"
            param["user_id"] = id
            param["product_id"] = sender.tag
            param["type"] = "1"
            print("Params ",param)
        }
        
        
        WebServices.postMethod(url:url1, parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    showBanner(title: "", message: "Deleted")
                    self.getFavCount()
                    ///used car
                    self.updateFavouritesDataForNewCar(id: sender.tag)
                    if self.selectedHeader == 1 {
                        //self.usedCarDelegate?.didDelete(deletedUsedCarId: sender.tag)
                        NotificationCenter.default.post(Notification(name: .usedCarFavourite, object: nil, userInfo: ["deletedIds": sender.tag]))
                        
                    }
                }
                else
                {
                    //showBanner(title: "", message: "Favourite list cleared", color: .green)
                }
        })
        
    }
    
    @objc func buttonClicked(sender : UIButton) {
        let confirmationVC = UIAlertController(title: "Delete", message: "Do you want to delete item from list?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.deleteItemFromList(sender: sender)
            confirmationVC.dismiss(animated: false, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "No", style: .default) { (action) in
            confirmationVC.dismiss(animated: true, completion: nil)
        }
        confirmationVC.addAction(okAction)
        confirmationVC.addAction(cancelAction)
        self.present(confirmationVC)
    }


}

enum FavouritesListSection{
    case newCars
    case usedCars
    case accessories
}
