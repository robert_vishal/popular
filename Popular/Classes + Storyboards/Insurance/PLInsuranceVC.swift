//
//  PLInsuranceVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import CHIPageControl

class PLInsuranceVC: UIViewController, InterfaceSettable, SideOptionsDelegate {

    @IBOutlet var slideShow: ImageSlideshow!
    @IBOutlet var pageControll: CHIPageControlJaloro!
    @IBOutlet var mobileTF: UITextField!
    @IBOutlet var advisorNoImage: UIImageView!
    @IBOutlet var advisorYesImage: UIImageView!
    @IBOutlet var advisorNoLBL: UILabel!
    @IBOutlet var advisorYesLBL: UILabel!
    @IBOutlet var insuranceDetailWV: UIWebView!
    @IBOutlet var heightWV: NSLayoutConstraint!
    @IBOutlet var locationTF: UITextField!
    @IBOutlet var vehicleNumberTF: UITextField!
    
    private var slideShowSource: [InputSource] = []
    private var currentSlideShowIndex: Double = 0
    private var advisorCallStatus: Int = 1
    private lazy var placeholderMobile = "Enter Mobile Number"
    private var locArry = [String:Any]()
    private var selectedIndexPath: IndexPath?
    private var locationId: Int = 0
    //private var insuranceId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getWebData()
        getLocation()
        setUpInterface()
    }

    func setUpInterface() {
        slideShow.layer.cornerRadius = 6
        slideShow.slideshowInterval = 5
        slideShow.contentScaleMode = .scaleAspectFill
        slideShow.pageIndicator = nil
        pageControll.progress = 0
        mobileTF.isUserInteractionEnabled = false
        insuranceDetailWV.scrollView.isScrollEnabled = false
        //mobileTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        if PLAppState.session.isLoggedIn {
            mobileTF.text = PLAppState.session.userMobileNumber
        }
        
        advisorYesTapped(UIButton())
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func locationTapped(_ sender: UIButton) {
        if !locArry.isEmpty {
            showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! JsonArray, isStringId: false))
            
        }else {
            getLocation(isFromLocationBTN: true)
        }

    }
    
    @IBAction func advisorYesTapped(_ sender: UIButton) {
        advisorYesImage.image = #imageLiteral(resourceName: "checked-symbol-greenINS")
        advisorNoImage.image = #imageLiteral(resourceName: "close-browser-grayINS")
        advisorYesLBL.textColor = .themeGreen
        advisorYesLBL.alpha = 1.0
        advisorNoLBL.textColor = .black
        advisorNoLBL.alpha = 0.15
        advisorCallStatus = 1

    }
    
    @IBAction func advisorNoTapped(_ sender: UIButton) {
        advisorYesImage.image = #imageLiteral(resourceName: "checked-symbol-grayINS")
        advisorNoImage.image = #imageLiteral(resourceName: "close-browser-greenINS")
        advisorYesLBL.textColor = .black
        advisorYesLBL.alpha = 0.15
        advisorNoLBL.textColor = .themeGreen
        advisorNoLBL.alpha = 1.0
        advisorCallStatus = 0
    }
    
    @IBAction func editMobileTapped(_ sender: UIButton) {
        mobileTF.isUserInteractionEnabled = true
    }
    
    @IBAction func submitTapped(_ sender: BaseButton) {
        guard validateData() else { return }
        postWebData()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    
    // Delegate from sideoption menu
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        locationTF.text = data.name
        locationId = data.id
        selectedIndexPath = indexPath
    }
    
    
    // Mark: -  functions
    private func validateData() -> Bool {
        if BaseValidator.isNotEmpty(string: mobileTF.text), mobileTF.text != placeholderMobile, locationId != 0, BaseValidator.isNotEmpty(string: locationTF.text), BaseValidator.isNotEmpty(string: vehicleNumberTF.text) {
            
            if BaseValidator.isValid(digits: mobileTF.text!), mobileTF.text!.count >= 10 {
                return true
            }else {
                showBanner(message: "Please enter valid mobile number")
                return false
            }
        }else {
            
            if !BaseValidator.isNotEmpty(string: mobileTF.text) || mobileTF.text == placeholderMobile {
                showBanner(message: "Please enter mobile number")
                return false
            
            }else if !BaseValidator.isNotEmpty(string: vehicleNumberTF.text){
                showBanner(message: "Please enter vehicle registration number")
                return false

            }else if locationId == 0 || !BaseValidator.isNotEmpty(string: locationTF.text) {
                showBanner(message: "Please choose the location")
                return false
            }
            
            showBanner(message: "Please fill all the fields")
            return false
        }
        
    }
    
    
    
    
}

// Mark:- Web view and textfield delegates
extension PLInsuranceVC: UIWebViewDelegate, UITextFieldDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.scrollView.isScrollEnabled = false
        heightWV.constant = webView.scrollView.contentSize.height
        self.view.setNeedsLayout()

    }
    
    /// text field handling - 13 digit phone validation
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == mobileTF {
            return restrictTextFor(limit: 13, fullString: textField.text!, range: range, string: string)

        }else {
            return true
        }
        
    }
    
 /*   @objc func textFieldDidChange(_ textField: UITextField) {
//        print("textFieldDidChange",textField.text)
        if !BaseValidator.isNotEmpty(string: textField.text) {
            mobileTF.text = placeholderMobile
        }else if textField.text == placeholderMobile {
     
        }
    }*/
    

}

// Mark:- Web services handling
extension PLInsuranceVC {
    private func getWebData(){
        var params = Json()
        params["user_id"] = PLAppState.session.userID
        let url = "getinsurance"
        WebServices.postMethod(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else {
                BaseThread.asyncMain {
                    self.dismiss()
                }
                return
            }
            
            guard let responseData = response["data"] as? Json else {
                BaseThread.asyncMain {
                    self.dismiss()
                }
                return
            }
            
            if let banners = responseData["banner"] as? [String] {
                banners.forEach({ (item) in
                    self.slideShowSource.append(KingfisherSource(urlString: item, placeholder: nil, options: [.requestModifier(getImageAuthorization())])!)

                })
                
                BaseThread.asyncMain {
                    self.slideShow.setImageInputs(self.slideShowSource)
                    self.pageControll.numberOfPages = self.slideShowSource.count
                    self.slideShow.currentPageChanged = { index in
                        self.currentSlideShowIndex = Double(index)
                        self.pageControll.progress = self.currentSlideShowIndex
                    }
                }
                
            }
            
            if let insurancedoc = responseData["insurancedoc"] as? String {
                BaseThread.asyncMain {
                    self.insuranceDetailWV.loadHTMLString(insurancedoc, baseURL: nil)
                }
            }
            /*
            if let mobile = responseData["mobile_number"] as? String {
                BaseThread.asyncMain {
                    self.mobileTF.text = mobile
                    self.mobileTF.isUserInteractionEnabled = false

                }
            }else {
                BaseThread.asyncMain {
                    self.mobileTF.text = nil //self.placeholderMobile
                    self.mobileTF.isUserInteractionEnabled = true

                }
            }
            */
            
        }
    }
    
    private func postWebData(){
        var params = Json()
        params["user_id"] = PLAppState.session.userID.description
        params["phone_number"] = mobileTF.text!
        params["advisor_call_status"] = advisorCallStatus.description
        params["location_id"] = locationId.description
        params["registeration_no"] = vehicleNumberTF.text!
        let url = "insuranceSubmit"
        let imageParam = JsonImage()
        //imageParam["document[]"] = insuranceDocuments
        
        WebServices.postMethodMultiPartImage(url: url, parameter: params, imageParameter: imageParam) { (isSucceeded, response) in
            guard isSucceeded else {
                showBanner(message: "Submission failed")
                return
            }
            
            guard let responseData = response["data"] as? Json else { return }
            if let insuranceId = responseData["insurance_id"] as? Int {
                params["insurance_id"] = insuranceId.description
            }
            
            BaseThread.asyncMain {
                let segueVC = self.storyBoardInsurance?.instantiateViewController(withIdentifier: "PLInsuranceThankingVC") as! PLInsuranceThankingVC
                segueVC.parameters = params
                self.present(segueVC)
            }
            //showBanner(message: "Submitted successfully")
            
        }
        
    }

    private func getLocation(isFromLocationBTN: Bool = false) {
        
        let url: String = "get/locations?type=3"
        WebServices.getMethodWithOutActivity(url:url, parameter: [:], CompletionHandler: { (isFetched, result) in
            if isFetched
            {
                self.locArry = result
                if isFromLocationBTN {
                    BaseThread.asyncMain {
                        self.locationTapped(UIButton())
                    }
                }
                
            }
            else
            {
            }
            
        })

    }
    
    
}
