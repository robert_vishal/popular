//
//  PLInsuranceUploadVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker

protocol InsuranceUploadDelegate {
    func didFinishUploading()
}

class PLInsuranceUploadVC: UIViewController, OpalImagePickerControllerDelegate {

    @IBOutlet var tapToUploadLBL: UILabel!
    @IBOutlet var documentTypeLBL: UILabel!
    @IBOutlet var rcView: UIView!
    @IBOutlet var insuranceView: UIView!
    @IBOutlet var vehicleView: UIView!
    @IBOutlet var rcImage: BaseImageView!
    @IBOutlet var insuranceImage: BaseImageView!
    @IBOutlet var vehicleImage: BaseImageView!
    @IBOutlet var stackContainerView: UIView!
    
    final var delegate: InsuranceUploadDelegate?
    final var parameters: Json = Json()

    private var isAddedRC: Bool = false
    private var isAddedInsurance: Bool = false
    private var isAddedVehicle: Bool = false
    private var selectedRCImage: UIImage?
    private var selectedinsuraceImage: UIImage?
    private var selectedVehicleImage: UIImage?
    private var documentImages: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackContainerView.isHidden = documentImages.isEmpty
        checkDocuments()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func rcCloseTapped(_ sender: UIButton) {
        isAddedRC = false
        selectedRCImage = nil
//        rcView.isHidden = true
//        rcImage.image = selectedRCImage
        checkDocuments()
    }
    
    @IBAction func insuranceCloseTapped(_ sender: UIButton) {
        isAddedInsurance = false
        selectedinsuraceImage = nil
//        insuranceView.isHidden = true
//        insuranceImage.image = selectedinsuraceImage
        checkDocuments()
    }
    
    @IBAction func vehicleCloseTapped(_ sender: UIButton) {
        isAddedVehicle = false
        selectedVehicleImage = nil

//        vehicleView.isHidden = true
//        vehicleImage.image = selectedVehicleImage
        checkDocuments()
    }
    
    @IBAction func uploadImageTapped(_ sender: UIButton) {
        if isAddedRC, isAddedInsurance, isAddedVehicle {
            if let image = selectedRCImage {
                documentImages.insert(image, at: 0)
            }
            if let image = selectedinsuraceImage {
                documentImages.insert(image, at: 1)
            }
            if let image = selectedVehicleImage {
                documentImages.insert(image, at: 2)
            }
            //print("selectedImages",documentImages)
            postWebData()
        }else {
            let imagePicker = OpalImagePickerController()
            imagePicker.maximumSelectionsAllowed = 1
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)

        }

    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        picker.dismiss()
        if !isAddedRC {
            selectedRCImage = images[0].resized(toWidth: 450)
            isAddedRC = true
        }else if !isAddedInsurance {
            selectedinsuraceImage = images[0].resized(toWidth: 450)
            isAddedInsurance = true
        }else if !isAddedVehicle {
            selectedVehicleImage = images[0].resized(toWidth: 450)
            isAddedVehicle = true
        }
        checkDocuments()
    }
    
    private func showHideImages() {
        rcImage.image = selectedRCImage
        rcView.isHidden = rcImage.image == nil
        
        insuranceImage.image = selectedinsuraceImage
        insuranceView.isHidden = insuranceImage.image == nil
        
        vehicleImage.image = selectedVehicleImage
        vehicleView.isHidden = vehicleImage.image == nil
        //stackContainerView.isHidden = false
        stackContainerView.isHidden = (selectedRCImage == nil && selectedinsuraceImage == nil && selectedVehicleImage == nil)
    }
    
    private func checkDocuments() {
        if !isAddedRC {
            tapToUploadLBL.text = "Tap to Upload RC Image"
            tapToUploadLBL.textColor = UIColor.white.withAlphaComponent(0.7)
            documentTypeLBL.isHidden = true
            showHideImages()
        } else if !isAddedInsurance {
            tapToUploadLBL.text = "Tap to Upload"
            tapToUploadLBL.textColor = UIColor.white.withAlphaComponent(0.7)
            documentTypeLBL.text = "Insurance Image"
            documentTypeLBL.isHidden = false
            showHideImages()
        }else if !isAddedVehicle {
            tapToUploadLBL.text = "Tap to Upload"
            tapToUploadLBL.textColor = UIColor.white.withAlphaComponent(0.7)
            documentTypeLBL.text = "Vehicle Image"
            documentTypeLBL.isHidden = false
            showHideImages()
        }else {
            tapToUploadLBL.text = "SUBMIT"
            tapToUploadLBL.textColor = .themeGreen
            documentTypeLBL.isHidden = true
            showHideImages()
        }
    }


}

// Mark:- Web services handling
extension PLInsuranceUploadVC {
    private func postWebData(){
        let url = "insuranceSubmit"
        var imageParam = JsonImage()
        imageParam["document[]"] = documentImages

        
        WebServices.postMethodMultiPartImage(url: url, parameter: parameters, imageParameter: imageParam) { (isSucceeded, response) in
            guard isSucceeded else {
                showBanner(message: "Submission failed")
                return
            }
            BaseThread.asyncMain {
                self.delegate?.didFinishUploading()
                self.dismiss()
            }
            
        }
        
    }

}

