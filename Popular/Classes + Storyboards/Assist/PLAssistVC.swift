//
//  PLAssistVC.swift
//  Popular
//
//  Created by Appzoc on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLAssistVC: UIViewController {

    @IBOutlet var contentTV: UITableView!
    
    private var contentSource: [String] = []
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.white//themeRed
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentTV.estimatedRowHeight = UITableViewAutomaticDimension
        contentTV.estimatedRowHeight = 40
        contentTV.addSubview(refreshControl)
        getDataWeb()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    

}

// Mark: Handling TableView
extension PLAssistVC: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLTheorySessionsTVC", for: indexPath) as? PLTheorySessionsTVC else { return UITableViewCell()}
        
        cell.descriptionLBL.text = contentSource[indexPath.row]
        return cell
    }
    
    // pull refresh
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //print("refresh")
        getDataWebWithOutActivity()
    }
    
    
}

// Mark: Handling Webservice
extension PLAssistVC {
    
    private func getDataWeb() {
        let url = "getNotes"
    
        WebServices.postMethod(url: url, parameter: ["note":"assist"]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            dataArray.forEach({ (item) in
                self.contentSource.append(item["description"] as? String ?? "")
            })
            BaseThread.asyncMain {
                
                self.contentTV.reloadData()
            }
            
        }
    }
    
    private func getDataWebWithOutActivity() {
        let url = "getNotes"
        WebServices.postMethodWithoutActivity(url: url, parameter: ["note":"assist"]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            self.contentSource.removeAll()
            
            var contentSourceNew: [String] = []
            dataArray.forEach({ (item) in
                contentSourceNew.append(item["description"] as? String ?? "")
            })
            BaseThread.asyncMain {
                if !contentSourceNew.isEmpty {
                    self.contentSource = contentSourceNew
                }
                self.refreshControl.endRefreshing()
                self.contentTV.reloadData()
            }
            
        }
    }
    
    
}

