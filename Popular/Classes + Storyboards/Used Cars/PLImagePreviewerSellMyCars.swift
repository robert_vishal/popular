//
//  PLImagePreviewerSellMyCars.swift
//  Popular
//
//  Created by Appzoc on 17/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLImagePreviewerSellMyCarsCVC: UICollectionViewCell {
    @IBOutlet var previewImage: UIImageView!
}

class PLImagePreviewerSellMyCarsVC : UIViewController{
    
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var imageCV: UICollectionView!
    fileprivate var vehicleImagesArray = [#imageLiteral(resourceName: "RITZsuperior-white.jpg"),#imageLiteral(resourceName: "maruti_suzuki_ritz.jpg"),#imageLiteral(resourceName: "RITZsuperior-white.jpg"),#imageLiteral(resourceName: "maruti_suzuki_ritz.jpg"),#imageLiteral(resourceName: "RITZsuperior-white.jpg"),#imageLiteral(resourceName: "RITZsuperior-white.jpg")]

    final var imageSource: [UIImage] = []
    final var selectedImageIndexPath: IndexPath = IndexPath(item: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //imageSource = vehicleImagesArray
        //print("imageSource",imageSource)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  scrollToSelectedImage()
    }
    
    func scrollToSelectedImage() {
        imageCV.scrollToItem(at: selectedImageIndexPath, at: .centeredHorizontally, animated: false)
        titleLBL.text = (selectedImageIndexPath.item + 1).description + " of " + imageSource.count.description
    }
    

}

extension PLImagePreviewerSellMyCarsVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLImagePreviewerSellMyCarsCVC", for: indexPath) as? PLImagePreviewerSellMyCarsCVC else { return UICollectionViewCell() }
        cell.previewImage.image = imageSource[indexPath.item]
       
        //print("cell created",indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        titleLBL.text = (((self.imageCV.indexPathsForVisibleItems.last)?.item)! + 1).description + " of " + imageSource.count.description
    }
}

