//
//  PLSortVC.swift
//  Popular
//
//  Created by Appzoc on 17/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLSortTVC: UITableViewCell {
    
    @IBOutlet var sortLBL: UILabel!
}

enum PLSortMethod: Int {
    case latest
    case priceLowToHigh
    case priceHighToLow
    case none
}

protocol SortDelegate {
    func didSelectSortMethod(sortBy: PLSortMethod)
}

class PLSortVC: UIViewController {

    @IBOutlet var sortMethodTV: UITableView!
    
    final var currentlyAppliedSort: PLSortMethod = .none
    final var delegate: SortDelegate?
    
    fileprivate let sortMehodSource = ["Latest", "Price - Low to High", "Price - High to Low"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    

}

// sort method datasource
extension PLSortVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLSortTVC", for: indexPath) as? PLSortTVC else { return UITableViewCell() }
        
        cell.sortLBL.text = sortMehodSource[indexPath.row]
        
        if currentlyAppliedSort.rawValue == indexPath.row {
            cell.sortLBL.textColor = UIColor.from(hex: "2EB0B5")
        }else{
            cell.sortLBL.textColor = .lightGray
        }
        
        
        return cell
    }
    
}


extension PLSortVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PLSortTVC
        cell.sortLBL.textColor = UIColor.from(hex: "2EB0B5")
        var method: PLSortMethod = .none
        switch indexPath.row {
        case 0:
            method = .latest
        case 1:
            method = .priceLowToHigh
        case 2:
            method = .priceHighToLow
        default:
            method = .none
        }
        currentlyAppliedSort = method
        delegate?.didSelectSortMethod(sortBy: method)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? PLSortTVC else { return }
        cell.sortLBL.textColor = .darkGray

    }
}
