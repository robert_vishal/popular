//
//  PLSellMyCarDetailsVC.swift
//  Popular
//
//  Created by Appzoc on 17/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLSellMyCarDetailsTVC: UITableViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var contentLBL: UILabel!
}

class PLSellMyCarDetailsCVC: UICollectionViewCell {
    @IBOutlet var vehicleImage: UIImageView!
}

class PLSellMyCarDetailsVC: UIViewController, InterfaceSettable {

    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var vehicleImagesCV: UICollectionView!
    @IBOutlet var specificationsTV: UITableView!
    @IBOutlet var exchangeImage: UIImageView!
    @IBOutlet var exchangeLBL: UILabel!
    
    @IBOutlet var specificationsTVHeight: NSLayoutConstraint!
    
    @IBOutlet var emptyBgView: UIView!
    final var carId: Int = 0
    
    private var specificationTitleArray: [String] = []
    
//    private var specificationContentArray = ["Maruthi Suzuki","Celerio VX CL 2012","B XI","KL 45 AB 2255","2017","3400000","Blue","Edappilly","example@gmail.com","1234567890","Good Condition, Tyres Renewed","340000"]
    private var vehicleImagesArray:[UIImage] = []
    private var selectedImageIndexPath: IndexPath = IndexPath(item: 0, section: 0)
    private var dataSource = Json()
    private var dataImageSource: [String] = []
    private var shouldExchangeMyCar: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emptyBgView.isHidden = false

        
        setUpInterface()

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setUpInterface() {
        specificationsTV.rowHeight = UITableViewAutomaticDimension
        specificationsTV.estimatedRowHeight = 100
        specificationsTV.contentInset = UIEdgeInsetsMake(0, 0, 35, 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        emptyBgView.isHidden = false
        getSellCarDetail(carID: carId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //print("before",specificationsTV.contentSize.height)
        self.specificationsTV.layoutIfNeeded()
        self.view.layoutIfNeeded()

        //print("first",specificationsTV.contentSize.height)
        specificationsTVHeight.constant = specificationsTV.contentSize.height + specificationsTV.contentInset.bottom
        self.view.layoutIfNeeded()
        print(specificationsTV.contentSize.height)
        
        specificationsTVHeight.constant = specificationsTV.contentSize.height + specificationsTV.contentInset.bottom
        self.view.layoutIfNeeded()
     //   print("specificationsTV.contentSize.height",specificationsTV.contentSize.height)

        /*
        specificationsTVHeight.constant = specificationsTV.contentSize.height + specificationsTV.contentInset.bottom
        specificationsTV.layoutIfNeeded()
        self.view.layoutIfNeeded()
*/
    }
    
  
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func vehicleImageTapped(_ sender: UIButton) {
        let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLImagePreviewerSellMyCarsVC") as! PLImagePreviewerSellMyCarsVC
        segueVC.imageSource = vehicleImagesArray
        segueVC.selectedImageIndexPath = self.selectedImageIndexPath
        present(segueVC, animated: true, completion: nil)
    }
    
}

// Handling Specifiactions table view data sources
extension   PLSellMyCarDetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specificationTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLSellMyCarDetailsTVC", for: indexPath) as? PLSellMyCarDetailsTVC else { return UITableViewCell() }
        
        let currentTitle = specificationTitleArray[indexPath.row]
        cell.titleLBL.text = currentTitle
        cell.contentLBL.text = dataSource[currentTitle] as? String ?? ""
        return cell
    }
    
}

// Handling vehicles image  collection view data sources
extension   PLSellMyCarDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataImageSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLSellMyCarDetailsCVC", for: indexPath) as? PLSellMyCarDetailsCVC else { return UICollectionViewCell() }
        
        
        cell.vehicleImage.kf.setImage(with: URL(string: dataImageSource[indexPath.item]), placeholder: #imageLiteral(resourceName: "placeholder_Image"), options: [.requestModifier(getImageAuthorization())], progressBlock: nil) { (currentImage, currentError, cache, currentURL) in
            
            if currentImage != nil {
                self.vehicleImagesArray.append(currentImage!)
            }
            
        }
        return cell
    }
    
}

// Handling vehicles image  collection view delegates
extension   PLSellMyCarDetailsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.vehicleImage.image = vehicleImagesArray[indexPath.item]
        self.selectedImageIndexPath = indexPath
    }
}




extension   PLSellMyCarDetailsVC {
    
    func getSellCarDetail(carID:Int) {
        
        var parameterSellCarDetail = [String:Any]()
        
        parameterSellCarDetail["usedcarid"] = carID
        
        
        WebServices.postMethod(url:"getsellmycardetails" ,
                               parameter: parameterSellCarDetail,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    
                    print(parameterSellCarDetail)
                    print(result)
                    //result[""]
                    let errorcode = result["errorcode"] as! Int
                    if errorcode == 0 {
                        let data = result["data"] as! Json
                        self.specificationTitleArray = ["Brand","Model Name","Varient","Register Number","Model Year","Kms Run","Exterior Color","Popular Used Cars Showroom","Email","Mobile Number","Exchange Details","Expected Price"]
                        
                        self.dataSource["Brand"] = (data["brand"] as? String).unwrapBy()
                        self.dataSource["Model Name"] = (data["model"] as? String).unwrapBy()
                        self.dataSource["Varient"] = (data["variant"] as? String).unwrapBy()
                        self.dataSource["Register Number"] = (data["reg_no"] as? String).unwrapBy()
                        
                        if let year = data["model_year"] as? Int {
                            self.dataSource["Model Year"] = year.description
                        }else {
                            self.dataSource["Model Year"] = notMentioned
                        }
                        
                        //self.dataSource["Kms Run"] = data["kilometer"] as? String ?? notMentioned
                        if let kilometer = data["kilometer"] as? Double {
                             self.dataSource["Kms Run"] = kilometer.description
                        }else {
                             self.dataSource["Kms Run"] = notMentioned
                        }

                        self.dataSource["Exterior Color"] = (data["exteriorcolor"] as? String).unwrapBy()
                        self.dataSource["Popular Used Cars Showroom"] = (data["showroom"] as? String).unwrapBy()
                        self.dataSource["Mobile Number"] = (data["phone"] as? String).unwrapBy()
                        self.dataSource["Exchange Details"] = (data["exchange_details"] as? String).unwrapBy()
                        self.dataSource["Email"] = (data["email"] as? String).unwrapBy()
                        
                        if let price = data["expectedprice"] as? Double {
                             self.dataSource["Expected Price"] = price.description
                        }else {
                             self.dataSource["Expected Price"] = notMentioned
                        }
                        
                        self.dataImageSource = data["images"] as? [String] ?? []
                        
                        BaseThread.asyncMain {
                            if !self.dataImageSource.isEmpty {
                                self.vehicleImage.kf.setImage(with: URL(string: self.dataImageSource[0]), placeholder: #imageLiteral(resourceName: "placeholder_Image"), options: [.requestModifier(getImageAuthorization())])

                            }
                            self.vehicleImagesCV.reloadData()
                            self.specificationsTV.reloadData()
                            
                            if self.shouldExchangeMyCar {
                                self.exchangeLBL.text = "Yes"
                                self.exchangeImage.image = #imageLiteral(resourceName: "checked-symbol-greenINS")
                            }else {
                                self.exchangeLBL.text = "No"
                                self.exchangeImage.image = #imageLiteral(resourceName: "close-browser-greenINS")
                                
                            }

                            self.viewDidAppear(true)
                            
                            self.emptyBgView.isHidden = true ////

                        }
                    }else {
                        print("ErroCode errorrrrr")
                    }
                    
                }
                else
                {
                    
                }
        })

        
        
    }
}



//MARK:- Model setting

class usedCarDetailModel {
    
    var carPrimaryData = PLCarDetail()
    var overViewData = [Overview]()
    var specifications = Specifications()
   
    func getUsedCarDetail(dataDic:[String:Any]) -> usedCarDetailModel {
        
        let data = dataDic["data"] as! [String:Any]
        
        carPrimaryData.kilometer = data["kilometer"] as? String ?? "Not given"
        carPrimaryData.exchangeMyCar = data["exchange_my_car"] as? String ?? "Not given"
        carPrimaryData.exchangeDetails = data["exchange_details"] as? String ?? "Not given"
        carPrimaryData.images = data["images"] as? [String] ?? ["Not given"]
        carPrimaryData.expectedPrice = data["expected_price"] as? String ?? "Not given"
        carPrimaryData.regNo = data["reg_no"] as? String ?? "Not given"
        carPrimaryData.modelYear = data["model_year"] as? String ?? "Not given"
        carPrimaryData.popularVerified = data["popular_verified"] as? String ?? "Not given"
        carPrimaryData.noOfOwners = data["no_of_owners"] as? String ?? "Not given"
        carPrimaryData.taxValidity = data["tax_validity"] as? String ?? "Not given"
        carPrimaryData.insuranceValidity = data["insurance_validity"] as? String ?? "Not given"
        carPrimaryData.registrationValidity = data["registration_validity"] as? String ?? "Not given"
        carPrimaryData.imageCount = data["image_count"] as? Int ?? -1
        carPrimaryData.brand = data["brand"] as? String ?? "Not given"
        carPrimaryData.variant = data["variant"] as? String ?? "Not given"
        carPrimaryData.shareLink = data["share_link"] as? String ?? "Not given"
        carPrimaryData.location = data["location"] as? String ?? "Not given"
        carPrimaryData.isfavourite = data["isfavourite"] as? Bool ?? false
       
        carPrimaryData.usedcarid = data["usedcarid"] as? Int ?? -1
        
        let temp = data["features"] as! [String:Any]
       
        carPrimaryData.featuresInData.airConditioning = temp["air_conditioning"] as? String ?? "Not given"
         carPrimaryData.featuresInData.powerWindows = temp["power_windows"] as? String ?? "Not given"
         carPrimaryData.featuresInData.musicSystem = temp["music_system"] as? String ?? "Not given"
        
        let temp2 = data["overview"] as! [[String: Any]]
        let overView = Overview ()
        for tempOverview in temp2 {
        
        overView.bodyType = tempOverview["body_type"] as? String ?? "Not given"
        overView.color = tempOverview["color"] as? String ?? "Not given"
        overView.transmission = tempOverview["transmission"] as? String ?? "Not given"
        overView.maxPower = tempOverview["max_power"] as? String ?? "Not given"
        overView.taxValidity = tempOverview["tax_validity"] as? String ?? "Not given"
        overView.insuranceValidity = tempOverview["insurance_validity"] as? String ?? "Not given"
        overView.model = tempOverview["model"] as? String ?? "Not given"
        overView.noOfOwners = tempOverview["no_of_owners"] as? String ?? "Not given"
        overView.registrationValidity = tempOverview["registration_validity"] as? String ?? "Not given"
        
        overViewData.append(overView)
        }
        
        let temp3 = data["specifications"] as! [String: Any]
        let specification = temp3["specification"] as! [[String: Any]]
        let spec = Specification()
        var specArray = [Specification]()
        for item in specification {
            
            spec.driveTrain = item["drive_train"] as? String ?? "Not given"
            spec.torque = item["torque"] as? String ?? "Not given"
            
            specArray.append(spec)
        }
        
        
        let features = temp3["features"] as! [[String: Any]]
        let featureSingle = Feature()
        var featureArray = [Feature]()
        for item in features {
            
            featureSingle.airConditioning = item["air_conditioning"] as? String ?? "Not given"
            featureSingle.powerWindows = item["power_windows"] as? String ?? "Not given"
            featureSingle.musicSystem = item["music_system"] as? String ?? "Not given"
            featureArray.append(featureSingle)
        }
        
        specifications.specification = specArray
        specifications.features = featureArray
        
        return self
    }
}



class PLCarDetail {
    
    var kilometer = " "
    var exchangeMyCar = " "
    var exchangeDetails = " "
    
    var maxPower = " "
    
    var expectedPrice = " "
    var regNo = " "
    var modelYear = " "
    var popularVerified = " "
    var noOfOwners = " "
    var taxValidity = " "
    var insuranceValidity = " "
    var registrationValidity = " "
    
    var brand = " "
    var variant = " "
    var shareLink = " "
    var location = " "
    
    var featuresInData = Feature()
    var isfavourite = false
    var images = [String]()
    var imageCount = -1
    var usedcarid = -1
   
}


class Overview {
    var bodyType = " "
    var color = " "
    var transmission = " "
    var maxPower = " "
    var taxValidity = " "
    var insuranceValidity = " "
    var model = " "
    var noOfOwners = " "
    var registrationValidity = " "
    var powerWindows = " "
    
}

class Specifications {
    var specification = [Specification]()
    var features =  [Feature]()
    
    
}

class Specification {
    var driveTrain = " "
    var torque = " "
}

class Feature {
    var airConditioning = " "
    var powerWindows = " "
    var musicSystem = " "
}






