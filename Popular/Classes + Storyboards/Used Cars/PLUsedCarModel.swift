//
//  PLUsedCarModel.swift
//  Popular
//
//  Created by admin on 19/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation


import Foundation

//class Pop {
//    let data: [Datum]
//    let favouritecount: Int
//    let banner: [String]
//
//}

class PLUsedCarModel
{
    var CarDetails: [UsedCarListDataModel] = []
    var favouritecount = 0
    var comparecount = 0
    var banner = [String]()
    var usedCarCount:Int = 0
    var batchSize:Int = 0
    
    init() {
    }
    
    func getData(object:Json?) -> PLUsedCarModel {
        guard let DataDict = object else { return PLUsedCarModel() }
        
        self.favouritecount = DataDict["favouritecount"] as? Int ?? 0
        self.banner = DataDict["banner"] as? [String] ?? [notMentioned]
        self.usedCarCount = DataDict["usedcarcount"] as? Int ?? -10
        self.batchSize = DataDict["data_count"] as? Int ?? 10
        if let carList = DataDict["data"] as? JsonArray {
            for carDetial in carList {
                let carData = UsedCarListDataModel()
                carData.usedcarid = carDetial["usedcarid"] as? Int ?? 0
                carData.location = carDetial["location"] as? String ?? notMentioned
                carData.kilometer = carDetial["kilometer"] as? Int ?? -1
                carData.images = carDetial["images"] as? String ?? notMentioned
                carData.expectedPrice = carDetial["expected_price"] as? String ?? notMentioned
                carData.modelYear = carDetial["model_year"] as? Int ?? 0
                if let popularVerified = carDetial["popular_verified"] as? String {
                    carData.popularVerified = popularVerified == "1" ? true : false
                }else{
                    //print("popularVerified cannot extract")
                    carData.popularVerified = false
                }

                carData.brand = carDetial["brand"] as? String ?? notMentioned
                carData.model = carDetial["model"] as? String ?? notMentioned
                carData.variant = carDetial["variant"] as? String ?? notMentioned
                carData.fuelType = carDetial["fuel_type"] as? String ?? notMentioned
                carData.imageCount = carDetial["image_count"] as? Int ?? -1
                carData.shareLink = carDetial["share_link"] as? String ?? notMentioned
                carData.isfavourite = carDetial["isfavourite"] as? Bool ?? false
                carData.iscomparelist = carDetial["iscomparelist"] as? Bool ?? false
                self.CarDetails.append(carData)
            }
        }
        
        return self
    }
}

class UsedCarListDataModel {
    var usedcarid = 0
    var kilometer:Int = 0
    var images = ""
    var expectedPrice = ""
    var modelYear:Int = 0
    var popularVerified = true
    var brand = ""
    var model = ""
    var variant = ""
    var location = ""
    var fuelType = ""
    var imageCount = 0
    var shareLink = ""
    var isfavourite = true
    var iscomparelist = true
}
