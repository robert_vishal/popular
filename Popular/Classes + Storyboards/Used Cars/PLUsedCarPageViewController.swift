//
//  PLUsedCarPageViewController.swift
//  Popular
//
//  Created by admin on 14/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

var iShowingActivity: Bool = true
internal var isViewDidLoadedPageVC: Bool = false

class PLUsedCarPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    
    let firstVC = UIStoryboard(name: "UsedCars", bundle: nil).instantiateViewController(withIdentifier: "PLUsedCarsListVC") as! PLUsedCarsListVC
    
    let secondVC = UIStoryboard(name: "UsedCars", bundle: nil).instantiateViewController(withIdentifier: "PLSellMyCarVC") as! PLSellMyCarVC
    
    let thirdVC = UIStoryboard(name: "UsedCars", bundle: nil).instantiateViewController(withIdentifier: "PLContactUsVC") as! PLContactUsVC
    
    var orderedViewControllers: [UIViewController] = []
    let activity = ActivityIndicator()

//    final var usedCarHomeVC: UIViewController =
    private var isViewLoadedFirstTime: Bool = false
    private var previousVCIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        firstVC.compareFavourDelegate = self
        
        orderedViewControllers = [firstVC,secondVC,thirdVC]
        // Do any additional setup after loading the view.
        self.dataSource = self
        self.delegate = self
        
        isViewLoadedFirstTime = true
        // This sets up the first view that will show up on our page control
        
        if let firstViewController = orderedViewControllers.first {
//            print(orderedViewControllers)
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        //showActivity()
        //print("UsedCarPageViewController_viewDidLoad")
        isViewDidLoadedPageVC = true // this varible is used to check and make webcall in usedcar list vc
    }

    override func viewWillAppear(_ animated: Bool) {
        //print("UsedCarPageViewController_viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // print("UsedCarPageViewController_vviewDidAppearr")
//        if isViewLoadedFirstTime { // this used for showing only first time activity
//            showActivity()
//        }
    }
    
//    func showActivity() {
//        print("showActivity-UsedCarPageViewController")
//        BaseThread.asyncMain {
//            self.activity.start()
//            iShowingActivity = true
//        }
//    }
//
//    func hideActivity(){
//        print("hideActivity")
//
//        BaseThread.asyncMain {
//            self.activity.stop()
//            iShowingActivity = false
//        }
//        isViewLoadedFirstTime = false
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func selectController(atIndex index: Int, animated: Bool) {
        let firstViewController = orderedViewControllers[index]
        NotificationCenter.default.post(name: NSNotification.Name("updateSelextedTab"), object: self, userInfo: ["name": index])
        
        var direction: UIPageViewControllerNavigationDirection = .forward
        
        if index < previousVCIndex {
            direction = .reverse

        }else if index > previousVCIndex {
            direction = .forward
        }
        previousVCIndex = index
        
        setViewControllers([firstViewController], direction: direction, animated: true, completion: nil)
        
        //self.reloadInputViews()
    }

    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        let indexValue = orderedViewControllers.index(of: pageContentViewController)!
        NotificationCenter.default.post(name: NSNotification.Name("updateSelextedTab"), object: self, userInfo: ["name": indexValue])
        //self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
            
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            //return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            //return orderedViewControllers.first
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}

