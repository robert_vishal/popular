//
//  PLUsedCarsListVC.swift
//  Popular
//
//  Created by Appzoc on 10/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import CHIPageControl
import Kingfisher

var needsReloadUsedCarList:Bool = false

//MARK:- UsedCarsSliderImagesTVC
class PLUsedCarsSliderImagesTVC: UITableViewCell {
    @IBOutlet var slideShow: ImageSlideshow!
    @IBOutlet var searchTF: UITextField!
    @IBOutlet var pageControll: CHIPageControlJaloro!
    
    override func awakeFromNib() {
        slideShow.layer.cornerRadius = 5
        
    }
    
    func configure(withData: Json){
        
    }
}


//MARK:- UsedCarsListTVC
class PLUsedCarsListTVC: UITableViewCell {
    
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var verifiedImage: UIImageView!
    @IBOutlet var addTofavouriteBTN: BaseButton!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var imageCountLBL: UILabel!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var priceSymbolLBL: UILabel!
    @IBOutlet var priceLBL: UILabel!
    @IBOutlet var addToCompareBTN: BaseButton!
    @IBOutlet var shareBTN: BaseButton!
    @IBOutlet var yearLBL: UILabel!
    @IBOutlet var kilometersLBL: UILabel!
    @IBOutlet var fuelTypeLBL: UILabel!
    @IBOutlet var segueLoginBTN: UIButton!
}

//MARK:- UsedCarsListVC
class PLUsedCarsListVC: UIViewController, InterfaceSettable, UITextFieldDelegate, SortDelegate, SmartSearchDelegate, UpdateFromFilter {
    
   
    @IBOutlet var vehicleTV: UITableView!
    @IBOutlet var filterCountLBL: UILabel!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTF: UITextField!
    
    
    fileprivate var isVerified: Bool = false
    fileprivate var isLoggedIn: Bool = false
    fileprivate var searchWord: String = ""
    fileprivate var oldSearchWord: String = ""

    fileprivate var favouritedIndexPath: [IndexPath] = []
    fileprivate let favBlankImage = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline.png")
    fileprivate let favFillImage = #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")
    fileprivate var slideShowsource: [ImageSource] = []
    fileprivate var currentSlideShowIndex: Double = 0
    fileprivate var appliedSortMethod: PLSortMethod = .latest
    fileprivate var parameterUsedCarList = [String:Any]()
    fileprivate var parameterUsedCarListPaginated = [String:Any]()

    fileprivate var usedCarsListData: PLUsedCarModel = PLUsedCarModel()
    fileprivate var propertySlidingImagesSource: [UIImage] = []
    fileprivate var isAddedEmptyView = false
    fileprivate var pagination = 1
    fileprivate var activityPaginated = ActivityIndicator()
    fileprivate var isPaginated: Bool = true
    fileprivate var paginationUpdateHalt:Bool = false
    
    fileprivate var tempDataForPagination:[UsedCarListDataModel] = []
    
    fileprivate var paginationCurrentLoadingCount:Int = 0
    
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white//themeRed
        return refreshControl
    }()
    
    private var isFromSmartSearch: Bool = false
    private var smartSearchResultCount: String = 0.description
    private var filterCount: Int = 0{
        didSet{
            checkFilterLabelVisibility()
        }
    }
    private var parameterForFilter = Json()
    private var parameterForSmartSearch = Json()
    private var filterCountCalculator = Json()
    fileprivate var passingJSON:[String:Any] = [:]
    private var currentScrollOffSet: CGPoint = CGPoint(x: 0, y: 0)
    private var selectedIndexPath: IndexPath?
    fileprivate var usedCarCount:Int = 0
    private var isBeginSearchEditing: Bool = false
    private var isBeginSearchEditingOuter: Bool = false
    private var isPaginatedProgressing: Bool = false
    private var isSearchEditingChanged: Bool = false
    private var isSearchEditingChangedOuter: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.alpha = 0
        setUpInterface()
        checkFilterLabelVisibility()
        _ = Notification.Name("notificationLogged")
        NotificationCenter.default.addObserver(self, selector: #selector(reloadVC), name: .loggedIn, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromFavourite(_:)), name: .usedCarFavourite, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromFavouriteAll), name: .usedCarFavouriteAll, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromCompare(_:)), name: .usedCarCompare, object: nil)

        vehicleTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //print("viewWillAppear-UsedCartList")
        super.viewWillAppear(false)
        filterCountLBL.text = filterCount.description
        //filterCountLBL.isHidden = filterCount == 0 ? true : false
        //getCompareFavouriteCountWeb()
        if needsReloadUsedCarList{
            getUsedCarsList()
        }else{
            if let _ = self.parent as? PLUsedCarPageViewController {
                if isViewDidLoadedPageVC {
                    getUsedCarsList()
                    getFilterData()
                    isViewDidLoadedPageVC = false
                }
            }
        }
    }
    
    
    ///    Mark:- notification delegate from compareVC
    @objc func reloadFromCompare(_ notification: NSNotification){
        if let deletedCarId = notification.userInfo?["deletedIds"] as? [Int] {
           // print("deleteedcarids",deletedCarId)
            guard !deletedCarId.isEmpty else { return }
            self.usedCarsListData.CarDetails.forEach { (current) in
                if deletedCarId.contains(current.usedcarid) {
                    current.iscomparelist = false
                }
            }
        }

    }

    ///    Mark:- notification delegate from favouriteVC
    @objc func reloadFromFavourite(_ notification: NSNotification){
        if let deletedId = notification.userInfo?["deletedIds"] as? Int {
            usedCarsListData.CarDetails.forEach { (currentCarData) in
                if currentCarData.usedcarid == deletedId {
                    currentCarData.isfavourite = false
                }
            }
            vehicleTV.reloadData()
        }
    }
    
    @objc func reloadFromFavouriteAll(){
        usedCarsListData.CarDetails.forEach { (currentCarData) in
            currentCarData.isfavourite = false
        }
        vehicleTV.reloadData()
    }

    // reloading data from the details page
    func reloadFromDetails(isFavourited: Bool , isCompared: Bool, carId: Int) {
        print(currentScrollOffSet,"reloadFromDetails",vehicleTV.contentOffset)
        usedCarsListData.CarDetails.forEach { (currentCarData) in
            if currentCarData.usedcarid == carId {
                currentCarData.isfavourite = isFavourited
                currentCarData.iscomparelist = isCompared
                vehicleTV.reloadData()
                if let indexPath = selectedIndexPath {
                    vehicleTV.scrollToRow(at: indexPath, at: UITableViewScrollPosition.none, animated: false)
                    print("vehicleTVVVVV")
                    print(currentScrollOffSet,"reloadFromDetails",vehicleTV.contentOffset)

                }
            }
        }

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        DispatchQueue.main.async {
            self.vehicleTV.reloadData()
            self.view.setNeedsDisplay()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
        
        favouritesListType = .usedCars
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
//        if isAddedEmptyView {
//            self.emptyBackground(vc: self, addOrRemove: false)
//        }
        isFromSmartSearch = false
        smartSearchResultCount = 0.description
        //print("usedcarlist-viewDidDisappear")
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc fileprivate func reloadVC(){
        //print("Reloading VC")
        DispatchQueue.main.async {
            self.vehicleTV.reloadData()
        }
        
    }
    

    /// this is to realod the used car list if the user pressed clear all in favourite_list
    func reloadVC_FavouriteCLearAll() {
        //print("reloadVC_FavouriteCLearAll")
        usedCarsListData.CarDetails.forEach { (currentCarData) in
            currentCarData.isfavourite = false
        }
        vehicleTV.reloadData()
    }
    /// this is to realod the used car list if the user pressed delete button in favourite_list
    func reloadVC_FavouriteDelete(deletedId: Int) {
       // print("reloadVC_FavouriteDelete")
        usedCarsListData.CarDetails.forEach { (currentCarData) in
            if currentCarData.usedcarid == deletedId {
                currentCarData.isfavourite = false
            }
        }
        vehicleTV.reloadData()
    }

    
    
    //MARK:- Setup Interface
    func setUpInterface() {
        vehicleTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
        //vehicleTV.addSubview(self.refreshControl)
        filterCountLBL.layer.cornerRadius = 8
        filterCountLBL.layer.masksToBounds = true
    }
    
    func checkFilterLabelVisibility(){
        DispatchQueue.main.async {
            if self.filterCount == 0{
                self.filterCountLBL.isHidden = true
            }else{
                self.filterCountLBL.isHidden = false
            }
        }
    }
    
    // Update
    func updateSharedFilterWIthSearchData(){
       let colorSource = PLSearchAttributes.shared.colorSource.map({$0.isSelected})
        for (index,item) in colorSource.enumerated(){
            sharedFilterSource?.color[index].isSelected = item
        }
        let fuelSource = PLSearchAttributes.shared.fuelSource.map({$0.isSelected})
        for (index,item) in fuelSource.enumerated(){
            sharedFilterSource?.fuelType[index].isSelected = item
        }
        let categorySource = PLSearchAttributes.shared.bodyTypeSource.map({$0.isSelected})
        for (index,item) in categorySource.enumerated(){
            sharedFilterSource?.category[index].isSelected = item
        }
        let transmissionSource = PLSearchAttributes.shared.transmissionSource.map({$0.isSelected})
        for (index,item) in transmissionSource.enumerated(){
            sharedFilterSource?.transmission[index].isSelected = item
        }
        sharedFilterSource?.price.currentMin = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
        sharedFilterSource?.price.currentMax = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
        sharedFilterSource?.kilometer.currentMin = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue
        sharedFilterSource?.kilometer.currentMax = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue
    }

    
    //MARK:- Actions
    @IBAction func smartSearchTapped(_ sender: UIButton) {
//        showBanner(message: "Coming Soon")
//        return
        let segueVC = storyBoardSearch!.instantiateViewController(withIdentifier: "PLSmartSearchVC") as! PLSmartSearchVC
        segueVC.delegate = self
        present(segueVC)
    }
    
    @IBAction func sortTapped(_ sender: UIButton) {
        let segueVC = storyBoardUsedCars!.instantiateViewController(withIdentifier: "PLSortVC") as! PLSortVC
        segueVC.currentlyAppliedSort = appliedSortMethod
        segueVC.delegate = self
        present(segueVC)
    }
    
    @IBAction func filterTapped(_ sender: UIButton) {
        updateSharedFilterWIthSearchData()
        let segueVC = UIStoryboard(name: "Filter", bundle: nil).instantiateViewController(withIdentifier: "PLFilterVC") as! PLFilterVC
        segueVC.filterJsonData = self.passingJSON
        segueVC.delegate = self
        present(segueVC)
    }
    
    // Handling Table Views Button Events
    
    @IBAction func addToFavouritesTapped(_ sender: BaseButton) {
        if PLAppState.session.userID == 0 {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        else
        {
            var parameter = Information()
            let selectedCarData = usedCarsListData.CarDetails[sender.indexPath!.row]
            
            parameter["user_id"] = PLAppState.session.userID.description
            parameter["usedcarid"] = selectedCarData.usedcarid.description
            
            if selectedCarData.isfavourite {
                sender.setImage(self.favBlankImage, for: [])
                self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = false

                deleteFromFavouriteWeb(params: parameter, completion: { (isSucceeded) in
                    if isSucceeded {
                        print("deleted from favourite")
                        //sender.setImage(self.favBlankImage, for: [])
//                        self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = false
                        BaseThread.asyncMain {
                            self.getCompareFavouriteCountWeb()
                        }
                    }else {
                        print("Not deleted from favourite")
                        sender.setImage(self.favFillImage, for: [])
                        self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = true

                    }
                })
            }else {
                sender.setImage(self.favFillImage, for: [])
                self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = true

                addToFavouriteWeb(params: parameter, completion: { (isSucceeded) in
                    if isSucceeded {
                        print("added to favourite")
                        BaseThread.asyncMain {
                            //sender.setImage(self.favFillImage, for: [])
                            //self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = true
                            BaseThread.asyncMain {
                                self.getCompareFavouriteCountWeb()
                            }
                            
                        }
                    }else {
                        print("Not added to favourite")
                        sender.setImage(self.favBlankImage, for: [])
                        self.usedCarsListData.CarDetails[sender.indexPath!.row].isfavourite = false

                    }
                    
                })
            }
        }
        
   
        
    }
    
    @IBAction func addToCompareTapped(_ sender: BaseButton) {
        
        if PLAppState.session.userID == 0 {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        else
        {
            guard compareCount < 2 else {
                showBanner(message: "Compare list is full")
                return
            }
            
            let selectedCarData = usedCarsListData.CarDetails[sender.indexPath!.row]
            
            guard !selectedCarData.iscomparelist else {
                showBanner(message: "Already in compare list")
                return
            }
            
            var parameter = Information()
            parameter["user_id"] = PLAppState.session.userID.description
            parameter["usedcarid"] = selectedCarData.usedcarid.description
            sender.isUserInteractionEnabled = false
            addToCompareWeb(params: parameter, completion: { (isSucceeded) in
                if isSucceeded {
                    print("addto to compare")
                    showBanner(message: "Added to Compare List")
                    /// self.compareCount += 1
                    self.usedCarsListData.CarDetails[sender.indexPath!.row].iscomparelist = true
                    BaseThread.asyncMain {
                        self.getCompareFavouriteCountWeb(senderBTN: sender ,isFromCompareAction: true)
                    }
                }else {
                    print("not add to compare")
                    BaseThread.asyncMain {
                        sender.isUserInteractionEnabled = true
                    }
                }
            })
            
        }
        
  
        
    }
    
    @IBAction func shareTapped(_ sender: BaseButton) {
        PLAppState.session.commonShare(vc: self, view: self.view, carName: usedCarsListData.CarDetails[sender.indexPath?.row ?? 0].model, carModel: usedCarsListData.CarDetails[sender.indexPath?.row ?? 0].variant, imgURL: usedCarsListData.CarDetails[sender.indexPath?.row ?? 0].images)
    }
    
    @IBAction func segueLoginTapped(_ sender: UIButton) {
        let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
        present(segueVC)

//        let segueVC = storyBoardMain!.instantiateViewController(withIdentifier: "PLLoginVC")
//        present(segueVC, animated: true, completion: nil)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        searchWord = sender.text ?? ""
        if searchTF != sender {
            isSearchEditingChanged = true
        }else {
            isSearchEditingChangedOuter = true

        }
        print("textFieldEditingChanged")
//        if sender == searchTF {
//            print("editing in TOP")
//        }else{
//            print("editing in CELL")
//        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField != searchTF {
            print("textFieldShouldBeginEditing-IF")
            isBeginSearchEditing = true
            return true
        }else {
            print("textFieldShouldBeginEditing-ELSE")
            isBeginSearchEditingOuter = true
            return true

        }
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Editigng Endddddd")
//        if let cell = vehicleTV.cellForRow(at: IndexPath(row: 0, section: 0)) as? PLUsedCarsSliderImagesTVC {
//            //cell.searchTF.resignFirstResponder()
//            print("searchTFsearchTF")
//        }

        if textField == searchTF {
            if searchWord.isNotEmpty && isSearchEditingChangedOuter {
                if isSearchEditingChangedOuter {
                    isSearchEditingChangedOuter = false
                }
                print("textField == searchTF, searchWord.isNotEmpty")
                parameterUsedCarList["search"] = searchWord
                //print("searchWord",searchWord)
                oldSearchWord = searchWord
                getUsedCarsListSortSearch()
                
                
            }else if searchWord.isEmpty && oldSearchWord.isNotEmpty && isSearchEditingChangedOuter {
                print("textField = searchTF , searchWord.isEmpty && oldSearchWord.isNotEmpty")
                if isSearchEditingChangedOuter {
                    isSearchEditingChangedOuter = false
                }
                if isSearchEditingChanged {
                    isSearchEditingChanged = false
                }
                
                parameterUsedCarList["search"] = searchWord
                //print("searchWord",searchWord)
                oldSearchWord = searchWord
                getUsedCarsListSortSearch()
                
            }else {
                
            }
        }else {
            
            if searchWord.isNotEmpty && isSearchEditingChanged {
                if isSearchEditingChanged {
                    isSearchEditingChanged = false
                }
                print("textField = CellTF , searchWord.isNotEmpty")
                parameterUsedCarList["search"] = searchWord
                //print("searchWord",searchWord)
                oldSearchWord = searchWord
                getUsedCarsListSortSearch()
                
            }else if searchWord.isEmpty && oldSearchWord.isNotEmpty && isSearchEditingChanged {
                print("textField = CellTF , searchWord.isEmpty && oldSearchWord.isNotEmpty")
                if isSearchEditingChanged {
                    isSearchEditingChanged = false
                }
                
                parameterUsedCarList["search"] = searchWord
                //print("searchWord",searchWord)
                oldSearchWord = searchWord
                getUsedCarsListSortSearch()
                
            }else {
                
            }
        }
        
//        if searchWord.isNotEmpty && (isSearchEditingChangedOuter || isSearchEditingChanged) {
//            if isSearchEditingChangedOuter {
//                isSearchEditingChangedOuter = false
//            }
//            if isSearchEditingChanged {
//                isSearchEditingChanged = false
//            }
//            print("searchWord.isNotEmpty")
//            parameterUsedCarList["search"] = searchWord
//            //print("searchWord",searchWord)
//            oldSearchWord = searchWord
//            getUsedCarsListSortSearch()
//
//
//        }else if searchWord.isEmpty && oldSearchWord.isNotEmpty && (isSearchEditingChangedOuter || isSearchEditingChanged) {
//            print("searchWord.isEmpty && oldSearchWord.isNotEmpty")
//            if isSearchEditingChangedOuter {
//                isSearchEditingChangedOuter = false
//            }
//            if isSearchEditingChanged {
//                isSearchEditingChanged = false
//            }
//
//            parameterUsedCarList["search"] = searchWord
//            //print("searchWord",searchWord)
//            oldSearchWord = searchWord
//            getUsedCarsListSortSearch()
//
//        }else {
//
//        }
//        guard searchWord.isNotEmpty else { return }
//        parameterUsedCarList["search"] = searchWord
//        print("searchWord",searchWord)
//        oldSearchWord = searchWord
//        getUsedCarsListSortSearch()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("textFieldShouldReturn")
        textField.resignFirstResponder()
        return true
    }

    //MARK:- Sort Delegate method
    func didSelectSortMethod(sortBy: PLSortMethod) {
        appliedSortMethod = sortBy
        
        //print(appliedSortMethod)
        switch appliedSortMethod {
            
        case .latest:
            parameterUsedCarList["sort"] = "latest"
        case .priceLowToHigh:
            parameterUsedCarList["sort"] = "price_lowtohigh"
        case .priceHighToLow:
            parameterUsedCarList["sort"] = "price_hightolow"

        default:
            break
        }
        pagination = 1     // new list with sort
        getUsedCarsListSortSearch()
        
        
    }
    
    
    //MARK:- SmartSearch Delegate method
    func appliedSmartSearch(with parameter: Json, resultCount: String, isFromShowAll: Bool) {
        print("appliedSmartSearch:", PLSearchAttributes.shared.appliedAttributesCount)
        var passingParam = parameter
        
        if resultCount == 0.description {
            isFromSmartSearch = false
            smartSearchResultCount = resultCount
        }else {
            if isFromShowAll {
                isFromSmartSearch = true

            }else {
                isFromSmartSearch = false

            }
            smartSearchResultCount = resultCount
            
            //parameterUsedCarList.removeAll()
            filterCountCalculator = passingParam
            
            
            if let listType = parameterForFilter[parameterKeys.listtypes.rawValue]{
                passingParam[parameterKeys.listtypes.rawValue] = listType
            }
            
            if let brand = parameterForFilter[parameterKeys.brand.rawValue]{
                passingParam[parameterKeys.brand.rawValue] = brand
            }
            
            if let models = parameterForFilter[parameterKeys.models.rawValue]{
                passingParam[parameterKeys.models.rawValue] = models
            }
            
            if let locations = parameterForFilter[parameterKeys.locations.rawValue]{
                passingParam[parameterKeys.locations.rawValue] = locations
            }
            
            if let year = parameterForFilter[parameterKeys.year.rawValue]{
                passingParam[parameterKeys.year.rawValue] = year
            }
            
            if let ownership = parameterForFilter[parameterKeys.ownership.rawValue]{
                passingParam[parameterKeys.ownership.rawValue] = ownership
            }
            
            guard let json = try? JSONSerialization.data(withJSONObject: passingParam, options: []) else { return }
            guard let content = String(data: json, encoding: String.Encoding.utf8) else { return }
            
            parameterForSmartSearch = passingParam
            parameterUsedCarList["filter"] = content
//            print("Passing parameters: ",passingParam)
            pagination = 1
            getUsedCarsList()
        }
        calculateFilterLabelCount()

    }

    
    //Mark:- delegates from filter
    func updateFromFilter() {
        print("Update From filter")
        paginationUpdateHalt = true
        pagination = 1
        isFromSmartSearch = true
        updateSearchDataWithFilterData()
        needsReloadUsedCarList = false
        generateFilterParamsAndWebCall()
    }
    
    func clearFromFilter() {
        print("Clear from filter")
        pagination = 1
        clearSearchDataFilterData()
        parameterUsedCarList.removeAll()
        filterCount = 0
        needsReloadUsedCarList = true
    }
    
    func clearTempData() {
        self.tempDataForPagination.removeAll()
    }
    
    func resumeFromHalt() {
        paginationUpdateHalt = false
        self.usedCarsListData.CarDetails.append(contentsOf: tempDataForPagination)
        self.tempDataForPagination.removeAll()
        DispatchQueue.main.async {
            self.vehicleTV.reloadData()
        }
    }
    
    func forceViewWillAppear() {
        self.viewWillAppear(true)
    }
    
    
    private func clearSearchDataFilterData(){
        let source = PLSearchAttributes.shared
        source.isSelectedPrice = false
        source.isSelectedKilometer = false
        source.colorSource.forEach { $0.isSelected = false }
        source.bodyTypeSource.forEach { $0.isSelected = false }
        source.fuelSource.forEach { $0.isSelected = false }
        //source.featuresSource.forEach { $0.isSelected = false }
        source.transmissionSource.forEach { $0.isSelected = false }
    }
    
    
    @inline(__always) func updateSearchDataWithFilterData(){
        if let filterData = sharedFilterSource{
            for (index,item) in filterData.color.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.colorSource[index].isSelected = true
                }else {
                    PLSearchAttributes.shared.colorSource[index].isSelected = false
                }
            }
            for (index,item) in filterData.fuelType.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.fuelSource[index].isSelected = true
                }else {
                    PLSearchAttributes.shared.fuelSource[index].isSelected = false

                }
            }
            for (index,item) in filterData.category.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.bodyTypeSource[index].isSelected = true
                }else {
                    PLSearchAttributes.shared.bodyTypeSource[index].isSelected = false

                }
            }
            for (index,item) in filterData.transmission.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.transmissionSource[index].isSelected = true
                }else {
                    PLSearchAttributes.shared.transmissionSource[index].isSelected = false

                }
            }
            PLSearchAttributes.shared.selectedPriceMin = filterData.price.currentMin.int64Value
            PLSearchAttributes.shared.selectedPriceMax = filterData.price.currentMax.int64Value
            PLSearchAttributes.shared.selectedKilometerMin = filterData.kilometer.currentMin.intValue
            PLSearchAttributes.shared.selectedKilometerMax = filterData.kilometer.currentMax.intValue
        }
        
    }
    
    func generateFilterParamsAndWebCall(){
        if let sharedFilter = sharedFilterSource{
            
            parameterForFilter.removeAll()
            
            if parameterForSmartSearch.keys.contains(parameterKeys.features.rawValue) {
                parameterForFilter[parameterKeys.features.rawValue] = parameterForSmartSearch[parameterKeys.features.rawValue]
            }
            
            var countFilterTemp = 0
            let brands = sharedFilter.brand.filter({$0.isSelected == true}).map({$0.id})
           // let models = sharedFilter.carModel.filter({$0.isSelected == true}).map({$0.modelid})
            let location = sharedFilter.location.filter({$0.isSelected}).map({$0.id})
            let fueltype = sharedFilter.fuelType.filter({$0.isSelected}).map({$0.id})
            let transmission = sharedFilter.transmission.filter({$0.isSelected}).map({$0.id})
            let categories = sharedFilter.category.filter({$0.isSelected}).map({$0.id})
            let features = sharedFilter.feature.filter({$0.isSelected}).map({$0.id})
            let price = [sharedFilter.price.currentMin,sharedFilter.price.currentMax]
            let kilometers = [sharedFilter.kilometer.currentMin,sharedFilter.kilometer.currentMax]
            let ownership = sharedFilter.selectedOwnerShip.filter({$0.isSelected}).map({$0.value})
            let color = sharedFilter.color.filter({$0.isSelected}).map({$0.color})
            let year = [sharedFilter.year.currentMin,sharedFilter.year.currentMax]
            
            if brands.count > 0{
                parameterForFilter[parameterKeys.brand.rawValue] = brands
                countFilterTemp += 1
            }
//            if models.count > 0{
//                parameterForFilter[parameterKeys.models.rawValue] = models
//                countFilterTemp += 1
//            }
            if location.count > 0{
                parameterForFilter[parameterKeys.locations.rawValue] = location
                countFilterTemp += 1
            }
            if fueltype.count > 0{
                parameterForFilter[parameterKeys.fueltypes.rawValue] = fueltype
                countFilterTemp += 1
            }
            if transmission.count > 0{
                parameterForFilter[parameterKeys.transmissions.rawValue] = transmission
                countFilterTemp += 1
            }
            if categories.count > 0{
                parameterForFilter[parameterKeys.categories.rawValue] = categories
                countFilterTemp += 1
            }
            if features.count > 0{
                parameterForFilter[parameterKeys.listtypes.rawValue] = features
              //  countFilterTemp += 1  // no need to include, this is only for smart search
            }
            if sharedFilter.price.currentMin != sharedFilter.price.min.CGFloatValue || sharedFilter.price.currentMax != sharedFilter.price.max.CGFloatValue{
                countFilterTemp += 1
                parameterForFilter[parameterKeys.price.rawValue] = price
            }
            if sharedFilter.kilometer.currentMin != sharedFilter.kilometer.min.CGFloatValue || sharedFilter.kilometer.currentMax != sharedFilter.kilometer.max.CGFloatValue{
                countFilterTemp += 1
                parameterForFilter[parameterKeys.kilometers.rawValue] = kilometers
            }
            if ownership.count > 0{
                parameterForFilter[parameterKeys.ownership.rawValue] = ownership
                countFilterTemp += 1
            }
            if color.count > 0{
                parameterForFilter[parameterKeys.colors.rawValue] = color
                countFilterTemp += 1
            }
            if sharedFilter.year.currentMin != sharedFilter.year.min.CGFloatValue || sharedFilter.year.currentMax != sharedFilter.year.max.CGFloatValue{
                countFilterTemp += 1
                parameterForFilter[parameterKeys.year.rawValue] = year
            }
            self.filterCount = countFilterTemp
            self.filterCountLBL.text = self.filterCount.description
            print("Filter Count ",filterCount)
            
            guard let json = try? JSONSerialization.data(withJSONObject: parameterForFilter, options: []) else { return }
            guard let content = String(data: json, encoding: String.Encoding.utf8) else { return }
            
            parameterUsedCarList["filter"] = content
//            print("Content : ",content)
//            print("Passing : ",parameterUsedCarList)
            getUsedCarsList()
            
        }
    }

    private func calculateFilterLabelCount() {
        print("calculateFilterLabelCount")
        if let sharedFilter = sharedFilterSource {
            var countFilterTemp = 0
            let brands = sharedFilter.brand.filter({$0.isSelected == true}).map({$0.id})
          //  let models = sharedFilter.carModel.filter({$0.isSelected == true}).map({$0.modelid})
            let location = sharedFilter.location.filter({$0.isSelected}).map({$0.id})
            let fueltype = sharedFilter.fuelType.filter({$0.isSelected}).map({$0.id})
            let transmission = sharedFilter.transmission.filter({$0.isSelected}).map({$0.id})
            let categories = sharedFilter.category.filter({$0.isSelected}).map({$0.id})
            let features = sharedFilter.feature.filter({$0.isSelected}).map({$0.id})
            //let price = [sharedFilter.price.currentMin,sharedFilter.price.currentMax]
            //let kilometers = [sharedFilter.kilometer.currentMin,sharedFilter.kilometer.currentMax]
            let ownership = sharedFilter.selectedOwnerShip.filter({$0.isSelected}).map({$0.value})
            let color = sharedFilter.color.filter({$0.isSelected}).map({$0.color})
            //let year = [sharedFilter.year.currentMin,sharedFilter.year.currentMax]
            
            if brands.count > 0{
                countFilterTemp += 1
                print("brands",brands.count)
            }
//            if models.count > 0{
//                countFilterTemp += 1
//                print("models",models.count)
//
//            }
            if location.count > 0{
                countFilterTemp += 1
                print("location",location.count)

            }
            if fueltype.count > 0{
                countFilterTemp += 1
                print("fueltype",fueltype.count)

            }
            if transmission.count > 0{
                countFilterTemp += 1
                print("transmission",transmission.count)

            }
            if categories.count > 0{
                countFilterTemp += 1
                print("categories",categories.count)

            }
            if features.count > 0{
                //  countFilterTemp += 1  // no need to include, this is only for smart search
                
            }
            if sharedFilter.price.currentMin != sharedFilter.price.min.CGFloatValue || sharedFilter.price.currentMax != sharedFilter.price.max.CGFloatValue{
                countFilterTemp += 1
                print("sharedFilter.price.currentMin")

            }
            if sharedFilter.kilometer.currentMin != sharedFilter.kilometer.min.CGFloatValue || sharedFilter.kilometer.currentMax != sharedFilter.kilometer.max.CGFloatValue{
                print("sharedFilter.kilometer.currentMin")
                countFilterTemp += 1
            }
            if ownership.count > 0{
                countFilterTemp += 1
                print("ownership",ownership.count)

            }
            if color.count > 0{
                countFilterTemp += 1
                print("color",color.count)

            }
            if sharedFilter.year.currentMin != sharedFilter.year.min.CGFloatValue || sharedFilter.year.currentMax != sharedFilter.year.max.CGFloatValue{
                countFilterTemp += 1
                print("sharedFilter.year.currentMin")

            }
            self.filterCount = countFilterTemp
            self.filterCountLBL.text = self.filterCount.description
            print("Filter Count_calculateFilterLabelCount ",filterCount)
        
        }else {
            self.filterCountLBL.isHidden = true
            self.filterCountLBL.text = 0.description
        }

    }
    
    

    
}


//MARK:- TableView Data source And Delegates
// Handling table view data source
extension PLUsedCarsListVC: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFromSmartSearch {
            if usedCarsListData.CarDetails.count == 0 {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text = noDataFound
                
                if let font = UIFont.init(name: "Roboto-Medium", size: 14.0) {
                    noDataLabel.font = font
                }else {
                    noDataLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
                }
                noDataLabel.textColor     = UIColor.black.withAlphaComponent(0.5)
                noDataLabel.textAlignment = .center
                noDataLabel.backgroundColor = .tableBackground
                tableView.backgroundView  = noDataLabel
                
                return 0
            }else {
                tableView.backgroundView = nil
                return 1
            }
        }else {
            tableView.backgroundView = nil
            return 1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isFromSmartSearch ? 62 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isFromSmartSearch {
            let headerView = UIView()
            let tableFrame = tableView.frame
            headerView.backgroundColor = #colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1)
            headerView.frame = CGRect(x: 0, y: 0, width: tableFrame.width, height: 62)
            let label = UILabel(frame: CGRect(x: 5, y: 8, width: headerView.frame.width - 10, height: 45))
            label.backgroundColor = .white
            label.layer.cornerRadius = 5
            label.clipsToBounds = true
            label.textAlignment = .center
            label.textColor = UIColor.black.withAlphaComponent(0.4)
            label.text = "Showing " + smartSearchResultCount + " Results"
            if let font = UIFont.init(name: "Roboto-Medium", size: 13.0) {
                // print("fontSettttt")
                label.font = font
            }else {
                // print("fontNOTTTTT")
                label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            }
            headerView.addSubview(label)
            return headerView

        }else {
            return nil
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = isFromSmartSearch ? usedCarsListData.CarDetails.count : usedCarsListData.CarDetails.count + 1
        //print("Number of rows : ",count)
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("CellforRowwwwwww")
        if indexPath.row == 0 && !isFromSmartSearch {
             guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsSliderImagesTVC", for: indexPath) as? PLUsedCarsSliderImagesTVC else { return UITableViewCell() }
            cell.searchTF.text = searchWord
            cell.slideShow.contentScaleMode = .scaleAspectFill
            cell.tag = 0
            if isBeginSearchEditing {
                cell.searchTF.becomeFirstResponder()
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsListTVC", for: indexPath) as? PLUsedCarsListTVC else { return UITableViewCell() }
            
            var position = indexPath.row - 1
            if isFromSmartSearch {
                position = indexPath.row
            }

            let indexOfCar = IndexPath(row: position, section: indexPath.section)
            
            if usedCarsListData.CarDetails.count > position {
                let carData = usedCarsListData.CarDetails[position]

                cell.vehicleImage.kf.setImage(with: URL(string: carData.images), placeholder: UIImage(named: "placeholderVehicle"))
                cell.addTofavouriteBTN.indexPath = indexOfCar
                cell.modelLBL.text = carData.brand + " " + carData.model //+ " " + carData.variant
                cell.locationLBL.text = carData.location
                cell.priceLBL.text = carData.expectedPrice
                cell.yearLBL.text = carData.modelYear.description
                cell.kilometersLBL.text = carData.kilometer.description
                cell.fuelTypeLBL.text = carData.fuelType
                
                cell.imageCountLBL.text = "\(carData.imageCount)"
                cell.addToCompareBTN.tag = carData.usedcarid
                cell.addToCompareBTN.indexPath = indexOfCar
                cell.shareBTN.indexPath = indexOfCar
                
                if carData.isfavourite {
                    cell.addTofavouriteBTN.setImage(favFillImage, for: [])
                }else {
                    cell.addTofavouriteBTN.setImage(favBlankImage, for: [])
                }
                
                cell.verifiedImage.isHidden = !carData.popularVerified

                var loginStatus = true
                if let loggedIn = UserDefaults.standard.value(forKey: "UserLoggedIn") as? Bool {
                    loginStatus = loggedIn
                }
                else {
                    loginStatus = false
                }
                if loginStatus {
                    isLoggedIn = true
                }else{
                    isLoggedIn = false
                }
                if isLoggedIn{
                    cell.segueLoginBTN.isHidden = true
                    cell.priceLBL.isHidden = false
                    cell.priceSymbolLBL.isHidden = false
                }else{
                    cell.segueLoginBTN.isHidden = false
                    cell.priceLBL.isHidden = true
                    cell.priceSymbolLBL.isHidden = true

                }
                
                if isFromSmartSearch{
//                    print(indexPath.row)
//                    print(usedCarsListData.CarDetails.count)
//                    print(usedCarsListData.batchSize)
                    
                    if indexPath.row > usedCarsListData.CarDetails.count - usedCarsListData.batchSize {
                        getUsedCarsListPaginated()
                    }
                }else{
                    if indexPath.row > usedCarsListData.CarDetails.count - usedCarsListData.batchSize{
                        getUsedCarsListPaginated()
                    }
                }
                cell.tag = carData.usedcarid
        }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //heightRatio > 175 ? heightRatio : 175
//        view.frame.height * 0.385
        //let height = UIScreen.main.bounds.width * 0.59
        
        return isFromSmartSearch ? 342 : indexPath.row == 0 ? 250 : 342  // first for top cell and other for car cell
        
    }
    
// Handling table view delegates

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if PLAppState.session.isLoggedIn{
            if isFromSmartSearch {
                if let cell = tableView.cellForRow(at: indexPath) {
                    let carId = cell.tag
                    print("selectedusedcar",carId)
                    let segueVC = self.storyBoardUsedCars?.instantiateViewController(withIdentifier: "PLUsedCarsDetailsVC") as! PLUsedCarsDetailsVC
                    segueVC.usedCarId = carId
                    self.present(segueVC, animated: true, completion: nil)
                    
                }

            }else {
                if let cell = tableView.cellForRow(at: indexPath), cell.tag != 0 {
                    let carId = cell.tag
                    print("selectedusedcar",carId)
                    let segueVC = self.storyBoardUsedCars?.instantiateViewController(withIdentifier: "PLUsedCarsDetailsVC") as! PLUsedCarsDetailsVC
                    segueVC.usedCarId = carId
                    self.present(segueVC)
                    
                }

            }
            selectedIndexPath = indexPath
        }else{
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastIndex = indexPath.row
        if isFromSmartSearch{
            if usedCarsListData.CarDetails.count < usedCarsListData.usedCarCount {
                if usedCarsListData.CarDetails.count - 1 == lastIndex {
//                    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                    indicator.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30)
//                    indicator.startAnimating()
                    //tableView.tableFooterView = indicator
                   // tableView.tableFooterView?.isHidden = false
                }
            }else{
                 tableView.tableFooterView = UIView()
            }
        }else{
            if usedCarsListData.CarDetails.count < usedCarsListData.usedCarCount{
                if usedCarsListData.CarDetails.count == lastIndex{
//                    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//                    indicator.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30)
//                    indicator.startAnimating()
//                    tableView.tableFooterView = indicator
//                    tableView.tableFooterView?.isHidden = false
                }
            }else{
                tableView.tableFooterView = UIView()
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        print("refreshControl-handleRefresh")
        searchWord = ""
        isFromSmartSearch = false
        if let _ = parameterUsedCarList["search"] as? String {
            parameterUsedCarList.removeValue(forKey: "search")
        }
        getUsedCarsListSortSearch()
        refreshControl.endRefreshing()
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("scrollViewDidScroll")
        //print(vehicleTV.contentOffset,"scrollViewDidScroll:",scrollView.contentOffset,"SourceCount:",usedCarsListData.CarDetails.count)
        currentScrollOffSet = scrollView.contentOffset
        isBeginSearchEditing = false
        if isBeginSearchEditingOuter {
            
        }else {
            searchTF.resignFirstResponder()

        }
       // print("scrollView",scrollView.contentOffset.y)

        if vehicleTV.contentOffset.y < 199 { /*((vehicleTV.indexPathsForVisibleRows?.contains(IndexPath(row: 0, section: 0)))!)*/
//            if let cell = vehicleTV.cellForRow(at: IndexPath(row: 0, section: 0)) as? PLUsedCarsSliderImagesTVC {
//                cell.searchTF.resignFirstResponder()
//                print("scrollCEllllllll")
////                print("vehicleTV.contentOffset",vehicleTV.contentOffset.y)
//            }
            
            UIView.animate(withDuration: 0.17, animations: {
                self.searchView.alpha = 0
            })
        }else{
//            print("conttttemt",scrollView.contentOffset.y)
            if isFromSmartSearch {
                //searchView.isHidden = true
                UIView.animate(withDuration: 0.17, animations: {
                    self.searchView.alpha = 0
                })

            }else{
                //searchView.isHidden = false
                UIView.animate(withDuration: 0.17, animations: {
                    self.searchView.alpha = 1
                })

                searchTF.text = searchWord
            }
        }

        
    }
}

// Mark:- Handling web service
extension PLUsedCarsListVC {
    
    fileprivate func getUsedCarsListPaginated(){
        //print("Pagination Count Before ",paginationCurrentLoadingCount )
        //print("Pagination Call Started ",paginationCurrentLoadingCount)
        if paginationCurrentLoadingCount == 0 {
            self.paginationUpdateHalt = false
        }
        if usedCarsListData.CarDetails.count < usedCarsListData.usedCarCount && paginationCurrentLoadingCount < 2, !isPaginatedProgressing {
            print("Count LHS ",usedCarsListData.CarDetails.count," RHS ", usedCarsListData.usedCarCount)
            isPaginatedProgressing = true
            pagination += 1
            paginationCurrentLoadingCount += 1
            //print("Pagination Count After ",paginationCurrentLoadingCount )
            parameterUsedCarListPaginated.removeAll()
            parameterUsedCarListPaginated = parameterUsedCarList
            parameterUsedCarListPaginated["page"] = pagination.description
            //print("Page ** ",pagination)
           // print("Total count : ",usedCarsListData.usedCarCount," Current Count : ",usedCarsListData.CarDetails.count)
            print("getUsedCarsListPaginated_params: ",self.parameterUsedCarListPaginated)
            
            WebServices.postMethodWithoutActivity(url:"getusedcarlist", parameter: parameterUsedCarListPaginated, CompletionHandler: { (isFetched, result) in
                self.activityPaginated.stop()
                self.paginationCurrentLoadingCount -= 1
                guard isFetched else {
                    return
                }
                
                guard let dataObject = result["data"] as? Json else {
                    return
                }
                
                let newData = PLUsedCarModel().getData(object: dataObject)
                let newList = newData.CarDetails
                if self.paginationUpdateHalt == false{
                    self.usedCarsListData.CarDetails.append(contentsOf: newList)
                    BaseThread.asyncMain {
                        //let offset = self.vehicleTV.contentOffset
                        self.vehicleTV.reloadData()
                        //self.vehicleTV.contentOffset = offset
                        self.isPaginated = true
                        self.isPaginatedProgressing = false
                       // self.view.setNeedsLayout()
                       // self.view.layoutIfNeeded()
                    }
                }else{
                    self.tempDataForPagination.append(contentsOf: newList)
                }
            })
        }
    }
    
    // this method is used to get the list from sort
    fileprivate func getUsedCarsListSortSearch(){
        parameterUsedCarList["user_id"] = PLAppState.session.userID.description
        parameterUsedCarList["page"] = 1.description
       // print(" - URL:",self.parameterUsedCarList)
        self.activityPaginated.start()
        WebServices.postMethodWithoutActivity(url:"getusedcarlist", parameter: self.parameterUsedCarList, CompletionHandler: { (isFetched, result) in
            self.activityPaginated.stop()

            guard isFetched else {
                BaseThread.asyncMain {
                    showBanner(title: "Error", message: noDataAvailable)
                }
                return
            }
            guard let dataObject = result["data"] as? Json else {
                BaseThread.asyncMain {
                    showBanner(message: noDataFound)
                }
                return
            }
            
            
            BaseThread.asyncMain {
                self.parameterUsedCarListPaginated = self.parameterUsedCarList
                self.usedCarsListData = PLUsedCarModel().getData(object: dataObject)

                
                if let dataList = dataObject["data"] as? JsonArray, dataList.isEmpty {
                    showBanner(message: noDataFound)
                    
                    //self.isFromSmartSearch = true
                    self.searchTF.text = self.searchWord
//                    if self.searchWord.isEmpty {
//                        self.isBeginSearchEditing = false
//                    }
                    self.isBeginSearchEditing = false

//                    UIView.animate(withDuration: 0.17, animations: {
//                        self.searchView.alpha = 1
//                    })
                    self.vehicleTV.reloadData()
                    print("getUsedCarsListSortSearch-web no result")

                    //self.vehicleTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                }else {
                    print("getUsedCarsListSortSearch-web result")
//                    if self.searchWord.isEmpty {
//                        self.isBeginSearchEditing = false
//                    }
                    self.isBeginSearchEditing = false

                    self.vehicleTV.reloadData()
                }
                
                if self.usedCarsListData.CarDetails.count > 0 && self.vehicleTV.numberOfSections > 0 {
                    print("getUsedCarsListSortSearch-Webservice-scrolltoRow")
                    self.vehicleTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                }


                self.view.layoutIfNeeded()
                //self.view.setNeedsDisplay()

            }
            
            
        })
    }

    
    fileprivate func getUsedCarsList(){
        parameterUsedCarList["user_id"] = PLAppState.session.userID.description
        parameterUsedCarList["page"] = 1.description
        //pagination = 1
        switch appliedSortMethod {
        case .latest:
            parameterUsedCarList["sort"] = "latest"
        case .priceLowToHigh:
            parameterUsedCarList["sort"] = "price_lowtohigh"
        case .priceHighToLow:
            parameterUsedCarList["sort"] = "price_hightolow"
        default:
            break
        }

        //print("parameterUsedCarList",parameterUsedCarList)
        WebServices.postMethod(url:"getusedcarlist", parameter: self.parameterUsedCarList, CompletionHandler: { (isFetched, result) in
            //self.showParentActivity(shouldShow: false)
            //self.paginationUpdateHalt = false
            guard isFetched else {
                print("nodata failure")

                BaseThread.asyncMain {
                    showBanner(title: "Error", message: noDataAvailable)
                    self.dismiss()
                }
                return
            }
            guard let dataObject = result["data"] as? Json else {
                print("noDataAvauiable")
                BaseThread.asyncMain {
                    showBanner(message: noDataAvailable)
                    self.dismiss()
                }
                return
            }
            self.parameterUsedCarListPaginated = self.parameterUsedCarList
            //self.parameterUsedCarList.removeAll()      // clearing present parameter for new search/sort
            self.usedCarsListData = PLUsedCarModel().getData(object: dataObject)
            self.smartSearchResultCount = self.usedCarsListData.usedCarCount.description
            var propertyImagesInputs: [InputSource] = []
            BaseThread.asyncMain {
                for item in self.usedCarsListData.banner {
                    let imageViewTemp = UIImageView()
                    imageViewTemp.kf.setImage(with: URL(string: item), options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: { (image, err, cache, URLRetrieve) in
                        if image != nil {
                            self.propertySlidingImagesSource.append(imageViewTemp.image!)
                            propertyImagesInputs.append(ImageSource(image: image!))
                            if let cell = self.vehicleTV.cellForRow(at: IndexPath(row: 0, section: 0)) as? PLUsedCarsSliderImagesTVC {
                                //print("CellAvailable")
                                cell.slideShow.setImageInputs(propertyImagesInputs)
                                cell.searchTF.text = self.searchWord
                                cell.pageControll.numberOfPages = propertyImagesInputs.count
                                cell.pageControll.progress = self.currentSlideShowIndex
                                cell.slideShow.slideshowInterval = 3
                                cell.slideShow.contentScaleMode = .scaleAspectFill
                                cell.slideShow.draggingEnabled = true
                                cell.slideShow.currentPageChanged = { index in
                                    self.currentSlideShowIndex = index.doubleValue
                                    cell.pageControll.progress = self.currentSlideShowIndex
                                }
                                cell.layoutIfNeeded()
                                //cell.layoutSubviews()
                                
                            }else{
                                print("no_slider_cell")
                            }

                        }else {
                            print("Image Nilll")
                        }
                    })
                    
                }
                
//                self.emptyBackground(vc: self, addOrRemove: false)
//                self.isAddedEmptyView = false
                print("self.vehicleTV.numberOfSections",self.vehicleTV.numberOfSections)

                self.vehicleTV.reloadData()
                //self.view.setNeedsLayout()
                self.view.layoutIfNeeded()

                if self.usedCarsListData.CarDetails.count > 0 && self.vehicleTV.numberOfSections > 0 {
                    //print("inside")
                    
                    if self.vehicleTV.numberOfRows(inSection: 0) > 0{
                        self.vehicleTV.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    }
                }
                
                //print("isViewDidLoadedPageVC",isViewDidLoadedPageVC)
            }
            
            
        })
    }
    
  /*  // Helper function to access activity indicator from parent vc in heirarchy
    fileprivate func showParentActivity(shouldShow: Bool){
        BaseThread.asyncMain {
            if let parentVC = self.parent as? PLUsedCarPageViewController {
                if shouldShow {
                    if !iShowingActivity {
                        parentVC.showActivity()
                    }
                }else {
                    parentVC.hideActivity()
                }
                
            }
            
        }
    }  */
    
    // Mark:- Adding/deleting favourite
    
    fileprivate func addToFavouriteWeb(params: Information, completion: @escaping ((Bool)->())){
        //print("addding")
        WebServices.postMethodWithoutActivityBanner(url:"addtofavlist", parameter: params, CompletionHandler: { (isFetched, result) in
        
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })

    }
    
    fileprivate func deleteFromFavouriteWeb(params: Information, completion: @escaping ((Bool)->())){
        //print("deleting")
        WebServices.postMethodWithoutActivityBanner(url:"deletefavlist", parameter: params, CompletionHandler: { (isFetched, result) in
            
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })
        
    }
    
    private func addToCompareWeb(params: Information, completion: @escaping ((Bool)->())){
        // http://app.appzoc.com/popular/backend/api/addtocomparelist
        let url = "addtocomparelist"
        
        WebServices.postMethodWithoutActivity(url: url, parameter: params, CompletionHandler: { (isFetched, result) in
            
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })
    }
    
    private func getCompareFavouriteCountWeb(senderBTN: UIButton? = nil, isFromCompareAction: Bool = false){
        let url = "getUsedCarsFav_comp_Count"
        var params = Json()
        params["user_id"] = PLAppState.session.userID
        
        WebServices.postMethodWithoutActivity(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else {
                BaseThread.asyncMain {
                    if let sender = senderBTN {
                        sender.isUserInteractionEnabled = true
                    }
                }
                return

            }
            guard let data = response["data"] as? Json, !data.isEmpty else { return }
            let compare = data["compare_count"] as! Int
            let favour = data["favourite_count"] as! Int
            //print(compare,"getUsedCarsFav_comp_Count",favour)
            compareCount = compare
            favourCount = favour
            BaseThread.asyncMain {
                if let sender = senderBTN {
                    sender.isUserInteractionEnabled = true
                }

                if let homeVC = self.parent?.parent as? PLUsedCarsHomeVC {
                    homeVC.favouriteCountLBL.text = favourCount.description
                    homeVC.compareCountLBL.text = compareCount.description
                    homeVC.compareCountLBL.isHidden = compareCount == 0 ? true : false
                    homeVC.favouriteCountLBL.isHidden = favourCount == 0 ? true : false
                    if isFromCompareAction && compareCount == 2 {
                        let segueVC = self.storyBoardCompare!.instantiateViewController(withIdentifier: "PLCompareVC") as! PLCompareVC
                        self.present(segueVC)

                    }

                }
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()

            }
            
        }
    }
    
    private func getFilterData(){
        // http://app.appzoc.com/popular/backend/api/getFilterdetails
        let url = "getFilterdetails"
        WebServices.getMethodWithOutActivity(url: url, parameter: nil) { (isSucceeded, response) in
            
            guard isSucceeded, let responseData = response["data"] as? Json, !responseData.isEmpty else {
                return
            }
            self.passingJSON = response
            self.parsePassedJSON(filterData: response)
            PLSearchAttributes.shared.updateWithWebData(attributes: responseData)
        }
        
    }

    func parsePassedJSON(filterData:[String:Any]){
        if let jsonData = filterData["data"] as? [String:Any]{
            do{
                let decoder = JSONDecoder()
                if let trueJson = serializeJSON(json: jsonData){
                    var parsedJSON = try decoder.decode(FilterParserModel.self, from: trueJson)
                    var orderedCategory = parsedJSON.category
                    orderedCategory.removeAll()
                    print("parsedJSON.category",parsedJSON.category)
                    
                    for var model in parsedJSON.category {
                        
                        if model.id == 9 {          // HATCHBACK
                            model.order = 1
                            orderedCategory.append(model)
                            
                        }else if model.id == 10 {               // MUV
                            model.order = 2
                            
                            orderedCategory.append(model)
                        }else if model.id == 4 {          // SUV
                            model.order = 3
                            
                            orderedCategory.append(model)
                            
                        }else if model.id == 8 {          //SEDAN
                            model.order = 4
                            
                            orderedCategory.append(model)
                            
                        }else if model.id == 5 {          //VAN
                            model.order = 5
                            
                            orderedCategory.append(model)
                            
                        }
                        
                    }
                    
                    parsedJSON.category.removeAll()
                    parsedJSON.category = orderedCategory.sorted(by: { $0.order < $1.order })
                    
                    print("=========")
                    print("Ordered:",parsedJSON.category)
                    sharedFilterSource = parsedJSON
                    for item in parsedJSON.ownership{
                        sharedFilterSource?.selectedOwnerShip.append(FilterParserModel.OwnerShipModel(data: item))
                    }
                    self.setSharedDataSourceMinMaxValues(with: parsedJSON)
                }
            }catch{
                print(error)
            }
        }
    }
    
    func setSharedDataSourceMinMaxValues(with data:FilterParserModel){
        sharedFilterSource?.price.currentMin = data.price.min.CGFloatValue
        sharedFilterSource?.price.currentMax = data.price.max.CGFloatValue
        sharedFilterSource?.kilometer.currentMin = data.kilometer.min.CGFloatValue
        sharedFilterSource?.kilometer.currentMax = data.kilometer.max.CGFloatValue
        sharedFilterSource?.year.currentMin = data.year.min.CGFloatValue
        sharedFilterSource?.year.currentMax = data.year.max.CGFloatValue
    }
    
}




