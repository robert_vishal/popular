//
//  PLUsedCarsDetailsVC.swift
//  Popular
//
//  Created by Appzoc on 23/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class PLUsedCarsDetailsVC: UIViewController , SideOptionsDelegate, InterfaceSettable{
    
    @IBOutlet var favouritesCountLBL: BaseLabel!
    @IBOutlet var compareCountLBL: BaseLabel!
    @IBOutlet var detailsTV: UITableView!
    @IBOutlet var vehicleImage: UIImageView!
    @IBOutlet var vehicleImageSlider: ImageSlideshow!
    @IBOutlet var verifiedImage: UIImageView!
    
    @IBOutlet var favouriteBTN: BaseButton!
    @IBOutlet var imageCountLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    @IBOutlet var vehicleNameLBL: UILabel!
    @IBOutlet var vehiclePriceLBL: UILabel!
    @IBOutlet var vehicleYearLBL: UILabel!
    @IBOutlet var vehicleKilometerLBL: UILabel!
    @IBOutlet var vehicleFuelTypeLBL: UILabel!
    @IBOutlet var contentView: UIView!
    
    // data receiving property using while passing
    final var usedCarId: Int = 0
    
    // properties
    private var sectionDataSource: Json = Json()
    private var rowDataSource:Json = Json()
    private var isCollapsed: [Bool] = []
    private var carDetails:PLUsedCarsDetailsModel = PLUsedCarsDetailsModel()
    private let overviewText = "Overview"
    private let specificationsText = "Specifications & Features"
    
    private let subSpecifications = "Specifications"
    private let subFeatures = "Features"
    private var selectedToggle = [0: false, 1: false]
    private var selectedToggleTag = 1
    private var sectionsRowsPair: [Int: Int] = [:]
    private let favBlankImage = #imageLiteral(resourceName: "1x_0007_favourite_gre_outline.png")
    private let favFillImage = #imageLiteral(resourceName: "1x_0008_favourite_white_fill_Red")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentView.alpha = 0
        self.contentView.isUserInteractionEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromFavourite(_:)), name: .usedCarFavourite, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromFavourite(_:)), name: .usedCarFavouriteAll, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFromCompare(_:)), name: .usedCarCompare, object: nil)

        
        let  vehicleImageSliderTapGesture = UITapGestureRecognizer(target: self, action:#selector(vehicleImageSliderTapped))
        vehicleImageSlider.addGestureRecognizer(vehicleImageSliderTapGesture)
        
        self.favouritesCountLBL.text = favourCount.description
        self.compareCountLBL.text = compareCount.description
        self.compareCountLBL.isHidden = compareCount.isZero
        self.favouritesCountLBL.isHidden = favourCount.isZero
        getCarDetailsWeb()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //print("abccccc")
        self.favouritesCountLBL.text = favourCount.description
        self.compareCountLBL.text = compareCount.description
        self.compareCountLBL.isHidden = compareCount.isZero
        self.favouritesCountLBL.isHidden = favourCount.isZero

    }
    @objc func vehicleImageSliderTapped(){
        //print("vehicleImageSliderTapped")
        let segueVC = self.storyBoardNewCars?.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
        segueVC.imageURLSource = carDetails.data.images
        segueVC.needsBaseURL = false
        present(segueVC, animated: true, completion: nil)

    }
    
    ///    Mark:- notification delegate from compareVC
    @objc func reloadFromCompare(_ notification: NSNotification){
        //print("reloadFromCompare-Details page")
        if let deletedId = notification.userInfo?["deletedIds"] as? [Int] {
            guard !deletedId.isEmpty else { return }
            if deletedId.contains(self.usedCarId) {
                carDetails.data.iscomparelist = false
            }
        }
        
        self.getCompareFavouriteCountWeb()

    }
    
    ///    Mark:- notification delegate from favouriteVC
    @objc func reloadFromFavourite(_ notification: NSNotification){
        //print("reloadFromFavourite-Details page")
        carDetails.data.isfavourite = false
        self.favouriteBTN.setImage(favBlankImage, for: .normal)
        self.getCompareFavouriteCountWeb()


    }


    
    // setting UI wih webservice results
    
    private func setUpImageSlider(){
        var imageURLSource: [KingfisherSource] = []
        for item in carDetails.data.images {
            imageURLSource.append(KingfisherSource(urlString: item, placeholder: #imageLiteral(resourceName: "placeholderVehicle"), options: nil)!)
        }
        vehicleImageSlider.setImageInputs(imageURLSource)
        //print("Number of Images : ",imageURLSource.count)
        //vehicleImageSlider.pageControl.isHidden = true
        vehicleImageSlider.circular = true
        vehicleImageSlider.slideshowInterval = 3
        vehicleImageSlider.contentScaleMode = .scaleAspectFill
        vehicleImageSlider.currentPageChanged = { index in
            //print("SliderImageIndexes:",index)
            self.imageCountLBL.text = (index + 1).description + "/" + self.carDetails.data.images.count.description
        }

    }
    
    func setUpInterface() {
        setUpImageSlider()
        self.favouritesCountLBL.text = favourCount.description
        self.compareCountLBL.text = compareCount.description
        self.compareCountLBL.isHidden = compareCount.isZero
        self.favouritesCountLBL.isHidden = favourCount.isZero

        let imageURL = URL(string: carDetails.data.images[0])
        self.vehicleImage.kf.setImage(with: imageURL)
        self.verifiedImage.isHidden = !carDetails.data.popular_verified
        
        if carDetails.data.isfavourite {
            self.favouriteBTN.setImage(favFillImage, for: .normal)
        }else {
            self.favouriteBTN.setImage(favBlankImage, for: .normal)
        }
        
        //print("Location ",carDetails.data.location)
        self.locationLBL.text = carDetails.data.location
        self.imageCountLBL.text = 1.description + "/" + carDetails.data.images.count.description
        self.imageCountLBL.sizeToFit()
        self.vehicleNameLBL.text = carDetails.data.nice_title
        self.vehiclePriceLBL.text = carDetails.data.price
        self.vehicleYearLBL.text = carDetails.data.model_year.description
        self.vehicleKilometerLBL.text = carDetails.data.kilometer.description
        self.vehicleFuelTypeLBL.text = carDetails.data.fueltype
        
        // setting data source for table
    
        
        sectionDataSource[overviewText] = carDetails.overview
        sectionDataSource[specificationsText] = carDetails.specifications
        
        for _ in sectionDataSource {
          //  print("sectionDataSource_Item",item)
            isCollapsed.append(false)
        }
        
        detailsTV.reloadData()
        self.contentView.alpha = 1.0
        self.contentView.isUserInteractionEnabled = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        detailsTV.reloadData()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
    
        if let _ = self.presentingViewController as? PLFavouritesVC{
            self.dismiss()
        }else{
        
            if let listVC = self.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLUsedCarsListVC {
                listVC.reloadFromDetails(isFavourited: self.carDetails.data.isfavourite , isCompared: carDetails.data.iscomparelist, carId: self.usedCarId)
            }
        
            dismiss()
        }
    }
    
    @IBAction func compareTapped(_ sender: UIButton) {
        if compareCount != 0 {
            let segueVC = self.storyBoardCompare!.instantiateViewController(withIdentifier: "PLCompareVC") as! PLCompareVC
            present(segueVC)
            
        }

    }
    
    @IBAction func favouritesTapped(_ sender: UIButton) {
        let segueVC = self.storyBoardFavourites!.instantiateViewController(withIdentifier: "PLFavouritesVC") as! PLFavouritesVC
        segueVC.selectedHeader = 1
        present(segueVC)
    }
    
    
    // header outlets
    @IBAction func addToFavouriteTapped(_ sender: BaseButton) {
        if PLAppState.session.userID == 0 {
            showBanner(message: "Please Login")
            return
        }

        var parameter = Information()
        
        parameter["user_id"] = PLAppState.session.userID.description
        parameter["usedcarid"] = carDetails.data.usedcarid.description

        if carDetails.data.isfavourite {
            sender.setImage(self.favBlankImage, for: [])
            self.carDetails.data.isfavourite = false

            deleteFromFavouriteWeb(params: parameter, completion: { (isSucceeded) in
                if isSucceeded {
                    //print("deleted from favourite")
//                    sender.setImage(self.favBlankImage, for: [])
//                    self.carDetails.data.isfavourite = false
                    BaseThread.asyncMain {
                        self.getCompareFavouriteCountWeb()
                    }
                }else {
                    //print("Not deleted from favourite")
                    sender.setImage(self.favFillImage, for: [])
                    self.carDetails.data.isfavourite = true

                }
            })
        }else {
            sender.setImage(self.favFillImage, for: [])
            self.carDetails.data.isfavourite = true

            addToFavouriteWeb(params: parameter, completion: { (isSucceeded) in
                if isSucceeded {
                    //print("added to favourite")
                    BaseThread.asyncMain {
//                        sender.setImage(self.favFillImage, for: [])
//                        self.carDetails.data.isfavourite = true
                        BaseThread.asyncMain {
                            self.getCompareFavouriteCountWeb()
                        }
                        
                    }
                }else {
                    //print("Not added to favourite")
                    sender.setImage(self.favBlankImage, for: [])
                    self.carDetails.data.isfavourite = false

                }
                
            })
        }

    }
    
    @IBAction func shareTapped(_ sender: BaseButton) {
        
        PLAppState.session.commonShare(vc: self, view: self.view, carName: carDetails.data.title, carModel: carDetails.data.variant, imgURL: carDetails.data.images[0])

    }
    
    @IBAction func addToCompareTapped(_ sender: BaseButton) {
        sender.isUserInteractionEnabled = false
        if PLAppState.session.userID == 0 {
            showBanner(message: "Please Login")
            return
        }
        
        guard compareCount < 2 else {
            showBanner(message: "Compare list is full")
            return
        }
        
        guard !carDetails.data.iscomparelist else {
            showBanner(message: "Already in compare list")
            return
        }
        
        var parameter = Information()
        parameter["user_id"] = PLAppState.session.userID.description
        parameter["usedcarid"] = carDetails.data.usedcarid.description
        
        addToCompareWeb(params: parameter, completion: { (isSucceeded) in
            sender.isUserInteractionEnabled = true
            if isSucceeded {
                showBanner(message: "Added To Compare List")
                //print("addto to compare")
                /// self.compareCount += 1
                self.carDetails.data.iscomparelist = true
                BaseThread.asyncMain {
                    self.getCompareFavouriteCountWeb(isFromCompareAction: true)
                }
            }else {
                //print("not add to compare")
                
            }
        })
    }
    
    @IBAction func requestForTestDriveTapped(_ sender: UIButton) {
        let nextVC =  self.storyBoardPopUps?.instantiateViewController(withIdentifier: "PLPopUpEnquiryVC") as! PLPopUpEnquiryVC
        nextVC.enquiryType = .usedCarTestDrive
        nextVC.carModelId = self.usedCarId.description
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func contactTapped(_ sender: UIButton) {
        //print("Call Tapped")
        makeCallTo(number: emergancyNumber)
    }
    
    @IBAction func sendEnquiryTapped(_ sender: UIButton) {
        let nextVC =  self.storyBoardPopUps!.instantiateViewController(withIdentifier: "PLExchangeEnquiryVC") as! PLExchangeEnquiryVC
        nextVC.carModelId = usedCarId.description
        self.present(nextVC, animated: true, completion: nil)

    }
    
    // delegate from side option
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
    }
    
    @IBAction func toggleFirstBtnPressed(_ sender: UIButton) {
        selectedToggleTag = 1
        detailsTV.reloadData()

        //detailsTV.reloadSections(IndexSet.init(integer: 1), with: UITableViewRowAnimation.none)
    }
    
    @IBAction func toggleSecondBtnPressed(_ sender: UIButton) {
        selectedToggleTag = 2
        //detailsTV.reloadSections(IndexSet.init(integer: 1), with: UITableViewRowAnimation.none)

        detailsTV.reloadData()
        
    }
    
    private enum overviewKeys: String {
        case body_type
        case color
        case insurance_validity
        case max_power
        case model
        case no_of_owners
        case reg_no
        case registration_validity
        case tax_validity
        case transmission
    }
    
    private enum specificationKeys: String {
        case drive_train
        case torque
    }
}

// Mark:- Table view handling
extension PLUsedCarsDetailsVC: UITableViewDataSource, UITableViewDelegate {

    
    @inline(__always) func getSectionHeaderText(of section: Int) ->String {
        let index = sectionDataSource.index(sectionDataSource.startIndex, offsetBy: section)
        let keyValue = sectionDataSource.keys[index]
        return keyValue
    }
    
    @inline(__always) func getRowSourceForSectionAsJson(forKey: String) -> Json {
        if let value = sectionDataSource[forKey] as? Json {
            return value
        } else{
            return Json()
        }
    }
    
    @inline(__always) func getDataForRow(of row: Int, section: Int = 0, from rowSource: Json) -> Json{
        var sourceData = Json()
        
        if section == 0 {
            switch row {
            case 0:
                let key = overviewKeys.reg_no.rawValue
                sourceData[key] = rowSource[key]
                
            case 1:
                let key = overviewKeys.no_of_owners.rawValue
                sourceData[key] = rowSource[key]
            case 2:
                let key = overviewKeys.body_type.rawValue
                sourceData[key] = rowSource[key]
            case 3:
                let key = overviewKeys.model.rawValue
                sourceData[key] = rowSource[key]
            case 4:
                let key = overviewKeys.color.rawValue
                sourceData[key] = rowSource[key]
            case 5:
                let key = overviewKeys.transmission.rawValue
                sourceData[key] = rowSource[key]
            case 6:
                let key = overviewKeys.max_power.rawValue
                sourceData[key] = rowSource[key]
            case 7:
                let key = overviewKeys.tax_validity.rawValue
                sourceData[key] = rowSource[key]
            case 8:
                let key = overviewKeys.insurance_validity.rawValue
                sourceData[key] = rowSource[key]
            case 9:
                let key = overviewKeys.registration_validity.rawValue
                sourceData[key] = rowSource[key]
            default: break
                
            }
            return sourceData

        }else {
            
            switch row {
            case 0:
                let key = specificationKeys.drive_train.rawValue
                sourceData[key] = rowSource[key]
                
            case 1:
                let key = specificationKeys.torque.rawValue
                sourceData[key] = rowSource[key]
            default: break
            }
            
            return sourceData
        }
        
//        let index = rowSource.index(rowSource.startIndex, offsetBy: row)
//        let key = rowSource.keys[index]
//        let value = rowSource.values[index]
//        sourceData[key] = value
//        return sourceData // key and value for single row source
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDataSource.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsDetailsHeaderTVC") as! PLUsedCarsDetailsHeaderTVC
        if section == 0 {
            headerCell.titileLBL.text = overviewText
        }else if section == 1 {
             headerCell.titileLBL.text = specificationsText
        }
        
        let headerCellContentView = headerCell.contentView
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        headerCellContentView.isUserInteractionEnabled = true
        headerCellContentView.tag = section
        headerCellContentView.addGestureRecognizer(tap)
 
        if self.isCollapsed[section] {
            UIView.animate(withDuration: 0.0, delay: 0, options: .curveLinear, animations: {
                headerCell.arrowIMGV.transform = CGAffineTransform.identity
            }, completion: { (success) in
                UIView.animate(withDuration: 0.35, delay: 0, options: .curveLinear, animations: {
                    headerCell.arrowIMGV.transform = CGAffineTransform(rotationAngle: .pi/2)
                }, completion: { (success) in
                })
            })

        } else {
            UIView.animate(withDuration: 0.0, delay: 0, options: .curveLinear, animations: {
                headerCell.arrowIMGV.transform = CGAffineTransform(rotationAngle: .pi/2)
            }, completion: { (success) in
                UIView.animate(withDuration: 0.35, delay: 0, options: .curveLinear, animations: {
                    headerCell.arrowIMGV.transform = CGAffineTransform.identity
                }, completion: { (success) in
                })
            })
        }

        return headerCellContentView
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowCount = 0
        
        if section == 0 {
            // overview
            rowCount = isCollapsed[section] ? getRowSourceForSectionAsJson(forKey: overviewText).count : 0
        }else if section == 1 {
            //specfications and features
            let source = sectionDataSource[specificationsText] as! PLUsedCarsDetailsSpecifications
            if selectedToggleTag == 1 {
                // specification -  first button selected
                rowCount = isCollapsed[section] ? source.specificationList.count + 1 : 0
            }else if selectedToggleTag == 2 {
                // features -  second button selected
                rowCount = isCollapsed[section] ? source.featuresList.count + 1 : 0
            }
        }else {
            //print("numberOfRowsInSection-Else case")
        }
        return rowCount
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = indexPath.section
        let row = indexPath.row
        if section == 0 {
            // overview section
//            print("overview")
            let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsDetailsOverViewTVC") as! PLUsedCarsDetailsOverViewTVC
            let rowSource = getRowSourceForSectionAsJson(forKey: overviewText)
            
            let source = getDataForRow(of: row, from: rowSource).first!

            
            if source.key.dispayString() == "Reg No" {
                cell.descriptionLBL.text = "Register Number"

            }else {
                cell.descriptionLBL.text = source.key.dispayString()

            }
            //cell.descriptionLBL.text = source.key.dispayString()
            
            var valueString: String = notMentioned
            /// checking any value is other than string type
            if let value = source.value as? String {
                valueString = value
            }else if let value = source.value as? Int {
                valueString = value.description
            }
            
            /// checking any key-value containes date and format it
            if source.key.dispayString().containsIgnoringCase(find: "validity") {
                cell.valueLBL.text = valueString.formatedDateString()
            }else {
                cell.valueLBL.text = valueString

            }

            return cell
        }else {
            /// specification rows add another row
            if row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsDetailsToggleTVC") as! PLUsedCarsDetailsToggleTVC
                cell.firstToggleLBL.text = subSpecifications
                cell.secondToggleLBL.text = subFeatures
                
                cell.firstToggleBTN.tag = section
                cell.secondToggleBTN.tag = section
                
                if selectedToggleTag == 1 {
                    /// suspension button
                    cell.firstToggleLBL.textColor = UIColor.white
                    UIView.animate(withDuration: 0.16, animations: {
                        cell.frstToggleView.layer.backgroundColor = UIColor.themeGreen.cgColor

                    })
                    cell.secondToggleLBL.textColor = UIColor.black.withAlphaComponent(0.55)
                    cell.secondToggleView.backgroundColor = UIColor.white
                    
                }else if selectedToggleTag == 2 {
                    /// features button
                    cell.firstToggleLBL.textColor = UIColor.black.withAlphaComponent(0.55)
                    cell.frstToggleView.backgroundColor = UIColor.white
                    
                    cell.secondToggleLBL.textColor = UIColor.white
                    UIView.animate(withDuration: 0.16, animations: {
                        cell.secondToggleView.layer.backgroundColor = UIColor.themeGreen.cgColor
                        
                    })
                    
                }else{
                    /// none of the button selected state - not occurs
                    cell.firstToggleLBL.textColor = UIColor.black.withAlphaComponent(0.55)//UIColor.DarkColor()
                    cell.frstToggleView.backgroundColor = UIColor.white
                    cell.secondToggleLBL.textColor = UIColor.black.withAlphaComponent(0.55) //DarkColor
                    cell.secondToggleView.backgroundColor = UIColor.white
                }
                
                return cell
            }else {
                let rowSource = sectionDataSource[specificationsText] as! PLUsedCarsDetailsSpecifications
                
                if selectedToggleTag == 1 {
                    // specifications
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsDetailsOverViewTVC") as! PLUsedCarsDetailsOverViewTVC
                    
                    let source = getDataForRow(of: row - 1,section: 1, from: rowSource.specificationList).first!
                    
                    cell.descriptionLBL.text = source.key.dispayString()
//                    cell.valueLBL.text = source.value as? String ?? ""
                    if let value = source.value as? String {
                        cell.valueLBL.text = value
                    }else if let value = source.value as? Int {
                        cell.valueLBL.text = value.description
                        
                    }else {
                        cell.valueLBL.text = notMentioned
                        
                    }
                    return cell
                }else {
                    // features yes/no type cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PLUsedCarsDetailsFeatureTVC") as! PLUsedCarsDetailsFeatureTVC
                    
                    cell.descriptionLBL.text = rowSource.featuresList[row - 1].dispayString()
                    
                    return cell
                    
                }
                
            }
            
        }

    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let section = sender.view!.tag
        isCollapsed[section] = !isCollapsed[section]
        self.detailsTV.reloadSections([section], with: UITableViewRowAnimation.fade)
        if isCollapsed[section] {
            let indexPath = IndexPath(row: 0, section: section)
            self.detailsTV.scrollToRow(at: indexPath, at: .none, animated: true)
        }

    }
    
    

}

// Mark:- Handle web service
extension PLUsedCarsDetailsVC {

    private func getCarDetailsWeb(){
        var params = Json()
        let url = "getusedcardetails"
        params["user_id"] = PLAppState.session.userID
        params["usedcarid"] = self.usedCarId
        WebServices.postMethod(url: url, parameter: params) { (isSucceeded, response) in
            
            guard isSucceeded, let responseData = response["data"] as? Json, !responseData.isEmpty  else { return }
            self.carDetails = PLUsedCarsDetailsModel(object: responseData)
            BaseThread.asyncMain {
                self.setUpInterface()
            }
        }
    }
    
    
    fileprivate func addToFavouriteWeb(params: Information, completion: @escaping ((Bool)->())){
       // print("addding")
        WebServices.postMethodWithoutActivityBanner(url:"addtofavlist", parameter: params, CompletionHandler: { (isFetched, result) in
            
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })
        
    }
    
    fileprivate func deleteFromFavouriteWeb(params: Information, completion: @escaping ((Bool)->())){
        //print("deleting")
        WebServices.postMethodWithoutActivityBanner(url:"deletefavlist", parameter: params, CompletionHandler: { (isFetched, result) in
            
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })
        
    }
    
    private func addToCompareWeb(params: Information, completion: @escaping ((Bool)->())){
        let url = "addtocomparelist"
        
        WebServices.postMethodWithoutActivity(url: url, parameter: params, CompletionHandler: { (isFetched, result) in
            
            if isFetched {
                completion(true)
            }else {
                completion(false)
            }
        })
    }
    
    private func getCompareFavouriteCountWeb(isFromCompareAction: Bool = false){
        let url = "getUsedCarsFav_comp_Count"
        var params = Json()
        params["user_id"] = PLAppState.session.userID
        
        WebServices.postMethodWithoutActivity(url: url, parameter: params) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["data"] as? Json, !data.isEmpty else { return }
            let compare = data["compare_count"] as! Int
            let favour = data["favourite_count"] as! Int
           // print(compare,"getUsedCarsFav_comp_Count",favour)
            compareCount = compare
            favourCount = favour
            BaseThread.asyncMain {
                
                self.favouritesCountLBL.text = favourCount.description
                self.compareCountLBL.text = compareCount.description
                self.compareCountLBL.isHidden = compareCount.isZero
                self.favouritesCountLBL.isHidden = favourCount.isZero
                
                if let homeVC = self.parent?.parent as? PLUsedCarsHomeVC {
                    homeVC.favouriteCountLBL.text = favourCount.description
                    homeVC.compareCountLBL.text = compareCount.description
                    homeVC.compareCountLBL.isHidden = compareCount == 0 ? true : false
                    homeVC.favouriteCountLBL.isHidden = favourCount == 0 ? true : false
                    if isFromCompareAction && compareCount == 2 {
                        let segueVC = self.storyBoardCompare!.instantiateViewController(withIdentifier: "PLCompareVC") as! PLCompareVC
                        self.present(segueVC)
                        
                    }
                    
                }
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                
            }
            
        }
    }

    
    
    
}

/*
 //    @inline(__always) func getRowCountForSectionJson(of section: Int) -> Json{
 //        let index = sectionDataSource.index(sectionDataSource.startIndex, offsetBy: section)
 //        let value = sectionDataSource.values[index]        //[index]
 //        print("getRowCountForSectionJson===========")
 //        print("section",section,"rowValue:",value)
 //        let rowSource = value as! Json
 //        print("rowSource:",rowSource)
 //        return rowSource // full sources of curresponing section
 //    }

 */

