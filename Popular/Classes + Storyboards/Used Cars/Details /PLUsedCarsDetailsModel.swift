//
//  PLUsedCarsDetailsModel.swift
//  Popular
//
//  Created by Appzoc on 23/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation

class PLUsedCarsDetailsModel {
    var data:PLUsedCarsDetailsData = PLUsedCarsDetailsData()
    var overview: Json = Json()     // var overview: PLUsedCarsDetailsOverview = PLUsedCarsDetailsOverview()
    var specifications:PLUsedCarsDetailsSpecifications = PLUsedCarsDetailsSpecifications()
    var comparecount: Int = 0
    var favouritecount: Int = 0
    
    init(object: Json) {
        data = PLUsedCarsDetailsData.init(dictionary: object["data"] as! Json)
        overview = object["overview"] as! Json
        
        //overview = PLUsedCarsDetailsOverview.init(dictionary: object["overview"] as! Json)
        specifications = PLUsedCarsDetailsSpecifications.init(dictionary:  object["specifications"] as! Json)
        comparecount = object["comparecount"] as? Int ?? 0
        favouritecount = object["favouritecount"] as? Int ?? 0

    }
    
    init() {
        
    }
    
    class PLUsedCarsDetailsData {
        
        var idx:String = ""
        var title:String = ""
        var nice_title:String = ""
        var slug:String = ""
        var location:String = ""
        var stock_id:String = ""
        var vin:String = ""
        var make_id:String = ""
        var model_tid:String = ""
        var year:String = ""
        var ins_type:String = ""
        var odometer:String = ""
        var mileage:String = ""
        var torque:String = ""
        var no_gears:String = ""
        var pw_ratio:String = ""
        var thumb:String = ""
        var price:String = ""
        var price_sale:String = ""
        var delivered:String = ""
        var fuel:String = ""
        var fuel_capacity:String = ""
        var ground_clearance:String = ""
        var drive_train:String = ""
        var engine:String = ""
        var transmission:String = ""
        var horse_power:String = ""
        var towing_capacity:String = ""
        var features:String = ""
        var body:String = ""
        var notes:String = ""
        var featured:String = ""
        var hits:String = ""
        var sold:String = ""
        var websale:String = ""
        var bid:String = ""
        var webuser:String = ""
        var certified:String = ""
        var is_owner:String = ""
        var rejected:String = ""
        var metakey:String = ""
        var metadesc:String = ""
        var soldexpire:String = ""
        var expire:String = ""
        var registration_expire:String = ""
        var tax:String = ""
        var insurance:String = ""
        var modified:String = ""
        var created:String = ""
        var status:String = ""
        var strvalues:String = ""
        var kilometer:Int = 0
        var expected_price:String = ""
        var model_year:Int = 0
        var popular_verified:Bool = false
        var tax_validity:String = ""
        var insurance_validity:String = ""
        var registration_validity:String = ""
        var images:[String] = []
        var image_count:Int = 0
        var brand:String = ""
        var variant:String = ""
        var fueltype:String = ""
        var share_link:String = ""
        var isfavourite:Bool = false
        var iscomparelist:Bool = false
        var usedcarid:Int = 0
        
        init(dictionary: Json) {
            
            idx = dictionary["idx"] as? String ?? ""
            title = dictionary["title"] as? String ?? ""
            nice_title = dictionary["nice_title"] as? String ?? ""
            slug = dictionary["slug"] as? String ?? ""
            location = dictionary["location"] as? String ?? ""
            stock_id = dictionary["stock_id"] as? String ?? ""
            vin = dictionary["vin"] as? String ?? ""
            make_id = dictionary["make_id"] as? String ?? ""
            model_tid = dictionary["model_tid"] as? String ?? ""
            year = dictionary["year"] as? String ?? "0"
            ins_type = dictionary["ins_type"] as? String ?? ""
            odometer = dictionary["odometer"] as? String ?? ""
            mileage = dictionary["mileage"] as? String ?? ""
            torque = dictionary["torque"] as? String ?? ""
            no_gears = dictionary["no_gears"] as? String ?? ""
            pw_ratio = dictionary["pw_ratio"] as? String ?? ""
            thumb = dictionary["thumb"] as? String ?? ""
            price = dictionary["price"] as? String ?? ""
            price_sale = dictionary["price_sale"] as? String ?? ""
            delivered = dictionary["delivered"] as? String ?? ""
            fuel = dictionary["fuel"] as? String ?? ""
            fuel_capacity = dictionary["fuel_capacity"] as? String ?? ""
            ground_clearance = dictionary["ground_clearance"] as? String ?? ""
            drive_train = dictionary["drive_train"] as? String ?? ""
            engine = dictionary["engine"] as? String ?? ""
            transmission = dictionary["transmission"] as? String ?? ""
            horse_power = dictionary["horse_power"] as? String ?? ""
            towing_capacity = dictionary["towing_capacity"] as? String ?? ""
            features = dictionary["features"] as? String ?? ""
            body = dictionary["body"] as? String ?? ""
            notes = dictionary["notes"] as? String ?? ""
            featured = dictionary["featured"] as? String ?? ""
            hits = dictionary["hits"] as? String ?? ""
            sold = dictionary["sold"] as? String ?? ""
            websale = dictionary["websale"] as? String ?? ""
            bid = dictionary["bid"] as? String ?? ""
            webuser = dictionary["webuser"] as? String ?? ""
            certified = dictionary["certified"] as? String ?? ""
            is_owner = dictionary["is_owner"] as? String ?? ""
            rejected = dictionary["rejected"] as? String ?? ""
            metakey = dictionary["metakey"] as? String ?? ""
            metadesc = dictionary["metadesc"] as? String ?? ""
            soldexpire = dictionary["soldexpire"] as? String ?? ""
            expire = dictionary["expire"] as? String ?? ""
            registration_expire = dictionary["registration_expire"] as? String ?? ""
            tax = dictionary["tax"] as? String ?? ""
            insurance = dictionary["insurance"] as? String ?? ""
            modified = dictionary["modified"] as? String ?? ""
            created = dictionary["created"] as? String ?? ""
            status = dictionary["status"] as? String ?? ""
            strvalues = dictionary["strvalues"] as? String ?? ""
            kilometer = dictionary["kilometer"] as? Int ?? 0
            expected_price = dictionary["expected_price"] as? String ?? ""
            model_year = dictionary["model_year"] as? Int ?? 0
            if let verified = dictionary["popular_verified"] as? String, verified == "1" {
                popular_verified = true
            }else {
                popular_verified = false
            }
            tax_validity = dictionary["tax_validity"] as? String ?? ""
            insurance_validity = dictionary["insurance_validity"] as? String ?? ""
            registration_validity = dictionary["registration_validity"] as? String ?? ""
            images = dictionary["images"] as? [String] ?? []
            image_count = dictionary["image_count"] as? Int ?? 0
            brand = dictionary["brand"] as? String ?? ""
            variant = dictionary["variant"] as? String ?? ""
            fueltype = dictionary["fueltype"] as? String ?? ""
            share_link = dictionary["share_link"] as? String ?? ""
            isfavourite = dictionary["isfavourite"] as? Bool ?? false
            iscomparelist = dictionary["iscomparelist"] as? Bool ?? false
            usedcarid = dictionary["usedcarid"] as? Int ?? 0
            
        }
        
//        class func getValue(fromArray: JsonArray) -> [PLUsedCarsDetailsModel]{
//            var result: [PLUsedCarsDetailsModel] = []
//            for item in result {
//                result.append(item)
//            }
//        }
        
        init() {
            
        }
        
    }

}

//Mark:- Specification and features
class PLUsedCarsDetailsSpecifications
{
    var specificationList:Json = Json()
    var featuresList:[String] = []
    
    init(dictionary:Json) {
        specificationList = dictionary["specification"] as? Json ?? Json()
        featuresList = dictionary["features"] as? [String] ?? []
    }
    
    init() {
        
    }
    
}

/*
 
 // Mark:- Overview
 class PLUsedCarsDetailsOverview
 {
 var body_type:String = ""
 var reg_no:String = ""
 var color:String = ""
 var transmission:String = ""
 var max_power:String = ""
 var tax_validity:String = ""
 var insurance_validity:String = ""
 var model:String = ""
 var no_of_owners:String = ""
 var registration_validity:String = ""
 
 init() {
 
 }
 
 init(dictionary:Json) {
 
 body_type = dictionary["body_type"] as? String ?? ""
 reg_no = dictionary["reg_no"] as? String ?? ""
 color = dictionary["color"] as? String ?? ""
 transmission = dictionary["transmission"] as? String ?? ""
 max_power = dictionary["max_power"] as? String ?? ""
 tax_validity = dictionary["tax_validity"] as? String ?? ""
 insurance_validity = dictionary["insurance_validity"] as? String ?? ""
 model = dictionary["model"] as? String ?? ""
 no_of_owners = dictionary["no_of_owners"] as? String ?? ""
 registration_validity = dictionary["registration_validity"] as? String ?? ""
 }
 
 }

 */
