//
//  PLUsedCarsDetailsTVC.swift
//  Popular
//
//  Created by Appzoc on 23/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit


class PLUsedCarsDetailsHeaderTVC: UITableViewCell {
    
    @IBOutlet weak var titileLBL: UILabel!
    @IBOutlet weak var arrowIMGV: UIImageView!

}


class PLUsedCarsDetailsOverViewTVC: UITableViewCell {
    
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var valueLBL: UILabel!

}

class PLUsedCarsDetailsToggleTVC: UITableViewCell {
    
    @IBOutlet weak var frstToggleView: BaseView!
    @IBOutlet weak var firstToggleLBL: UILabel!
    @IBOutlet weak var firstToggleBTN: UIButton!
    @IBOutlet weak var secondToggleView: BaseView!
    @IBOutlet weak var secondToggleLBL: UILabel!
    @IBOutlet weak var secondToggleBTN: UIButton!
    
}

class PLUsedCarsDetailsFeatureTVC: UITableViewCell {
    
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var valueLBL: UILabel!

}
