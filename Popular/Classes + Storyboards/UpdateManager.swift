//
//  UpdateManager.swift
//  Popular
//
//  Created by Appzoc on 22/02/19.
//  Copyright © 2019 Appzoc. All rights reserved.
//

import Foundation

class UpdateManager{
    
    static let main:UpdateManager = UpdateManager()
    
    private init(){}
    
    var normalUpdateAvailable:Bool = false
    var forceUpdateAvailable:Bool = false
    
    func checkUpdate(){
        WebServices.postMethod(url: "forceUpdatenew", parameter: ["type":"ios"]) { (isComplete, json) in
            if isComplete{
                print("Update details ",json.debugDescription)
                if let jsonDataArray = json["data"] as? [[String:Any]], let jsonData = jsonDataArray.first{
                    print("JSONData ",jsonData["ios_forceupdate_version"])
                    let storeCurrentVersion = jsonData["ios_normalupdate_version"] as? Double ?? 0
                    let storeForceUpdateVersion = jsonData["ios_forceupdate_version"] as? Double ?? 800
                    if let currentAppVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String{
                        let digitizedVersion = self.getDigitizedVersion(currentAppVersion)
                        if digitizedVersion < storeForceUpdateVersion{
                            print("Force Update Required")
                            self.forceUpdateAvailable = true
                            NotificationCenter.default.post(name: .forceUpdateNotify, object: nil)
                        }
                        print("Current App Version ",currentAppVersion)
                        print("Digitized Version ",digitizedVersion.description)
                        print("Store Force Update Version ",storeForceUpdateVersion.description)
                    }
                }
            }
        }
    }
    //"1.0.42"
    private func getDigitizedVersion(_ version:String) -> Double{
        let numberArray = version.components(separatedBy: ".")
        let numberString = numberArray.joined()
        let digitizedVersion = Double(numberString) ?? 0
        return digitizedVersion
    }
    
    func showUpdateAlert(on vc:UIViewController){
        guard UpdateManager.main.forceUpdateAvailable else { return }
        let alertVC = UIAlertController(title: "Update Required", message: "Please update application to continue.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            if let storeURL = URL(string: "https://itunes.apple.com/app/id1453987246"){
                UIApplication.shared.open(storeURL, options: [:], completionHandler: nil)
            }
        })
        alertVC.addAction(okAction)
        vc.present(alertVC, animated: true, completion: nil)
    }
}

extension Notification.Name{
    static var forceUpdateNotify:Notification.Name{
        return Notification.Name.init("forceUpdateNotify")
    }
}
