//
//  PLCompareVC.swift
//  Popular
//
//  Created by Appzoc on 26/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

protocol CompareUsedCarDelegate {
    func didFinishCompare(deletedCarId: [Int])
}

class PLCompareVC: UIViewController {
    
    //MARK:- Top Container Outlets
    //Left
    @IBOutlet var leftPriceLBL: UILabel!
    @IBOutlet var leftMakeLBL: UILabel!
    @IBOutlet var leftModelLBL: UILabel!
    @IBOutlet var leftYearLBL: UILabel!
    @IBOutlet var leftStack: UIStackView!
    @IBOutlet var leftTapLabelView: UIView!
    @IBOutlet var leftCarImage: UIImageView!
    @IBOutlet var leftTapButtonView: UIView!
    @IBOutlet var leftDeleteButtonView: BaseView!
    @IBOutlet var leftDeleteBTNRef: UIButton!
    
    //Right
    @IBOutlet var rightPriceLBL: UILabel!
    @IBOutlet var rightMakeLBL: UILabel!
    @IBOutlet var rightModelLBL: UILabel!
    @IBOutlet var rightYearLBL: UILabel!
    @IBOutlet var rightStack: UIStackView!
    @IBOutlet var rightTapLabelView: UIView!
    @IBOutlet var rightTapButtonView: UIView!
    @IBOutlet var rightCarImage: UIImageView!
    @IBOutlet var rightDeleteButtonView: BaseView!
    @IBOutlet var rightDeleteBTNRef: UIButton!
    
    
    @IBOutlet var tableHeight: NSLayoutConstraint!
    @IBOutlet var tableRef: UITableView!
    
    //final var usedCarDelegate: CompareUsedCarDelegate? //  this is for reload the used car list vc if the user removed car from used car list

    var leftCarState:CarState = .absent{didSet{setLeftCarState()}}
    var rightCarState:CarState = .absent{didSet{setRightCarState()}}
    var overviewSectionCollapsed:Bool = true
    var featuresSectionCollapsed:Bool = true
    
    var compareData:[CompareCarModel] = []{
        didSet{
            if self.compareData.count > 0{
                self.leftCarState = .present
            }else{
                self.leftCarState = .absent
            }
            if self.compareData.count > 1{
                self.rightCarState = .present
            }else{
                self.rightCarState = .absent
            }
        }
    }
    
    var leftOnlySet:Set<String> = Set()
    var rightOnlySet:Set<String> = Set()
    var commonSet:Set<String> = Set()
    private var deletedCarIds: [Int] = []
    private var compareDataCount: Int = 0
    final var usedCarDelegate: CompareUsedCarDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftCarState()
        setRightCarState()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        setUpWebCall()
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpWebCall()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant = self.tableRef.contentSize.height
    }
    
    //MARK:- Button Actions
    @IBAction func toggleStateForSection(_ sender: BaseButton) {
        if sender.tag == 0{
         toggleOverviewState()
        }else if sender.tag == 1{
         toggleFeaturesState()
        }
    }
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        //print("Delete Tapped")
        let usedCarID = sender.tag
        let userId =  PLAppState.session.userID
        WebServices.postMethod(url: "deletecomparelist", parameter: ["user_id": userId,"usedcarid":"\(usedCarID)"]) { (isComplete, json) in
            if isComplete{
                self.deletedCarIds.append(usedCarID)
                
                if self.compareDataCount == 2 { // if there are two cars in the comparelist
//                    if self.deletedCarIds.count == 1 { // if one car is deleted
//                        self.setUpWebCall()
//                    }
                    self.setUpWebCall()

                }else {
//                    if let listVC = self.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLUsedCarsListVC {
//                        listVC.didFinishCompare(deletedCarId: self.deletedCarIds)
//                    }
                    
                    //self.usedCarDelegate?.didFinishCompare(deletedCarId: self.deletedCarIds)
                    NotificationCenter.default.post(Notification(name: .usedCarCompare, object: nil, userInfo: ["deletedIds": self.deletedCarIds]))
                    
                    self.dismiss()

                }
                
                
               // self.setUpWebCall()
            }
        }
    }
    
    @IBAction func addCarToCompareTapped(_ sender: UIButton) {
       // print("Popped")
//
//        if let listVC = self.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLUsedCarsListVC {
//            listVC.didFinishCompare(deletedCarId: deletedCarIds)
//        }
        //self.usedCarDelegate?.didFinishCompare(deletedCarId: self.deletedCarIds)
        NotificationCenter.default.post(Notification(name: .usedCarCompare, object: nil, userInfo: ["deletedIds": self.deletedCarIds]))

        dismiss()//self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
//        if let listVC = self.presentingViewController?.childViewControllers[0].childViewControllers[0] as? PLUsedCarsListVC {
//            listVC.didFinishCompare(deletedCarId: deletedCarIds)
//
//        }
        //self.usedCarDelegate?.didFinishCompare(deletedCarId: self.deletedCarIds)
        //print("Deleted ComparedCar Ids")
        NotificationCenter.default.post(Notification(name: .usedCarCompare, object: nil, userInfo: ["deletedIds": self.deletedCarIds]))

        dismiss()
    }
    
    
    
    //MARK:- Methods
    func setLeftCarState(){
        switch leftCarState{
            case .present:
                DispatchQueue.main.async {
                    self.leftStack.isHidden = false
                    self.leftTapLabelView.isHidden = true
                    self.leftTapButtonView.isHidden = true
                    self.leftDeleteButtonView.isHidden = false
                    self.leftCarImage.isHidden = false
                    self.leftPriceLBL.isHidden = false
                }
            case .absent:
                DispatchQueue.main.async {
                    self.leftStack.isHidden = true
                    self.leftTapLabelView.isHidden = false
                    self.leftTapButtonView.isHidden = false
                    self.leftDeleteButtonView.isHidden = true
                    self.leftCarImage.isHidden = true
                    self.leftPriceLBL.isHidden = true
                }
        }
    }

    func setRightCarState(){
        switch rightCarState{
        case .present:
            DispatchQueue.main.async {
                self.rightStack.isHidden = false
                self.rightTapLabelView.isHidden = true
                self.rightTapButtonView.isHidden = true
                self.rightDeleteButtonView.isHidden = false
                self.rightCarImage.isHidden = false
                self.rightPriceLBL.isHidden = false
            }
        case .absent:
            DispatchQueue.main.async {
                self.rightStack.isHidden = true
                self.rightTapLabelView.isHidden = false
                self.rightTapButtonView.isHidden = false
                self.rightDeleteButtonView.isHidden = true
                self.rightCarImage.isHidden = true
                self.rightPriceLBL.isHidden = true
            }
        }
    }

    func setUpTopContainer(){
        if compareData.count > 0{
            let firstCarData = compareData[0]
            DispatchQueue.main.async {
                self.leftPriceLBL.text = firstCarData.expectedPrice.changeToPriceInLakhs()
                self.leftMakeLBL.text = firstCarData.brand
                self.leftModelLBL.text = firstCarData.model
                self.leftYearLBL.text = firstCarData.year.description
                let carImage = URL(string: "\(firstCarData.image)")
                self.leftCarImage.kf.setImage(with: carImage, placeholder: nil, options: [.requestModifier(getImageAuthorization())])
                self.leftDeleteBTNRef.tag = firstCarData.usedCarID
            }
        }
        
        if compareData.count > 1{
            let secondCarData = compareData[1]
            DispatchQueue.main.async {
                self.rightPriceLBL.text = secondCarData.expectedPrice.changeToPriceInLakhs()
                self.rightMakeLBL.text = secondCarData.brand
                self.rightModelLBL.text = secondCarData.model
                self.rightYearLBL.text = secondCarData.year.description
                let carImage = URL(string: "\(secondCarData.image)")
                self.rightCarImage.kf.setImage(with: carImage, placeholder: nil, options: [.requestModifier(getImageAuthorization())])
                self.rightDeleteBTNRef.tag = secondCarData.usedCarID
            }
        }
    }
    
    func toggleOverviewState(){
        if overviewSectionCollapsed{
            overviewSectionCollapsed = false
        }else{
            overviewSectionCollapsed = true
        }
        tableRef.reloadData()
        viewWillLayoutSubviews()
//        DispatchQueue.main.async {
//            self.tableHeight.constant = self.tableRef.contentSize.height
//        }
       // tableRef.reloadSections(IndexSet(integer: 0), with: .none)
    }
    
    func toggleFeaturesState(){
        if featuresSectionCollapsed{
            featuresSectionCollapsed = false
        }else{
            featuresSectionCollapsed = true
        }
        tableRef.reloadData()
        viewWillLayoutSubviews()
//        DispatchQueue.main.async {
//            self.tableHeight.constant = self.tableRef.contentSize.height
//        }
       // tableRef.reloadSections(IndexSet(integer: 1), with: .none)
    }
    
    func setUpWebCall(){
        let userId = PLAppState.session.userID
        WebServices.postMethod(url: "getcomparelist", parameter: ["user_id":userId]) { (isComplete, json) in
            if isComplete{
                if let jsonData = json["data"] as? [[String:Any]]{
                    self.compareData.removeAll()
                    for item in jsonData{
                        let parsedData = CompareCarModel(json: item)
                        self.compareData.append(parsedData)
                    }
                    self.compareDataCount = self.compareData.count
                    self.valuateSets()
                    self.setUpTopContainer()
                    self.tableRef.reloadData()
                    self.tableHeight.constant = self.tableRef.contentSize.height
                }
            }
        }
    }
    
    func valuateSets(){
        leftOnlySet.removeAll()
        rightOnlySet.removeAll()
        commonSet.removeAll()
        if self.compareData.count > 1{
            let firstSet = Set(compareData[0].features)
            let secondSet = Set(compareData[1].features)
            leftOnlySet = firstSet.subtracting(secondSet)
            rightOnlySet = secondSet.subtracting(firstSet)
            commonSet = firstSet.intersection(secondSet)
        }else if self.compareData.count > 0{
            commonSet = Set(compareData[0].features)
        }
    }

}



