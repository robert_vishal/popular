//
//  AppDelegate.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreData
import SideMenu
import UserNotifications
var isEnteredBackGround: Bool = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        _ = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        //print(Urls[Urls.count - 1] as! URL)
        SideMenuManager.default.menuFadeStatusBar = false
        registerForPushNotifications()
        UpdateManager.main.checkUpdate()
        if (launchOptions?[.remoteNotification] as? [String: AnyObject]) != nil {
            PLAppState.session.isLaunchByNotification = true
        }
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.set(deviceTokenString, forKey: "DeviceToken")
        print("APNs device token: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.standard.set("null", forKey: "DeviceToken")
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Push notification received: \(String(describing: userInfo["aps"])),\(userInfo)")
        
        if let aps =  userInfo["aps"] as? [AnyHashable : Any], let alert = aps["alert"] as? [AnyHashable:Any], let message = alert["title"] as? String{
            showBanner(title: "", message: message, style: .success)
        }
        
        if ( application.applicationState == .active ){
            print("Push Received")
            
            
          //  showBanner(message: <#T##String#>)
            // app was already in the foreground
        }
        else{
              self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PLHomeVC")
              let segueVC = UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "PLNotificationVC") as! PLNotificationVC
               self.window?.rootViewController?.present(segueVC)
            }
    }
    
    func registerForPushNotifications(){
        if #available(iOS 10, *){
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound], completionHandler: { (granted, error) in
                guard granted else {return}
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }else /*if #available(iOS 9, *)*/{
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Popular")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //print("applicationDidEnterBackground")
        isEnteredBackGround = true
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //print("applicationDidBecomeActive")
    }

}

