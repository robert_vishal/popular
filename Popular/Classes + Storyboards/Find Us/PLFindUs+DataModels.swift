//
//  PLFindUs+DataModels.swift
//  Popular
//
//  Created by Appzoc on 10/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation

extension PLFindUsVC{
    
    struct FindUsModel: Codable{
        let mainbranch:[AddressData]
        let branchtypes:[BranchType]
        
        struct AddressData: Codable{
            let id:Int
            let name:String
            let address:String
            let latitude:String = "0"
            let longitude:String = "0"
            let typeid:Int
            let location_id:Int
            let branch_head:String?
            let main:Int
            let location:String = ""
            let map_link:String?
            let head_email:String?
            let head_mobile:String?
        }
        
        struct BranchType: Codable{
            let id:Int
            let category:String
            let type: Int
        }
    }
    
    
    struct LocationDataModel: Codable{
        let id:Int
        let location:String?
    }
    
    
}
