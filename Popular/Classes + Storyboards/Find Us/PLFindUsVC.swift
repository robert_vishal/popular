//
//  PLFindUsVC.swift
//  Popular
//
//  Created by Appzoc on 07/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLFindUsVC: UIViewController {
    
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    
    @IBOutlet var typeLBL: UILabel!
    @IBOutlet var locationLBL: UILabel!
    
    var typeListData:[PLSideOptionsModel] = []
    var typeListFullData:[FindUsModel.BranchType] = []

    var locationListData:[PLSideOptionsModel] = []
    var selectingType:Bool = true
    
    var latitude:String = "0"
    var longitude:String = "0"
    
    var mapLink:String = ""
    
    var selectedType:Int = 0
    private var selectedTypeForLocation: Int = 0
    
    var selectedLocation:Int = 0
    var passingAddressData:[FindUsModel.AddressData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpWebCall()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    

    @IBAction func viewInMapTapped(_ sender: BaseButton) {
        //print("View In Map Tapped")
        self.openInMap()
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
       // print("Search Tapped")
        guard validate() else { return }
        let param = ["type_id":"\(selectedType)","location_id":"\(selectedLocation)"]
        print(param.description)
        WebServices.postMethod(url: "getaddress", parameter: param) { (isComplete, json) in
            if isComplete{
                do{
                    let decoder = JSONDecoder()
                    let requiredJson = json["data"] as? [[String:Any]]
                    let jsonData = serializeJSON(json: requiredJson as Any)
                    if let _ = jsonData{
                        let parsedData = try decoder.decode([FindUsModel.AddressData].self, from: jsonData!)
                        self.passingAddressData = parsedData
                        let segueVC = self.storyBoardFindUs?.instantiateViewController(withIdentifier: "PLFindUsTableListingVC") as! PLFindUsTableListingVC
                        segueVC.addressData = parsedData
                        self.present(segueVC)
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    @IBAction func typeTapped(_ sender: UIButton) {
       // print("Type Tapped")
        self.selectingType = true
        showSideOption(presentOn: self, delegate: self, source: typeListData)
    }
    
    @IBAction func locationTapped(_ sender: UIButton) {
       // print("Location Tapped")
        if selectedType != 0{
            self.selectingType = false
            showSideOption(presentOn: self, delegate: self, source: self.locationListData)
        }else{
            showBanner(message: "Please Select Type")
        }
    }
    
    func setUpWebCall(){
        WebServices.getMethodWith(url: "findUs", parameter: nil) { (isComplete, json) in
            if isComplete{
                do{
                    let decoder = JSONDecoder()
                    let requiredJson = json["data"] as? [String:Any]
                    let jsonData = serializeJSON(json: requiredJson as Any)
                    if let _ = jsonData{
                        let parsedData = try decoder.decode(FindUsModel.self, from: jsonData!)
                        self.typeListData = parsedData.branchtypes.map({PLSideOptionsModel(sourceName: $0.category, sourceID: $0.id)})
                        self.typeListFullData = parsedData.branchtypes
                        self.setUpInterface(with: parsedData)
                        
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func setUpInterface(with parsedData:FindUsModel){
        if let _ = parsedData.mainbranch.first{
            DispatchQueue.main.async {
                self.titleLBL.text = parsedData.mainbranch.first!.name
                self.addressLBL.text = "\(parsedData.mainbranch.first!.address) \n\(parsedData.mainbranch.first!.location)"
                self.latitude = parsedData.mainbranch.first!.latitude
                self.longitude = parsedData.mainbranch.first!.longitude
            }
            self.mapLink = parsedData.mainbranch.first!.map_link ?? "https://www.google.com/maps/@\(self.latitude),\(self.longitude),18z"
        }
    }
    
    func retrieveLocationForType(){
        //http://popular.dev.webcastle.in/api/get/locations?type=5
        //type_id
        WebServices.getMethodWith(url: "get/locations", parameter: ["type":"\(selectedTypeForLocation)"]) { (isComplete, response) in
            if isComplete {
                guard let responseData = response["data"] as? JsonArray else { return }
                BaseThread.asyncMain {
                    self.locationListData = PLSideOptionsModel.getData(fromArray: responseData, idKey: "id", nameKey: "name", isStringId: false)
                }
                
//                do{
//                    let decoder = JSONDecoder()
//                    let requiredJson = json["data"] as? [[String:Any]]
//                    let jsonData = serializeJSON(json: requiredJson as Any)
//                    if let _ = jsonData{
//                        let parsedData = try decoder.decode([LocationDataModel].self, from: jsonData!)
//                        self.locationListData = parsedData.map({PLSideOptionsModel(sourceName: $0.location ?? "null", sourceID: $0.id)})
//                    }
//                }catch{
//                    print(error)
//                }
            }else{
                self.selectedType = 0
            }
        }
    }
    
    func openInMap(){
        let mapAppURL = "comgooglemaps://?center=\(latitude),\(longitude)&zoom=18"
        let browserURL = mapLink //"https://www.google.com/maps/@\(latitude),\(longitude),18z"
//        print("Browser URL ",browserURL)
//        print("App URL ",mapAppURL)
        if let _ = URL(string: "comgooglemaps://"), UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!), latitude != "0", longitude != "0"{
            
            //UIApplication.shared.openURL(URL(string: "\(mapAppURL)")!)
            UIApplication.shared.open(URL(string: "\(mapAppURL)")!, options: [:], completionHandler: nil)
        }else if let browserURLCheck = URL(string: "\(browserURL)"), UIApplication.shared.canOpenURL(browserURLCheck){
            
            UIApplication.shared.open(URL(string: "\(browserURL)")!, options: [:], completionHandler: nil)

           // UIApplication.shared.openURL(URL(string: "\(browserURL)")!)
        }
    }
    
    func validate() -> Bool{
        if selectedType == 0{
            showBanner(message: "Select Type")
            return false
        }
        if selectedLocation == 0{
            showBanner(message: "Select Location")
            return false
        }
        
        return true
    }
    
}

extension PLFindUsVC: SideOptionsDelegate{
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        if selectingType{
            DispatchQueue.main.async {
                self.typeLBL.text = data.name
                
                self.selectedTypeForLocation = self.typeListFullData.filter({$0.id == data.id }).first?.type ?? 0
                self.selectedType = data.id
                self.locationLBL.text = "Choose Location"
                self.retrieveLocationForType()
            }
            self.selectedLocation = 0
        }else{
            DispatchQueue.main.async {
                self.locationLBL.text = data.name
                self.selectedLocation = data.id
            }
        }
    }
    
    
}
