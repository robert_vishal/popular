//
//  PLSmartSearchVC.swift
//  Poc_Popular
//
//  Created by Appzoc on 31/07/18.
//  Copyright © 2018 POC. All rights reserved.
//

import UIKit
import RangeSeekSlider

class PLSmartSearchColorCVC : UICollectionViewCell {
    @IBOutlet var selectionView: BaseView!
    @IBOutlet var colorView: BaseView!
    @IBOutlet var selectionImage: UIImageView!
    
}

class PLSmartSearchImageCVC : UICollectionViewCell {
    @IBOutlet var optionIconImage: BaseImageView!
    @IBOutlet var optionTitleLBL: UILabel!
    
}

// call this for getting the smart search parameter
protocol SmartSearchDelegate: class {
    func appliedSmartSearch(with parameter: Json, resultCount: String, isFromShowAll: Bool)
}

class PLSmartSearchVC: UIViewController, InterfaceSettable {

    @IBOutlet var rangeView: UIView!
    @IBOutlet var optionsColorCV: UICollectionView!
    @IBOutlet var optionsImageCV: UICollectionView!
    
    @IBOutlet var topHideViewForColorCV: UIView!
    
    @IBOutlet var bottomHideViewForColorCV: UIView!
    @IBOutlet var resultCountLBL: UILabel!
    
    @IBOutlet var priceMenuView: BaseView!
    @IBOutlet var colorMenuView: BaseView!
    @IBOutlet var bodyTypeMenuView: BaseView!
    @IBOutlet var kilometersMenuView: BaseView!
    @IBOutlet var fuelTypeMenuView: BaseView!
    @IBOutlet var featuresMenuView: BaseView!
    @IBOutlet var gearTypeMenuView: BaseView!

    @IBOutlet var priceIndicatorView: UIView!
    @IBOutlet var colorIndicatorView: UIView!
    @IBOutlet var bodyTypeIndicatorView: UIView!
    @IBOutlet var kilometerIndicatorView: UIView!
    @IBOutlet var fuelTypeIndicatorView: UIView!
    @IBOutlet var featuresIndicatorView: UIView!
    @IBOutlet var gearTypeIndicatorView: UIView!

    @IBOutlet var priceMenuLBL: UILabel!
    @IBOutlet var colorMenuLBL: UILabel!
    @IBOutlet var bodyTypeMenuLBL: UILabel!
    @IBOutlet var kilometersMenuLBL: UILabel!
    @IBOutlet var fuelTypeMenuLBL: UILabel!
    @IBOutlet var FeaturesMenuLBL: UILabel!
    @IBOutlet var gearTypeMenuLBL: UILabel!
    
    @IBOutlet var priceImage: UIImageView!
    @IBOutlet var colorImage: UIImageView!
    @IBOutlet var carImage: UIImageView!
    @IBOutlet var kilometerImage: UIImageView!
    @IBOutlet var fuelImage: UIImageView!
    @IBOutlet var featureImage: UIImageView!
    @IBOutlet var trasmissionImage: UIImageView!
    
    //
    @IBOutlet var containerRangeSlider: UIView!
    @IBOutlet var rangeSelector: RangeSeekSlider!
    @IBOutlet var minRangeLBL: UILabel!
    @IBOutlet var maxRangeLBL: UILabel!
    
    // accessible properties outise of the vc
    final weak var delegate:SmartSearchDelegate?
    
    // Properties
    private var previousMenuView: UIView? = nil
    private var prevoiusMenuLabel: UILabel? = nil
    private var menuType: PLSFTypes = .price
    private var currentMinPositionX:CGFloat = 0.0
    private var currentMinPositionY:CGFloat = 0.0
    private var currentMaxPositionX:CGFloat = 0.0
    private var currentMaxPositionY:CGFloat = 0.0
    private var minLBLPositionX: CGFloat = 0.0 {
        didSet{
            currentMinPositionX = oldValue
        }
    }
    private var minLBLPositionY: CGFloat = 0.0 {
        didSet{
            currentMinPositionY = oldValue
        }
    }
    private var maxLBLPositionX: CGFloat = 0.0 {
        didSet{
            currentMaxPositionX = oldValue
        }
    }
    private var maxLBLPositionY: CGFloat = 0.0 {
        didSet{
            currentMaxPositionY = oldValue
        }
    }
    private let menuAlphaDeselected: CGFloat = 0.8
    private let menuAlphaSelected: CGFloat = 1.0
    private let menuViewDisplacement:CGFloat = 20
    private var parameterForCount = Json()
    //private var parameterForSearch = Json()
    private var parameterForSearchFinal = Json()
    private var appliedAttributesCount = 0
    private var isResetAll: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpRangeSlider()
        containerRangeSlider.transform = CGAffineTransform.init(rotationAngle: -.pi/2)
        minRangeLBL.transform = CGAffineTransform.init(rotationAngle: .pi/2)
        maxRangeLBL.transform = CGAffineTransform.init(rotationAngle: .pi/2)
        
        optionsColorCV.scrollIndicatorInsets = UIEdgeInsets(top: 13, left: 0, bottom: 13, right: 0)
        
        setUpInterface()
    }

    override func viewDidAppear(_ animated: Bool) {
        menu1PriceTapped(UIButton())
    }
    
    private func setUpRangeSlider() {
        rangeSelector.delegate = self
        rangeSelector.handleImage = #imageLiteral(resourceName: "rangesliderThumb")
        rangeSelector.selectedHandleDiameterMultiplier = 1.3
        rangeSelector.lineHeight = 10.0
        rangeSelector.hideLabels = true
        
        minRangeLBL.layer.borderWidth = 2.5
        minRangeLBL.layer.cornerRadius = 12
        minRangeLBL.layer.borderColor = UIColor.themeGreenDark.cgColor
        //minRangeLBL.textColor = UIColor.themeGreenDark2

        minRangeLBL.layer.masksToBounds = true
        maxRangeLBL.layer.borderWidth = 2.5
        maxRangeLBL.layer.cornerRadius = 12
        maxRangeLBL.layer.borderColor = UIColor.themeGreenDark.cgColor
        //maxRangeLBL.textColor = UIColor.themeGreenDark2
        maxRangeLBL.layer.masksToBounds = true
        
        rangeSelector.minValue = PLSearchAttributes.shared.priceMin.CGFloatValue
        rangeSelector.maxValue = PLSearchAttributes.shared.priceMax.CGFloatValue
        rangeSelector.selectedMinValue = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
        rangeSelector.selectedMaxValue = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
        //print("priceMinSetup",PLSearchAttributes.shared.priceMin.CGFloatValue)
        //print("priceMaxSetup",PLSearchAttributes.shared.priceMax.CGFloatValue)

    }
    
    func setUpInterface() {
        
        //resetAll()
       // showAttributesForMenu()
        
        //menu1PriceTapped(UIButton())
        rangeView.isHidden = false
        optionsColorCV.isHidden = true
        topHideViewForColorCV.isHidden = true
        bottomHideViewForColorCV.isHidden = true
        optionsImageCV.isHidden = true

            
        showMenuIndicatorAndMakeWebCall()
        //getSearchData()
    }
    
    private func resetAll(){
        
//        menuType = .price
        let source = PLSearchAttributes.shared
        source.isSelectedPrice = false
        source.selectedPriceMin = PLSearchAttributes.shared.priceMin
        source.selectedPriceMax = PLSearchAttributes.shared.priceMax
        source.isSelectedKilometer = false
        source.selectedKilometerMin = PLSearchAttributes.shared.kilometerMin
        source.selectedKilometerMax = PLSearchAttributes.shared.kilometerMax

        source.colorSource.forEach { $0.isSelected = false }
        source.bodyTypeSource.forEach { $0.isSelected = false }
        source.fuelSource.forEach { $0.isSelected = false }
        source.featuresSource.forEach { $0.isSelected = false }
        source.transmissionSource.forEach { $0.isSelected = false }
        appliedAttributesCount = 0
        source.appliedAttributesCount = appliedAttributesCount
        // hiding all the selection indicators
        priceIndicatorView.isHidden = true
        colorIndicatorView.isHidden = true
        bodyTypeIndicatorView.isHidden = true
        kilometerIndicatorView.isHidden = true
        fuelTypeIndicatorView.isHidden = true
        featuresIndicatorView.isHidden = true
        gearTypeIndicatorView.isHidden = true
        
        sharedFilterSource = nil // this is for clearing global data in filter
        
      /*  // applying alpha to the menu list for the unselected state
        priceMenuView.alpha = menuAlphaSelected
        colorMenuView.alpha = menuAlphaDeselected
        bodyTypeMenuView.alpha = menuAlphaDeselected
        kilometersMenuView.alpha = menuAlphaDeselected
        fuelTypeMenuView.alpha = menuAlphaDeselected
        featuresMenuView.alpha = menuAlphaDeselected
        gearTypeMenuView.alpha = menuAlphaDeselected */

    /*    // options view
        rangeView.isHidden = false
        optionsColorCV.isHidden = true
        optionsImageCV.isHidden = true */
        
        rangeView.isHidden = !rangeView.isHidden
        optionsColorCV.reloadData()
        optionsImageCV.reloadData()
        rangeView.isHidden = !rangeView.isHidden

        parameterForCount.removeAll()
        parameterForSearchFinal.removeAll()
        if menuType == .price {

            rangeSelector.minValue = PLSearchAttributes.shared.priceMin.CGFloatValue
            rangeSelector.maxValue = PLSearchAttributes.shared.priceMax.CGFloatValue
            rangeSelector.selectedMinValue = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
            rangeSelector.selectedMaxValue = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
            minRangeLBL.text = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue.formatInRuppees()
            maxRangeLBL.text = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue.formatInRuppees()

        } else if menuType == .kilometers {
            rangeSelector.minValue = PLSearchAttributes.shared.kilometerMin.CGFloatValue
            rangeSelector.maxValue = PLSearchAttributes.shared.kilometerMax.CGFloatValue
            rangeSelector.selectedMinValue = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue
            rangeSelector.selectedMaxValue = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue
            minRangeLBL.text = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue.formatInKilometer()
            maxRangeLBL.text = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue.formatInKilometer()

        }
        self.view.setNeedsLayout()
        self.rangeSelector.setNeedsLayout()
        getSearchData()
        isResetAll = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        if isResetAll && PLSearchAttributes.shared.isSearchApplied {
            PLSearchAttributes.shared.isSearchApplied = false
            delegate?.appliedSmartSearch(with: Json(), resultCount: resultCountLBL.text ?? "0", isFromShowAll: false)
            dismiss()
        }else {
            dismiss()
        }
        
    }
    
    @IBAction func resetAllTapped(_ sender: UIButton) {
        resetAll()
    }
    
    
    @IBAction func menu1PriceTapped(_ sender: UIButton) {
        menuType = .price
        
        showAttributesForMenu()
        animateMenu(selectedMenu: priceMenuView, selectedLabel: priceMenuLBL, indicatorView: priceIndicatorView, imageView: priceImage)
        
    }
    
    @IBAction func menu2ColorTapped(_ sender: UIButton) {
        menuType = .color
        showAttributesForMenu()
        animateMenu(selectedMenu: colorMenuView, selectedLabel: colorMenuLBL, indicatorView: colorIndicatorView, imageView: colorImage)
        
    }
    
    @IBAction func menu3BodyTypeTapped(_ sender: UIButton) {
        menuType = .bodyType
        showAttributesForMenu()
        animateMenu(selectedMenu: bodyTypeMenuView, selectedLabel: bodyTypeMenuLBL, indicatorView: bodyTypeIndicatorView, imageView: carImage)

    }
    
    @IBAction func menu4KilometerTapped(_ sender: UIButton) {
        menuType = .kilometers
        showAttributesForMenu()
        animateMenu(selectedMenu: kilometersMenuView, selectedLabel: kilometersMenuLBL, indicatorView: kilometerIndicatorView, imageView: kilometerImage)
    }
    
    @IBAction func menu5FuelTypeTapped(_ sender: UIButton) {
        menuType = .fueltype
        showAttributesForMenu()
        animateMenu(selectedMenu: fuelTypeMenuView, selectedLabel: fuelTypeMenuLBL, indicatorView: fuelTypeIndicatorView, imageView: fuelImage)

    }
    
    @IBAction func menu6FeaturesTapped(_ sender: UIButton) {
        menuType = .features
        showAttributesForMenu()
        animateMenu(selectedMenu: featuresMenuView, selectedLabel: FeaturesMenuLBL, indicatorView: featuresIndicatorView, imageView: featureImage)

    }
    @IBAction func menu7GearTypeTapped(_ sender: UIButton) {
        menuType = .transmission
        showAttributesForMenu()
        animateMenu(selectedMenu: gearTypeMenuView, selectedLabel: gearTypeMenuLBL, indicatorView: gearTypeIndicatorView, imageView: trasmissionImage)
        
    }
    
    @IBAction func showAllTapped(_ sender: BaseButton) {
        //parameterForSearch = parameterForSearchFinal
       // print( PLSearchAttributes.shared.appliedAttributesCount,"appliedAttributesCount",appliedAttributesCount)
        guard let resultCount = resultCountLBL.text, Int(resultCount) != 0 else {
            showBanner(message: "No data to show")
            return
        }
        
        PLSearchAttributes.shared.appliedAttributesCount = appliedAttributesCount
        PLSearchAttributes.shared.isSearchApplied = true
        updateSharedFilterWIthSearchData()
        delegate?.appliedSmartSearch(with: parameterForCount, resultCount: Int(resultCount)?.description ?? "0", isFromShowAll: true)
        dismiss()
    }
    
    @inline(__always) func updateSharedFilterWIthSearchData(){
        let colorSource = PLSearchAttributes.shared.colorSource.map({$0.isSelected})
        for (index,item) in colorSource.enumerated(){
            sharedFilterSource?.color[index].isSelected = item
        }
        let fuelSource = PLSearchAttributes.shared.fuelSource.map({$0.isSelected})
        for (index,item) in fuelSource.enumerated(){
            sharedFilterSource?.fuelType[index].isSelected = item
        }
        let categorySource = PLSearchAttributes.shared.bodyTypeSource.map({$0.isSelected})
        for (index,item) in categorySource.enumerated(){
            sharedFilterSource?.category[index].isSelected = item
        }
        let transmissionSource = PLSearchAttributes.shared.transmissionSource.map({$0.isSelected})
        for (index,item) in transmissionSource.enumerated(){
            sharedFilterSource?.transmission[index].isSelected = item
        }
        sharedFilterSource?.price.currentMin = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
        sharedFilterSource?.price.currentMax = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
        sharedFilterSource?.kilometer.currentMin = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue
        sharedFilterSource?.kilometer.currentMax = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue
    }

    
    // This method is used for update the right side i.e, the
    private func showAttributesForMenu(){
        switch self.menuType {
            
        case .price:
            rangeView.isHidden = true

            rangeSelector.minValue = PLSearchAttributes.shared.priceMin.CGFloatValue
            rangeSelector.maxValue = PLSearchAttributes.shared.priceMax.CGFloatValue
            rangeSelector.selectedMinValue = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue
            rangeSelector.selectedMaxValue = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue
            minRangeLBL.text = PLSearchAttributes.shared.selectedPriceMin.CGFloatValue.formatInRuppees()
            maxRangeLBL.text = PLSearchAttributes.shared.selectedPriceMax.CGFloatValue.formatInRuppees()
//            print("Price",PLSearchAttributes.shared.priceMin.CGFloatValue,"maxxx",PLSearchAttributes.shared.priceMax.CGFloatValue)
//        print("selectedPrice",PLSearchAttributes.shared.selectedPriceMin.CGFloatValue,"maxxx",PLSearchAttributes.shared.selectedPriceMax.CGFloatValue)
            rangeView.isHidden = false
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = true
            self.rangeSelector.setNeedsLayout()

        case .color:
            optionsColorCV.reloadData()
            
            rangeView.isHidden = true
            optionsColorCV.isHidden = false
            topHideViewForColorCV.isHidden = false
            bottomHideViewForColorCV.isHidden = false

            optionsImageCV.isHidden = true

        case .bodyType:
            optionsImageCV.reloadData()

            rangeView.isHidden = true
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = false

        case .kilometers:
            rangeView.isHidden = true

            rangeSelector.minValue = PLSearchAttributes.shared.kilometerMin.CGFloatValue
            rangeSelector.maxValue = PLSearchAttributes.shared.kilometerMax.CGFloatValue
            rangeSelector.selectedMinValue = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue
            rangeSelector.selectedMaxValue = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue
            minRangeLBL.text = PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue.formatInKilometer()
            maxRangeLBL.text = PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue.formatInKilometer()
//            print("kilometers",PLSearchAttributes.shared.kilometerMin.CGFloatValue,"maxxx",PLSearchAttributes.shared.kilometerMax.CGFloatValue)
//        print("selectedkilometers",PLSearchAttributes.shared.selectedKilometerMin.CGFloatValue,"maxxx",PLSearchAttributes.shared.selectedKilometerMax.CGFloatValue)

            rangeView.isHidden = false
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = true
            self.rangeSelector.setNeedsLayout()

        case .fueltype:
            optionsImageCV.reloadData()
            
            rangeView.isHidden = true
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = false

        case .features:
            optionsImageCV.reloadData()

            rangeView.isHidden = true
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = false

        case .transmission:
            optionsImageCV.reloadData()

            rangeView.isHidden = true
            optionsColorCV.isHidden = true
            topHideViewForColorCV.isHidden = true
            bottomHideViewForColorCV.isHidden = true

            optionsImageCV.isHidden = false

        case .none:
            break
           // print("None Case")
        }

    }
    
    // this is used for showing red dot for the selected menu attributes and instant web call for showing car count
    private func showMenuIndicatorAndMakeWebCall() {
        let source = PLSearchAttributes.shared
        if source.isSelectedPrice {
            priceIndicatorView.isHidden = false
            let param = [source.selectedPriceMin,source.selectedPriceMax]
            parameterForCount[parameterKeys.price.rawValue] = param

        }else {
            priceIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.price.rawValue)
        }
        
        if source.isSelectedKilometer {
            kilometerIndicatorView.isHidden = false
            let param = [source.selectedKilometerMin,source.selectedKilometerMax]
            parameterForCount[parameterKeys.kilometers.rawValue] = param

        }else {
            kilometerIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.kilometers.rawValue)

        }
        
        let selectedColors = source.colorSource.filter { $0.isSelected == true }
        if !selectedColors.isEmpty {
            let param = selectedColors.map { $0.name }
            parameterForCount[parameterKeys.colors.rawValue] = param
            colorIndicatorView.isHidden = false

        }else{
            colorIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.colors.rawValue)

        }
        
        let selectedBodyTypes = source.bodyTypeSource.filter { $0.isSelected == true }
        if !selectedBodyTypes.isEmpty {
            let param = selectedBodyTypes.map { $0.id }
            parameterForCount[parameterKeys.categories.rawValue] = param
            bodyTypeIndicatorView.isHidden = false

        }else{
            bodyTypeIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.categories.rawValue)

        }
        
        let selectedFuelTypes = source.fuelSource.filter { $0.isSelected == true }
        if !selectedFuelTypes.isEmpty {
            let param = selectedFuelTypes.map { $0.id }
            parameterForCount[parameterKeys.fueltypes.rawValue] = param
            fuelTypeIndicatorView.isHidden = false

        }else {
            fuelTypeIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.fueltypes.rawValue)

        }
        
        let selectedFeatures = source.featuresSource.filter { $0.isSelected == true }
        if !selectedFeatures.isEmpty {
            let param = selectedFeatures.map { $0.id }
            parameterForCount[parameterKeys.features.rawValue] = param
            featuresIndicatorView.isHidden = false

        }else {
            featuresIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.features.rawValue)

        }
        
        let selectedTransmissions = source.transmissionSource.filter { $0.isSelected == true }
        if !selectedTransmissions.isEmpty {
            let param = selectedTransmissions.map { $0.id }
            parameterForCount[parameterKeys.transmissions.rawValue] = param
            gearTypeIndicatorView.isHidden = false
        }else {
            gearTypeIndicatorView.isHidden = true
            parameterForCount.removeValue(forKey: parameterKeys.transmissions.rawValue)

        }
        //print("appliedAttributesCount:",appliedAttributesCount)

        appliedAttributesCount = parameterForCount.keys.count
        if parameterForCount.keys.contains(parameterKeys.features.rawValue) {
            appliedAttributesCount = parameterForCount.keys.count
            appliedAttributesCount -= 1
        }
        
//        if appliedAttributesCount > 0 {
//            getSearchData()
//        }
        
        getSearchData()

        
        
    }
    
}


// animating functions
extension PLSmartSearchVC {
    
    private func animateMenu(selectedMenu: UIView, selectedLabel: UILabel ,indicatorView: UIView, imageView: UIImageView) {
       // print(selectedMenu.frame,"SelectedMenuFrame")
        if previousMenuView == nil {
            // first time loading
            selectionAnimation(animateView: selectedMenu, label: selectedLabel, indicatorView: indicatorView, imageView: imageView, completion: {
                self.previousMenuView = selectedMenu
                self.prevoiusMenuLabel = selectedLabel
            })
        }else if selectedMenu == previousMenuView {
            /*
            // tapping already selected menu again
            deselectionAnimation(animateView: selectedMenu, label: selectedLabel, completion: {
                self.previousMenuView = nil
                self.prevoiusMenuLabel = nil
            })
            */
        }else {
            // tapping menu
            selectionAnimation(animateView: selectedMenu, label: selectedLabel, indicatorView: indicatorView, imageView: imageView, completion: {
                
            })
            deselectionAnimation(animateView: previousMenuView, label: prevoiusMenuLabel, indicatorView: indicatorView, imageView: imageView, completion: {
                self.previousMenuView = selectedMenu
                self.prevoiusMenuLabel = selectedLabel
            })
            
        }
        
    }
    
    private func selectionAnimation(animateView: UIView, label: UILabel, indicatorView: UIView, imageView: UIImageView, completion: @escaping(() -> ())) {
        
        //view zoom in animation
        let easingOffset: CGFloat = 0.2
        let duration: TimeInterval = 0.13
        let easeScale: CGFloat = 1.0 + easingOffset
        
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        animateView.layer.zPosition = 1
        let transformScale = CGAffineTransform(scaleX: 1.05, y: 1.19)
        UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            animateView.frame.origin.x += 20//self.menuViewDisplacement
            animateView.alpha = self.menuAlphaSelected
            animateView.transform = transformScale
        }, completion: { (completed: Bool) -> Void in
            label.transform = transformScale.inverted()
           // indicatorView.transform = transformScale.inverted()
            indicatorView.transform = .identity
            imageView.transform = .identity

            completion()
        })

        // label animation
        UIView.animate(withDuration: 0.1) {
            label.alpha = 1.0
            //label.isHidden = false
            // self.imageMenu.frame.origin.y += 5
        }
        label.transform = transformScale.inverted()
        indicatorView.transform = .identity
        imageView.transform = transformScale.inverted()

    }
    
    private func deselectionAnimation(animateView: UIView?, label: UILabel?, indicatorView: UIView,imageView: UIImageView, completion: @escaping(() -> ())){
        guard let animateView = animateView, let label = label else { return }
        // view zoom out animation
        let easeScale: CGFloat = 1.0
        animateView.layer.zPosition = 0
        UIView.animate(withDuration: 0.13, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            animateView.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
            animateView.frame.origin.x = 0// -= 20//self.menuViewDisplacement
            animateView.alpha = self.menuAlphaDeselected

        }, completion: { (completed: Bool) -> Void in
           indicatorView.transform = .identity
            imageView.transform = .identity
            completion()
        })

        // label animation
        UIView.animate(withDuration: 0.1) {
            label.alpha = 0.0
            //label.isHidden = true
            // self.imageMenu.frame.origin.y -= 5
        }
        indicatorView.transform = .identity
    }


}

//Mark:- Price range and kilometer range selector
extension PLSmartSearchVC: RangeSeekSliderDelegate {

    func labelPositionsOfRangeSeekSlider(minLabelPoint: CGPoint, maxLabelPoint: CGPoint) {
        //print("minLBLpoint:",minLabelPoint,"maxLabelPoint:",maxLabelPoint)
        
        minLBLPositionX = minLabelPoint.x
        maxLBLPositionX = maxLabelPoint.x
        minLBLPositionY = minLabelPoint.y
        maxLBLPositionY = maxLabelPoint.y
        
        if detectPhoneModel() == .model_X {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2
            minRangeLBL.frame.origin.y = currentMinPositionY + 58 // plus value move x towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 52
        }else if detectPhoneModel() == .model_6P_6sP_7P_8P {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2
            minRangeLBL.frame.origin.y = currentMinPositionY + 59 // adding the value move towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX - 2
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 53
        }else if detectPhoneModel() == .model_6_6s_7_8 {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2
            minRangeLBL.frame.origin.y = currentMinPositionY + 58 // plus value move x towards left
            
            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 52
        }else {
            minRangeLBL.frame.origin.x = currentMinPositionX - 2 // minus value upwards changing y
            minRangeLBL.frame.origin.y = currentMinPositionY + 50

            maxRangeLBL.frame.origin.x = currentMaxPositionX + 6
            maxRangeLBL.frame.origin.y = currentMaxPositionY - 48 // minus value move right changing x
        }
        
    
    }
    
    
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {

        if menuType == .price {
            
            let min = PLSearchAttributes.shared.priceMin
            let max = PLSearchAttributes.shared.priceMax
            
            if min.CGFloatValue == minValue {
                minRangeLBL.text = minValue.formatInRuppees()
                PLSearchAttributes.shared.selectedPriceMin = minValue.int64Value
            }else {
                minRangeLBL.text = minValue.roundedToTenThousand.formatInRuppees()
                PLSearchAttributes.shared.selectedPriceMin = minValue.roundedToTenThousand.int64Value

            }
            
            if max.CGFloatValue == maxValue {
                maxRangeLBL.text = maxValue.formatInRuppees()
                PLSearchAttributes.shared.selectedPriceMax = maxValue.int64Value
            }else {
                maxRangeLBL.text = maxValue.roundedToTenThousand.formatInRuppees()
                PLSearchAttributes.shared.selectedPriceMax = maxValue.roundedToTenThousand.int64Value
                
            }
            
            if minValue.int64Value == PLSearchAttributes.shared.priceMin && maxValue.int64Value == PLSearchAttributes.shared.priceMax {
                //print("Selected-False")
                PLSearchAttributes.shared.isSelectedPrice = false

            }else {
                //print("Selected-True")
                PLSearchAttributes.shared.isSelectedPrice = true

            }
        }else if menuType == .kilometers {
            
            let min = PLSearchAttributes.shared.kilometerMin
            let max = PLSearchAttributes.shared.kilometerMax
            
            if min.CGFloatValue == minValue {
                minRangeLBL.text = minValue.formatInKilometer()
                PLSearchAttributes.shared.selectedKilometerMin = minValue.intValue
            }else {
                minRangeLBL.text = minValue.roundedToFiveThousand.formatInKilometer()
                PLSearchAttributes.shared.selectedKilometerMin = minValue.roundedToFiveThousand.intValue
                
            }
            
            if max.CGFloatValue == maxValue {
                maxRangeLBL.text = maxValue.formatInKilometer()
                PLSearchAttributes.shared.selectedKilometerMax = maxValue.intValue
            }else {
                maxRangeLBL.text = maxValue.roundedToFiveThousand.formatInKilometer()
                PLSearchAttributes.shared.selectedKilometerMax = maxValue.roundedToFiveThousand.intValue
                
            }
            
            if minValue.int64Value == PLSearchAttributes.shared.kilometerMin && maxValue.int64Value == PLSearchAttributes.shared.kilometerMax {
                //print("Kilometer-False")
                PLSearchAttributes.shared.isSelectedKilometer = false
            }else {
                //print("Kilometer-True")
                PLSearchAttributes.shared.isSelectedKilometer = true
            }

        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        // print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
       // print("did end touches")
        
        showMenuIndicatorAndMakeWebCall()

    }
    
}


//Handling collection view data source and delegates
extension PLSmartSearchVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == optionsColorCV {
            //print("PLSearchAttributes.shared.colorSource.count",PLSearchAttributes.shared.colorSource.count)
            let sourceCount = PLSearchAttributes.shared.colorSource.count
            return sourceCount
        }else {
            var sourceCount: Int = 0
            if menuType == .bodyType {
                sourceCount = PLSearchAttributes.shared.bodyTypeSource.count
//                print("SourceCount:Body",sourceCount)
            }else if menuType == .features {
                sourceCount = PLSearchAttributes.shared.featuresSource.count
//                print("SourceCount:features",sourceCount)
            }else if menuType == .fueltype {
                sourceCount = PLSearchAttributes.shared.fuelSource.count
//                print("SourceCount:Fuel",sourceCount)
            }else if menuType == .transmission {
                sourceCount = PLSearchAttributes.shared.transmissionSource.count
//                print("SourceCount:trasmission",sourceCount)
            }
            return sourceCount
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == optionsColorCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLSmartSearchColorCVC", for: indexPath) as! PLSmartSearchColorCVC
            let source = PLSearchAttributes.shared.colorSource[indexPath.item]
            let hexColor = source.hexcode
            //print("sourceColor",source.hexcode)
            cell.colorView.backgroundColor = UIColor.from(hex: hexColor)
            if source.isSelected {
                cell.layer.zPosition = 1
                cell.selectionView.shadowColor = .lightGray
                cell.selectionImage.isHidden = false
            }else {
                cell.layer.zPosition = 0
                cell.selectionView.shadowColor = .clear
                cell.selectionImage.isHidden = true
            }

            return cell

        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLSmartSearchImageCVC", for: indexPath) as! PLSmartSearchImageCVC
            
            if menuType == .bodyType { //category
                let source = PLSearchAttributes.shared.bodyTypeSource[indexPath.item]
                cell.optionTitleLBL.text = source.name
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    cell.optionTitleLBL.alpha = source.selectedAlpha
                }else{
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionTitleLBL.alpha = source.unselectedAlpha
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)

                }
            }else if menuType == .features {
                let source = PLSearchAttributes.shared.featuresSource[indexPath.item]
                cell.optionTitleLBL.text = source.name
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    
                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else{
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionTitleLBL.alpha = source.unselectedAlpha
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)

                }
            }else if menuType == .fueltype {
                let source = PLSearchAttributes.shared.fuelSource[indexPath.item]
                cell.optionTitleLBL.text = source.name
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark

                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else{
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionTitleLBL.alpha = source.unselectedAlpha
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)

                }
            }else if menuType == .transmission {
                let source = PLSearchAttributes.shared.transmissionSource[indexPath.item]
                cell.optionTitleLBL.text = source.name
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark

                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else{
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)
                    cell.optionTitleLBL.alpha = source.unselectedAlpha

                }

            }
            
            return cell

        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.cellForItem(at: indexPath)?.isSelected = false
        if collectionView == optionsColorCV {
            let cell = collectionView.cellForItem(at: indexPath) as! PLSmartSearchColorCVC
            let source = PLSearchAttributes.shared.colorSource[indexPath.item]
            source.isSelected = !source.isSelected
            
            if source.isSelected {
                cell.layer.zPosition = 1
                cell.selectionView.shadowColor = .lightGray
                cell.selectionImage.isHidden = false
            }else {
                cell.layer.zPosition = 0
                cell.selectionView.shadowColor = .clear
                cell.selectionImage.isHidden = true

            }
            showMenuIndicatorAndMakeWebCall()
        }else {
            let cell = collectionView.cellForItem(at: indexPath) as! PLSmartSearchImageCVC

            if menuType == .bodyType {
                let source = PLSearchAttributes.shared.bodyTypeSource[indexPath.item]
                source.isSelected = !source.isSelected
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else {
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)
                    cell.optionTitleLBL.alpha = source.unselectedAlpha

                }
            }else if menuType == .features {
                let source = PLSearchAttributes.shared.featuresSource[indexPath.item]
                source.isSelected = !source.isSelected
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else {
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)
                    cell.optionTitleLBL.alpha = source.unselectedAlpha

                }

            }else if menuType == .fueltype {
                let source = PLSearchAttributes.shared.fuelSource[indexPath.item]
                source.isSelected = !source.isSelected
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else {
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)
                    cell.optionTitleLBL.alpha = source.unselectedAlpha

                }

            }else if menuType == .transmission {
                let source = PLSearchAttributes.shared.transmissionSource[indexPath.item]
                source.isSelected = !source.isSelected
                if source.isSelected {
                    cell.optionIconImage.image = source.selectedImage
                    cell.optionIconImage.borderColor = .themeGreenDark
                    cell.optionTitleLBL.alpha = source.selectedAlpha

                }else {
                    cell.optionIconImage.image = source.unselectedImage
                    cell.optionIconImage.borderColor = UIColor.black.withAlphaComponent(0.1)
                    cell.optionTitleLBL.alpha = source.unselectedAlpha

                }

            }

            showMenuIndicatorAndMakeWebCall()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == optionsColorCV {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount = 3
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = (collectionView.bounds.width - totalSpace) / CGFloat(cellsCount)
            //print("Color=====cellSizeFloat",size)
            
            return CGSize(width: size, height: size)

        }else {
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellsCount: CGFloat = 2
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsCount - 1))
            let size = (collectionView.bounds.width - totalSpace) / cellsCount
            let height = menuType == .features ? size * 1.21 : size
            //print("cellSize:",size,"Height:",height)
            return CGSize(width: size, height: height)

        }
        
    }
    

    
}

// Handling web service
extension PLSmartSearchVC {
    private func getFilterData(){
        let url = "getFilterdetails"
        WebServices.getMethodWith(url: url, parameter: nil) { (isSucceeded, response) in
            
            guard isSucceeded, let responseData = response["data"] as? Json, !responseData.isEmpty else {
                BaseThread.asyncMain {
                    self.dismiss()
                }
                return
            }
            
            PLSearchAttributes.shared.updateWithWebData(attributes: responseData)
            
            
        }
        
    }
    
    private func getSearchData(){
        // {"price":[100,1000000],"kilometers":[], "colors":[],"categories":[],"fueltypes":[], "transmissions":[], "features":[1,5] }
        let url = "smartsearch"
        
        guard let json = try? JSONSerialization.data(withJSONObject: parameterForCount, options: []) else { return }
        guard let content = String(data: json, encoding: String.Encoding.utf8) else { return }
        parameterForSearchFinal["filter"] = content

        //print("SearchCount_URL:",url)
        print("parameterForSearchFinal",parameterForSearchFinal)
        WebServices.postMethodWithoutActivity(url: url, parameter: parameterForSearchFinal) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let data = response["data"] as? Json, !data.isEmpty else { return }
            BaseThread.asyncMain {
                if let carCount = data["carscount"] as? Int {
                    if carCount < 10 {
                        self.resultCountLBL.text = carCount.leadingZero
                    }else {
                        self.resultCountLBL.text = carCount.description
                    }
                }else{
                    self.resultCountLBL.text = "00"
                }
            }
        }
        
    }
    
    
}

/*
 
 ////
 
 
 //        if (PLSearchAttributes.shared.transmissionSource.map({ $0.isSelected })).contains(true) {
 //            gearTypeIndicatorView.isHidden = false
 //        }else {
 //            gearTypeIndicatorView.isHidden = true
 //
 //        }

 func paramGenerator(){
 let source = PLSearchAttributes.shared
 if source.isSelectedPrice {
 let param = [source.selectedPriceMin,source.selectedPriceMax]
 parameterForCount["price"] = param
 }
 
 if PLSearchAttributes.shared.isSelectedKilometer {
 let param = [source.selectedKilometerMin,source.selectedKilometerMax]
 parameterForCount["kilometers"] = param
 
 }
 
 let selectedColors = source.colorSource.filter { $0.isSelected == true }
 if !selectedColors.isEmpty {
 let param = selectedColors.map { $0.name }
 parameterForCount["colors"] = param
 
 }
 
 let selectedBodyTypes = source.bodyTypeSource.filter { $0.isSelected == true }
 if !selectedBodyTypes.isEmpty {
 let param = selectedBodyTypes.map { $0.id }
 parameterForCount["categories"] = param
 
 }
 
 let selectedFuelTypes = source.fuelSource.filter { $0.isSelected == true }
 if !selectedFuelTypes.isEmpty {
 let param = selectedFuelTypes.map { $0.id }
 parameterForCount["fueltypes"] = param
 
 }
 
 let selectedFeatures = source.featuresSource.filter { $0.isSelected == true }
 if !selectedFeatures.isEmpty {
 let param = selectedFeatures.map { $0.id }
 parameterForCount["features"] = param
 
 }
 
 let selectedTransmissions = source.transmissionSource.filter { $0.isSelected == true }
 if !selectedTransmissions.isEmpty {
 let param = selectedTransmissions.map { $0.id }
 parameterForCount["transmissions"] = param
 
 }

 }
 private func zoomInView(animateview: UIView, duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
 let easeScale = 1.0 + easingOffset
 let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
 let scalingDuration = duration - easingDuration
 animateview.layer.zPosition = 1
 UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
 animateview.frame.origin.x += 20
 animateview.transform = CGAffineTransform(scaleX: 1.03, y: 1.18)
 }, completion: { (completed: Bool) -> Void in
 })
 }
 
 private func zoomOutView(animateView: UIView, duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.0) {
 let easeScale = 1.0 + easingOffset
 animateView.layer.zPosition = 0
 UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
 animateView.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
 animateView.frame.origin.x -= 20
 }, completion: { (completed: Bool) -> Void in
 })
 }
 if menuType == .bodyType {
 
 }else if menuType == .kilometers {
 
 }else if menuType == .features {
 
 }else if menuType == .fueltype {
 
 }else if menuType == .transmission {
 
 }
 */
