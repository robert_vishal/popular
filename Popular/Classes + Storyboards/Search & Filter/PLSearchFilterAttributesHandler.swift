//
//  PLSearchFilterAttributesHandler.swift
//  Popular
//
//  Created by Appzoc on 27/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

enum PLSFTypes: Int {
    case price
    case color
    case bodyType
    case kilometers
    case fueltype
    case features
    case transmission
    case none
}
// {"brands":[],"models":[],"locations":[], "fueltypes":[],"transmissions":[],"categories":[], "listtypes":[],"price":[1000,500000],"year":[],"kilometers":[], "ownership":["2+"], "colors":[], "features":[1,5] } (optional)


enum parameterKeys: String {
    case price
    case colors
    case categories
    case kilometers
    case fueltypes
    case features
    case transmissions
    case brand = "brands"
    case models
    case locations
    case listtypes
    case year
    case ownership
    
}

// search sources
class PLSearchAttributes {
    
// source data for cells
//    var type: PLSFTypes = .none
    
    var priceMin: Int64 = 0
    var selectedPriceMin: Int64 = 0
    var priceMax: Int64 = 0
    var selectedPriceMax: Int64 = 0
    var isSelectedPrice: Bool = false // this is for showing red dot if it is selected
    
    var colorSource: [PLSFIdNameAttribute] = []
    var bodyTypeSource: [PLSFIdNameAttribute] = []
    
    var kilometerMin: Int = 0
    var selectedKilometerMin: Int = 0
    var kilometerMax: Int = 0
    var selectedKilometerMax: Int = 0
    var isSelectedKilometer: Bool = false
    
    var fuelSource: [PLSFIdNameAttribute] = []

    var featuresSource: [PLSFIdNameAttribute] = []

    var transmissionSource: [PLSFIdNameAttribute] = []

    static let shared = PLSearchAttributes()
    
    //
    var appliedAttributesCount: Int = 0
    
    // below variable is used to find if showall button pressed or smartsearch applied to the used carlist. if so, this is used to reload the used car list screen if the user resetall and press back button inorder to make the UI change//******
    var isSearchApplied: Bool = false
    
    
    private init() {
        
    }
    
    func updateWithWebData(attributes: Json) {
        isSearchApplied = false
        // get price details
        if let data = attributes["price"] as? Json {
            priceMin = data["min"] as? Int64 ?? 0 //Int64(data["min"] as? String ?? "0")!
            priceMax = data["max"] as? Int64 ?? 0
            selectedPriceMin = priceMin
            selectedPriceMax = priceMax
            isSelectedPrice = false

           // print("priceData",priceMin,"Max",priceMax)
           // print("SelectedpriceData",selectedPriceMin,"Max",selectedPriceMax)

        }
        
        if let data = attributes["color"] as? JsonArray, !data.isEmpty {
            colorSource = PLSFIdNameAttribute.getValues(fromArray: data, type: .color)
            //print("ColorSource",colorSource)
        }

        if let data = attributes["category"] as? JsonArray, !data.isEmpty {
            bodyTypeSource = PLSFIdNameAttribute.getValues(fromArray: data, type: .bodyType).sorted(by: { $0.order < $1.order })
            
        }
        
        if let data = attributes["kilometer"] as? Json {
            kilometerMin = data["min"] as? Int ?? 0
            kilometerMax = data["max"] as? Int ?? 0
            selectedKilometerMin = kilometerMin
            selectedKilometerMax = kilometerMax
            isSelectedKilometer = false

            //print("kilometer",kilometerMin,"Max",kilometerMax)
        }
        
        if let data = attributes["fuel_type"] as? JsonArray, !data.isEmpty {
            fuelSource = PLSFIdNameAttribute.getValues(fromArray: data, type: .fueltype)
            //print("fuelSource",fuelSource)
        }

        if let data = attributes["feature"] as? JsonArray, !data.isEmpty {
            featuresSource = PLSFIdNameAttribute.getValues(fromArray: data, type: .features).sorted(by: { $0.order < $1.order })
            
        }

        if let data = attributes["transmission"] as? JsonArray, !data.isEmpty {
            transmissionSource = PLSFIdNameAttribute.getValues(fromArray: data, type: .transmission)
            //print("transmission",transmissionSource)
        }
    }
}



class PLSFIdNameAttribute {
    
    var id: Int = 0
    var name: String = ""
    var hexcode: String = "" // this is for color cell
    var selectedImage: UIImage?
    var unselectedImage: UIImage?
    
    // Display properties
    var isSelected: Bool = false
    var selectedAlpha: CGFloat = 0.57
    var unselectedAlpha: CGFloat = 0.20
    var order: Int = 0
    
    class func getValues(fromArray: JsonArray, type: PLSFTypes) -> [PLSFIdNameAttribute]{
        var modelArray: [PLSFIdNameAttribute] = []
        for item in fromArray {
           // print("Itme-",item)
            let model: PLSFIdNameAttribute = PLSFIdNameAttribute()
            switch type {
            case .color:
                model.id = item["color_id"] as? Int ?? 0 //Int(item["color_id"] as? String ?? "0")!
                model.name = item["color"] as? String ?? ""
                model.hexcode = item["hexcode"] as? String ?? ""
                model.isSelected = false
                //print("hexCodeeee",model.hexcode)
            case .price:
                break
            case .bodyType:
                model.id = item["id"] as? Int ?? 0
                model.name = item["name"] as? String ?? ""
                model.isSelected = false
                
                if model.id == 9 {          // HATCHBACK
                    model.unselectedImage = #imageLiteral(resourceName: "hatchback-car-variant-side-view-silhouette")
                    model.selectedImage = #imageLiteral(resourceName: "hatch (1)")
                    model.order = 1
                }else if model.id == 10 {               // MUV
                    model.unselectedImage = #imageLiteral(resourceName: "muv")
                    model.selectedImage = #imageLiteral(resourceName: "muv (1)")
                    model.order = 2
                }else if model.id == 4 {          // SUV
                    model.unselectedImage = #imageLiteral(resourceName: "suv")
                    model.selectedImage = #imageLiteral(resourceName: "suv (1)")
                    model.order = 3

                }else if model.id == 8 {          //SEDAN
                    model.unselectedImage = #imageLiteral(resourceName: "sedan")
                    model.selectedImage = #imageLiteral(resourceName: "sedan (1)")
                    model.order = 4

                }else if model.id == 5 {          //VAN
                    model.unselectedImage = #imageLiteral(resourceName: "van")
                    model.selectedImage = #imageLiteral(resourceName: "van (1)")
                    model.order = 5

                }

            case .kilometers:
                break
            case .fueltype:
                model.id = item["id"] as? Int ?? 0
                model.name = item["name"] as? String ?? ""
                model.isSelected = false

                if model.id == 3 {                // Diesel
                    model.unselectedImage = #imageLiteral(resourceName: "Subtraction 18")
                    model.selectedImage = #imageLiteral(resourceName: "Subtraction 24")
                }else if model.id == 6 {          // Petrol
                    model.unselectedImage = #imageLiteral(resourceName: "Subtraction 17")
                    model.selectedImage = #imageLiteral(resourceName: "Subtraction 21")
                }else {
                    
                }

            case .features:
                model.id = item["id"] as? Int ?? 0
                model.name = item["name"] as? String ?? ""
                model.isSelected = false

                if model.id == 1 {                  // AC
                    model.unselectedImage = #imageLiteral(resourceName: "ice-crystal")
                    model.selectedImage = #imageLiteral(resourceName: "ice-crystal color")
                    model.order = 1

                }else if model.id == 31 {          //Reverse Sensor
                    model.unselectedImage = #imageLiteral(resourceName: "Group 970")
                    model.selectedImage = #imageLiteral(resourceName: "Group 971")
                    model.order = 2

                }else if model.id == 5 {           // Navigation Console
                    model.unselectedImage = #imageLiteral(resourceName: "compass (1)")
                    model.selectedImage = #imageLiteral(resourceName: "compass (1) color")
                    model.order = 3

                }else if model.id == 18 {          //Music System
                    model.unselectedImage = #imageLiteral(resourceName: "sound-system (1)")
                    model.selectedImage = #imageLiteral(resourceName: "sound-system (1) color")
                    model.order = 4

                }else if model.id == 33 {          // Alloy Wheel
                    model.unselectedImage = #imageLiteral(resourceName: "alloy-wheel (1)")
                    model.selectedImage = #imageLiteral(resourceName: "alloy-wheel Color(1)")
                    model.order = 5

                }else {
                    
                }

            case .transmission:
                model.id = item["id"] as? Int ?? 0
                model.name = item["transmission"] as? String ?? ""
                model.isSelected = false
                if model.id == 1 {                // Automatic
                    model.unselectedImage = #imageLiteral(resourceName: "Group 978")
                    model.selectedImage = #imageLiteral(resourceName: "Group 979")
                }else if model.id == 2 {          // Manual
                    model.unselectedImage = #imageLiteral(resourceName: "Path 391")
                    model.selectedImage = #imageLiteral(resourceName: "Path 392")
                }else {
                    
                }

            case .none:
                break
            }
            
            modelArray.append(model)
        }
        return modelArray
    }
    
}





