//
//  PLFilterVC+DataModels.swift
//  Popular
//
//  Created by Appzoc on 08/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

protocol UpdateFromFilter{
    func updateFromFilter()
    func clearFromFilter()
    func forceViewWillAppear()
    func resumeFromHalt()
    func clearTempData()
}

protocol FilterMainDataContainer{
    var mainSourceData:FilterParserModel?{get set}
    var menuDataSource:MenuTableSource{get set}
    var lisType:CurrentListType{get set}
    func reloadMenuTable()
}

enum CurrentListType:Int{
    case brand = 0
    //case model = 0
    case location
    case exteriorColor
    case transmission
    case fuelType
    case category
    case feature
    case price
    case year
    case kilometers
    case ownership
}

struct FilterParserModel: Codable{
    
    var brand:[NameIDParser]
    var carModel:[ModelIDParser]?
    var location:[NameIDParser]
    var color:[ColorModel]
    var fuelType:[NameIDParser]
    var transmission:[TransmissionParser]
    var feature:[StaticNameIDParser] = []
    var category:[NameIDParser]
    var price:MinMaxParser
    var kilometer:MinMaxParser
    var year:MinMaxParser
    var ownership:[String]
    var selectedOwnerShip:[OwnerShipModel] = []
    
    private enum CodingKeys: String, CodingKey{
        case brand, carModel = "carmodel", location, color, fuelType = "fuel_type", transmission, category, price, kilometer, year, ownership
    }
    
    struct NameIDParser: Codable {
        let name:String
        let id:Int
        var isSelected:Bool = false
        var order: Int = 0
        
        private enum CodingKeys: String, CodingKey{
            case name, id
        }
    }
    
    struct StaticNameIDParser{
        let name:String
        let id:Int
        var isSelected:Bool = false
    }
    
    struct ModelIDParser: Codable{
        let modelid:Int
        let model:String
        var isSelected:Bool = false
        
        private enum CodingKeys: String, CodingKey{
            case modelid, model
        }
    }
    
    struct ColorModel: Codable{
        let colorID:Int
        let color:String
        let hexcode:String
        var isSelected:Bool = false
        
        private enum CodingKeys: String, CodingKey{
            case colorID = "color_id", color , hexcode
        }
    }
    
    struct TransmissionParser: Codable{
        let id:Int
        let transmission:String
        var isSelected:Bool = false
        
        private enum CodingKeys: String, CodingKey{
            case id, transmission
        }
    }
    
    struct MinMaxParser: Codable{
        let min:Int
        let max:Int
        var currentMin:CGFloat = 0
        var currentMax:CGFloat = -110
        
        private enum CodingKeys: String, CodingKey{
            case min, max
        }
    }
    
    struct OwnerShipModel{
        var value:String
        var isSelected:Bool
        init(data:String) {
            value = data
            isSelected = false
        }
    }
}

struct MenuTableSource{
    
    var item:[MenuModel]
    
    init(){
        var tempModel:[MenuModel] = []
        let brand = MenuModel(mainLabel: "Brand", isIndicatorVisible: false)
      //  var model = MenuModel(mainLabel: "Model", isIndicatorVisible: false)
        let location = MenuModel(mainLabel: "Location", isIndicatorVisible: false)
        let listType = MenuModel(mainLabel: "List Type", isIndicatorVisible: false)
        let year = MenuModel(mainLabel: "Year", isIndicatorVisible: false)
        let ownership = MenuModel(mainLabel: "Ownership", isIndicatorVisible: false)
        
        var color = MenuModel(mainLabel: "Exterior Color", isIndicatorVisible: false)
        let fuelType = MenuModel(mainLabel: "Fuel Type", isIndicatorVisible: false)
        var category = MenuModel(mainLabel: "Category", isIndicatorVisible: false)
        var price = MenuModel(mainLabel: "Price", isIndicatorVisible: false)
        var kilometers = MenuModel(mainLabel: "Kilometers", isIndicatorVisible: false)
        var transmission = MenuModel(mainLabel: "Transmission", isIndicatorVisible: false)
        
        if PLSearchAttributes.shared.colorSource.filter({$0.isSelected == true}).count > 0{
            color.isIndicatorVisible = true
        }
        if PLSearchAttributes.shared.fuelSource.filter({$0.isSelected == true}).count > 0{
            color.isIndicatorVisible = true
        }
        if PLSearchAttributes.shared.bodyTypeSource.filter({$0.isSelected == true}).count > 0{
            category.isIndicatorVisible = true
        }
        if PLSearchAttributes.shared.transmissionSource.filter({$0.isSelected == true}).count > 0{
            transmission.isIndicatorVisible = true
        }
        price.isIndicatorVisible = PLSearchAttributes.shared.isSelectedPrice
        kilometers.isIndicatorVisible = PLSearchAttributes.shared.isSelectedKilometer
        
        tempModel = [brand,location,color,transmission,fuelType,category,listType,price,year,kilometers,ownership]
        self.item = tempModel
    }
    
    mutating func setIndicator(type:CurrentListType, isVisible:Bool){
        switch type {
        case .brand:
            item[0].isIndicatorVisible = isVisible
//        case .model:
//            item[0].isIndicatorVisible = isVisible
        case .location:
            item[1].isIndicatorVisible = isVisible
        case .exteriorColor:
            item[2].isIndicatorVisible = isVisible
        case .transmission:
            item[3].isIndicatorVisible = isVisible
        case .fuelType:
            item[4].isIndicatorVisible = isVisible
        case .category:
            item[5].isIndicatorVisible = isVisible
        case .feature:
            item[6].isIndicatorVisible = isVisible
        case .price:
            item[7].isIndicatorVisible = isVisible
        case .year:
            item[8].isIndicatorVisible = isVisible
        case .kilometers:
            item[9].isIndicatorVisible = isVisible
        case .ownership:
            item[10].isIndicatorVisible = isVisible
        }
    }
    
    struct MenuModel{
        var mainLabel:String
        var isIndicatorVisible:Bool
    }
    
}

/*  Backup-Original
 
 struct FilterParserModel: Codable{
 
 let brand:[NameIDParser]
 let carModel:[ModelIDParser]
 let location:[NameIDParser]
 let color:[ColorModel]
 let fuelType:[NameIDParser]
 let transmission:[TransmissionParser]
 let feature:[NameIDParser]
 let category:[NameIDParser]
 let price:[MinMaxParser]
 let kilometer:[MinMaxParser]
 let year:[MinMaxParser]
 let ownership:[String]
 
 private enum CodingKeys: String, CodingKey{
 case brand, carModel = "carmodel", location, color, fuelType = "fuel_type", transmission, feature, category, price, kilometer, year, ownership
 }
 
 struct NameIDParser: Codable{
 let name:String
 let id:Int
 }
 
 struct ModelIDParser: Codable{
 let modelid:String
 let model:String
 }
 
 struct ColorModel: Codable{
 let colorID:String
 let color:String
 let hexcode:String
 
 private enum CodingKeys: String, CodingKey{
 case colorID = "color_id", color , hexcode
 }
 }
 
 struct TransmissionParser: Codable{
 let id:Int
 let transmission:String
 }
 
 struct MinMaxParser: Codable{
 let min:Int
 let max:Int
 }
 }
 
 
 
 
 */
