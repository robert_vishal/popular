//
//  PLFilterVC.swift
//  Popular
//
//  Created by Appzoc on 07/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import RangeSeekSlider

var sharedFilterSource:FilterParserModel? = nil

class PLFilterVC: UIViewController, FilterMainDataContainer {
    
    @IBOutlet var menuTV: UITableView!//Leftmenu
    @IBOutlet var listTV: UITableView!
    
    //Passing Data
    var filterJsonData:[String:Any] = [:]
    var delegate:UpdateFromFilter?
    
    //Class Data
    var mainSourceData:FilterParserModel?
    var menuDataSource:MenuTableSource = MenuTableSource()
    var lisType:CurrentListType = .brand
    
    var leftCurrentSelectedIndex:IndexPath?
    var rightCurrentSelectedIndex:IndexPath = IndexPath(row: 0, section: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        leftCurrentSelectedIndex = IndexPath(row: 0, section: 0)
        listTV.register(UINib(nibName: "VerticalSlider", bundle: nil), forCellReuseIdentifier: "VerticalSlider")
        menuTV.tableFooterView = UIView(frame: CGRect.zero)
        listTV.tableFooterView = UIView(frame: CGRect.zero)
        //tempSetUpWebCall()
        //menuTV.reloadData()
        //menuTV.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        //menuTV.delegate?.tableView!(menuTV, didSelectRowAt: IndexPath(row: 0, section: 0))
        //print("Filter View Loaded")
        delegate?.clearTempData()
        parsePassedJSON()
    }
    
    func reloadMenuTable(){
        DispatchQueue.main.async {
            self.menuTV.reloadData()
        }
    }
    
    func setUpSideMenuSelection(){
        if let _/*globalSource*/ = sharedFilterSource {
            if mainSourceData?.brand.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .brand, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .brand, isVisible: true)
            }
//            if mainSourceData?.carModel.filter({$0.isSelected == true}).count == 0{
//                menuDataSource.setIndicator(type: .model, isVisible: false)
//            }else{
//                menuDataSource.setIndicator(type: .model, isVisible: true)
//            }
            if mainSourceData?.location.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .location, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .location, isVisible: true)
            }
            if mainSourceData?.color.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .exteriorColor, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .exteriorColor, isVisible: true)
            }
            if mainSourceData?.transmission.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .transmission, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .transmission, isVisible: true)
            }
            if mainSourceData?.fuelType.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .fuelType, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .fuelType, isVisible: true)
            }
            if mainSourceData?.category.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .category, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .category, isVisible: true)
            }
            if mainSourceData?.feature.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .feature, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .feature, isVisible: true)
            }
            if mainSourceData?.selectedOwnerShip.filter({$0.isSelected == true}).count == 0{
                menuDataSource.setIndicator(type: .ownership, isVisible: false)
            }else{
                menuDataSource.setIndicator(type: .ownership, isVisible: true)
            }
            
            if mainSourceData?.price.currentMin != mainSourceData?.price.min.CGFloatValue || mainSourceData?.price.currentMax != mainSourceData?.price.max.CGFloatValue {
                menuDataSource.setIndicator(type: .price, isVisible: true)
            }
            if mainSourceData?.kilometer.currentMin != mainSourceData?.kilometer.min.CGFloatValue || mainSourceData?.kilometer.currentMax != mainSourceData?.kilometer.max.CGFloatValue {
                menuDataSource.setIndicator(type: .kilometers, isVisible: true)
            }
//            print("Year Current Min ",mainSourceData?.year.currentMin," Min Value ",mainSourceData?.year.min.CGFloatValue," Current Max Value ",mainSourceData?.year.currentMax," Max Value ",mainSourceData?.year.max.CGFloatValue)
//            print("Kilometer Current Min ",mainSourceData?.kilometer.currentMin," Min Value ",mainSourceData?.kilometer.min.CGFloatValue," Current Max Value ",mainSourceData?.kilometer.currentMax," Max Value ",mainSourceData?.kilometer.max.CGFloatValue)
            if mainSourceData?.year.currentMin != mainSourceData?.year.min.CGFloatValue || mainSourceData?.year.currentMax != mainSourceData?.year.max.CGFloatValue {
                menuDataSource.setIndicator(type: .year, isVisible: true)
            }
            
            self.menuTV.reloadData()
        }else{
            self.menuTV.reloadData()
        }
    }
    
    
    @IBAction func closeTapped(_ sender: UIButton) {
        sharedFilterSource = self.mainSourceData
        if needsReloadUsedCarList{
            delegate?.forceViewWillAppear()
            needsReloadUsedCarList = false
        }
        delegate?.resumeFromHalt()
        dismiss()
    }
    
    @IBAction func applyFilterTapped(_ sender: BaseButton) {
        sharedFilterSource = self.mainSourceData
        //Call Delegate
        //print("Temp %% listtype Count ",sharedFilterSource?.feature.map({$0.name}))
        if let _ = delegate{
            delegate!.updateFromFilter()
            delegate?.clearTempData()
        }
        dismiss()
    }
    
    @IBAction func clearFilterTapped(_ sender: UIButton) {
        leftCurrentSelectedIndex = IndexPath(row: 0, section: 0)
        sharedFilterSource = nil
        parsePassedJSON()
        if let _ = delegate{
            delegate!.clearFromFilter()
            delegate!.clearTempData()
        }
        menuDataSource = MenuTableSource()
        menuTV.reloadData()
//        if let _ = leftCurrentSelectedIndex{
//            menuTV.deselectRow(at: leftCurrentSelectedIndex!, animated: true)
//            menuTV.delegate?.tableView!(menuTV, didDeselectRowAt: leftCurrentSelectedIndex!)
//        }
        lisType = .brand
        listTV.reloadData()
        //Delegate clear
    }
    
    
    func setUpInterface(){
        
    }
    
    func tempSetUpWebCall(){
        WebServices.getMethodWith(url: "getFilterdetails", parameter: nil) { (isComplete, json) in
            self.filterJsonData = json
            self.parsePassedJSON()
        }
    }
    
    func parsePassedJSON(){
        if let jsonData = filterJsonData["data"] as? [String:Any]{
            do{
                let decoder = JSONDecoder()
                if let trueJson = serializeJSON(json: jsonData){
                    let parsedJSON = try decoder.decode(FilterParserModel.self, from: trueJson)
                    mainSourceData = parsedJSON
//                    print("Parsed List Type ", parsedJSON.feature.map({$0.id}))
//                    print("Parsed List Type Name ", parsedJSON.feature.map({$0.name}))
                    self.updateMainSourceWithGlobalSource()
                    if sharedFilterSource == nil || (sharedFilterSource?.price.currentMin == 0 && sharedFilterSource?.price.currentMax == -110){
                        self.setMainDataSourceMinMaxValues(with: parsedJSON)
                    }else{
                       // self.setUpYearValueWithMinMax(with: parsedJSON)
                    }
                    
                    for item in parsedJSON.ownership{
                        self.mainSourceData?.selectedOwnerShip.append(FilterParserModel.OwnerShipModel(data: item))
                    }
                    addListTypeData()
                    self.setUpSideMenuSelection()
                    listTV.reloadData()
                    //menuTV.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
//                    print("FilterData : ",parsedJSON.transmission[0].transmission)
//                    print("Trans",parsedJSON.transmission[0].isSelected)
                }
            }catch{
                print(error)
            }
        }
    }
    
    func addListTypeData(){
        var isAssuredSelected = false
        var isSellerSelected = false
        if let global = sharedFilterSource{
            for item in global.feature{
                if item.id == 1, item.isSelected == true{
                    isAssuredSelected = true
                }
                if item.id == 2, item.isSelected == true{
                    isSellerSelected = true
                }
            }
        }
        mainSourceData?.feature.removeAll()
        let assuredList = FilterParserModel.StaticNameIDParser(name: "Seller's List", id: 1, isSelected: isAssuredSelected)
        let sellerList = FilterParserModel.StaticNameIDParser(name: "Popular Assured List", id: 2, isSelected: isSellerSelected)
        mainSourceData?.feature.append(assuredList)
        mainSourceData?.feature.append(sellerList)
    }

    func setMainDataSourceMinMaxValues(with data:FilterParserModel){
        mainSourceData?.price.currentMin = data.price.min.CGFloatValue
        mainSourceData?.price.currentMax = data.price.max.CGFloatValue
        mainSourceData?.kilometer.currentMin = data.kilometer.min.CGFloatValue
        mainSourceData?.kilometer.currentMax = data.kilometer.max.CGFloatValue
        mainSourceData?.year.currentMin = data.year.min.CGFloatValue
        mainSourceData?.year.currentMax = data.year.max.CGFloatValue
        
        sharedFilterSource?.price.currentMin = data.price.min.CGFloatValue
        sharedFilterSource?.price.currentMax = data.price.max.CGFloatValue
        sharedFilterSource?.kilometer.currentMin = data.kilometer.min.CGFloatValue
        sharedFilterSource?.kilometer.currentMax = data.kilometer.max.CGFloatValue
        sharedFilterSource?.year.currentMin = data.year.min.CGFloatValue
        sharedFilterSource?.year.currentMax = data.year.max.CGFloatValue
    }
    
    func setUpYearValueWithMinMax(with data:FilterParserModel){
        mainSourceData?.year.currentMin = data.year.min.CGFloatValue
        mainSourceData?.year.currentMax = data.year.max.CGFloatValue
    }
    
    func updateMainSourceWithGlobalSource(){
        if let global = sharedFilterSource{
            mainSourceData = global
        }else{
            sharedFilterSource = mainSourceData
        }
    }
    
    func updateSearchDataWithFilterData(){
        if let filterData = sharedFilterSource{
            for (index,item) in filterData.color.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.colorSource[index].isSelected = true
                }
            }
            for (index,item) in filterData.fuelType.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.fuelSource[index].isSelected = true
                }
            }
            for (index,item) in filterData.category.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.bodyTypeSource[index].isSelected = true
                }
            }
            for (index,item) in filterData.transmission.enumerated(){
                if item.isSelected{
                    PLSearchAttributes.shared.transmissionSource[index].isSelected = true
                }
            }
            PLSearchAttributes.shared.selectedPriceMin = filterData.price.currentMin.int64Value
            PLSearchAttributes.shared.selectedPriceMax = filterData.price.currentMax.int64Value
            PLSearchAttributes.shared.selectedKilometerMin = filterData.kilometer.currentMin.intValue
            PLSearchAttributes.shared.selectedKilometerMax = filterData.kilometer.currentMax.intValue
        }
    }
   

}






/*
 
 @IBOutlet var kilometerView: BaseView!
 @IBOutlet var kilometerContainerView: UIView!
 @IBOutlet var kilometerSlider: RangeSeekSlider!
 
 @IBOutlet var yearView: BaseView!
 @IBOutlet var yearContainerView: UIView!
 @IBOutlet var yearSlider: RangeSeekSlider!
 
 @IBOutlet var priceView: BaseView!
 @IBOutlet var priceContainerView: UIView!
 @IBOutlet var priceSlider: RangeSeekSlider!
 */
