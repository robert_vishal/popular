//
//  TMSettingsManager.swift
//  TaskMario
//
//  Created by Mohamed Shafi on 13/02/19.
//  Copyright © 2019 Appzoc. All rights reserved.
//
// Check

import UIKit

typealias AlertButtonActionHandler = (UIAlertAction) -> Void

class PPCommon: NSObject {
    
    static let sharedInstance = PPCommon()
    
    func showAlertwithCompletion(message:String, controller:UIViewController, okButtonAction: AlertButtonActionHandler?)
    {
        let alert = UIAlertController(title: "Popular", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            okButtonAction
            
        }()))
        
        controller.present(alert, animated: true, completion:nil)

    }
    
    func showConfirmationAlert(_ message: String, onViewController viewController: UIViewController?, withCancelButtonTitle cancelButtonTitle: String, withOkButtonTitle okButtonTitle: String,withCancelButtonAction cancelButtonAction: AlertButtonActionHandler?,withOkButtonAction okButtonAction: AlertButtonActionHandler?) {
        
        if UIApplication.shared.keyWindow == nil {
            return }
        let alert = UIAlertController(title: "Popular", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: {
            cancelButtonAction
        }()))
        alert.addAction(UIAlertAction(title: okButtonTitle, style: UIAlertActionStyle.default, handler: {
            okButtonAction
            
        }()))
        viewController?.present(alert, animated: true, completion:nil)
    }

    
}

