//
//  PLImagePreviewerVC.swift
//  Popular
//
//  Created by Appzoc on 09/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLImagePreviewCVC: UICollectionViewCell, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet var zoomingScrollView: UIScrollView!
    @IBOutlet var previewImage: UIImageView!
    @IBOutlet var imageScrollView: UIScrollView!
    @IBOutlet var imageContainerView: BaseView!
    
    func setImageZooming() {
        zoomingScrollView.delegate = self
        zoomingScrollView.minimumZoomScale = 1.0
        zoomingScrollView.maximumZoomScale = 10.0
        zoomingScrollView.setZoomScale(1.0, animated: true)
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGR.delegate = self
        tapGR.numberOfTapsRequired = 2
        zoomingScrollView.addGestureRecognizer(tapGR)

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return previewImage
    }
    
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
        if zoomingScrollView.zoomScale > 1.0 {
            zoomingScrollView.setZoomScale(1.0, animated: true)
        }else {
            zoomingScrollView.setZoomScale(5.0, animated: true)
        }
    }

    
}

class PLImagePreviewerVC: UIViewController {

    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var largeImageCV: UICollectionView!
    @IBOutlet var thumbnailImageCV: UICollectionView!

    
    final var imageSource: [UIImage] = []
    final var imageURLSource: [String] = []
    
    var currentSource:ImageSourceDoc = .License
    var documentSource:[PLMyVehiclesDocumentsUploadVC.VehicleDocModel]?
    var licenseSource:[PLMyProfileVC.LicenseData]?
    
    var vehicleID:String = ""
    
    var currentIndex:Int = 0
    
    private var CVWidth: CGFloat = 0.0
    private var selectedIndexPath: IndexPath = IndexPath(item: 0, section: 0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        imageScrollView.minimumZoomScale = 1.0
//        imageScrollView.maximumZoomScale = 5.0
//
//        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
//        doubleTapGest.numberOfTapsRequired = 2
//        imageScrollView.addGestureRecognizer(doubleTapGest)
//
//        imageScrollView.delegate = self
//       // imageScrollView.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
//        imageScrollView.alwaysBounceVertical = false
//        imageScrollView.alwaysBounceHorizontal = false
//        imageScrollView.showsVerticalScrollIndicator = true
//        imageScrollView.flashScrollIndicators()
//
//        imageScrollView.minimumZoomScale = 1.0
//        imageScrollView.maximumZoomScale = 3.0
        
    }

    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
//        if imageScrollView.zoomScale == 1 {
//            imageScrollView.zoom(to: zoomRectForScale(scale: imageScrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
//        } else {
//            imageScrollView.setZoomScale(1, animated: true)
//        }
    }
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView?
//    {
//        let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
//
//        return cell.previewImage
//    }
    
//    func getSelectedImageView() -> UIImageView {
//        let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
//        return cell.previewImage
//
//    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
        
        zoomRect.size.height = cell.previewImage.frame.size.height / scale
        zoomRect.size.width  = cell.previewImage.frame.size.width  / scale
        //let newCenter = cell.previewImage.convert(center, from: imageScrollView)
//        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
//        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //print("viewdidappear....")
        CVWidth = largeImageCV.frame.size.width
        largeImageCV.reloadData()
        thumbnailImageCV.reloadData()
        if currentSource != .License{
            largeImageCV.scrollToItem(at: IndexPath(row: currentIndex, section: 0), at: .centeredHorizontally, animated: false)
        }
    }

    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func deleteImageTapped(_ sender: UIButton) {
        switch currentSource {
        case .License:
            if let license = licenseSource,license.count > 0{
                deleteLicenseDoc(id: "\(license[currentIndex].id)")
            }
            //print("LicenseDelete")
        case .InsuranceDoc:
            if let document = documentSource, document.count > 0{
                deleteInsuranceDoc(id: "\(document[currentIndex].imageID)")
            }
        case .PollutionDoc:
            if let document = documentSource, document.count > 0{
                deletePollutionDoc(id: "\(document[currentIndex].imageID)")
            }
        }
    }
    
    func deleteInsuranceDoc(id:String){
        WebServices.postMethod(url: "deleteInsurancedocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)","id":"\(id)"]) { (isComplete, json) in
            if isComplete{
                NotificationCenter.default.post(name: .documentDeleted, object: nil)
                self.documentSource?.remove(at: self.currentIndex)
                self.largeImageCV.reloadData()
                self.thumbnailImageCV.reloadData()
                if self.documentSource?.count == 0{
                    self.dismiss()
                }
            }
        }
    }

    func deletePollutionDoc(id:String){
        WebServices.postMethod(url: "deletePollutiondocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)","id":"\(id)"]) { (isComplete, json) in
            if isComplete{
                NotificationCenter.default.post(name: .documentDeleted, object: nil)
                self.documentSource?.remove(at: self.currentIndex)
                self.largeImageCV.reloadData()
                self.thumbnailImageCV.reloadData()
                if self.documentSource?.count == 0{
                    self.dismiss()
                }
            }
        }
    }
    
    func deleteLicenseDoc(id:String){
        WebServices.postMethod(url: "delete_licenseimage", parameter: ["user_id":"\(PLAppState.session.userID)","image_id":"\(id)"]) { (isComplete, json) in
            if isComplete{
                NotificationCenter.default.post(name: .licenseDeleted, object: nil)
                self.licenseSource?.remove(at: self.currentIndex)
                self.largeImageCV.reloadData()
                self.thumbnailImageCV.reloadData()
                if self.licenseSource?.count == 0{
                    self.dismiss()
                }
            }
        }
    }
    
}

enum ImageSourceDoc{
    case License
    case InsuranceDoc
    case PollutionDoc
}

// handling collection view datasources and size
extension PLImagePreviewerVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let source = documentSource{
         return source.count
        }
        if let license = licenseSource{
            return license.count
        }
        return imageSource.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLImagePreviewCVC", for: indexPath) as? PLImagePreviewCVC else { return UICollectionViewCell() }
        if collectionView == largeImageCV {
            cell.previewImage.layer.cornerRadius = 5
            cell.previewImage.layer.masksToBounds = true
            
        }
        
        if let source = documentSource{
            let docURL = URL(string: baseURLImage + source[indexPath.row].imageLink)
            cell.previewImage.kf.setImage(with: docURL, options: [.requestModifier(getImageAuthorization())])
        }else if let license = licenseSource{
            let docURL = URL(string: license[indexPath.row].imagelink)
             cell.previewImage.kf.setImage(with: docURL, options: [.requestModifier(getImageAuthorization())])
        }else{
            cell.previewImage.image = imageSource[indexPath.item]
        }
        //print("cellForIndex",indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == largeImageCV ? CGSize(width: CVWidth, height: 255) : CGSize(width: 120, height: 123)
    }
    
}

// handling collection view delegates
extension PLImagePreviewerVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == thumbnailImageCV {
            largeImageCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            currentIndex = indexPath.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == largeImageCV {
            if let license = licenseSource{
                titleLBL.text = (indexPath.item + 1).description + " of " + license.count.description
                currentIndex = indexPath.row
            }else if let source = documentSource{
                titleLBL.text = (indexPath.item + 1).description + " of " + source.count.description
                currentIndex = indexPath.row
            }else{
                titleLBL.text = (indexPath.item + 1).description + " of " + imageSource.count.description
                currentIndex = indexPath.row
            }
        }
    }
    

}
