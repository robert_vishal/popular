//
//  PLMyVehiclesTVC.swift
//  Popular
//
//  Created by Appzoc on 03/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLMyVehiclesTVC: UITableViewCell {
    //@IBOutlet var vehicleImage: UIImageView!
    
    @IBOutlet var vehicleImage: BaseImageView!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var regNoLBL: UILabel!
    @IBOutlet var editBTN: BaseButton!
    @IBOutlet var deleteBTN: BaseButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(data:PLMyVehiclesVC.MyVehicleModel){
        self.vehicleImage.kf.setImage(with: URL(string: baseURLImage + data.image), placeholder: nil, options: [.requestModifier(getImageAuthorization())])
        let modelDisplayName = "\(data.makeName ?? "") \(data.carModel)"
//        print("Model : ",modelDisplayName)
//        print("URL : ",baseURLImage + data.image)
        self.modelLBL.text = modelDisplayName//data.makeName ?? "" + " asda" + data.carModel //"\(data.makeName) \(data.carModel)"
        self.varientLBL.attributedText = attributedVariantString(for: data.variantName) //"Variant: \(data.variantName)"
        self.regNoLBL.attributedText = attributedRegNumberString(for: data.registrationNo) //"Reg. No: \(data.registrationNo)"
    }
    
    func attributedVariantString(for text:String) -> NSMutableAttributedString{
        let boldString = NSMutableAttributedString(string: "Variant: ", attributes: [NSAttributedStringKey.font: UIFont(name: "Roboto-Bold", size: 12) as Any])
        let normalString = NSMutableAttributedString(string: "\(text)", attributes: [NSAttributedStringKey.font: UIFont(name: "Roboto-Regular", size: 12) as Any])
        boldString.append(normalString)
        return boldString
    }
    
    func attributedRegNumberString(for text:String) -> NSMutableAttributedString{
        let boldString = NSMutableAttributedString(string: "Reg. No: ", attributes: [NSAttributedStringKey.font: UIFont(name: "Roboto-Bold", size: 12)!])
        
        let normalString = NSMutableAttributedString(string: "\(text.uppercased())", attributes: [NSAttributedStringKey.font: UIFont(name: "Roboto-Regular", size: 12)!])
        boldString.append(normalString)
        return boldString
    }
    
}
