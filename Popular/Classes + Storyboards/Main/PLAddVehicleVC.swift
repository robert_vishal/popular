//
//  PLAddVehicleVC.swift
//  Popular
//
//  Created by Appzoc on 03/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import DropDown

protocol PLAddVehicleDelegate {
    func vehicleAdded(with information: Information)
}

class PLAddVehicleVC: UIViewController, InterfaceSettable, SideOptionsDelegate {
 
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var registrationNoTF: UITextField!
    @IBOutlet var continueBTN: UIButton!
    @IBOutlet var variantBTNRef: UIButton!
    @IBOutlet var makeLBL: UILabel!
    
    // data receiving properties
    final var delegate: PLAddVehicleDelegate?
    final var vehicleInfo: Information = [:]
    final var isEditingVehicle: Bool = false
    final var isModelSelected: Bool = false
    final var isMakeSelected:Bool = false
    
    // vc properties
    fileprivate var optionDropDown = DropDown()
//    fileprivate var dataSourceModels: [String] = []
//    fileprivate var dataSourceVarients: [String] = []
    fileprivate var dropDownType:TappedDropDown = .make
    
    fileprivate var selectedVariant:String = "0"
    fileprivate var selectedModel:String = "0"
    fileprivate var selectedMake:String = "0"
    
    fileprivate var rawModels:[CarModel] = []
    fileprivate var rawVariants:[CarVariant] = []
    private var modelSideMenuSource:[PLSideOptionsModel] = []
    var vehicleModelForEdit:PLMyVehiclesVC.MyVehicleModel?
    var vehicleID:Int = 0
    private var selectedIndexPathModel: IndexPath?
    private var selectedIndexPathMake: IndexPath?
    private var selectedIndexPathVariant: IndexPath?
    
    enum TappedDropDown{
        case make
        case model
        case variant
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if GenericCarDetails.shared.makes.isEmpty {
//            getMakeData()
//        }
        fetchModelList()
        setUpInterface()
    }
    

    
    func setUpInterface() {
        if isEditingVehicle {
            titleLBL.text = "Maruthi"
            continueBTN.setTitle("SAVE", for: .normal)
        }else {
            titleLBL.text = "Add Your Vehicle"
            continueBTN.setTitle("CONTINUE", for: .normal)
        }
        
        if let passedData = vehicleModelForEdit{
            modelLBL.text = passedData.carModel
            selectedModel = passedData.carModelID.description
            modelSelected()
            selectedVariant = passedData.variantID.description
            varientLBL.text = passedData.variantName
            registrationNoTF.text = passedData.registrationNo
        }

    }

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        optionDropDown.hide()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        optionDropDown.width = registrationNoTF.frame.width
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func makeTapped(_ sender: UIButton) {
        dropDownType = .make
        showSideOption(presentOn: self, delegate: self, source: GenericCarDetails.shared.makes, filtertype: .anywhere, indexPath: selectedIndexPathMake)

    }
    
    
    @IBAction func modelTapped(_ sender: UIButton) {
      //  if isMakeSelected{
            var modelSideMenuSource:[PLSideOptionsModel] = []
            for item in rawModels{
                let obj  = PLSideOptionsModel(sourceName: item.name, sourceID: Int(item.id) )
                modelSideMenuSource.append(obj)
            }
            dropDownType = .model
            showSideOption(presentOn: self, delegate: self, source: modelSideMenuSource)
//        }else{
//            showBanner(message: "Please Select Make")
//        }
    }
    
    @IBAction func varientTapped(_ sender: UIButton) {
        if isModelSelected{
            dropDownType = .variant
            var variantSideMenuSource:[PLSideOptionsModel] = []
            for item in rawVariants{
                let obj = PLSideOptionsModel(sourceName: item.name, sourceID: item.id)
                variantSideMenuSource.append(obj)
            }
            showSideOption(presentOn: self, delegate: self, source: variantSideMenuSource)
        }else{
            showBanner(message: "Please Select Model")
        }
    }
    
    //MARK: Delegate method
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        switch dropDownType{
        case .make:
            //print("Make Tapped")
            self.selectedMake = data.id.description
            self.makeLBL.text = data.name
            self.selectedModel = "0"
            self.modelLBL.text = "Vehicle Model"
            self.varientLBL.text = "Vehicle Variant"
            self.selectedVariant = "0"
            selectedIndexPathMake = indexPath
            //self.fetchModelList()
        case .model:
            self.selectedModel = data.id.description
            self.modelLBL.text = data.name
            self.selectedVariant = "0"
            self.varientLBL.text = "Vehicle Variant"
            selectedIndexPathModel = indexPath
            modelSelected()
        case .variant:
            self.selectedVariant = data.id.description
            self.varientLBL.text = data.name
            selectedIndexPathVariant = indexPath
        }
        
    }
    
    
    @IBAction func continueTapped(_ sender: UIButton) {
        _ = Information()
        guard validateData() else{ return }
        if let text = registrationNoTF.text, text.isNotEmpty{
            var url = "addMyVehicles"
            var param:[String:String] = ["user_id":PLAppState.session.userID.description,"carmodel_id":"\(selectedModel)","variant_id":"\(selectedVariant)","register_number":"\(text)", "brand_id": selectedMake]

            if isEditingVehicle{
                param["vehicle_id"] = "\(vehicleID)"
                url = "EditMyVehicles"
            }
            
            WebServices.postMethod(url: url, parameter: param) { (isComplete, json) in
                if isComplete{
                   // self.delegate?.vehicleAdded(with: vehicleInfo)
                    NotificationCenter.default.post(name: .refreshData, object: nil)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    NotificationCenter.default.post(name: .refreshData, object: nil)
                }
            }
        }else{
            showBanner(message: "Please Enter Registration Number")
        }
    }
    
    func validateData() -> Bool{
        guard selectedModel != "0" else{
            showBanner(message: "Please Select Model")
            return false
        }
        guard selectedVariant != "0" else{
            showBanner(message: "Please Select Variant")
            return false
        }
        if let regNo = registrationNoTF.text, regNo.containSpecialCharactersIncludingHyphen(){
            showBanner(message: "Registration Number Cannot Include Special Characters")
            return false
        }
        return true
    }


}

//Mark:- Handling web service
extension PLAddVehicleVC {
    
    private func getMakeData() {
        let url = "getMake"
        WebServices.postMethodWithoutActivity(url: url, parameter: [:]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let responseData = response["data"] as? JsonArray else { return }
            
            let resultArray = PLSideOptionsModel.getData(fromArray: responseData, idKey: "id", nameKey: "name", isStringId: false)
            BaseThread.asyncMain {
                GenericCarDetails.shared.makes = resultArray
            }
        }
        
    }
    
    func fetchModelList(){
        let param:[String:String] = ["user_id":PLAppState.session.userID.description, "makeid":selectedMake]
        WebServices.postMethod(url: "getModelList", parameter: param) { (isComplete, json) in
            do{
                let decoder = JSONDecoder()
                let dataJSON = json["data"]
                
                let parsedData = try decoder.decode([CarModel].self, from: serializeJSON(json: dataJSON as Any)!)
                self.rawModels = parsedData
                self.isMakeSelected = true
                // self.dataSourceModels = parsedData.map({$0.name})
                //print(parsedData.map({$0.name}))
            }catch{
                print(error)
            }
            //print("Model List \(json)")
        }
    }

    
    func modelSelected(){
        WebServices.postMethod(url: "getvariantList", parameter: ["user_id":PLAppState.session.userID.description,"model_id":"\(selectedModel)"]) { (isComplete, json) in
            //print("Variant List \(json)")
            do{
                let decoder = JSONDecoder()
                let dataJSON = json["data"]
                let parsedData = try decoder.decode([CarVariant].self, from: serializeJSON(json: dataJSON as Any)!)
                self.rawVariants = parsedData
                self.isModelSelected = true
                // self.dataSourceVarients = parsedData.map({$0.name})
                //print(parsedData.map({$0.name}))
            }catch{
                print(error)
            }
        }
    }
    
}



struct CarModel:Codable{
    let id:Int
    let name:String
    
    private enum CodingKeys:  String, CodingKey {
        case id = "carmodelid", name = "carmodelname"
    }
}



struct CarVariant: Codable{
    let id:Int
    let name:String
    
    private enum CodingKeys: String, CodingKey {
        case id = "id", name = "name"
    }
}

/*
 fileprivate func setUpDropDown() {
 let appearance = DropDown.appearance()
 appearance.cellHeight = 50
 appearance.backgroundColor = UIColor.white
 appearance.selectionBackgroundColor = UIColor.lightGray
 appearance.separatorColor = UIColor.lightGray
 appearance.cornerRadius = 4
 appearance.shadowColor = UIColor.lightGray
 appearance.shadowOpacity = 0.9
 appearance.shadowRadius = 2
 appearance.animationduration = 0.2
 appearance.textColor = .black
 
 optionDropDown.dismissMode = .automatic
 optionDropDown.direction = .any
 optionDropDown.anchorView = modelLBL
 let offSet = modelLBL.frame.height + 2
 optionDropDown.bottomOffset = CGPoint(x: 0, y: offSet)
 optionDropDown.topOffset = CGPoint(x: 0, y: -offSet)
 optionDropDown.selectionAction = { (index: Int, item: String) in
 self.didSelectDropDown(item: item, at: index)
 }
 
 }
 
 
 fileprivate func didSelectDropDown(item: String, at index: Int){
 switch dropDownType{
 case .make:
 makeLBL.text = item
 case .model:
 modelLBL.text = item
 case .variant:
 varientLBL.text = item
 }
 }

 */
