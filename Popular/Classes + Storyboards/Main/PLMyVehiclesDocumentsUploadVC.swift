//
//  PLMyVehiclesDocumentsUploadVC.swift
//  Popular
//
//  Created by Appzoc on 04/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker

class PLAddDocumentCVC: UICollectionViewCell {
    
    @IBOutlet var documentImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}


class PLMyVehiclesDocumentsUploadVC: UIViewController, InterfaceSettable, OpalImagePickerControllerDelegate {

    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var modelLBL: UILabel!
    @IBOutlet var varientLBL: UILabel!
    @IBOutlet var registrationNoLBL: UILabel!
    @IBOutlet var insuranceCV: UICollectionView!
    @IBOutlet var pollutionCV: UICollectionView!
    
    fileprivate var insuranceSource: [VehicleDocModel] = []
    fileprivate var rawInsuranceDoc:[UIImage] = []
    fileprivate var pollutionSource: [VehicleDocModel] = []
    fileprivate var rawPollutionDoc:[UIImage] = []
    
    var vehicleID:Int = 0
    var vehicleData:PLMyVehiclesVC.MyVehicleModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(setUpInterface), name: .documentDeleted, object: nil)
        setUpInterface()
    }

   @objc func setUpInterface() {
        if let vehicleInfo = vehicleData{
            titleLBL.text = (vehicleInfo.makeName ?? "") + " " + vehicleInfo.carModel
            modelLBL.text = (vehicleInfo.makeName ?? "") + " " + vehicleInfo.carModel
            varientLBL.text = "Variant : " + vehicleInfo.variantName.uppercased()
            registrationNoLBL.text = vehicleInfo.registrationNo.uppercased()
        }
        
        insuranceSource.removeAll()
        rawInsuranceDoc.removeAll()
        rawPollutionDoc.removeAll()
        pollutionSource.removeAll()
        setUpWebCall()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func stackOverflowAnswer() {
        if let data = UIImagePNGRepresentation(#imageLiteral(resourceName: "VanGogh.jpg")) as Data? {
            print("There were \(data.count) bytes")
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let string = bcf.string(fromByteCount: Int64(data.count))
            print("formatted result: \(string)")
        }
    }

    
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        if picker.restorationIdentifier == "imagePickerInsurance" {
            
            images.forEach({ (currentImage) in
                rawInsuranceDoc.append(currentImage.resized(toWidth: 700)!.compressToBelow1MB(sizeInKB: 800))
            })
            
            uploadInsuranceDocs()
            //insuranceCV.reloadData()
        }else{
            images.forEach({ (currentImage) in
                rawPollutionDoc.append(currentImage.resized(toWidth: 700)!.compressToBelow1MB(sizeInKB: 800))
            })

            //rawPollutionDoc = images
            uploadPollutionDocs()
           // pollutionCV.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        
    }
    
    func setUpWebCall(conditional:Int = 0){
        if conditional == 0 || conditional == 1{
            WebServices.postMethod(url: "getInsurancedocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)"]) { (isComplete, json) in
                if isComplete{
                    let decoder = JSONDecoder()
                    let jsonData = json["data"] as? [[String:Any]]
                    do{
                        let trueData = try JSONSerialization.data(withJSONObject: jsonData as Any, options: .prettyPrinted)
                        let parseddata = try decoder.decode([VehicleDocModel].self, from: trueData)
                        self.insuranceSource = parseddata
                        self.insuranceCV.reloadData()
                    }catch{
                        print(error)
                    }
                }
            }
        }
        if conditional == 0 || conditional == 2{
            WebServices.postMethod(url: "getPollutiondocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)"]) { (isComplete, json) in
                if isComplete{
                    let decoder = JSONDecoder()
                    let jsonData = json["data"] as? [[String:Any]]
                    do{
                        let trueData = try JSONSerialization.data(withJSONObject: jsonData as Any, options: .prettyPrinted)
                        
                        let parseddata = try decoder.decode([VehicleDocModel].self, from: trueData)
                        self.pollutionSource = parseddata
                        self.pollutionCV.reloadData()
                    }catch{
                        print(error)
                    }
                }
            }
        }
    }
    
    
    func uploadInsuranceDocs(){
        WebServices.postMethodMultiPartImage(url: "uploadInsurancedocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)"], imageParameter: ["document[]" : rawInsuranceDoc]) { (isComplete, json) in
            if isComplete{
                self.setUpWebCall(conditional: 1)
                showBanner(title: "Insurance Document Uploaded", message: "", style: .success)
            }
        }
    }
    
    func uploadPollutionDocs(){
        WebServices.postMethodMultiPartImage(url: "uploadPollutiondocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)"], imageParameter: ["document[]" : rawPollutionDoc]) { (isComplete, json) in
            if isComplete{
                self.setUpWebCall(conditional: 2)
                showBanner(title: "Pollution Document Uploaded", message: "", style: .success)
            }
        }
    }
    
//    func deleteInsuranceDoc(id:String){
//        WebServices.postMethod(url: "deleteInsurancedocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)","id":"\(id)"]) { (isComplete, json) in
//            if isComplete{
//
//            }
//        }
//    }
//
//    func deletePollutionDoc(id:String){
//        WebServices.postMethod(url: "deletePollutiondocs", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(vehicleID)","id":"\(id)"]) { (isComplete, json) in
//            if isComplete{
//
//            }
//        }
//    }

}

extension PLMyVehiclesDocumentsUploadVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == insuranceCV{
            if insuranceSource.count < 2{
                return insuranceSource.count + 1
            }else{
                return insuranceSource.count
            }
        }else{
            if pollutionSource.count < 2{
                return pollutionSource.count + 1
            }else{
                return pollutionSource.count
            }
        }
       // return collectionView == insuranceCV ? insuranceSource.count + 1 : pollutionSource.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let addCell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath)
        let documentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "documentCell", for: indexPath) as! PLAddDocumentCVC
        
        if collectionView == insuranceCV {
            if indexPath.item == insuranceSource.count {
                return addCell
            }else {
                let docURL = URL(string: baseURLImage + insuranceSource[indexPath.row].imageLink)
                documentCell.documentImage.kf.setImage(with: docURL, options: [.requestModifier(getImageAuthorization())])
                return documentCell
            }
        }else {
            if indexPath.item == pollutionSource.count {
                return addCell
            }else {
                let docURL = URL(string: baseURLImage + pollutionSource[indexPath.row].imageLink)
                documentCell.documentImage.kf.setImage(with: docURL, options: [.requestModifier(getImageAuthorization())])
                return documentCell
            }
        }
       
    }
    
    
}

extension PLMyVehiclesDocumentsUploadVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == insuranceCV {
            if indexPath.item == insuranceSource.count {
                let imagePickerInsurance = OpalImagePickerController()
                imagePickerInsurance.maximumSelectionsAllowed = 2 - insuranceSource.count
                imagePickerInsurance.imagePickerDelegate = self
                imagePickerInsurance.restorationIdentifier = "imagePickerInsurance"
                present(imagePickerInsurance, animated: true, completion: nil)
            }else{
                let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLImagePreviewerVC") as! PLImagePreviewerVC
                vc.documentSource = self.insuranceSource
                vc.vehicleID = "\(self.vehicleID)"
                vc.currentSource = .InsuranceDoc
                vc.currentIndex = indexPath.row
                present(vc, animated: true, completion: nil)
            }
        }else{
            if indexPath.item == pollutionSource.count {
                let imagePickerPollution = OpalImagePickerController()
                imagePickerPollution.maximumSelectionsAllowed = 2 - pollutionSource.count
                imagePickerPollution.imagePickerDelegate = self
                imagePickerPollution.restorationIdentifier = "imagePickerPollution"
                present(imagePickerPollution, animated: true, completion: nil)
            }else{
                let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLImagePreviewerVC") as! PLImagePreviewerVC
                vc.documentSource = self.pollutionSource
                vc.vehicleID = "\(self.vehicleID)"
                vc.currentSource = .PollutionDoc
                vc.currentIndex = indexPath.row
                present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension PLMyVehiclesDocumentsUploadVC{
    
    struct VehicleDocModel:Codable{
        let imageID:Int
        let imageLink:String
        
        private enum CodingKeys:String, CodingKey{
            case imageID = "image_id", imageLink = "image_link"
        }
    }
    
}




