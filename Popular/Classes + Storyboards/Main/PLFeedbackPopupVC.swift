//
//  PLFeedbackPopupVC.swift
//  Popular
//
//  Created by Appzoc on 15/10/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLFeedbackPopupVC: UIViewController, UITextViewDelegate {

    
    @IBOutlet var feedBackTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedBackTextView.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            if let touchedView = touch.view{
                if touchedView == self.view{
                    DispatchQueue.main.async {
                        self.view.backgroundColor = UIColor.clear
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
        guard validate() else {
            showBanner(message: "Please type your feedback")
            return
        }
        //http://popular.dev.webcastle.in/api/post/comment/100
        WebServices.postMethod(url: "post/comment/\(PLAppState.session.userID)", parameter: ["feedback":"\(feedBackTextView.text)"]) { (isComplete, json) in
            if isComplete{
                showBanner(message: "Feedback submitted successfully")
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    func validate() -> Bool{
        if let text = feedBackTextView.text{
            if text == "Type here"{
                return false
            }else{
                return true
            }
        }else{
            return false
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count <= 300{
            return true
        }else{
            return false
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type here"{
            DispatchQueue.main.async {
                textView.textColor = UIColor.black
                textView.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: CharacterSet(charactersIn: " ")).isEmpty {
            DispatchQueue.main.async {
                textView.textColor = UIColor.black.withAlphaComponent(0.5)
                textView.text = "Type here"
            }
        }
    }

}
