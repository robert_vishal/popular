//
//  PLOTPVerificationVC.swift
//  Popular
//
//  Created by Appzoc on 23/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import PinCodeTextField

class PLOTPVerificationVC: UIViewController, InterfaceSettable {

    @IBOutlet var mobileLBL: UILabel!
    @IBOutlet var otpTF: PinCodeTextField!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    // data passing variables
    final var mobileNumber = ""
    final var otpFrom:String = ""
    final var userID:Int = 0
    final var userName: String = ""
    final var isEmployee:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardListener()
        setUpInterface()
        
    }
    
    func setUpInterface() {
        otpTF.becomeFirstResponder()
        otpTF.delegate = self
        otpTF.keyboardType = .numberPad

        if mobileNumber.isNotEmpty {
            // to +91 *******210
            mobileLBL.text = "to +91 *******" + mobileNumber.suffix(3)
        }
    }
    
    func addKeyboardListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resendTapped(_ sender: UIButton) {
        resendOtpWeb()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        otpTF.resignFirstResponder()
    }
    
    @objc func keyBoardWillShow(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstraint.constant += keyboardHeight
        }
    }
    
    
    @objc func keyBoardWillHide(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstraint.constant -= keyboardHeight
        }
    }
}

// Mark: - Handling otp textfiled
extension PLOTPVerificationVC: PinCodeTextFieldDelegate {
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool { return true }

    func textFieldValueChanged(_ textField: PinCodeTextField) {
        //debugPrint(textField.text?.count as AnyObject,"value:",textField.text as AnyObject)
        if textField.text?.count == 4 {
            if "\(self.otpFrom)" == textField.text
            {
                getOTPVerificationWeb()
            }
            else
            {
                showBanner(message: "Wrong OTP")
            }
        }
    }
    
}

// Mark: - Handling web service
extension PLOTPVerificationVC {
    private func getOTPVerificationWeb(){
        UserDefaults.standard.set(true, forKey: "UserLoggedIn")
        UserDefaults.standard.set(self.userID, forKey: "UserID")
        UserDefaults.standard.set("\(self.userName)", forKey: "UserName")
        UserDefaults.standard.set(self.mobileNumber, forKey: "UserMobile")
        // UserDefaults.standard.set(self.isEmployee, forKey: "isEmployee")
        
        if self.isEmployee == 1 {
            let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLEmployeeVerificationVC") as! PLEmployeeVerificationVC
            self.present(segueVC, animated: true, completion: {
            })
        }else{
            let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLHomeVC")
            self.navigationController?.pushViewController(segueVC, animated: true)
        }
        

   /*     print("userID:",userID)
        let param = ["user_id":userID.description,"otp":otpFrom, "name": userName]
        
        WebServices.postMethod(url: "otpverify", parameter: param) { (isCompleted, json) in
            if isCompleted{
                UserDefaults.standard.set(true, forKey: "UserLoggedIn")
                UserDefaults.standard.set(self.userID, forKey: "UserID")
                UserDefaults.standard.set("\(self.userName)", forKey: "UserName")
                UserDefaults.standard.set(self.mobileNumber, forKey: "UserMobile")
               // UserDefaults.standard.set(self.isEmployee, forKey: "isEmployee")
         
                if self.isEmployee == 1 {
                    let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLEmployeeVerificationVC") as! PLEmployeeVerificationVC
                    self.present(segueVC, animated: true, completion: {
                    })
                }else{
                    let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLHomeVC")
                    self.navigationController?.pushViewController(segueVC, animated: true)
                }
                
            }
        }
        */
    }
    
    private func resendOtpWeb(){
        //Api name : otpresend
        var param = [String:Any]()
        param["user_id"] = self.userID.description
        param["mobile_number"] = self.mobileNumber //UserDefaults.standard.string(forKey: "UserID")
        print("Parameters ",param.description)
        BaseThread.asyncMain {
            WebServices.postMethod(url:"otpresend" ,
                       parameter: param,
                       CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        
                        self.otpFrom =  ((result["data"] as! [String:Any]) ["otp"] as? String ?? "").base64Decoded() ?? ""
                       // showBanner(message: "OTP successfully resent. " + self.otpFrom)
                    }
                    else
                    {
                        //showBanner(message: "Oops error..!",color:.red)
                    }
            })
        }
    }
}







