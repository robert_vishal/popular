//
//  PLPopupEmployeeVerificationVC.swift
//  Popular
//
//  Created by Appzoc on 12/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLPopupEmployeeVerificationVC: UIViewController {

    
    //isEMployee - 9441111111

    @IBOutlet var employeeIDTF: UITextField!
    
    override func viewDidAppear(_ animated: Bool) {
        //transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    
    @IBAction func submitBTNTapped(_ sender: UIButton) {
        if let employeeID = employeeIDTF.text, employeeID.isNotEmpty{
            let param = ["user_id":"\(PLAppState.session.userID)","employeeid":"\(employeeID)"]
            WebServices.postMethod(url: "employeeid_submission", parameter: param, CompletionHandler: { (isComplete, json) in
                if isComplete{
                    //let jsonData = json["data"] as? [String:Any]
                    UserDefaults.standard.set("1", forKey: "isEmployee")
                    NotificationCenter.default.post(name: .loggedIn, object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: NSNotification.Name("notificationLogged"), object: self, userInfo: nil)

                    self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                    NotificationCenter.default.post(name: .popupLoginDriving, object: self, userInfo: nil)

                }else {
//                    NotificationCenter.default.post(name: .loggedIn, object: nil, userInfo: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name("notificationLogged"), object: self, userInfo: nil)
//
//                    self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
//                    NotificationCenter.default.post(name: .popupLoginDriving, object: self, userInfo: nil)
                    showBanner(message: "Invalid Employee ID")
                }
            })
        }else{
            showBanner(message: "Enter Employee ID")
        }
    }
    
    @IBAction func dismissVCTapped(_ sender: UIButton) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func skipBTNTapped(_ sender: UIButton) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
    }
}
