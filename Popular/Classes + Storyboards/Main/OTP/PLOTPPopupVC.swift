//
//  PLOTPPopupVC.swift
//  Popular
//
//  Created by Appzoc on 12/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import PinCodeTextField

class PLOTPPopupVC: UIViewController {
    
    @IBOutlet var pincodeTFRef: PinCodeTextField!
    @IBOutlet var containerView: UIView!
    
    @IBOutlet var numberInfoLBL: UILabel!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    var otpFrom:String = ""
    var userID:Int = 0
    var mobileNumber:String = ""
    var userName:String = ""
    var isEmployee:Int = 0
    
    var keyboardShown:Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
       // transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        //showOTPasBanner()
        addKeyboardListener()
    }
    
    func addKeyboardListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func setUpInterface(){
        pincodeTFRef.becomeFirstResponder()
        pincodeTFRef.delegate = self
        pincodeTFRef.keyboardType = .numberPad
        let starFormattedNumber = "*******" + mobileNumber.suffix(3)
        numberInfoLBL.text = "Please type the verification code sent \n to +91 \(starFormattedNumber)"
    }

    func postOTPVerification(){
        UserDefaults.standard.set(true, forKey: "UserLoggedIn")
        UserDefaults.standard.set(self.userID, forKey: "UserID")
        UserDefaults.standard.set(self.userName, forKey: "UserName")
        UserDefaults.standard.set(self.mobileNumber, forKey: "UserMobile")
        PLAppState.session.homeDelegate?.refreshHomeOnLogin()
        if self.isEmployee == 1{
            let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLPopupEmployeeVerificationVC") as! PLPopupEmployeeVerificationVC
            self.present(segueVC, animated: true, completion: {
                self.view.isHidden = true
            })
        }else{
            NotificationCenter.default.post(name: NSNotification.Name("notificationLogged"), object: self, userInfo: nil)
            NotificationCenter.default.post(name: .loggedIn, object: nil, userInfo: nil)
            self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
            NotificationCenter.default.post(name: .popupLoginDriving, object: self, userInfo: nil)
            
        }

        // av
   /*     WebServices.postMethod(url: "otpverify", parameter: param) { (isCompleted, json) in
            if isCompleted{
                UserDefaults.standard.set(true, forKey: "UserLoggedIn")
                UserDefaults.standard.set(self.userID, forKey: "UserID")
                UserDefaults.standard.set("\(self.userName)", forKey: "UserName")
                UserDefaults.standard.set(self.mobileNumber, forKey: "UserMobile")
                
                BaseThread.asyncMain {
                    PLAppState.session.homeDelegate?.refreshHomeOnLogin()

                    if self.isEmployee == 1{
                        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLPopupEmployeeVerificationVC") as! PLPopupEmployeeVerificationVC
                        
                        self.present(segueVC, animated: true, completion: {
                            self.view.isHidden = true
                        })
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name("notificationLogged"), object: self, userInfo: nil)
                        NotificationCenter.default.post(name: .loggedIn, object: nil, userInfo: nil)
                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                        NotificationCenter.default.post(name: .popupLoginDriving, object: self, userInfo: nil)
                        
                    }

                }
                
            }else {
                
            }
        } */
        
    }
    
    func showOTPasBanner(){
        showBanner(title: "OTP", message: "\(otpFrom)", style: .success, color: .darkGray)
    }
    
    @IBAction func submitBTNTapped(_ sender: UIButton) {
    }
    
    @IBAction func dismissPopupTapped(_ sender: UIButton) {
        if keyboardShown{
            pincodeTFRef.resignFirstResponder()
        }else{
            self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func keyBoardWillShow(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            keyboardShown = true
            bottomConstraint.constant += keyboardHeight
        }
    }
    
    
    @objc func keyBoardWillHide(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            keyboardShown = false
            bottomConstraint.constant -= keyboardHeight
        }
    }
    
}

extension PLOTPPopupVC: PinCodeTextFieldDelegate{
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        //debugPrint(textField.text?.count as AnyObject,"value:",textField.text as AnyObject)
        if textField.text?.count == 4 {
            if "\(self.otpFrom)" == textField.text
            {
                postOTPVerification()
            }
            else
            {
                showBanner(message: "Wrong OTP")
            }
        }
    }
}

protocol RefreshHomePage{
    func refreshHomeOnLogin()
}
