//
//  PLEmployeeVerificationVC.swift
//  Popular
//
//  Created by Appzoc on 27/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import TextFieldEffects
class PLEmployeeVerificationVC: UIViewController {

    @IBOutlet var employeeIdTF: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLHomeVC")
        self.present(segueVC, animated: true, completion: nil)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        if BaseValidator.isNotEmpty(string: employeeIdTF.text) {
            if let employeeID = employeeIdTF.text, employeeID.isNotEmpty{
                let param = ["user_id":"\(PLAppState.session.userID)","employeeid":"\(employeeID)"]
                WebServices.postMethod(url: "employeeid_submission", parameter: param, CompletionHandler: { (isComplete, json) in
                    if isComplete{
                        UserDefaults.standard.set("1", forKey: "isEmployee")
                        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLHomeVC")
                        self.present(segueVC, animated: true, completion: nil)
                    }else{
                        showBanner(message: "Invalid Employee ID")
                    }
                })
            }
            
        }else{
            showBanner(message: "Enter Employee ID")
        }
    }
    

}
