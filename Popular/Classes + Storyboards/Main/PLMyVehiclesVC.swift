//
//  PLMyVehiclesVC.swift
//  Popular
//
//  Created by Appzoc on 03/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLMyVehiclesVC: UIViewController, InterfaceSettable, PLAddVehicleDelegate {
    @IBOutlet var noVehiclesView: BaseView!
    @IBOutlet var vehicleTV: UITableView!
    @IBOutlet var addVehicleRoundBTN: BaseButton!
    
    fileprivate var isMyVehicleEmpty: Bool{
        if myVehiclesSource.count > 0{
            return false
        }else{
            return true
        }
    }
    
    var myVehiclesSource:[MyVehicleModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
        addListener()
        vehicleTV.estimatedRowHeight = 250
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpWebCall()
    }
    
    func addListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(setUpWebCall), name: .refreshData, object: nil)
       
    }

    @objc func setUpWebCall(){
        let param = ["user_id":PLAppState.session.userID.description]
        WebServices.postMethod(url: "getMyVehicles", parameter: param) { (isComplete, json) in
            print("JSON : \(json)")
            if let jsonData = json["data"] {
                let decoder = JSONDecoder()
                if let serializedData = serializeJSON(json: jsonData){
                    do{
                        let parsedData = try decoder.decode([MyVehicleModel].self, from: serializedData)
                        self.myVehiclesSource = parsedData
                        self.vehicleTV.reloadData()
                        if parsedData.count > 0{
                            print(parsedData[0].makeName as Any)
                        }
                    }catch{
                        print(error)
                    }
                }
            }
            self.setUpInterface()
        }
    }
    
    func setUpInterface() {
        vehicleTV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        if isMyVehicleEmpty {
            noVehiclesView.isHidden = false
            addVehicleRoundBTN.isHidden = true
            vehicleTV.isHidden = true
        }else {
            noVehiclesView.isHidden = true
            addVehicleRoundBTN.isHidden = false
            vehicleTV.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addVehicleTapped(_ sender: BaseButton) {
        let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLAddVehicleVC") as! PLAddVehicleVC
        segueVC.delegate = self
        present(segueVC, animated: true, completion: nil)
    }
    
    // delegate from vehicle adding/ editing VC
    func vehicleAdded(with information: Information) {
//        isMyVehicleEmpty = false
        vehicleTV.reloadData()
        setUpInterface()
    }
    
    @IBAction func editVehicleTapped(_ sender: BaseButton) {
        //print(sender.indexPath as Any)
        let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLAddVehicleVC") as! PLAddVehicleVC
        segueVC.delegate = self
        segueVC.isEditingVehicle = true
        segueVC.vehicleID = sender.tag
        let source = myVehiclesSource[sender.indexPath!.row]
        segueVC.vehicleModelForEdit = source
        present(segueVC, animated: true, completion: nil)
    }
    
    @IBAction func deleteVehicleTapped(_ sender: BaseButton) {
        //print(sender.indexPath as Any)
        WebServices.postMethod(url: "delete_vehicle", parameter: ["user_id":"\(PLAppState.session.userID)","vehicle_id":"\(sender.tag)"]) { (isComplete, json) in
            if isComplete{
                NotificationCenter.default.post(name: .refreshData, object: nil)
                showBanner(title: "Success", message: "", style: .success)
            }else{
                showBanner(message: "Failed")
            }
        }
        
    }
    
    /*
    "vehicleid": 41,
    "reg_no": "c yd",
    "image": "http://app.appzoc.com/popular/backend/storage/app/http://app.appzoc.com/popular/backend/storage/app/uploads/usedcars/bvDA3crthcVgzqE6j43ZS0O1f0pFA1EFO2Sn6M64.png",
    "carmodelid": 1,
    "carmodel": "Alto 800",
    "variantid": "STD",
    "makename": "Maruti Suzuki"
 */
    
    struct MyVehicleModel: Codable{
        let vehicleID:Int
        let registrationNo:String
        let image:String
        let carModelID:Int
        let carModel:String
        let variantID:Int
        let makeName:String?
        let variantName:String
        
        private enum CodingKeys:String, CodingKey{
            case vehicleID = "vehicleid",registrationNo = "reg_no",image = "image",carModelID = "carmodelid",carModel = "carmodel",variantID = "variantid", makeName = "makename", variantName = "variantname"
        }
    }
}

extension PLMyVehiclesVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myVehiclesSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLMyVehiclesTVC", for: indexPath) as? PLMyVehiclesTVC else { return UITableViewCell() }
        
        cell.editBTN.indexPath = indexPath
        cell.deleteBTN.indexPath = indexPath
        cell.editBTN.tag = myVehiclesSource[indexPath.row].vehicleID
        cell.deleteBTN.tag = myVehiclesSource[indexPath.row].vehicleID
        cell.configureCellWith(data: myVehiclesSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension //252.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
}


extension PLMyVehiclesVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLMyVehiclesDocumentsUploadVC") as! PLMyVehiclesDocumentsUploadVC
        segueVC.vehicleID = myVehiclesSource[indexPath.row].vehicleID
        segueVC.vehicleData = myVehiclesSource[indexPath.row]
        present(segueVC, animated: true, completion: nil)

    }
}



