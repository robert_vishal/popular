//
//  PLMyProfileVC.swift
//  Popular
//
//  Created by Appzoc on 03/04/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import OpalImagePicker
import Kingfisher
import Photos
//import Presentr

class PLMyProfileVC: UIViewController, InterfaceSettable, OpalImagePickerControllerDelegate, ImageSelectorDelegate {

    //@IBOutlet var profileImage: BaseImageView!
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var profileNameTF: UITextField!
    
    @IBOutlet var mailIdTF: UITextField!
    @IBOutlet var mobileTF: UITextField!
    @IBOutlet var addViewLicenseLBL: UILabel!
    @IBOutlet var licenseImage: UIImageView!
    @IBOutlet var editEmailIcon: UIImageView!
    
    @IBOutlet var editNameIcon: UIImageView!
    
    
    fileprivate var isPickingProfileImage:Bool = true
    
    fileprivate let imagePicker = OpalImagePickerController()
    fileprivate var isPresentLicense: Bool = false
    fileprivate var licenseImages: [LicenseData] = []
    fileprivate var licenseImagesRaw: [UIImage] = []
    fileprivate var isEditingMail:Bool = false
    fileprivate var isEditingProfileName:Bool = false
    
    fileprivate lazy var addLicenseString = "Add License"
    fileprivate lazy var viewLicenseString = "View License"
    
    var isReloadRequired:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
       NotificationCenter.default.addObserver(self, selector: #selector(setUpInterface), name: .licenseDeleted, object: nil)
       // jsonConversion()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if isReloadRequired{
         setUpInterface()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func setUpInterface() {
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 1
        mailIdTF.delegate = self
        mailIdTF.isUserInteractionEnabled = false
        mobileTF.isUserInteractionEnabled = false
//        profileImage.layer.cornerRadius = profileImage.frame.width.half
//        profileImage.layer.masksToBounds = true
        
        let param = ["user_id":"\(PLAppState.session.userID)"]
        WebServices.postMethod(url: "getUserProfile", parameter: param) { (isComplete, json) in
            print("JSON",json)
            if let jsonData = json["data"] as? [String:Any]{
                let name = jsonData["Username"] as? String ?? ""
                UserDefaults.standard.set("\(name)", forKey: "UserName")
                self.profileNameTF.text = "Hello " + "\(PLAppState.session.userName.capitalized)" + " !"
                    //"Hello " + "\(PLAppState.session.userName.capitalized)" + " !"
                self.mailIdTF.text = jsonData["Email"] as? String
                self.mobileTF.text = jsonData["Mobile number"] as? String
                if let imageLink = jsonData["user_image"] as? String{
                self.profileImage.kf.setImage(with: URL(string: "\(baseURLImage)\(imageLink)"), placeholder: UIImage(named: "layer13"), options: [.requestModifier(getImageAuthorization())])
                }
                let hasLicense = jsonData["is_license"] as! String
                if hasLicense == "true"{
                    self.fetchLicenseData()
                    self.isPresentLicense = true
                    self.licenseImage.image = UIImage(named: "viewLicense")
                    self.addViewLicenseLBL.text = self.viewLicenseString
                }else{
                    self.addViewLicenseLBL.text = self.addLicenseString
                    self.licenseImage.image = UIImage(named: "addlicense")
                    self.isPresentLicense = false
                }
            }
        }
        
        if isPresentLicense{
            addViewLicenseLBL.text = viewLicenseString
            licenseImage.image = UIImage(named: "viewLicense")
            //viewLicense
        }else{
            addViewLicenseLBL.text = addLicenseString
            licenseImage.image = UIImage(named: "addlicense")
            //addlicense
        }
        //print("ProfileImage22",profileImage.frame.width,profileImage.frame.height)
    }
    override func viewDidAppear(_ animated: Bool) {
        //print("ProfileImageDidAppear",profileImage.frame.width,profileImage.frame.height)

    }
    
    @IBAction func editProfileName(){
        if isEditingProfileName{
            
            updateProfileName()
        }else{
            profileNameTF.text = PLAppState.session.userName
            profileNameTF.isUserInteractionEnabled = true
            profileNameTF.becomeFirstResponder()
            editNameIcon.image = UIImage(named: "upload-sign")
            isEditingProfileName = true
        }
    }
    
    func updateProfileName(){
        guard let _ = profileNameTF.text, BaseValidator.isNotEmpty(string: profileNameTF.text) else {
            showBanner(title: "Enter name", message: "", style: .danger, color:.RedColorTo())
            return
        }
        let param = ["user_id": PLAppState.session.userID.description,"name":"\(profileNameTF.text!)"]
        WebServices.postMethod(url: "updateuserName", parameter: param) { (isComplete, json) in
            if isComplete{
                let name = self.profileNameTF.text ?? ""
                UserDefaults.standard.set("\(name)", forKey: "UserName")
                DispatchQueue.main.async {
                    self.isEditingProfileName = false
                    self.profileNameTF.resignFirstResponder()
                    self.editNameIcon.image = UIImage(named: "pencil-edit-button-gray")
                    self.profileNameTF.text = "Hello " + "\(PLAppState.session.userName.capitalized)" + " !"
                }
            }
        }
    }
    
    
    func updateUserEmail(){
        guard let _ = mailIdTF.text, BaseValidator.isNotEmpty(string: mailIdTF.text), BaseValidator.isValid(email: mailIdTF.text) else {
            showBanner(title: "Enter a valid Email-ID", message: "", style: .danger, color:.RedColorTo())
                return
        }
        let param = ["user_id": PLAppState.session.userID.description,"email":"\(mailIdTF.text!)"]
        WebServices.postMethod(url: "postuserProfile", parameter: param) { (isComplete, json) in
            if isComplete{
                self.editEmailIcon.image = UIImage(named: "pencil-edit-button-gray")
            }
        }
    }
    
    func fetchLicenseData(){
        WebServices.postMethod(url: "getlicenseImages", parameter: ["user_id":PLAppState.session.userID.description], CompletionHandler: { (isComplete, json) in
            do{
                let dataJson = json["data"]
                let decoder = JSONDecoder()
                let parsedData = try decoder.decode([LicenseData].self, from: serializeJSON(json: dataJson as Any)!)
                self.licenseImages = parsedData
                
//                for image in parsedData{
//                    KingfisherManager.shared.retrieveImage(with: URL(string: "\(image.imagelink)")! , options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: { (img, err, cache, url) in
//                        if let downLoadedImg = img{
//                            self.licenseImages.append(downLoadedImg)
//                        }
//                    })
//                }
            }catch{
                print(error)
            }
        })
    }
    
    func updateUserImage(image:UIImage){
        let param = ["user_id":PLAppState.session.userID.description]
        let dataParam = ["image":[image]]
        WebServices.postMethodMultiPartImage(url: "UserImageupload", parameter: param, imageParameter: dataParam) { (isComplete, json) in
            self.isReloadRequired = true
        }
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        if !images.isEmpty {
            
            if self.isPickingProfileImage{
                DispatchQueue.main.async {
                    self.profileImage.image = images[0]
                }
                self.updateUserImage(image: images[0])
                self.isReloadRequired = false
            }else{
                self.licenseImagesRaw = images
                self.uploadLicenseDocuments(images: images)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func uploadLicenseDocuments(images:[UIImage]){
        WebServices.postMethodMultiPartImage(url: "postlicenseImages", parameter: ["user_id":"\(PLAppState.session.userID)"], imageParameter: ["image[]":images]) { (isComplete, json) in
             print("Upload Success \(json)")
            if let jsonData = json["data"] as? [String:Any]{
                let isLicenseUploaded = jsonData["is_license"] as! String
                if isLicenseUploaded == "true"{
                    self.isPresentLicense = true
                }else{
                     self.isPresentLicense = false
                }
                self.setUpInterface()
            }
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func profileImageTapped(_ sender: UIButton) {
        
        let status = PHPhotoLibrary.authorizationStatus()
        //print("Status : ",status)
        switch status{
        case .authorized:
            pickImage()
        default:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized{
                    self.pickImage()
                }else{
                    redirectToSettings(key: "Settings", vc: self)
                }
            })
        }
    }
    
    func pickImage(){
        imagePicker.maximumSelectionsAllowed = 1
        self.isPickingProfileImage = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func editMailIdTapped(_ sender: Any) {
        if isEditingMail{
            isEditingMail = false
            mailIdTF.resignFirstResponder()
            updateUserEmail()
        }else{
            mailIdTF.isUserInteractionEnabled = true
            mailIdTF.becomeFirstResponder()
            editEmailIcon.image = UIImage(named: "upload-sign")
            isEditingMail = true
        }
    }
    
    @IBAction func myVehiclesTapped(_ sender: UIButton) {
        let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLMyVehiclesVC") as! PLMyVehiclesVC
        present(segueVC, animated: true, completion: nil)
    }
  
    @IBAction func addViewLicenseTapped(_ sender: UIButton) {
        pickLicence()
//        let status = PHPhotoLibrary.authorizationStatus()
//        print("Status : ",status)
//        switch status{
//        case .authorized:
//            pickLicence()
//        default:
//            PHPhotoLibrary.requestAuthorization({ (status) in
//                if status == .authorized{
//                    self.pickLicence()
//                }
//            })
  
        
//        if isPresentLicense {
//            let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLImagePreviewerVC") as! PLImagePreviewerVC
//            segueVC.currentSource = .License
//            segueVC.licenseSource = licenseImages
//            present(segueVC, animated: true, completion: nil)
//        }else{
////            let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLImageSelectorVC") as! PLImageSelectorVC
////            segueVC.delegate = self
////            present(segueVC, animated: true, completion: nil)
//            imagePicker.maximumSelectionsAllowed = 2
//            isPickingProfileImage = false
//            present(imagePicker, animated: true, completion: nil)
//        }
}
    
    func pickLicence(){
        
        if isPresentLicense {
            let segueVC = self.storyboard?.instantiateViewController(withIdentifier: "PLImagePreviewerVC") as! PLImagePreviewerVC
            segueVC.currentSource = .License
            segueVC.licenseSource = licenseImages
            present(segueVC)
        }else{
            let status = PHPhotoLibrary.authorizationStatus()
            let imagePickerLicence = OpalImagePickerController()
            imagePickerLicence.imagePickerDelegate = self
            //print("Status : ",status)
            switch status{
            case .authorized:
                imagePickerLicence.maximumSelectionsAllowed = 2
                isPickingProfileImage = false
                present(imagePickerLicence, animated: true, completion: nil)
            default:
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized{
                        imagePickerLicence.maximumSelectionsAllowed = 2
                        self.isPickingProfileImage = false
                        self.present(imagePickerLicence)
                    }else{
                        redirectToSettings(key: "Settings", vc: self)
                    }
                })
            }
        }
        
    }
    
    // delegate from PLImageSelectorVC
    func imageSelector(isSelected: Bool, didFinishSelecting images: [UIImage]) {
        isPresentLicense = isSelected
        if isPresentLicense {
            addViewLicenseLBL.text  = viewLicenseString
            licenseImage.image = UIImage(named: "viewLicense")
            images.forEach { (currentImage) in
                licenseImagesRaw.append(currentImage.resized(toWidth: 700)!.compressToBelow1MB(sizeInKB: 800))
            }

            //licenseImagesRaw = images
        }else{
            addViewLicenseLBL.text  = addLicenseString
            licenseImage.image = UIImage(named: "addlicense")
        }
    }
    
    @IBAction func signOutTapped(_ sender: UIButton) {
        
        let confirmationVC = UIAlertController(title: "Sign Out", message: "Do you want to Sign Out?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Sign Out", style: .default) { (action) in
            PLAppState.session.logout()
            confirmationVC.dismiss(animated: false, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            confirmationVC.dismiss(animated: true, completion: nil)
        }
        confirmationVC.addAction(okAction)
        confirmationVC.addAction(cancelAction)
        self.present(confirmationVC)
//        PLAppState.session.logout()
//        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func provideFeedbackTapped(_ sender: UIButton) {
        let segueVC = self.storyBoardMain?.instantiateViewController(withIdentifier: "PLFeedbackPopupVC") as! PLFeedbackPopupVC
        self.present(segueVC, animated: true, completion: nil)
    }
    
    
    struct LicenseData:Codable{
        let id:Int
        let imagelink:String
    }
    
}

extension PLMyProfileVC: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
      //  updateUserEmail()
    }
}
