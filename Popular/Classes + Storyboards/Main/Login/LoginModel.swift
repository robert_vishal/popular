//
//  LoginModel.swift
//  Popular
//
//  Created by Appzoc on 23/05/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation


class Log: Codable {
    let errorCode: Int
    let data: [Datum]
    let message: String
    
    
    init(errorCode: Int, data: [Datum], message: String) {
        self.errorCode = errorCode
        self.data = data
        self.message = message
    }
}

class Datum: Codable {
    let userID, otp: Int
    
    
    init(userID: Int, otp: Int) {
        self.userID = userID
        self.otp = otp
    }
}

