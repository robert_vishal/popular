//
//  PLLoginVC.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import TextFieldEffects

class PLLoginVC: UIViewController, InterfaceSettable {
    
    @IBOutlet var nameTF: HoshiTextField!
    @IBOutlet var mobileTF: HoshiTextField!
    
    fileprivate var userName = ""
    fileprivate var password = ""
    
    var parameterLogin = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PLAppState.session.isLoggedIn{
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PLHomeVC") as! PLHomeVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        setUpInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpInterface() {
        
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        nameTF.resignFirstResponder()
        mobileTF.resignFirstResponder()
        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLHomeVC") as! PLHomeVC
        //segueVC.mobileNumber = self.mobileTF.text.unwrap
       // self.present(segueVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(segueVC, animated: true)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
//        loadHomeVC(vc: self)
//        return
        if isValidData() {
            parameterLogin["name"] = nameTF.text?.lowercased().capitalized
            parameterLogin["mobile_number"] = mobileTF.text
            parameterLogin["device_type"] = "ios"
            parameterLogin["device_token"] = PLAppState.session.deviceTocken
            getLoginWeb()
        }
    }
    
    private func isValidData() -> Bool {
        if !BaseValidator.isNotEmpty(string: nameTF.text) {
            
            showBanner(message: "Please Enter Your Name")
            return false
        } else if !BaseValidator.isNotEmpty(string: mobileTF.text) {
            
            showBanner(message: "Please Enter Mobile Number")
            return false
            
        } else if !BaseValidator.isValid(digits: mobileTF.text!) {
            
            showBanner(message: "Please Enter Valid Mobile Number")
            return false
        }else if let num = mobileTF.text{
            //print("Num count",num.count)
            if !(num.count <= allowedPhoneDigits) {
                showBanner(message: "Please Enter Valid Mobile Number")
                return false
            }
        }
        if let name = nameTF.text, name.containSpecialCharacters(){
            showBanner(message: "No Special Characters are allowed")
            return false
        }
        
        return true
    }
    
    
    
    func loginWithUser(resultData:[String:Any])
    {
        let userID = (resultData["data"] as! [String:Any]) ["user_id"] as? Int ?? 0
        let otp = (resultData["data"] as! [String:Any]) ["otp"] as? String ?? ""
        let userName = (resultData["data"] as! [String:Any]) ["name"] as? String ?? ""
        let isEmployee = (resultData["data"] as! [String:Any]) ["is_employee"] as? Int ?? 0
        print("OOTTPP : ",otp)
        let segueVC = self.storyboard!.instantiateViewController(withIdentifier: "PLOTPVerificationVC") as! PLOTPVerificationVC
         //showBanner(message: "\(otp.base64Decoded() ?? "")")
         segueVC.mobileNumber = self.mobileTF.text.unwrap
         segueVC.otpFrom = otp.base64Decoded() ?? ""
         segueVC.userID = userID
         segueVC.userName = userName
         segueVC.isEmployee = isEmployee
         self.navigationController?.pushViewController(segueVC, animated: true)
    }
    
}

// Mark:- Handling text fields
extension PLLoginVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return restrictTextFor(limit: allowedPhoneDigits, fullString: textField.text!, range: range, string: string)

//        guard let text = textField.text else { return true }
//        let newLength = text.count + string.count - range.length
//        return newLength <= allowedPhoneDigits
    }

}

// Mark:- Handling web service
extension PLLoginVC {
    private func getLoginWeb(){
        BaseThread.asyncMain {
            WebServices.postMethod(url:"login", parameter: self.parameterLogin, CompletionHandler: { (isFetched, result) in
                    if isFetched
                    {
                        self.loginWithUser(resultData:result)
                    }
                    else
                    {
                        //showBanner(message: "Oops error..!")
                    }
            })
        }
    }
}










