//
//  PLLoginPopupVC.swift
//  Popular
//
//  Created by Appzoc on 12/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

protocol LoginPopUpDelegate {
    func didFinishLogin()
}

class PLLoginPopupVC: UIViewController {

    @IBOutlet var nameTF: UITextField!
    @IBOutlet var mobileTF: UITextField!
    
    final var delegate: LoginPopUpDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileTF.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    @IBAction func submitBTNTapped(_ sender: UIButton) {

        var parameterLogin = Dictionary<String,String>()
        if isValidData() {
            parameterLogin["name"] = nameTF.text?.lowercased().capitalized
            parameterLogin["mobile_number"] = mobileTF.text
            parameterLogin["device_type"] = "ios"
            parameterLogin["device_token"] = PLAppState.session.deviceTocken
        
            WebServices.postMethod(url:"login" ,
                                   parameter: parameterLogin,
                                   CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        BaseThread.asyncMain {
                            let segueVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PLOTPPopupVC") as! PLOTPPopupVC
                            let userID = (result["data"] as! [String:Any]) ["user_id"] as? Int ?? 0
                            let otp = (result["data"] as! [String:Any]) ["otp"] as? String ?? ""
                            let isEmployee = (result["data"] as! [String:Any]) ["is_employee"] as? Int ?? 0
                            segueVC.otpFrom = otp.base64Decoded() ?? ""
                            print("OOTTPP : ",otp.base64Decoded() ?? "")
                            segueVC.userID = userID
                            segueVC.userName = self.nameTF.text!
                            segueVC.mobileNumber = self.mobileTF.text!
                            segueVC.isEmployee = isEmployee
                            self.present(segueVC)
                        }

                    }
                    else
                    {
                        // showBanner(message: "Oops error..!")
                    }
            })

        }
    }
    
    @IBAction func dismissPopupTapped(_ sender: UIButton) {
        if nameTF.isFirstResponder {
            nameTF.resignFirstResponder()
        }else if mobileTF.isFirstResponder{
            mobileTF.resignFirstResponder()
        }else{
            dismiss()
        }
    }
    
    private func isValidData() -> Bool {
        if !BaseValidator.isNotEmpty(string: nameTF.text) {
            showBanner(message: "Please Enter Your Name")
            return false
        }else if nameTF.text!.count < 3{
            showBanner(message: "Name must be atleast 3 letter long.")
            return false
        }
        else if !BaseValidator.isNotEmpty(string: mobileTF.text) {
            showBanner(message: "Please Enter Mobile Number")
            return false
        }
        else if !BaseValidator.isValid(digits: mobileTF.text!)
        {
            showBanner(message: "Please Enter Valid Mobile Number")
            return false
        }else if let num = mobileTF.text{
            //print("Num count",num.count)
            if !(num.count <= allowedPhoneDigits){
                showBanner(message: "Please Enter Valid Mobile Number")
                return false
            }
        }
        if let name = nameTF.text, name.containSpecialCharacters(){
            showBanner(message: "No Special Characters are allowed")
            return false
        }
        
        return true
    }

}

extension PLLoginPopupVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return restrictTextFor(limit: allowedPhoneDigits, fullString: textField.text!, range: range, string: string)
//        guard let text = textField.text else { return true }
//        let newLength = text.count + string.count - range.length
//        return newLength <= allowedPhoneDigits
    }
    
}
