//
//  PLContactReferalVC.swift
//  Popular
//
//  Created by Appzoc on 03/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLContactReferalVC: UIViewController,SideOptionsDelegate,DateTimeDelegate {
    
    var popUPType = ""
    var typeID = -1
    
    var locArry = [String:Any]()
    
    final var isFromUsedCarDetail: Bool = false
    
    @IBOutlet var popUpTypeLB: UILabel!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var mobilenumberTF: UITextField!
    @IBOutlet var locationTF: UITextField!
    
    
    var postParaMeters = [String:Any]()
    private var isLocationGetOngoing: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.popUpTypeLB.text = popUPType
        //self.mobilenumberTF.text = PLAppState.session.userMobileNumber
        //self.nameTF.text = PLAppState.session.userName
        
        postParaMeters["userid"] = PLAppState.session.userID
        postParaMeters["name"] = PLAppState.session.userName
        postParaMeters["type"] = typeID
        postParaMeters["call_date"] = "18-8-208"
        postParaMeters["from_call_time"] = "11:45 AM"
        postParaMeters["to_call_time"] = "11:45 PM"
        postParaMeters["carmodelid"] = "11:45 PM"
        
        getLocationWithoutActivity()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //http://app.appzoc.com/popular/backend/api/getBranches
    
    @IBAction func dismisPopUpBT(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }

        self.didMissVC()
    }
    
    @IBAction func selectLocationBT(_ sender: UIButton) {
        
        if locArry.isEmpty, !isLocationGetOngoing {
            getLocation()
        }else {
            if !locArry.isEmpty {
                showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! [[String:Any]] ))

            }

        }
        
        
    }
    
    @IBAction func callBackTapped(_ sender: UIButton) {
        showDateTimePopUp(presentOn: self, delegate: self)
    }
    
    //Mark:- delegate from the date and
    func selectedDateAndTime(dateString: String, startTimeString: String, endTimeString: String) {
        
    }
    
    //Mark:- delegate from side menu options
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
        //print(data.id,data.name)
        self.locationTF.text = data.name
        
        postParaMeters["locationid"] = "11:45 PM"
        
    }
    
    
    
    @IBAction func submitBT(_ sender: UIButton) {
        
        
        if nameTF.text?.count != 0
        {
            if mobilenumberTF.text?.count != 0
            {
                if locationTF.text?.count != 0
                {
                    postEnq()
                }
                else
                {
                    showBanner(message: "Please Select Location ")
                }
            }
            else
            {
                showBanner(message: "Please Enter Mobile Number ")
            }
        }
        else
        {
            showBanner(message: "Please Enter Name ")
        }
        
        
    }
    
    
    
    func getLocation()
    {
        BaseThread.asyncMain {
            
            
            
            WebServices.getMethodWith(url:"getBranches" ,
                                      parameter: ["":""],
                                      CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        self.locArry = result
                        
                    }
                    else
                    {
                        
                        
                        
                    }
            })
            
            
            
        }
}
    

    func getLocationWithoutActivity()
    {
        BaseThread.asyncMain {
            
            self.isLocationGetOngoing = true

            
            WebServices.getMethodWithOutActivity(url:"getBranches" ,
                                      parameter: ["":""],
                                      CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        self.locArry = result
                        
                    }
                    else
                    {
                        
                        
                        
                    }
                    
                    BaseThread.asyncMain {
                        self.isLocationGetOngoing = false
                    }

            })
            
            
            
        }
    }
    
}
extension PLContactReferalVC
{
    
    func postEnq()
    {
        
        BaseThread.asyncMain {
            
            
            
            WebServices.postMethod(url:"carmodelEnquiry" ,
                                   parameter: self.postParaMeters,
                                   CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        //self.didMissVC()
                        
                        
                        
                        
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        
                        if self.typeID == -2
                        {
                            nextVC.dismisType = 1
                        }
                        else
                        {
                            nextVC.dismisType = 0
                        }
                        
                        
                        
                        self.present(nextVC, animated: true, completion: nil)
                        
                        
                        
                    }
                    else
                    {
                        
                        
                        
                    }
            })
            
            
            
        }
        
        
    }
    
    func didMissVC()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
