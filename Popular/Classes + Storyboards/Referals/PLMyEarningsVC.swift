//
//  PLMyEarningsVC.swift
//  Popular
//
//  Created by Appzoc on 03/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLMyEarningsVC: UIViewController {
    
    @IBOutlet var ErnAmountLB: UILabel!
    
    @IBOutlet var ReferalLB: UILabel!
    
    @IBOutlet var ValidReferalLB: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.getdata()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func BackButtonTapped(_ sender: UIButton) {
        
        dismiss()
    }
    @IBAction func but_HowToReedeem(_ sender: UIButton) {
        
    
        
        if isNetworkAvailabel() {
            
            //HowToReedemVC
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "HowToReedemVC") as! HowToReedemVC
            self.present(vc, animated: true)

        }
        else
        {
            
            showBanner(title: "Network Error", message: "No internet connectivity")
            
        }
        
    }
    
    @IBAction func RedeemcashTapped(_ sender: UIButton) {
        
        if isNetworkAvailabel() {
            
            //HowToReedemVC
            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "RedeemCashVC") as! RedeemCashVC
            self.present(vc, animated: true)
            
        }
        else
        {
            
            showBanner(title: "Network Error", message: "No internet connectivity")
            
        }

        
    }
    
    
    func getdata()
    {
        
        var param = [String:Any]()
        param["user_id"] = PLAppState.session.userID
        
        WebServices.postMethod(url:"getMyEarnings", parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    let da = result["data"] as! [String:Any]
                    let earned = da["earned"] as! Int
                    let valid_referals = da["valid_referals"] as! Int
                    let referals = da["referals"] as! Int
                    
                   self.ErnAmountLB.text = "₹ " + earned.description
                    self.ReferalLB.text = referals.description
                    self.ValidReferalLB.text = valid_referals.description
                }
                else
                {
                    //showBanner(title: "Server Error", message: "Something went wrong while connecting")
                }
                
                
                
                
        })
    }
    


}
