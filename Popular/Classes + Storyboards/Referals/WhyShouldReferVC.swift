//
//  WhyShouldReferVC.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import WebKit

class WhyShouldReferVC: UIViewController {

    @IBOutlet var listWV: UIWebView!
     let activity = ActivityIndicator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackBTNTapped(_ sender: UIButton) {
        
        dismiss()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getdata()
        
        
    }
    

    func webViewDidStartLoad(_ webView: UIWebView) {
       
        activity.start()
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activity.stop()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
       activity.stop()
    }
    
    func getdata()
    {
        var param = [String:Any]()
        param["note"] = "refer"
        
        WebServices.postMethod(url:"getNotes", parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    let da = result["data"] as! [String:Any]
                   // let das = da[0]
                    let description = da["description"] as! String
                    
                    self.listWV.loadHTMLString(description, baseURL: nil)
                    //let textT = description[0][""]
                    
                }
                else
                {
                    //showBanner(title: "", message: "Favourite list cleared", color: .green)
                }
                
                
                
                
        })
    }

}
