//
//  ServiceENQPopup.swift
//  Popular
//
//  Created by Appzoc on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class ServiceENQPopup: UIViewController,SideOptionsDelegate, UITextFieldDelegate {
    
    var isnewcarref = false
    
    
    @IBOutlet var vechicleNumber: UITextField!
    
    @IBOutlet var popUpTypeLB: UILabel!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var mobilenumberTF: UITextField!
    @IBOutlet var locationTF: UITextField!
    
    var postParaMeters = [String:Any]()
    var locArry = [String:Any]()
    private var isLocationGetOngoing: Bool = false
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popUpTypeLB.text = "I Would Like To Refer"
        //self.mobilenumberTF.text = PLAppState.session.userMobileNumber
        //self.nameTF.text = PLAppState.session.userName
        
        
        postParaMeters["user_id"] = PLAppState.session.userID
       
        
        
            postParaMeters["type_id"] = "3"
        
        
        
        getLocationWithoutActivity()

        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismisPopUpBT(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }
        dismiss()
    }
    
    @IBAction func selectLocationBT(_ sender: UIButton) {
        
        if locArry.isEmpty, !isLocationGetOngoing {
            getLocation()
        }else {
            if !locArry.isEmpty {
                showSideOption(presentOn: self, delegate: self, source: PLSideOptionsModel.getData(fromArray: self.locArry["data"] as! [[String:Any]], idKey: "id", nameKey: "name", isStringId: false))

            }

        }

        
        
        //(fromArray: self.locArry["data"] as! [[String:Any]], isStringId = false)
    }
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        
      //  self.locationTF.text = data.name
        
        
//        postParaMeters["location_id"] = data.id
        
    }
    
    
    func getLocation()
    {
        BaseThread.asyncMain {
            
            self.isLocationGetOngoing = true
            
            WebServices.getMethodWith(url:"get/locations?type=2" ,
                                      parameter: [:],
                                      CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        self.locArry = result
                        
                    }
                    else
                    {
                        
                        
                        
                    }
                    
                    BaseThread.asyncMain {
                        self.isLocationGetOngoing = false
                    }
            })
            
            
            
        }
    }
    
    
    func getLocationWithoutActivity()
    {
        BaseThread.asyncMain {
            
            self.isLocationGetOngoing = true
            
            WebServices.getMethodWithOutActivity(url:"get/locations?type=2" ,
                                                 parameter: [:],
                                      CompletionHandler:
                { (isFetched, result) in
                    if isFetched
                    {
                        self.locArry = result
                        
                    }
                    else
                    {
                        
                        
                        
                    }
                    
                    BaseThread.asyncMain {
                        self.isLocationGetOngoing = false
                    }
            })
            
            
            
        }
    }

    
    @IBAction func submitBT(_ sender: UIButton) {
        
        
        if nameTF.text?.count != 0
        {
            if mobilenumberTF.text?.count != 0
            {
                
                if BaseValidator.isNotEmpty(string: mobilenumberTF.text), BaseValidator.isValid(digits: mobilenumberTF.text!) {
                    if vechicleNumber.text?.count != 0
                    {
                        if (locationTF.text ?? "").count != 0
                        {
                            postParaMeters["name"] = nameTF.text
                            postParaMeters["mobile_number"] = mobilenumberTF.text
                            postParaMeters["vehicle_number"] = vechicleNumber.text
                            postParaMeters["location_id"] = locationTF.text ?? ""
                            postEnq()
                        }
                        else
                        {
                            showBanner(message: "Please Select Location")
                        }
                    }
                    else
                    {
                        showBanner(message: "Please Enter Vehicle Number ")
                    }

                }else {
                    showBanner(message: "Please Enter Valid Mobile Number ")
                }
                
            }
            else
            {
                showBanner(message: "Please Enter Mobile Number ")
            }
        }
        else
        {
            showBanner(message: "Please Enter Name ")
        }
        
        
    }
    
    
    /// text field handling - 13 digit phone validation
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobilenumberTF {
            return restrictTextFor(limit: allowedPhoneDigits, fullString: textField.text!, range: range, string: string)
            
        }else {
            return true
        }
        
    }

    
    
    func postEnq()
    {
        WebServices.postMethod(url:"post_refer" ,
                               parameter: self.postParaMeters,
                               CompletionHandler:
            { (isFetched, result) in
                if isFetched
                {
                    //self.didMissVC()
                    BaseThread.asyncMain {
                        let nextVC =  self.storyBoardNewCars?.instantiateViewController(withIdentifier: "PLThankYouVC") as! PLThankYouVC
                        nextVC.dismisType = 0
                        self.present(nextVC, animated: true, completion: nil)

                    }
                    
                }
                else
                {
                    
                    
                    
                }
        })
        
        
    }
    
    
}
