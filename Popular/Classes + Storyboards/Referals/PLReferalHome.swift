//
//  PLReferalHome.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit


class PLReferalHome: UIViewController {
    
    @IBOutlet weak var referalHomeTV: UITableView!
    
    let tabSource = ["Why Should I Refer ?","Let me earn","My Network","My Earnings","Contact Us"]
    var isErnClicked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func reloadVCFromPopUps() {
        //print("reloadVCFromPopUps")
        referalHomeTV.reloadData()
        if let cell = referalHomeTV.cellForRow(at: IndexPath(row: 1, section: 1)) as? ReferalTVCEXP {
            //print("Cell")
            cell.listCV.reloadData()
        }
        
    }
    
    func isGotoNext() -> Bool
    {
        
        
        let id = PLAppState.session.userID
        
        if id != 0
        {
            if isNetworkAvailabel() {
                
                return true
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
        }
        else
        {
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
        return false
        
    }
    
    
}

extension PLReferalHome: UITableViewDelegate,UITableViewDataSource
{
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
            return 5
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1
        {
            if isErnClicked
            {
                return 2
            }
            else
            {
               return 1
            }

        }
        else
        {
           return 1
        }
        
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 1
        {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "ReferalTVCEXP") as! ReferalTVCEXP
            
            headerCell.prsentVC1 = self
            
            return headerCell
        }
        else
        {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "ReferalTVC") as! ReferalTVC
            
            headerCell.tittleLB.text = self.tabSource[indexPath.section]
            
            if indexPath.section == 1
            {
                if isErnClicked
                {

                    headerCell.tittleLB.textColor = UIColor.white
                    headerCell.baseVIew.backgroundColor = UIColor.from(hex: "2EB0B5")
                    headerCell.IconIMV.image = #imageLiteral(resourceName: "letmeearn_white")
                    headerCell.rightArrowImage.setImageColor(.white)
                }
                else
                {
                    headerCell.tittleLB.textColor = UIColor.black
                    headerCell.baseVIew.backgroundColor = UIColor.white
                    headerCell.IconIMV.image = #imageLiteral(resourceName: "letmeearn")
                    headerCell.rightArrowImage.setImageColor(.black)

                }
            }
            
            
            switch indexPath.section {
            case 0: //why should i refer - section
                   headerCell.IconIMV.image = #imageLiteral(resourceName: "whyshouldrefer.png")
                   headerCell.rightArrowImage.isHidden = false

                break
            case 1: // let me earn section
                //headerCell.IconIMV.image = #imageLiteral(resourceName: "letmeearn")
                headerCell.rightArrowImage.isHidden = false

                break
            case 2:// my network
                headerCell.IconIMV.image = #imageLiteral(resourceName: "mynetwork.png")
                headerCell.rightArrowImage.isHidden = false

                break
            case 3: // my earnings
                headerCell.IconIMV.image = #imageLiteral(resourceName: "myearnings.png")
                headerCell.rightArrowImage.isHidden = false

                break
            case 4: // contact us
                headerCell.IconIMV.image = #imageLiteral(resourceName: "contact-us.png")
                headerCell.rightArrowImage.isHidden = true
                break
            default:
                //print("defa")
                break
            }
            
            
            

            return headerCell
        }
        
        
        
    }
    
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 1
        {
            return 167
        }
        
        return 84.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //print(indexPath.section,indexPath.row)
        
        self.referalHomeTV.deselectRow(at: indexPath, animated: false)
        
        if indexPath.section == 1
        {
            let headerCell = tableView.cellForRow(at: indexPath) as! ReferalTVC
            if isErnClicked
            {
                headerCell.IconIMV.image = #imageLiteral(resourceName: "letmeearn_white")

                isErnClicked = false

                
                UIView.transition(with: tableView,
                                  duration: 0.35,
                                  options: .transitionCrossDissolve,
                                  animations: { self.referalHomeTV.reloadData() })
                
            }
            else
            {
                isErnClicked = true
                headerCell.IconIMV.image = #imageLiteral(resourceName: "letmeearn")

                UIView.transition(with: tableView,
                                  duration: 0.35,
                                  options: .transitionCrossDissolve,
                                  animations: { self.referalHomeTV.reloadData() })
                
            }
        }
        
        
        
        //WhyShouldReferVC
        
        switch indexPath.section {
        case 0:
           
            if isNetworkAvailabel() {
                
                let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "WhyShouldReferVC") as! WhyShouldReferVC
                self.present(vc, animated: true)
                
                
                
            }
            else
            {
                
                showBanner(title: "Network Error", message: "No internet connectivity")
                
            }
            
            break
        case 1:
            
            break
        case 2:
            
            if isGotoNext()
            {
                let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "MyNetworkVC") as! MyNetworkVC
                self.present(vc, animated: true)
            }
            
            break
        case 3:
            
                 if isGotoNext()
                 {
                    let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "PLMyEarningsVC") as! PLMyEarningsVC
                    self.present(vc, animated: true)
                 }
            
             //MyNetworkVC
            
            break
        case 4:
            
            
               makeCallTo(number: emergancyNumber)
//
//            if isGotoNext()
//            {
//            let vc = UIStoryboard(name: "Referals", bundle: nil).instantiateViewController(withIdentifier: "PLContactReferalVC") as! PLContactReferalVC
//            self.present(vc, animated: true)
//            //PLContactReferalVC
//
//            }
            
            break
        default:
            //print("defa")
            break
        }
        
        
    }
    
}
