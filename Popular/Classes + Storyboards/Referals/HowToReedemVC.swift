//
//  HowToReedemVC.swift
//  Popular
//
//  Created by Appzoc on 05/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class HowToReedemVC: UIViewController {

    @IBOutlet var DataWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getdata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNTapped(_ sender: UIButton) {
        if let parentVC = self.presentingViewController as? PLReferalHome {
            parentVC.reloadVCFromPopUps()
        }

        dismiss()
    }
    
    func getdata()
    {
        var param = [String:Any]()
        param["note"] = "redeem"
        
        WebServices.postMethod(url:"getNotes", parameter: param ,
                               CompletionHandler:
            { (isFetched, result) in
                
                
                if isFetched
                {
                    let da = result["data"] as! [String:Any]
                    //let das = da[0]
                    let description = da["description"] as! String
                    
                    self.DataWebView.loadHTMLString(description, baseURL: nil)
                    //let textT = description[0][""]
                    
                }
                else
                {
                    //showBanner(title: "", message: "Favourite list cleared", color: .green)
                }
                
                
                
                
        })
    }

}
