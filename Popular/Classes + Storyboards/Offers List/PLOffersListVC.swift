//
//  PLOffersListVC.swift
//  Popular
//
//  Created by Appzoc on 09/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit




class PLOfferListCell: UITableViewCell{
    
    @IBOutlet var offerImage: BaseImageView!
    @IBOutlet var offerTitleLBL: UILabel!
    @IBOutlet var leftColorIndicator: UIView!
    @IBOutlet var offerExpireLBL: UILabel!
    @IBOutlet var offerDescriptionLBL: UILabel!
    
    func configureWithData(data: PLOffersListVC.PLOfferListModel.OfferData, position: PLOffersListVC.Position){
        offerTitleLBL.text = data.title
        offerExpireLBL.text = "Expires on \(data.expiryDate)"
        offerDescriptionLBL.text = data.description
        let imageURL = URL(string: data.image)
        offerImage.kf.setImage(with: imageURL, placeholder: nil, options: [.requestModifier(getImageAuthorization())])
        switch position {
        case .odd:
            leftColorIndicator.backgroundColor = UIColor.from(hex: "EC4C2E")
        case .even:
            leftColorIndicator.backgroundColor = UIColor.from(hex: "00B0B4")
        }
        
    }
}

class PLOffersListVC: UIViewController {

    var currenPage:Int = 0
    var lastPage:Int = 0
    var totalItemCount:Int = 0
    
    var offerTableSource:[PLOfferListModel.OfferData] = []
    
    @IBOutlet var tableRef: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOfferList(page: 1)
    }
    

    @IBAction func backtapped(_ sender: UIButton) {
        self.dismiss()
    }
    

}


extension PLOffersListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerTableSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"PLOfferListCell") as! PLOfferListCell
        let position:Position = indexPath.row % 2 == 0 ? .even : .odd
        cell.configureWithData(data: offerTableSource[indexPath.row], position: position)
        if indexPath.row == offerTableSource.count - 1 {
            loadMore(completion: {
                DispatchQueue.main.async {
                    self.tableRef.reloadData()
                }
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let size = tableView.frame.height * 3 / 5
        return size
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "PLOffersDetailVC") as! PLOffersDetailVC
        vc.passingExpiryDate = "Expires on " + offerTableSource[indexPath.row].expiryDate
        vc.passingOfferID = offerTableSource[indexPath.row].id
        let passingColor = indexPath.row % 2 == 0 ? UIColor.from(hex: "00B0B4") : UIColor.from(hex: "EC4C2E")
        vc.indicatorColor = passingColor
        self.present(vc)
    }
    
}

extension PLOffersListVC: UITableViewDataSourcePrefetching{


    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        print("Using Prefetch DataSource")
        for index in indexPaths{
            if offerTableSource.count <= index.row{
                if currenPage < lastPage{
                    self.loadMore(completion: {
                        DispatchQueue.main.async {
                            self.tableRef.reloadRows(at: [index], with: .none)
                        }
                    })
                }
            }
        }

    }

}

extension PLOffersListVC {
    
    func getOfferList(page:Int){
        var param = [String:Int]()
        param["page"] = page
        WebServices.postMethod(url: "offerslist", parameter: param) { (isComplete, data) in
            if isComplete{
                print(data.description)
                if let jsonData = serializeJSON(json: data["data"] as Any){
                    do{
                        let decoder = JSONDecoder()
                        let parsedData = try decoder.decode(PLOfferListModel.self, from: jsonData)
                        for item in parsedData.data{
                            self.offerTableSource.append(item)
                        }
                        self.currenPage = parsedData.currentPage
                        self.lastPage = parsedData.lastPage
                        self.totalItemCount = parsedData.totalItems
                        self.tableRef.reloadData()
                    }catch{
                        print(error)
                    }
                }
            }
        }
    }
    
    
    func loadMore( completion: @escaping () -> ()){
        if currenPage < lastPage{
            var param = [String:Int]()
            currenPage += 1
            param["page"] = currenPage
            WebServices.postMethodWithoutActivity(url: "offerslist", parameter: param) { (isComplete, data) in
                if isComplete{
                    if let jsonData = serializeJSON(json: data["data"] as Any){
                        do{
                            let decoder = JSONDecoder()
                            let parsedData = try decoder.decode(PLOfferListModel.self, from: jsonData)
                            for item in parsedData.data{
                                self.offerTableSource.append(item)
                            }
                            //self.currenPage = parsedData.currentPage
                            self.lastPage = parsedData.lastPage
                            self.totalItemCount = parsedData.totalItems
                            //self.tableRef.reloadData()
                            completion()
                        }catch{
                            print(error)
                            completion()
                        }
                    }else{ completion() }
                }else{
                    completion()
                }
            }
        }
    }
    
    
    struct PLOfferListModel:Codable{
        let currentPage:Int
        let data:[OfferData]
        let lastPage:Int
        let totalItems:Int
        
        private enum CodingKeys: String, CodingKey{
            case currentPage = "current_page", data, lastPage = "last_page", totalItems = "total"
        }
        
        struct OfferData:Codable{
            let id:Int
            let categoryID:Int?
            let title:String
            let description:String
            let expiryDate:String
            let image:String
            let type:String
            
            private enum CodingKeys:String, CodingKey{
                case id, categoryID = "category_id", title, description, expiryDate = "expiry_date", image , type
            }
        }
    }
    
    enum Position {
        case odd
        case even
    }
}





