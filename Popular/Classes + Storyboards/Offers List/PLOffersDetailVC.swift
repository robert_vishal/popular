//
//  PLOffersDetailVC.swift
//  Popular
//
//  Created by Appzoc on 09/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLOffersDetailVC: UIViewController {

    
    @IBOutlet var offerTitleLBL: UILabel!
    @IBOutlet var offerExpireDateLBL: UILabel!
    @IBOutlet var offerDescriptionLBL: UILabel!
    @IBOutlet var colorIndicatorView: UIView!
    @IBOutlet var offerImage: BaseImageView!
    
    var passingOfferID:Int = 0
    var passingExpiryDate:String = ""
    var indicatorColor:UIColor = UIColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpWebCall()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    
    func setUpWebCall(){
        WebServices.postMethod(url: "offersdetail", parameter: ["offerid":"\(passingOfferID)"]) { (isComplete, json) in
            if isComplete{
                if let jsonData = serializeJSON(json: json["data"] as Any as Any){
                    do{
                        let decoder = JSONDecoder()
                        let parsedData = try decoder.decode([OfferDetailModel].self, from: jsonData)
                        if parsedData.count > 0{
                            self.updateInterface(with: parsedData[0])
                        }
                    }catch{
                        print(error)
                    }
                }
            }
        }
    }
    
    func updateInterface(with data:OfferDetailModel){
        offerTitleLBL.text = data.title
        offerExpireDateLBL.text = passingExpiryDate
        offerDescriptionLBL.text = data.description
        colorIndicatorView.backgroundColor = indicatorColor
        let imageURL = URL(string: data.image)
        offerImage.kf.setImage(with: imageURL, options: [.requestModifier(getImageAuthorization())])
    }
    
    struct OfferDetailModel: Codable{
        let id:Int
        let title:String
        let description:String
        let image:String
        let type:String
    }
   

}
