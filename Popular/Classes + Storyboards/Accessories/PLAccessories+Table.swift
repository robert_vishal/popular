//
//  PLAccessories+Table.swift
//  Popular
//
//  Created by Appzoc on 28/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class PLAccesoryChooseCarTVC : UITableViewCell{
    @IBOutlet var mainLabel: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    @IBOutlet var clearBTN: UIButton!
    
}

class PLAccesoryTVC: UITableViewCell{
    
    @IBOutlet var accessoryImage: UIImageView!
    @IBOutlet var accessoryName: UILabel!
    @IBOutlet var accessoryModel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var callBackBTN: UIButton!
    
    
    func configureWithData(_ data:PLAccesoriesVC.AccessoryModel){
        accessoryName.text = data.name
        accessoryImage.kf.setImage(with: URL(string: baseURLImage + data.image), placeholder: nil, options: [.requestModifier(getImageAuthorization())])
        accessoryModel.text = data.model
        priceLabel.text = "Rs : \(data.price)/-"
        callBackBTN.tag = data.id
    }
    
}

extension PLAccesoriesVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("\n\nCurrent Number of rows : ",currentSource.count + 1,"\n\n")
        return currentSource.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let accessoryCell = tableView.dequeueReusableCell(withIdentifier: "PLAccesoryTVC") as! PLAccesoryTVC
        let chooseCarCell = tableView.dequeueReusableCell(withIdentifier: "PLAccesoryChooseCarTVC") as! PLAccesoryChooseCarTVC
        
        if indexPath.row == 0{
            if let carModel = selectedCarModel{
                chooseCarCell.mainLabel.text = carModel.name
                chooseCarCell.clearBTN.isHidden = false
                chooseCarCell.arrowImage.isHidden = true
            }else{
                chooseCarCell.mainLabel.text = "Choose a Car"
                chooseCarCell.clearBTN.isHidden = true
                chooseCarCell.arrowImage.isHidden = false
            }
            return chooseCarCell
        }else{
            accessoryCell.configureWithData(currentSource[indexPath.row - 1])
            return accessoryCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0{
//            return 76
//        }else{
//            return 136
//        }
        return UITableViewAutomaticDimension
    }
    
}
