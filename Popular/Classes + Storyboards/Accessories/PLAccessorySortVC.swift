//
//  PLAccessorySortVC.swift
//  Popular
//
//  Created by Appzoc on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLAccessorySortVC: UIViewController {

    @IBOutlet var orderLatestLBL: UILabel!
    @IBOutlet var orderLowToHighLBL: UILabel!
    @IBOutlet var orderHighToLowLBL: UILabel!
    
    var delegate:AccessorySortSelector!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //accessorySortType = .latest
        setUpInterface()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        transitionBackgroundToBlack(view: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.clear
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view{
                self.dismiss()
            }
        }
    }
    
    func setUpInterface(){
        switch accessorySortType {
        case .latest:
            orderLatestLBL.textColor = UIColor.from(hex: "00B0B4")
        case .lowToHigh:
            orderLowToHighLBL.textColor = UIColor.from(hex: "00B0B4")
        case .highToLow:
            orderHighToLowLBL.textColor = UIColor.from(hex: "00B0B4")
        default:
            resetLabelColors()
        }
    }
    
    
    @IBAction func orderLatestTapped(_ sender: UIButton) {
        resetLabelColors()
        orderLatestLBL.textColor = UIColor.from(hex: "00B0B4")
        accessorySortType = .latest
        delegate.setSortCategory()
        self.dismiss()
    }
    
    @IBAction func orderLowToHighTapped(_ sender: UIButton) {
        resetLabelColors()
        orderLowToHighLBL.textColor = UIColor.from(hex: "00B0B4")
        accessorySortType = .lowToHigh
        delegate.setSortCategory()
        self.dismiss()
    }
    
    @IBAction func orderHighToLowTapped(_ sender: UIButton) {
        resetLabelColors()
        orderHighToLowLBL.textColor = UIColor.from(hex: "00B0B4")
        accessorySortType = .highToLow
        delegate.setSortCategory()
        self.dismiss()
    }
    //Blue Color  - 00B0B4
    
    func resetLabelColors(){
        orderLatestLBL.textColor = UIColor.darkGray
        orderLowToHighLBL.textColor = UIColor.darkGray
        orderHighToLowLBL.textColor = UIColor.darkGray
    }
}
