//
//  PLAccessorySearchPopUpVC.swift
//  Popular
//
//  Created by Appzoc on 29/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLAccessorySearchPopUpVC: UIViewController, UITextFieldDelegate{

    @IBOutlet var tableRef: UITableView!
    
    @IBOutlet var searchField: UITextField!
    
    @IBOutlet var dataEmptyIndicatorLBL: UILabel!
    
    
    var delegate:AccessorySearch!
    
    var searchResultData:[PLAccesoriesVC.AccessoryModel] = []{
        didSet{
            if searchResultData.count == 0{
                DispatchQueue.main.async {
                    self.dataEmptyIndicatorLBL.isHidden = false
                }
            }else{
                DispatchQueue.main.async {
                    self.dataEmptyIndicatorLBL.isHidden = true
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchField.delegate = self
        searchField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        searchField.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.view{
                //dismiss()
            }

        }
    }
    
    
    @IBAction func requestCallBackTapped(_ sender: UIButton) {
        //print("Request callback")
        if PLAppState.session.isLoggedIn{
            WebServices.postMethod(url: "product_callback", parameter: ["user_id":"\(PLAppState.session.userID)","product_id":"\(sender.tag)"]) { (isComplete, json) in
                if isComplete{
                    let segueVC = UIStoryboard(name: "Accessory", bundle: nil).instantiateViewController(withIdentifier: "PLAccessoriesCallBackVC") as! PLAccessoriesCallBackVC
                    self.present(segueVC)
                }
            }
        }else{
            let segueVC = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            present(segueVC, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func searchTapped(_ sender: UIButton) {
        //print("Searching")
        if let text = searchField.text, text.isNotEmpty{
            performSearch(searchKey: text)
        }
        //self.dismiss()
    }
    
    
    @IBAction func closeSearchTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    func performSearch(searchKey: String) {
        WebServices.getMethodWith(url: "get/accessories", parameter: ["category":"null","sort":"0","search":"\(searchKey)"]) { (isComplete, json) in
            if isComplete{
                print("Completed")
                do{
                    let decoder = JSONDecoder()
                    let trueJson = json["data"]
                    if let jsonData = serializeJSON(json: trueJson as Any){
                        let parsedData = try decoder.decode([PLAccesoriesVC.AccessoryModel].self, from: jsonData)
                        self.searchResultData = parsedData
                        self.tableRef.reloadData()
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    @objc func editingChanged(_ sender:UITextField){
       // print("Text : ",sender.text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = searchField.text, text.isNotEmpty{
            performSearch(searchKey: text)
        }
        textField.resignFirstResponder()
        //self.dismiss()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("Did End Editing")
        if let text = textField.text, text.isNotEmpty{
            performSearch(searchKey: text)
        }
    }

}

extension PLAccessorySearchPopUpVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PLAccesoryTVC") as! PLAccesoryTVC
        cell.configureWithData(searchResultData[indexPath.row])
        return cell
    }
    
}
