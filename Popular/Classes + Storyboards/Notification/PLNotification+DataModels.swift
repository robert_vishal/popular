//
//  PLNotification+DataModels.swift
//  Popular
//
//  Created by Appzoc on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation

extension PLNotificationVC{
    
    struct NotificationSection {
        var sectionName:String
        var sectionID:Int
        var isSelected:Bool
    }
    
    
    
    struct NotificationDataMain: Codable{
        
        let all:[NotificationData]
        let newcar:[NotificationData]
        let usedcar:[NotificationData]
        let employee:[NotificationData]?
        
        struct NotificationData:Codable{
            let id:Int
            let title:String
            let body:String
            let image:String?
            let type:String
            let created_at:String
            let updated_at:String
        }
    }
    
}
