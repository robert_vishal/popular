//
//  PLNotification+Table.swift
//  Popular
//
//  Created by Appzoc on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class PLNotificationTVC: UITableViewCell{
    
    @IBOutlet var notificationTitleLBL: UILabel!
    @IBOutlet var descriptionLBL: UILabel!
    @IBOutlet var timeLBL: UILabel!
    
    @IBOutlet var notificationSectionImage: UIImageView!
    
    @IBOutlet var backgroundImage: UIImageView!
    
    private let newCarImage:UIImage = UIImage(named: "new cars")!
    private let usedCarImage:UIImage = UIImage(named: "used")!
    private let hrImage:UIImage = UIImage(named: "hr")!
    private let serviceImage:UIImage = UIImage(named: "service-1")!
    
    func configure(with data:PLNotificationVC.NotificationDataMain.NotificationData){
        notificationTitleLBL.text = data.title
        descriptionLBL.text = data.body

        switch data.type{
        case "newcar":
            notificationSectionImage.image = newCarImage
        case "usedcar":
            notificationSectionImage.image = usedCarImage
        case "employee":
            notificationSectionImage.image = hrImage
        default:
            notificationSectionImage.image = serviceImage
        }
        if let imageLink = data.image{
            let imageURL = URL(string: baseURLImage + imageLink )
            backgroundImage.kf.setImage(with: imageURL, options: [.requestModifier(getImageAuthorization())])
        }
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        if let date = dateFormatter.date(from: data.created_at){
            timeLBL.text = Date().offset(from: date) + " ago"
        }
    }
    
    override func prepareForReuse() {
        backgroundImage.image = UIImage(named: "placeholder_Image")
    }
}


extension PLNotificationVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"PLNotificationTVC") as! PLNotificationTVC
        cell.configure(with: currentSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height/2 - 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellData = currentSource[indexPath.row]
        
        print("\(cellData.type)")
        
        switch cellData.type {
        case "newcarnotification","newcaroffer":
            let segueVC = storyBoardNewCars?.instantiateViewController(withIdentifier: "PLNewCarListVC") as! PLNewCarListVC
            self.present(segueVC)
        case "usedcaroffer","usedcarnotification":
            let segueVC = storyBoardUsedCars?.instantiateViewController(withIdentifier: "PLUsedCarsHomeVC") as! PLUsedCarsHomeVC
            self.present(segueVC)
        case "referraloffer","referralcommonnotification","referralnotification":
            let vc = storyBoardOffers?.instantiateViewController(withIdentifier: "PLOffersListVC") as! PLOffersListVC
            self.present(vc)
        case "servicecommonnotification","serviceoffer","servicenotification":
            let segueVC = self.storyBoardService!.instantiateViewController(withIdentifier: "PLServiceHome")
            present(segueVC)
        case "insurancenotification","insuranceoffer":
            if PLAppState.session.isLoggedIn {
                let segueVC = self.storyBoardInsurance!.instantiateViewController(withIdentifier: "PLInsuranceVC")
                present(segueVC, animated: true, completion: nil)
            }else {
              //  showLoginPopUp()
            }
        case "drivingschooloffer","drivingschoolnotification":
            let segueVC = self.storyBoardLearnDriving!.instantiateViewController(withIdentifier: "PLLearnDrivingVC")
            present(segueVC)
        case "accessoriesnotification","accessoriesoffer":
            print("Coming Soon")
        case "hrmsoffer","hrms":
            print("Employee")
        case "lifestyleoffer","lifestylenotification":
            print("Coming Soon")
        default:
            break
            //print("Null Section") 
        }
    }
}
