//
//  PLNotification+Collection.swift
//  Popular
//
//  Created by Appzoc on 11/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLNotificationCVC: UICollectionViewCell{
    
    @IBOutlet var notificationSectionLBL: UILabel!
    
}


extension PLNotificationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellData = sectionList[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLNotificationCVC", for: indexPath) as! PLNotificationCVC
        cell.notificationSectionLBL.text = cellData.sectionName
        if cellData.isSelected{
            cell.notificationSectionLBL.textColor = UIColor.white
        }else{
            cell.notificationSectionLBL.textColor = UIColor.white.withAlphaComponent(0.5)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        deselectAllSection()
        sectionList[indexPath.row].isSelected = true
        selectSection(section: indexPath.row)
        collectionRef.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if sectionList.count == 3{
            let size = collectionView.frame.size
            return CGSize(width: size.width/3 - 10, height: size.height)
        }else{
            let fillerLabel = UILabel()
            fillerLabel.text = sectionList[indexPath.row].sectionName
            fillerLabel.font = UIFont.systemFont(ofSize: 24)
            //fillerLabel.sizeToFit()
            let returnWidth = fillerLabel.intrinsicContentSize.width
            var excessWidth = collectionView.frame.width - (returnWidth * 4)
            if excessWidth <= 0 {
                excessWidth = 1
            }
            return CGSize(width: returnWidth + (excessWidth/4) - 10 , height: fillerLabel.intrinsicContentSize.height + 10)
        }
    }
    
    func deselectAllSection(){
        for i in 0..<sectionList.count{
            sectionList[i].isSelected = false
        }
    }
    
    
    
    
    
}
