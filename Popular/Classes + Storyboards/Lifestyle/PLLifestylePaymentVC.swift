//
//  PLLifestylePaymentVC.swift
//  Popular
//
//  Created by Appzoc on 06/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestylePaymentVC: UIViewController {
    
    
    
    @IBOutlet var totalAmountLBL: UILabel!
    @IBOutlet var taxAmountLBL: UILabel!
    @IBOutlet var amountPayableLBL: UILabel!
    
    var totalAmount:String = ""
    var taxAmount:String = ""
    var amountPayable:String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
    }
    
    func setUpInterface(){
        DispatchQueue.main.async {
            self.totalAmountLBL.text = "₹ " + self.totalAmount
            self.taxAmountLBL.text = "+ ₹ " + self.taxAmount
            self.amountPayableLBL.text = "₹ " + self.amountPayable
        }
        
    }

    @IBAction func proceedTapped(_ sender: UIButton) {
        showBanner(message: "Coming Soon")
        return
        let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestyleAddressVC") as! PLLifestyleAddressVC
        segueVC.totalAmount = totalAmount
        segueVC.taxAmount = taxAmount
        segueVC.amountPayable = amountPayable
        self.present(segueVC)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    

}
