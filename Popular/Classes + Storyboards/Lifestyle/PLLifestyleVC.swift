//
//  PLLifestyleVC.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleVC: UIViewController {
    
    @IBOutlet var tableRef: UITableView!
    @IBOutlet var cartCountLBL: UILabel!
    @IBOutlet var favouriteCountLBL: UILabel!
    @IBOutlet var noDataIndicatorLBL: UILabel!
    
    var lifestyleSource:[LifeStyleDataModel] = []{
        didSet{
            if lifestyleSource.count == 0{
                noDataIndicatorLBL.isHidden = false
            }else{
                noDataIndicatorLBL.isHidden = true
            }
        }
    }
    
    var searchTerm:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpWebCall()
        setupCountLabels()
        getCountWebCall()
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    @IBAction func viewCartTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn{
            let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestyleCartVC") as! PLLifestyleCartVC
            present(segueVC)
        }else{
            let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            self.present(vc)
        }
    }
    
    @IBAction func favouritesListTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn{
            let segueVC = self.storyBoardFavourites!.instantiateViewController(withIdentifier: "PLFavouritesVC") as! PLFavouritesVC
            segueVC.selectedHeader = 2
            present(segueVC)
        }else{
            let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            self.present(vc)
        }
    }
    
    @IBAction func performSearch(_ sender: UITextField) {
            searchTerm = sender.text ?? ""
            setUpWebCall()
    }
    
    
    @IBAction func addToFavouritesTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn{
            let cell = tableRef.cellForRow(at: IndexPath(row: sender.tag, section: 1)) as! PLLifestyleTVC
            cell.toggeleFavourite()
            //http://popular.dev.webcastle.in/api/product_fav?user_id=31&product_id=1
            let param = ["user_id":"\(PLAppState.session.userID)","product_id":"\(lifestyleSource[sender.tag].id)"]
            print("Paramters ",param.description)
            WebServices.getMethodWithOutActivity(url: "product_fav", parameter: param) { (isComplete, json) in
                if isComplete{
                    print("Favourite Added")
                    self.getCountWebCall()
                }
            }
        }else{
            let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            self.present(vc)
        }
    }
    
    @IBAction func addToCart(_ sender: UIButton) {
        //http://popular.dev.webcastle.in/api/add/cart?type=1
        /*
        user_id:1
        product_id:22
        type:1
 */
        if PLAppState.session.isLoggedIn{
            let param = ["type":"1","user_id":PLAppState.session.userID.description,"product_id":"\(lifestyleSource[sender.tag].id)"]
            print("Paramteres : ",param.description)
            WebServices.postMethodWithoutActivity(url: "add/cart", parameter: param) { (isComplete, json) in
                if isComplete{
                    print("Added to cart")
                    self.getCountWebCall()
                }
            }
        }else{
            let vc = storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            self.present(vc)
        }
    }
    
    //http://popular.dev.webcastle.in/api/get/counts/1
    func getCountWebCall(){
        if PLAppState.session.isLoggedIn{
            WebServices.getMethodWith(url: "get/counts/\(PLAppState.session.userID)", parameter: nil) { (isComplete, json) in
                if isComplete{
                    DispatchQueue.main.async {
                        if let cartCount = json["cart_count"] as? Int{
                            if cartCount > 0{
                                self.cartCountLBL.isHidden = false
                                self.cartCountLBL.text = cartCount.description
                            }else{
                                self.cartCountLBL.isHidden = true
                            }
                        }
                        if let favCount = json["fav_count"] as? Int{
                            if favCount > 0{
                                self.favouriteCountLBL.isHidden = false
                                self.favouriteCountLBL.text = favCount.description
                            }else{
                                self.favouriteCountLBL.isHidden = true
                            }
                        }
                    }
                }
            }
        }else{
            DispatchQueue.main.async {
                self.favouriteCountLBL.isHidden = true
                self.cartCountLBL.isHidden = true
            }
        }
    }
    
    func setupCountLabels(){
        DispatchQueue.main.async {
            self.cartCountLBL.layer.cornerRadius = 8.25
            self.cartCountLBL.clipsToBounds = true
            self.favouriteCountLBL.layer.cornerRadius = 8.25
            self.favouriteCountLBL.clipsToBounds = true
        }
    }
    
    func setUpWebCall(){
        let param = ["search":"\(searchTerm)","user_id":PLAppState.session.userID.description]
        print("Parameter ",param.description)
        WebServices.getMethodWith(url: "get/lifestyle", parameter: param) { (isComplete, json) in
            if isComplete{
                do{
                    let decoder = JSONDecoder()
                    let trueJson = json["data"]
                    if let jsonData = serializeJSON(json: trueJson as Any){
                        let parsedData = try decoder.decode([LifeStyleDataModel].self, from: jsonData)
                        self.lifestyleSource = parsedData
                        self.tableRef.reloadData()
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
}
