//
//  PLLifestyle+Table.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleSearchTVC: UITableViewCell, UITextFieldDelegate{
    
    @IBOutlet var searchField: UITextField!
    
    var delegate:PLLifestyleVC?
    
    func setValueChangeListener() {
        searchField.delegate = self
        searchField.addTarget(self, action: #selector(performSearch), for: .editingChanged)
    }
    
    @objc func performSearch(){
        //print("Performing Search")
        //delegate?.setUpWebCall()
    }
    
    
    
}

class PLLifestyleTVC: UITableViewCell{
    
    @IBOutlet var itemImage: BaseImageView!
    @IBOutlet var itemName: UILabel!
    @IBOutlet var itemModel: UILabel!
    @IBOutlet var itemPrice: UILabel!
    @IBOutlet var favBTN: UIButton!
    @IBOutlet var addToCartBTN: UIButton!
    
    
    let filledImage = UIImage(named: "1x_0008_favourite_white_fill_Red")
    let unfilledImage = UIImage(named: "1x_0007_favourite_gre_outline")
    
    func configureCell(with data:PLLifestyleVC.LifeStyleDataModel, index:IndexPath){
        itemImage.kf.setImage(with: URL(string: baseURLImage + data.image), options: [.requestModifier(getImageAuthorization())])
        itemName.text = data.name
        itemModel.text = "Model : " + data.model
        itemPrice.text = "Rs: " + data.price.description + "/-"
        favBTN.tag = index.row
        addToCartBTN.tag = index.row
        if data.favProduct == 0{
            self.favBTN.setImage(unfilledImage, for: [])
        }else{
            self.favBTN.setImage(filledImage, for: [])
        }
    }
    
    func toggeleFavourite(){
        DispatchQueue.main.async {
            if self.favBTN.imageView?.image == self.filledImage{
                self.favBTN.setImage(self.unfilledImage, for: [])
            }else{
                self.favBTN.setImage(self.filledImage, for: [])
            }
        }
    }
    
}

extension PLLifestyleVC: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return lifestyleSource.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchCell = tableView.dequeueReusableCell(withIdentifier: "PLLifestyleSearchTVC") as! PLLifestyleSearchTVC
        let lifeStyleCell = tableView.dequeueReusableCell(withIdentifier: "PLLifestyleTVC") as! PLLifestyleTVC
        if indexPath.section == 0{
            //searchCell.setValueChangeListener()
            searchCell.delegate = self
            return searchCell
        }else{
            lifeStyleCell.configureCell(with: lifestyleSource[indexPath.row], index: indexPath)
            return lifeStyleCell
        }
    }
}

extension PLLifestyleVC: UITableViewDataSourcePrefetching{
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    
    
}
