//
//  PLLifestyleSubmitVC.swift
//  Popular
//
//  Created by Appzoc on 06/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleSubmitVC: UIViewController {
    
    
    
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var addressLine1: UILabel!
    @IBOutlet var addressLine2: UILabel!
    @IBOutlet var addressLine3: UILabel!
    @IBOutlet var phoneNumberLBL: UILabel!
    
    
    //Passing Data
    
    var name:String = ""
    var streetAddress:String = ""
    var address:String = ""
    var landmark:String = ""
    var phoneNumber:String = ""
    
    
    var delegate:PLLifestyleAddressVC?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInterface()
    }

    @IBAction func editTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    
    @IBAction func deliverTapped(_ sender: UIButton) {
        delegate?.deliverHere(viewController: self)
    }
    
    func setUpInterface(){
        DispatchQueue.main.async {
            self.nameLBL.text = self.name
            self.addressLine1.text = self.streetAddress
            self.addressLine2.text = self.address
            self.addressLine3.text = self.landmark
            self.phoneNumberLBL.text = "Ph : " + self.phoneNumber
        }
    }
    

}
