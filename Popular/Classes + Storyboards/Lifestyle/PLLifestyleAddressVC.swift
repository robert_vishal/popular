//
//  PLLifestyleAddressVC.swift
//  Popular
//
//  Created by Appzoc on 06/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLLifestyleAddressVC: UIViewController {

    
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var phoneNumberTF: UITextField!
    @IBOutlet var cityTF: UITextField!
    @IBOutlet var localityTF: UITextField!
    @IBOutlet var flatNoTF: UITextField!
    @IBOutlet var pincodeTF: UITextField!
    @IBOutlet var stateTF: UITextField!
    @IBOutlet var landmarkTF: UITextField!
    
    //Passing Data
    var totalAmount = ""
    var taxAmount = ""
    var amountPayable = ""
    
    
    var stateList:[State] = []
    
    var selectedStateID:Int = 0
    
//    var sideOptionsState:[PLSideOptionsModel]{
//        var array = stateList.map({PLSideOptionsModel(sourceName: $0.name, sourceID: $0.id)})
//        return array
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.delegate = self
    }
        
    
    override func viewWillAppear(_ animated: Bool) {
        getStates()
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func stateTapped(_ sender: UIButton) {
        
        nameTF.resignFirstResponder()
        phoneNumberTF.resignFirstResponder()
        cityTF.resignFirstResponder()
        localityTF.resignFirstResponder()
        flatNoTF.resignFirstResponder()
        pincodeTF.resignFirstResponder()
        stateTF.resignFirstResponder()
        landmarkTF.resignFirstResponder()
        showSideOption(presentOn: self, delegate: self, source: stateList.map({PLSideOptionsModel(sourceName: $0.name, sourceID: $0.id)}))
    }
    
    //http://popular.dev.webcastle.in/api/add/product_purchase
    @IBAction func proceedTapped(_ sender: BaseButton) {
//        var param = ["user_id":"33",
//            "billAmount":"12300",
//            "discount":"12",
//            "tax":"12",
//            "totalAmount":"200",
//            "name":"varun",
//            "mobile":"15448890",
//            "city":"1",
//            "state":"1",
//            "pin":"6802",
//            "address":"india kerala",
//            "street":"kochi",
//            "landmark":"landmark",
//            "cart_id":"6"]
        
        //var param = ["user_id":"1395"]
        guard validate() else { return }
        let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestyleSubmitVC") as! PLLifestyleSubmitVC
        segueVC.name = self.nameTF.text!
        segueVC.streetAddress = self.flatNoTF.text!
        segueVC.address = self.landmarkTF.text!
        segueVC.landmark = self.cityTF.text! + " " + self.stateTF.text!
        segueVC.phoneNumber = self.phoneNumberTF.text!
        segueVC.delegate = self
        self.present(segueVC)
        
    }
    
    
    func deliverHere(viewController:UIViewController){
        let param = ["user_id":"\(PLAppState.session.userID)",
            "billAmount":amountPayable,
            "discount":"12",
            "tax":taxAmount,
            "totalAmount":totalAmount,
            "name":nameTF.text!,
            "mobile":phoneNumberTF.text!,
            "city":cityTF.text!,
            "state":selectedStateID.description,
            "pin":pincodeTF.text!,
            "address":flatNoTF.text!,
            "street":"kochi",
            "landmark":landmarkTF.text!,
            "cart_id":"6"]
        
        WebServices.postMethod(url: "add/product_purchase", parameter: param) { (isComplete, json) in
            if isComplete{
                print("Complete")
                let segueVC = UIStoryboard(name: "Lifestyle", bundle: nil).instantiateViewController(withIdentifier: "PLLifestyleThankYouVC") as! PLLifestyleThankYouVC
                viewController.present(segueVC)
            }
        }
    }
    
    
    //http://popular.dev.webcastle.in/api/get/states
    func getStates(){
        WebServices.getMethodWith(url: "get/states", parameter: nil) { (isComplete, json) in
            if isComplete{
                do{
                    let decoder = JSONDecoder()
                    if let jsonData = serializeJSON(json: json["data"] as Any){
                        let parsedData = try decoder.decode([State].self, from: jsonData)
                        self.stateList = parsedData
                    }
                }catch{
                    print(error)
                }
            }
        }
    }
    
    func validate() -> Bool{
        if BaseValidator.isNotEmpty(string: nameTF.text){
            
        }else{
            showBanner(message: "Please Enter Name")
            return false
            
        }
        
        if let phone = phoneNumberTF.text, phone.isNotEmpty, BaseValidator.isValid(digits: phone), phone.count == 10{
            
        }else{
            showBanner(message: "Please Enter Valid Mobile Number")
            return false
            
        }
        
        if let city = cityTF.text, city.isNotEmpty{
            
        }else{
            showBanner(message: "Please Enter City")
            return false
            
        }
        
        if let address = localityTF.text, address.isNotEmpty{
            
        }else{
            showBanner(message: "Please Enter Address")
            return false
            
        }
       
        
        if let address = flatNoTF.text, address.isNotEmpty{
            
        }else{
            showBanner(message: "Please Enter Address")
            return false
            
        }
        
        if let landmark = landmarkTF.text, landmark.isNotEmpty{
            
        }else{
            showBanner(message: "Please Enter Landmark")
            return false
            
        }
        
        if let pin = pincodeTF.text, pin.isNotEmpty, BaseValidator.isValid(digits: pin){
            
        }else{
            showBanner(message: "Please Enter Valid Pincode")
            return false
            
        }
        
        if selectedStateID != 0{
            
        }else{
            showBanner(message: "Please Select State")
            return false
            
        }
        
        return true
    }
    
    

}

extension PLLifestyleAddressVC: UITextFieldDelegate, SideOptionsDelegate{
    
    func sideOptionSelected(data: PLSideOptionsModel, indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.selectedStateID = data.id
            self.stateTF.text = data.name
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTF{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }else{
            return true
        }
    }
    
    
    
}

extension PLLifestyleAddressVC{
    
    struct State: Codable{
        let id:Int
        let name:String
    }
    
}
