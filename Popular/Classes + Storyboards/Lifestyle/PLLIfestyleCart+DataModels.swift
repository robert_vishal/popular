//
//  PLLIfestyleCart+DataModels.swift
//  Popular
//
//  Created by Appzoc on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//


extension PLLifestyleCartVC{
    
    struct LifestyleCartDataModel: Codable{
        let cartCount:Int
        let totalAmount:Int
        let tax:Int
        let amountPayable:Int
        let cart:[CartModel]
        
        private enum CodingKeys: String, CodingKey{
            case cartCount = "cart_count", totalAmount = "total_amount", tax, amountPayable = "amount_payable", cart
        }
        
        struct CartModel: Codable{
            let id:Int
            let productID:Int
            let userID:Int
            var quantity:Int
            let purchaseID:Int?
            let cartProduct:CartProduct?
            
            private enum CodingKeys: String, CodingKey{
                case id, productID = "product_id", userID = "user_id", quantity, purchaseID = "purchase_id", cartProduct = "cart_product"
            }
            
            struct CartProduct: Codable{
                let id:Int
                let name:String
                let model:String
                let price:Int
                let carModel:Int
                let carVariant:Int
                let category:Int
                let type:Int
                let image:String
                
                private enum CodingKeys: String, CodingKey{
                    case id, name, model, price, carModel = "car_model", carVariant = "car_varient", category, type, image
                }
            }
        }
    }
    
}
