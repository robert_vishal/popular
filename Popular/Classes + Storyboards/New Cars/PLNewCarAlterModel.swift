//
//  PLNewCarAlterModel.swift
//  Popular
//
//  Created by Appzoc on 02/10/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

struct NewCarSelectedParent{
    var selectedParent:Int
    var sectionSelected:Bool
}

struct NewCarVariantData:Codable{
    let variantDetail:[VariantDetail]
    
    init() {
        self.variantDetail = []
    }
    
    struct VariantDetail: Codable{
        let id:Int
        let metallicPrice:String
        let nonMetallicPrice:String
        let features:[Features]
        
        struct Features: Codable{
            let id:Int
            let name:String
            let parentDetails:Int
            let parent:[ParentData]
            
            struct ParentData: Codable{
                let id:Int?
                let name:String?
                let details:[Details]?
                
                let key:String?
                let value:String?
                
                struct Details: Codable{
                    let id:Int
                    let value:String
                    let key:String
                    
                    private enum CodingKeys: String, CodingKey{
                        case id, key = "kei", value = "values"
                    }
                }
                
                private enum CodingKeys: String, CodingKey{
                    case id, name, details, key = "kei", value = "values"
                }
            }
            
            private enum CodingKeys: String, CodingKey{
                case id, name, parentDetails = "parent_details", parent
            }
        }
        
        private enum CodingKeys: String, CodingKey{
            case id, metallicPrice = "metallic", nonMetallicPrice = "non_metallic", features
        }
    }
    
    
    private enum CodingKeys: String, CodingKey{
        case variantDetail = "variant_details"
    }
}
