//
//  PLGalleryVC.swift
//  Popular
//
//  Created by admin on 03/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher



class galleryHeaderCVC: UICollectionViewCell {
    
    @IBOutlet weak var titleLBL: UILabel!
}

class galleryDataCVC: UICollectionViewCell {
    
    @IBOutlet weak var prevImg: UIImageView!
    
}


class PLGalleryVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var headerCV: UICollectionView!
    @IBOutlet weak var galleryCV: UICollectionView!
    
    
    var isVideo = false
    
    var selectedHeader = 0
    
    var indX = 0
    
    var interior = [String]()
    var exterior = [String]()
    var videos = [String]()
    var previewImages = [String]()
    
    var dSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerCV.dataSource = self
        headerCV.delegate = self
        galleryCV.dataSource = self
        galleryCV.delegate = self
        
        self.videos = dataSource.carListData[indX].videos
        self.previewImages = [""]
        
        for it in dataSource.carListData[indX].GalImages
        {
            if it.part == 0
            {
                self.exterior.append(it.path)
            }
            else
            {
                self.interior.append(it.path)
            }
            
        }
        
        switch selectedHeader{
        case 0:
            dSource = interior
            isVideo = false
        case 1:
            dSource = exterior
            isVideo = false
        case 2:
            dSource = videos
            isVideo = true
        default:
            break
            //print("Default Choice")
        }
        
        DispatchQueue.main.async {
            self.headerCV.reloadData()
            self.galleryCV.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func but_BackTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        
    }
    func getFavCount(CidFor:Int)
    {
        
        WebServices.postMethod(url:"getGallerydetail", parameter: ["carmodelid":CidFor] ,
                                              CompletionHandler:
            { (isFetched, result) in
                
                //VarientSource = [varientDetails]()
                
                guard let _ = result["data"] as? [String:Any] else { return }
                
                //let cou = dt["count"] as! Int
                
                
                
        })
        
    }

}

extension PLGalleryVC {
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        NSInteger viewWidth = COLLECTIONVIEW_WIDTH;
//        NSInteger totalCellWidth = CELL_WIDTH * _numberOfCells;
//        NSInteger totalSpacingWidth = CELL_SPACING * (_numberOfCells -1);
//
//        NSInteger leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2;
//        NSInteger rightInset = leftInset;
        
        if collectionView == headerCV {
        
        let viewWidth = Int(collectionView.frame.width)
        let totalCellWidth = 225
        let totalSpacingWidth = 0
        let leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2
        return UIEdgeInsetsMake(0, CGFloat(leftInset), 0, CGFloat(leftInset))
        } else {
            return UIEdgeInsetsMake(5, 10, 5, 10)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == galleryCV {
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.invalidateLayout()
            
            return CGSize(width: ((self.view.frame.width/2)-15), height:((self.view.frame.width / 2) * 130/191 - 20))
        } else {
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.invalidateLayout()
            return CGSize(width: 75, height: self.headerCV.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == headerCV {
            return 3
            
        }
        else {
            if selectedHeader == 0 {
               return dSource.count
            } else if selectedHeader == 1 {
                return dSource.count
            } else {
                return dSource.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == headerCV {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryHeaderCVC", for: indexPath) as! galleryHeaderCVC
           //set title
            if indexPath.row == 0 {
                cell.titleLBL.text = "Exterior"
                
            } else if indexPath.row == 1 {
                cell.titleLBL.text = "Interior"
            } else {
                cell.titleLBL.text = "Videos"
            }
           
            //set color for font
            if indexPath.row == selectedHeader {
                cell.titleLBL.textColor = UIColor.white
            } else {
                cell.titleLBL.textColor = UIColor.lightGray
            }
            
           return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryDataCVC", for: indexPath) as! galleryDataCVC
            
            
             let mURL = imageURL + dSource[indexPath.row]
             let previewImageURL = dSource[indexPath.row]
//             print("Priewview URL ",previewImageURL)
//            print("Index Path ",indexPath.row)
            
            cell.prevImg.layer.masksToBounds = false
            cell.prevImg.layer.shadowColor = UIColor.lightGray.cgColor
            cell.prevImg.layer.shadowOpacity = 1
            cell.prevImg.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.prevImg.layer.shadowRadius = 3
            
           /* cell.prevImg.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            cell.prevImg.layer.shouldRasterize = true
            cell.prevImg.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
            */
            
            
//            if indexPath.row == 0 {
//                cell.prevImg.kf.setImage(with: URL(string:mURL), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//
//            } else if indexPath.row == 1 {
//                cell.prevImg.kf.setImage(with: URL(string:mURL), placeholder: nil, options: [.requestModifier(getImageAuthorization())], progressBlock: nil, completionHandler: nil)
//            } else {
//                cell.prevImg.image = #imageLiteral(resourceName: "placeholder_Image.jpg")
//
//            }
            if selectedHeader == 2 {
                if let thumbnailImage = getThumbnailImage(forUrl: previewImageURL){
                    //print("Thumbnail Image = ",thumbnailImage)
                    cell.prevImg.kf.setImage(with: URL(string: thumbnailImage))
                }
            } else{
                cell.prevImg.kf.setImage(with: URL(string:mURL), options: [.requestModifier(getImageAuthorization())])
            }
          
            
            return cell
        }
        
    }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            _ = indexPath.row
            if collectionView == headerCV {
                if indexPath.row == 0 {
                    
                    isVideo = false
                    
                    dSource = interior
                    selectedHeader = 0
                    galleryCV.reloadData()
                    headerCV.reloadData()
                    
                }
                else if indexPath.row == 1 {
                    
                    isVideo = false
                    dSource = exterior
                    selectedHeader = 1
                    galleryCV.reloadData()
                    headerCV.reloadData()
                    
                }
                else if indexPath.row == 2
                {
                    
                    isVideo = true
                   dSource = dataSource.carListData[indX].videos
                    selectedHeader = 2
                    galleryCV.reloadData()
                    headerCV.reloadData()
                }
                
            }
            else
            {
                //ImagePreviewVC
                
                
                
                
                //print(indexPath.row)
                
                if isVideo
                {
                    
                    
                  //  print(dSource)

                    
                    let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "PLVideoPlayer") as! PLVideoPlayer
                    
                       nextVC.UrlPlay = dSource[indexPath.row]
                    
                    self.present(nextVC, animated: true, completion: nil)
                    
                    
                    
                    
                }
                else
                {
                
                let nextVC =  storyBoardNewCars?.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
                
                nextVC.imageURLSource = dSource
                
                self.present(nextVC, animated: true, completion: nil)
                }
                
            }

            
            
        
    }
    
    
}
