//
//  ImagePreviewVC.swift
//  Popular
//
//  Created by Appzoc on 06/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//


import UIKit
    
    class PLImagePreviewCVC1: UICollectionViewCell {
        
        @IBOutlet var previewImage: UIImageView!
        @IBOutlet var imageScrollView: UIScrollView!
        
    }
    
class ImagePreviewVC: UIViewController {
        
        @IBOutlet var titleLBL: UILabel!
        @IBOutlet var largeImageCV: UICollectionView!
        @IBOutlet var thumbnailImageCV: UICollectionView!
        
        var needsBaseURL:Bool = true
 
        final var imageURLSource: [String] = []
        
  
        var vehicleID:String = ""
        
        var currentIndex:Int = 0
        
        private var CVWidth: CGFloat = 0.0
        private var selectedIndexPath: IndexPath = IndexPath(item: 0, section: 0)
        private var chosenImageIndexpath: IndexPath = IndexPath(item: 0, section: 0)
        private var willDisplayImageIndexpath: IndexPath = IndexPath(item: 0, section: 0)
        private var endDisplayImageIndexpath: IndexPath = IndexPath(item: 0, section: 0)

        override func viewDidLoad() {
            super.viewDidLoad()
            
            //        imageScrollView.minimumZoomScale = 1.0
            //        imageScrollView.maximumZoomScale = 5.0
            //
            //        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
            //        doubleTapGest.numberOfTapsRequired = 2
            //        imageScrollView.addGestureRecognizer(doubleTapGest)
            //
            //        imageScrollView.delegate = self
            //       // imageScrollView.backgroundColor = UIColor(red: 90, green: 90, blue: 90, alpha: 0.90)
            //        imageScrollView.alwaysBounceVertical = false
            //        imageScrollView.alwaysBounceHorizontal = false
            //        imageScrollView.showsVerticalScrollIndicator = true
            //        imageScrollView.flashScrollIndicators()
            //
            //        imageScrollView.minimumZoomScale = 1.0
            //        imageScrollView.maximumZoomScale = 3.0
            
        }
        
        
        @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
            //        if imageScrollView.zoomScale == 1 {
            //            imageScrollView.zoom(to: zoomRectForScale(scale: imageScrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
            //        } else {
            //            imageScrollView.setZoomScale(1, animated: true)
            //        }
        }
        
        //    func viewForZooming(in scrollView: UIScrollView) -> UIView?
        //    {
        //        let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
        //
        //        return cell.previewImage
        //    }
        
        //    func getSelectedImageView() -> UIImageView {
        //        let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
        //        return cell.previewImage
        //
        //    }
        
        func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
            var zoomRect = CGRect.zero
            let cell = largeImageCV.cellForItem(at: selectedIndexPath) as! PLImagePreviewCVC
            
            zoomRect.size.height = cell.previewImage.frame.size.height / scale
            zoomRect.size.width  = cell.previewImage.frame.size.width  / scale
            //let newCenter = cell.previewImage.convert(center, from: imageScrollView)
            //        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
            //        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
            return zoomRect
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        override func viewDidAppear(_ animated: Bool) {
            //print("viewdidappear....")
            CVWidth = largeImageCV.frame.size.width
            largeImageCV.reloadData()
            thumbnailImageCV.reloadData()

        }
        
        @IBAction func backTapped(_ sender: UIButton) {
            dismiss(animated: true, completion: nil)
            
        }
        

        

        

}


    
    // handling collection view datasources and size
extension ImagePreviewVC: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return imageURLSource.count
            
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PLImagePreviewCVC", for: indexPath) as? PLImagePreviewCVC else { return UICollectionViewCell() }
     
            
             //imageURLSource
            var urlFor = imageURLSource[indexPath.item]
            if needsBaseURL{
                urlFor = imageURL + imageURLSource[indexPath.item]
            }
            
            cell.previewImage.kf.setImage(with: URL(string:urlFor), options: [.requestModifier(getImageAuthorization())])
            
            if collectionView == largeImageCV {
               cell.setImageZooming()
            }else {
                if chosenImageIndexpath == indexPath {
                    cell.imageContainerView.borderColor = .themeRed
                    cell.imageContainerView.borderWidth = 1.0
                }else {
                    cell.imageContainerView.borderColor = .clear
                    cell.imageContainerView.borderWidth = 0.0
                }

            }
            
        
               return cell
        }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == largeImageCV ? CGSize(width: CVWidth, height: 325) : CGSize(width: 128, height: 128)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == largeImageCV {
            titleLBL.text = (indexPath.item + 1).description + " of " + imageURLSource.count.description
            currentIndex = indexPath.row
            willDisplayImageIndexpath = indexPath
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == largeImageCV {
            endDisplayImageIndexpath = indexPath
        }
    }
    
    
}
    
     //handling collection view delegates
extension ImagePreviewVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == thumbnailImageCV {
            largeImageCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            currentIndex = indexPath.row
            //let cell = collectionView.cellForItem(at: indexPath) as! PLImagePreviewCVC
            chosenImageIndexpath = indexPath
            thumbnailImageCV.reloadData()
//            cell.imageContainerView.borderColor = .themeRed
//            cell.imageContainerView.borderWidth = 1.0
        }
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == largeImageCV {
            let visibleIndexPaths = largeImageCV.indexPathsForVisibleItems
            if visibleIndexPaths.contains(willDisplayImageIndexpath) {
                if willDisplayImageIndexpath == endDisplayImageIndexpath {
                    if let indexPathFirst = visibleIndexPaths.first, indexPathFirst == willDisplayImageIndexpath {
                        if let lastIndexPath = visibleIndexPaths.last {
                            chosenImageIndexpath = lastIndexPath
                            thumbnailImageCV.reloadData()
                            thumbnailImageCV.scrollToItem(at: chosenImageIndexpath, at: .centeredHorizontally, animated: true)
                        }
                    }else if let indexPathLast = visibleIndexPaths.last, indexPathLast == willDisplayImageIndexpath {
                        if let firstIndexPath = visibleIndexPaths.first {
                            chosenImageIndexpath = firstIndexPath
                            thumbnailImageCV.reloadData()
                            thumbnailImageCV.scrollToItem(at: chosenImageIndexpath, at: .centeredHorizontally, animated: true)
                        }
                    }
                }else {
                    chosenImageIndexpath = willDisplayImageIndexpath
                    thumbnailImageCV.reloadData()
                    thumbnailImageCV.scrollToItem(at: chosenImageIndexpath, at: .centeredHorizontally, animated: true)
                    
                }
            }else {
                if let indexPath = visibleIndexPaths.first {
                    chosenImageIndexpath = indexPath
                    thumbnailImageCV.reloadData()
                    thumbnailImageCV.scrollToItem(at: chosenImageIndexpath, at: .centeredHorizontally, animated: true)
                }
                
            }
        }
        

        
        
    }
    
}




//
//
//}

