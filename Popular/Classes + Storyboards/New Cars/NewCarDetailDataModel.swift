//
//  NewCarDetailDataModel.swift
//  Popular
//
//  Created by admin on 05/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

//public typealias Json = [String: Any]
//public typealias JsonArray = [Json]

import Foundation

class PLNewcarsDetailModel
{
    var id:Int = 0
    var isnexa:String = ""
    var variants:[Variant] = []
    
    init(){}
    init(dictionary:Json?) {
        guard let dictionary = dictionary else { return }
        id = dictionary["id"] as! Int
        isnexa = dictionary["isnexa"] as! String
        variants = Variant.getValue(fromArray: dictionary["variants"] as! JsonArray)
    }
    
    
    class func getValue(fromArray:JsonArray) -> [PLNewcarsDetailModel]
    {
        var result:[PLNewcarsDetailModel] = []
        for item in fromArray {
            result.append(PLNewcarsDetailModel(dictionary: item))
        }
        return result
    }
    
    
    //  Mark:- Varient model
    class Variant
    {
        var id:Int = 0
        var carmodel_id:String = ""
        var name:String = ""
        var mileage:String = ""
        var transmission:String = ""
        var no_of_gears:String = ""
        var seating:String = ""
        var engine_type:String = ""
        var engine_capacity:String = ""
        var max_power:String = ""
        var max_torque:String = ""
        var drive_train:String = ""
        var fuel_type:String = ""
        var fuel_capacity:String = ""
        var kerb_weight:String = ""
        var gross_weight:String = ""
        var driver_airbag:String = ""
        var passenger_airbag:String = ""
        var headlight_levelling:String = ""
        var front_fog_lamps:String = ""
        var air_conditioning:String = ""
        var music_system:String = ""
        var central_lock:String = ""
        var mirror_type:String = ""
        var power_steering:String = ""
        var tachometer:String = ""
        var gear_position_indicator:String = ""
        var abs:String = ""
        var power_windows:String = ""
        var brakes_type:String = ""
        var front_system:String = ""
        var back_system:String = ""
        var tyre_size:String = ""
        var dual_airbag:String = ""
        var created_at:String = ""
        var updated_at:String = ""
        var night_irvm:String = ""
        var aloy_wheels:String = ""
        var ground_clearance:String = ""
        var horse_power:String = ""
        var isnexa:String = ""
        var variant:String = ""
        var isfavourite:Bool = true
        var start_prize:String = ""
        var end_prize:String = ""
        var video_count:Int = 0
        var image_count:Int = 0
        var variantname:String = ""
        var exterior:String = ""
        var breaks:String = ""
        
//        var features:Features?
//        var suspension:Suspension?
//        var capacity:Capacity?
//        var safety:Safety?
//        var dimension:Dimension?
//        var vehicle_price:VehiclePrice?
//        var colors:[Color] = []
//        var vehicle_images:[VehicleImage] = []
//        var vehicle_videos:[VehicleVideo] = []
//        var performance:Performance?
//        var weight:Weight?
        
        
        var features = [String:String]()
        
        var vehicle_price:VehiclePrice?
        var colors:[Color] = []
        var vehicle_images:[VehicleImage] = []
        var vehicle_videos:[VehicleVideo] = []
        
        var performance = [String:String]()
        var suspension = [String:String]()
        var capacity  = [String:String]()
        var dimension = [String:String]()
        var weight = [String:String]()
        var safety = [String:String]()
        
        init(){}
        init(dictionary:Json?) {
            guard let dictionary = dictionary else { return }
            id = dictionary["id"] as! Int
            carmodel_id = dictionary["carmodel_id"] as! String
            name = dictionary["name"] as! String
            mileage = dictionary["mileage"] as! String
            transmission = dictionary["transmission"] as! String
            no_of_gears = dictionary["no_of_gears"] as! String
            seating = dictionary["seating"] as! String
            engine_type = dictionary["engine_type"] as! String
            engine_capacity = dictionary["engine_capacity"] as! String
            max_power = dictionary["max_power"] as! String
            max_torque = dictionary["max_torque"] as! String
            drive_train = dictionary["drive_train"] as! String
            fuel_type = dictionary["fuel_type"] as! String
            fuel_capacity = dictionary["fuel_capacity"] as! String
            kerb_weight = dictionary["kerb_weight"] as! String
            gross_weight = dictionary["gross_weight"] as! String
            driver_airbag = dictionary["driver_airbag"] as! String
            passenger_airbag = dictionary["passenger_airbag"] as! String
            headlight_levelling = dictionary["headlight_levelling"] as! String
            front_fog_lamps = dictionary["front_fog_lamps"] as! String
            air_conditioning = dictionary["air_conditioning"] as! String
            music_system = dictionary["music_system"] as! String
            central_lock = dictionary["central_lock"] as! String
            mirror_type = dictionary["mirror_type"] as! String
            power_steering = dictionary["power_steering"] as! String
            tachometer = dictionary["tachometer"] as! String
            gear_position_indicator = dictionary["gear_position_indicator"] as! String
            abs = dictionary["abs"] as! String
            power_windows = dictionary["power_windows"] as! String
            brakes_type = dictionary["brakes_type"] as! String
            front_system = dictionary["front_system"] as! String
            back_system = dictionary["back_system"] as! String
            tyre_size = dictionary["tyre_size"] as! String
            dual_airbag = dictionary["dual_airbag"] as! String
            created_at = dictionary["created_at"] as! String
            updated_at = dictionary["updated_at"] as! String
            night_irvm = dictionary["night_irvm"] as! String
            aloy_wheels = dictionary["aloy_wheels"] as! String
            ground_clearance = dictionary["ground_clearance"] as! String
            horse_power = dictionary["horse_power"] as! String
            isnexa = dictionary["isnexa"] as! String
            variant = dictionary["variantname"] as! String
            isfavourite = dictionary["isfavourite"] as! Bool
            start_prize = dictionary["start_prize"] as! String
            end_prize = dictionary["end_prize"] as! String
            breaks = dictionary["break"] as! String
            video_count = dictionary["video_count"] as! Int
            image_count = dictionary["image_count"] as! Int
            variantname = dictionary["variantname"] as! String
            exterior = dictionary["exterior"] as! String
            
//            weight =   Weight.init(dictionary: dictionary["weight"] as? Json)
//            features =  Features.init(dictionary: dictionary["features"] as? Json)
//            performance =  Performance.init(dictionary: dictionary["performance"] as? Json)
//            suspension =  Suspension.init(dictionary: dictionary["suspension"] as? Json)
//            capacity =  Capacity.init(dictionary: dictionary["capacity"] as? Json)
//            safety =  Safety.init(dictionary: dictionary["safety"] as? Json)
//            dimension =  Dimension.init(dictionary: dictionary["dimension"] as? Json)
//            vehicle_price =  VehiclePrice.init(dictionary: dictionary["vehicle_price"] as? Json)
//            colors = Color.getValue(fromArray: (dictionary["color"] as! JsonArray))
//            vehicle_images = VehicleImage.getValue(fromArray: dictionary["vehicle_images"] as! JsonArray)
//            vehicle_videos = VehicleVideo.getValue(fromArray: dictionary["vehicle_videos"] as! JsonArray)
           
            
            vehicle_price =  VehiclePrice.init(dictionary: dictionary["vehicle_price"] as? Json)
            colors = Color.getValue(fromArray: (dictionary["color"] as! JsonArray))
            vehicle_images = VehicleImage.getValue(fromArray: dictionary["vehicle_images"] as! JsonArray)
            vehicle_videos = VehicleVideo.getValue(fromArray: dictionary["vehicle_videos"] as! JsonArray)
            
            performance = dictionary["performance"] as! [String:String]
            suspension = dictionary["suspension"] as! [String:String]
            capacity = dictionary["capacity"] as! [String:String]
            dimension = dictionary["dimension"] as! [String:String]
            weight = dictionary["weight"] as! [String:String]
            safety = dictionary["safety"] as! [String:String]
            features = dictionary["features"] as! [String:String]
        }
        class func getValue(fromArray:JsonArray) -> [Variant]
        {
            var result:[Variant] = []
            for item in fromArray
            {
                result.append(Variant(dictionary: item))
            }
            return result
        }
        
        
        // Mark:- Weight Model
        class Weight
        {
            var lx:String = ""
            var lxi:String = ""
            var v2:String = ""
            
            init(){}
            init(dictionary: Json?) {
                guard let dictionary = dictionary else { return }
                lx = dictionary["lx"] as! String
                lxi = dictionary["lxi"] as! String
                v2 = dictionary["v2"] as! String
            }
            class func getValue(fromArray: JsonArray) -> [Weight]
            {
                var result:[Weight] = []
                for item in fromArray
                {
                    result.append(Weight(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Features Model
        class Features
        {
            var dual_airbag:String = ""
            var night_irvm:String = ""
            var aloy_wheels:String = ""
            var front_fog_lamps:String = ""
            
            init(){}
            init(dictionary: Json?) {
                guard let dictionary = dictionary else { return }
                dual_airbag = dictionary["dual_airbag"] as! String
                night_irvm = dictionary["night_irvm"] as! String
                aloy_wheels = dictionary["aloy_wheels"] as! String
                front_fog_lamps = dictionary["front_fog_lamps"] as! String
            }
            class func getValue(fromArray: JsonArray) -> [Features]
            {
                var result:[Features] = []
                for item in fromArray
                {
                    result.append(Features(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Performance Model
        class Performance
        {
            var engine_type:String = ""
            var engine_capacity:String = ""
            var max_power:String = ""
            var max_torque:String = ""
            var fuel_type:String = ""
            var transmission:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                engine_type = dictionary["engine_type"] as! String
                engine_capacity = dictionary["engine_capacity"] as! String
                max_power = dictionary["max_power"] as! String
                max_torque = dictionary["max_torque"] as! String
                fuel_type = dictionary["fuel_type"] as! String
                transmission = dictionary["transmission"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [Performance]
            {
                var result:[Performance] = []
                for item in fromArray
                {
                    
                    result.append(Performance(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Suspension Model
        class Suspension
        {
            var front_system:String = ""
            var back_system:String = ""
            var tyre_size:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                front_system = dictionary["front_system"] as! String
                back_system = dictionary["back_system"] as! String
                tyre_size = dictionary["tyre_size"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [Suspension]
            {
                var result:[Suspension] = []
                for item in fromArray
                {
                    result.append(Suspension(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Capacity Model
        class Capacity
        {
            var seating:String = ""
            var fuel_capacity:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                seating = dictionary["seating"] as! String
                fuel_capacity = dictionary["fuel_capacity"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [Capacity]
            {
                var result:[Capacity] = []
                for item in fromArray
                {
                    result.append(Capacity(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Safety Model
        class Safety
        {
            var driver_airbag:String = ""
            var headlight_levelling:String = ""
            var front_fog_lamps:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                driver_airbag = dictionary["driver_airbag"] as! String
                headlight_levelling = dictionary["headlight_levelling"] as! String
                front_fog_lamps = dictionary["front_fog_lamps"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [Safety]
            {
                var result:[Safety] = []
                for item in fromArray
                {
                    result.append(Safety(dictionary: item))
                }
                return result
            }
            
        }
        
        // Mark:- Dimension Model
        class Dimension
        {
            var dimension:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                dimension = dictionary["dimension"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [Dimension]
            {
                var result:[Dimension] = []
                for item in fromArray
                {
                    result.append(Dimension(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- vehicle_price Model
        class VehiclePrice
        {
            var metalic_price:String = ""
            var nonMetalice_price:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                metalic_price = dictionary["metalic_price"] as! String
                nonMetalice_price = dictionary["nonMetalice_price"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [VehiclePrice]
            {
                var result:[VehiclePrice] = []
                for item in fromArray
                {
                    result.append(VehiclePrice(dictionary: item))
                }
                return result
            }
            
        }
        
        
        
        
        // Mark:- Color Model
        class Color
        {
            var hexcode:String = ""
            var name:String = ""
            var pivot:Pivot?
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                hexcode = dictionary["hexcode"] as! String
                name = dictionary["name"] as! String
                pivot = Pivot.init(dictionary: dictionary["pivot"] as? Json)
            }
            class func getValue(fromArray : JsonArray) -> [Color]
            {
                var result:[Color] = []
                for item in fromArray
                {
                    result.append(Color.init(dictionary: item))
                }
                return result
            }
            
            // Mark:- Pivot Model
            class Pivot
            {
                var carmodel_id:String = ""
                var color_id:String = ""
                
                init(){}
                
                init(dictionary:Json?) {
                    guard let dictionary = dictionary else { return }
                    carmodel_id = dictionary["carmodel_id"] as! String
                    color_id = dictionary["color_id"] as! String
                }
                class func getValue(fromArray : JsonArray) -> [Pivot]
                {
                    var result:[Pivot] = []
                    for item in fromArray
                    {
                        result.append(Pivot(dictionary: item))
                    }
                    return result
                }
                
            }
            
        }
        
        
        // Mark: - vehicle images model
        class VehicleImage
        {
            var id:Int = 0
            var urls:[String] = []
            var color_id:String = ""
            var carmodel_id:String = ""
            var variant_id:String = ""
            var type_id:String = ""
            var created_at:String = ""
            var updated_at:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                id = dictionary["id"] as! Int
                urls = dictionary["url"] as! [String]
                color_id = dictionary["color_id"] as! String
                carmodel_id = dictionary["carmodel_id"] as! String
                variant_id = dictionary["variant_id"] as! String
                type_id = dictionary["type_id"] as! String
                created_at = dictionary["created_at"] as! String
                updated_at = dictionary["updated_at"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [VehicleImage]
            {
                var result:[VehicleImage] = []
                for item in fromArray
                {
                    result.append(VehicleImage(dictionary: item))
                }
                return result
            }
            
        }
        
        
        // Mark:- Vehicle videos model
        class VehicleVideo
        {
            var id:Int = 0
            var urls:[String] = []
            var color_id:String = ""
            var carmodel_id:String = ""
            var variant_id:String = ""
            var type_id:String = ""
            var created_at:String = ""
            var updated_at:String = ""
            
            init(){}
            
            init(dictionary:Json?) {
                guard let dictionary = dictionary else { return }
                
                id = dictionary["id"] as! Int
                urls = dictionary["url"] as! [String]
                color_id = dictionary["color_id"] as! String
                carmodel_id = dictionary["carmodel_id"] as! String
                variant_id = dictionary["variant_id"] as! String
                type_id = dictionary["type_id"] as! String
                created_at = dictionary["created_at"] as! String
                updated_at = dictionary["updated_at"] as! String
            }
            class func getValue(fromArray : JsonArray) -> [VehicleVideo]
            {
                var result:[VehicleVideo] = []
                for item in fromArray
                {
                    result.append(VehicleVideo(dictionary: item))
                }
                return result
            }
            
        }
        
        
        
    }
    
    
    
}


/*
import Foundation

class Pivot
{
    var carmodel_id:String = ""
    var color_id:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        carmodel_id = dictionary["carmodel_id"] as! String
        color_id = dictionary["color_id"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Pivot]
    {
        var result:[Pivot] = []
        for item in array
        {
            var newItem = Pivot()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Color
{
    var name:String = ""
    var hexcode:String = ""
    var pivot:Pivot = 0
    
    func Populate(dictionary:NSDictionary) {
        
        name = dictionary["name"] as! String
        hexcode = dictionary["hexcode"] as! String
        pivot = dictionary["pivot"] as! Pivot
    }
    class func PopulateArray(array:NSArray) -> [Color]
    {
        var result:[Color] = []
        for item in array
        {
            var newItem = Color()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Variant
{
    var id:Int = 0
    var carmodel_id:String = ""
    var name:String = ""
    var mileage:String = ""
    var transmission:String = ""
    var no_of_gears:String = ""
    var seating:String = ""
    var engine_type:String = ""
    var engine_capacity:String = ""
    var max_power:String = ""
    var max_torque:String = ""
    var drive_train:String = ""
    var fuel_type:String = ""
    var fuel_capacity:String = ""
    var kerb_weight:String = ""
    var gross_weight:String = ""
    var driver_airbag:String = ""
    var passenger_airbag:String = ""
    var headlight_levelling:String = ""
    var front_fog_lamps:String = ""
    var air_conditioning:String = ""
    var music_system:String = ""
    var central_lock:String = ""
    var mirror_type:String = ""
    var power_steering:String = ""
    var tachometer:String = ""
    var gear_position_indicator:String = ""
    var abs:String = ""
    var power_windows:String = ""
    var brakes_type:String = ""
    var front_system:String = ""
    var back_system:String = ""
    var tyre_size:String = ""
    var dual_airbag:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var night_irvm:String = ""
    var aloy_wheels:String = ""
    var ground_clearance:String = ""
    var horse_power:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        id = dictionary["id"] as! Int
        carmodel_id = dictionary["carmodel_id"] as! String
        name = dictionary["name"] as! String
        mileage = dictionary["mileage"] as! String
        transmission = dictionary["transmission"] as! String
        no_of_gears = dictionary["no_of_gears"] as! String
        seating = dictionary["seating"] as! String
        engine_type = dictionary["engine_type"] as! String
        engine_capacity = dictionary["engine_capacity"] as! String
        max_power = dictionary["max_power"] as! String
        max_torque = dictionary["max_torque"] as! String
        drive_train = dictionary["drive_train"] as! String
        fuel_type = dictionary["fuel_type"] as! String
        fuel_capacity = dictionary["fuel_capacity"] as! String
        kerb_weight = dictionary["kerb_weight"] as! String
        gross_weight = dictionary["gross_weight"] as! String
        driver_airbag = dictionary["driver_airbag"] as! String
        passenger_airbag = dictionary["passenger_airbag"] as! String
        headlight_levelling = dictionary["headlight_levelling"] as! String
        front_fog_lamps = dictionary["front_fog_lamps"] as! String
        air_conditioning = dictionary["air_conditioning"] as! String
        music_system = dictionary["music_system"] as! String
        central_lock = dictionary["central_lock"] as! String
        mirror_type = dictionary["mirror_type"] as! String
        power_steering = dictionary["power_steering"] as! String
        tachometer = dictionary["tachometer"] as! String
        gear_position_indicator = dictionary["gear_position_indicator"] as! String
        abs = dictionary["abs"] as! String
        power_windows = dictionary["power_windows"] as! String
        brakes_type = dictionary["brakes_type"] as! String
        front_system = dictionary["front_system"] as! String
        back_system = dictionary["back_system"] as! String
        tyre_size = dictionary["tyre_size"] as! String
        dual_airbag = dictionary["dual_airbag"] as! String
        created_at = dictionary["created_at"] as! String
        updated_at = dictionary["updated_at"] as! String
        night_irvm = dictionary["night_irvm"] as! String
        aloy_wheels = dictionary["aloy_wheels"] as! String
        ground_clearance = dictionary["ground_clearance"] as! String
        horse_power = dictionary["horse_power"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Variant]
    {
        var result:[Variant] = []
        for item in array
        {
            var newItem = Variant()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Weight
{
    var lx:String = ""
    var lxi:String = ""
    var v2:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        lx = dictionary["lx"] as! String
        lxi = dictionary["lxi"] as! String
        v2 = dictionary["v2"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Weight]
    {
        var result:[Weight] = []
        for item in array
        {
            var newItem = Weight()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Features
{
    var dual_airbag:String = ""
    var night_irvm:String = ""
    var aloy_wheels:String = ""
    var front_fog_lamps:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        dual_airbag = dictionary["dual_airbag"] as! String
        night_irvm = dictionary["night_irvm"] as! String
        aloy_wheels = dictionary["aloy_wheels"] as! String
        front_fog_lamps = dictionary["front_fog_lamps"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Features]
    {
        var result:[Features] = []
        for item in array
        {
            var newItem = Features()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Performance
{
    var engine_type:String = ""
    var engine_capacity:String = ""
    var max_power:String = ""
    var max_torque:String = ""
    var fuel_type:String = ""
    var transmission:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        engine_type = dictionary["engine_type"] as! String
        engine_capacity = dictionary["engine_capacity"] as! String
        max_power = dictionary["max_power"] as! String
        max_torque = dictionary["max_torque"] as! String
        fuel_type = dictionary["fuel_type"] as! String
        transmission = dictionary["transmission"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Performance]
    {
        var result:[Performance] = []
        for item in array
        {
            var newItem = Performance()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Suspension
{
    var front_system:String = ""
    var back_system:String = ""
    var tyre_size:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        front_system = dictionary["front_system"] as! String
        back_system = dictionary["back_system"] as! String
        tyre_size = dictionary["tyre_size"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Suspension]
    {
        var result:[Suspension] = []
        for item in array
        {
            var newItem = Suspension()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Dimension
{
    var dimension:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        dimension = dictionary["dimension"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Dimension]
    {
        var result:[Dimension] = []
        for item in array
        {
            var newItem = Dimension()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Capacity
{
    var seating:String = ""
    var fuel_capacity:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        seating = dictionary["seating"] as! String
        fuel_capacity = dictionary["fuel_capacity"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Capacity]
    {
        var result:[Capacity] = []
        for item in array
        {
            var newItem = Capacity()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class Safety
{
    var driver_airbag:String = ""
    var headlight_levelling:String = ""
    var front_fog_lamps:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        driver_airbag = dictionary["driver_airbag"] as! String
        headlight_levelling = dictionary["headlight_levelling"] as! String
        front_fog_lamps = dictionary["front_fog_lamps"] as! String
    }
    class func PopulateArray(array:NSArray) -> [Safety]
    {
        var result:[Safety] = []
        for item in array
        {
            var newItem = Safety()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class VehiclePrice
{
    var metalic_price:String = ""
    var nonMetalice_price:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        metalic_price = dictionary["metalic_price"] as! String
        nonMetalice_price = dictionary["nonMetalice_price"] as! String
    }
    class func PopulateArray(array:NSArray) -> [VehiclePrice]
    {
        var result:[VehiclePrice] = []
        for item in array
        {
            var newItem = VehiclePrice()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}



class VehicleVideos
{
    var id:Int = 0
    var urls:[String] = []
    var color_id:String = ""
    var carmodel_id:String = ""
    var type_id:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    
    func Populate(dictionary:NSDictionary) {
        
        id = dictionary["id"] as! Int
        urls = String.PopulateArray(dictionary["url"] as! [NSArray])
        color_id = dictionary["color_id"] as! String
        carmodel_id = dictionary["carmodel_id"] as! String
        type_id = dictionary["type_id"] as! String
        created_at = dictionary["created_at"] as! String
        updated_at = dictionary["updated_at"] as! String
    }
    class func PopulateArray(array:NSArray) -> [VehicleVideos]
    {
        var result:[VehicleVideos] = []
        for item in array
        {
            var newItem = VehicleVideos()
            newItem.Populate(item as! NSDictionary)
            result.append(newItem)
        }
        return result
    }
    
}

class NewCarDetailDataModel
{
    var id:Int = 0
    var isnexa:String = ""
    var carmodelid:Int = 0
    var carmodel:String = ""
    var variant:String = ""
    var isfavourite:Bool = Bool()
    var start_prize:String = ""
    var end_prize:String = ""
    var mileage:String = ""
    var fuel_type:String = ""
    var transmission:String = ""
    var engine_type:String = ""
    var image_count:Int = 0
    var video_count:Int = 0
    
    var breakmodel:String = ""
    var safety:Safety = Safety()
    var exterior:String = ""
    var vehicle_price:VehiclePrice = VehiclePrice()
    var vehicle_videos:VehicleVideos = VehicleVideos()
    var variants:[Variant] = []
    var colors:[Color] = []
    var weight:Weight = Weight()
    var features:Features = Features()
    var performance:Performance = Performance()
    var suspension:Suspension = Suspension()
    var dimension:Dimension = Dimension()
    var capacity:Capacity = Capacity()
    var vehicle_images:[VehicleImage] = []
    
    init(dictionary:Json) {
        
        id = dictionary["id"] as? Int ?? 0
        isnexa = dictionary["isnexa"] as! String
        carmodelid = dictionary["carmodelid"] as? Int ?? 0
        carmodel = dictionary["carmodel"] as! String
        variant = dictionary["variant"] as! String
        colors = Color.PopulateArray(array: dictionary["color"] as! [Color] as NSArray)
        isfavourite = dictionary["isfavourite"] as! Bool
        start_prize = dictionary["start_prize"] as! String
        end_prize = dictionary["end_prize"] as! String
        mileage = dictionary["mileage"] as! String
        fuel_type = dictionary["fuel_type"] as! String
        transmission = dictionary["transmission"] as! String
        engine_type = dictionary["engine_type"] as! String
        variants = Variant.PopulateArray(array: dictionary["variants"] as! [Variant] as NSArray)
        weight = dictionary["weight"] as! Weight
        features = dictionary["features"] as! Features
        performance = dictionary["performance"] as! Performance
        suspension = dictionary["suspension"] as! Suspension
        dimension = dictionary["dimension"] as! Dimension
        capacity = dictionary["capacity"] as! Capacity
        breakmodel = dictionary["break"] as! String
        safety = dictionary["safety"] as! Safety
        exterior = dictionary["exterior"] as! String
        vehicle_price = dictionary["vehicle_price"] as! VehiclePrice
        vehicle_images = VehicleImage.getValue(fromArray: dictionary["images"] as! JsonArray)
        image_count = dictionary["image_count"] as! Int
        vehicle_videos = dictionary["vehicle_videos"] as! VehicleVideos
        video_count = dictionary["video_count"] as! Int
        
    }
    
    private init(){}
    
    class func getValue(fromObject:Json?) -> NewCarDetailDataModel
    {
        guard let object = fromObject else { return NewCarDetailDataModel() }
        return NewCarDetailDataModel(dictionary: object)
    }
    
    class func getValue(fromArray: JsonArray) -> [NewCarDetailDataModel]
    {
        var modelArray: [NewCarDetailDataModel] = []
        for item in fromArray {
            modelArray.append(NewCarDetailDataModel.getValue(fromObject: item))
        }
        return modelArray
    }
    
    
    class VehicleImage
    {
        var id:Int = 0
        var urls:[String] = []
        var color_id:String = ""
        var carmodel_id:String = ""
        var type_id:String = ""
        var created_at:String = ""
        var updated_at:String = ""
        
        init(dictionary:Json) {
            
            id = dictionary["id"] as! Int
            urls = dictionary["url"] as! [String]
            color_id = dictionary["color_id"] as! String
            carmodel_id = dictionary["carmodel_id"] as! String
            type_id = dictionary["type_id"] as! String
            created_at = dictionary["created_at"] as! String
            updated_at = dictionary["updated_at"] as! String
        }
        
        private init(){}
        
        class func getValue(fromObject:Json?) -> VehicleImage
        {
            guard let object = fromObject else { return VehicleImage() }
            return VehicleImage(dictionary: object)
        }
        
        class func getValue(fromArray: JsonArray) -> [VehicleImage]
        {
            var modelArray: [VehicleImage] = []
            for item in fromArray {
                modelArray.append(VehicleImage.getValue(fromObject: item))
            }
            return modelArray
        }
        
    }
}


*/

//["id": 1, "isnexa": 1, "variants": <__NSArrayI 0x604000259140>(
//    {
//    abs = "";
//    "air_conditioning" = 1;
//    "aloy_wheels" = 1;
//    "back_system" = backsystem1;
//    "brakes_type" = "";
//    break = "";
//    capacity =     {
//    "fuel_capacity" = 0;
//    seating = 2;
//    };
//    "carmodel_id" = 1;
//    "central_lock" = "";
//    color =     (
//    {
//    hexcode = FF0000;
//    name = red;
//    pivot =             {
//    "carmodel_id" = 1;
//    "color_id" = 1;
//    };
//    },
//    {
//    hexcode = F06216;
//    name = orange;
//    pivot =             {
//    "carmodel_id" = 1;
//    "color_id" = 2;
//    };
//    }
//    );
//    "created_at" = "2018-07-07 09:54:56";
//    dimension =     {
//    dimension = "";
//    };
//    "drive_train" = "";
//    "driver_airbag" = "driver_airbag1";
//    "dual_airbag" = "dual_airbag1";
//    "end_prize" = 245000;
//    "engine_capacity" = "";
//    "engine_type" = 0;
//    exterior = "";
//    features =     {
//    "aloy_wheels" = 1;
//    "dual_airbag" = "dual_airbag1";
//    "front_fog_lamps" = "front_fog_lamp1";
//    "night_irvm" = "nigh_irvm1";
//    };
//    "front_fog_lamps" = "front_fog_lamp1";
//    "front_system" = frontsystem1;
//    "fuel_capacity" = 0;
//    "fuel_type" = petrol;
//    "gear_position_indicator" = "";
//    "gross_weight" = "";
//    "ground_clearance" = "";
//    "headlight_levelling" = "headlight_leveling1";
//    "horse_power" = "";
//    id = 1;
//    "image_count" = 2;
//    isfavourite = 0;
//    isnexa = 1;
//    "kerb_weight" = 70;
//    "max_power" = 0;
//    "max_torque" = "";
//    mileage = 100;
//    "mirror_type" = "";
//    "music_system" = 1;
//    name = lx;
//    "night_irvm" = "nigh_irvm1";
//    "no_of_gears" = 5;
//    "passenger_airbag" = "";
//    performance =     {
//    "engine_capacity" = "";
//    "engine_type" = 0;
//    "fuel_type" = petrol;
//    "max_power" = 0;
//    "max_torque" = "";
//    transmission = manual;
//    };
//    "power_steering" = "";
//    "power_windows" = "";
//    safety =     {
//    "driver_airbag" = "driver_airbag1";
//    "front_fog_lamps" = "front_fog_lamp1";
//    "headlight_levelling" = "headlight_leveling1";
//    };
//    seating = 2;
//    "start_prize" = 200000;
//    suspension =     {
//    "back_system" = backsystem1;
//    "front_system" = frontsystem1;
//    "tyre_size" = tyresize1;
//    };
//    tachometer = "";
//    transmission = manual;
//    "tyre_size" = tyresize1;
//    "updated_at" = "2018-06-27 09:53:40";
//    variant = lx;
//    variantname = lx;
//    "vehicle_images" =     (
//    {
//    "carmodel_id" = 1;
//    "color_id" = 1;
//    "created_at" = "2018-07-06 03:42:29";
//    id = 1;
//    "type_id" = 1;
//    "updated_at" = "2018-06-28 05:53:54";
//    url =             (
//    "http://app.appzoc.com/popular/backend//storage/app/uploads/usedcars/Kjx5pNXeRGyDmsFWJBvoN0e0IMIrFRmFzuZbXGHC.jpeg",
//    "http://app.appzoc.com/popular/backend//storage/app/uploads/usedcars/Kjx5pNXeRGyDmsFWJBvoN0e0IMIrFRmFzuZbXGHC.jpeg"
//    );
//    "variant_id" = 1;
//    }
//    );
//    "vehicle_price" =     {
//    "metalic_price" = 245000;
//    "nonMetalice_price" = 240000;
//    };
//    "vehicle_videos" =     (
//    );
//    "video_count" = 0;
//    weight =     {
//    lx = 70;
//    lxi = 75;
//    v2 = 0;
//    };
//    }
//    )
//]

