//
//  PLVideoPlayer.swift
//  Popular
//
//  Created by Appzoc on 07/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
import SnapKit

class PLVideoPlayer: UIViewController {

    
    @IBOutlet weak var youtubePlayer: YTPlayerView!
    var UrlPlay = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let playerVars:[String:Any] = [
            "playsinline":0,
            "autoplay" : 1,
            "modestbranding" : 1,
            "rel": 0
        ]

        print("URL :",UrlPlay)
        if let videoID = getIDFromURL(url: UrlPlay){
            self.youtubePlayer?.load(withVideoId: videoID, playerVars: playerVars)
        }else{
            showBanner(message: "Invalid URL")
            dismiss()
        }
       // self.youtubePlayer.loadVideo(byURL: "\(UrlPlay)", startSeconds: 0, suggestedQuality: .large)
        
    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.dismiss()
    }
    
    
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        
    }


}

extension PLVideoPlayer: YTPlayerViewDelegate{
    
}


