//
//  PLLearnDrivingVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 31/08/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit
class PLLearnDrivingHeaderTVC: UITableViewCell {
    @IBOutlet var containerView: BaseView!
    @IBOutlet var titleLBL: UILabel!
    
}
class PLLearnDrivingTVC: UITableViewCell {
    @IBOutlet var containerView: BaseView!
    @IBOutlet var nameLBL: UILabel!
    @IBOutlet var addressLBL: UILabel!
    @IBOutlet var mailLBL: UILabel!
    @IBOutlet var phoneLBL: UILabel!
    @IBOutlet var cornerView: BaseView!
    @IBOutlet var addressImage: UIImageView!
    @IBOutlet var contactImage: UIImageView!
}
class PLLearnDrivingVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet var programsView: UIView!
    @IBOutlet var contactUsView: UIView!
    @IBOutlet var contactUsTV: UITableView!
    @IBOutlet var homeScrollView: UIScrollView!
    @IBOutlet var programsBTN: UIButton!
    @IBOutlet var contactUsBTN: UIButton!
    
    // properties
    private var contactUsSource: [PLLearnDrivingContactModel] = []
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white//themeRed
        return refreshControl
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactUsTV.addSubview(refreshControl)
        getContactUsDataWeb()
        NotificationCenter.default.addObserver(self, selector: #selector(userLogged), name: .popupLoginDriving, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        programsTapped(UIButton())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        contactUsTV.reloadData()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Menu actions
    @IBAction func programsTapped(_ sender: UIButton) {

        homeScrollView.scrollToLeft(animated: true)
        UIView.animate(withDuration: 0.15) {
            self.contactUsBTN.alpha = 0.5
            self.programsBTN.alpha = 1.0

        }
    }
    
    @IBAction func contactUsTapped(_ sender: UIButton) {

        homeScrollView.scrollToRight(animated: true)
        if contactUsSource.isEmpty {
            getContactUsDataWeb()
        }
        UIView.animate(withDuration: 0.15) {
            self.contactUsBTN.alpha = 1.0
            self.programsBTN.alpha = 0.5
        }

    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    
    
    
    /// Prorgams actions
    @IBAction func theorySessionTapped(_ sender: UIButton) {
        let nextVC =  self.storyBoardLearnDriving!.instantiateViewController(withIdentifier: "PLTheorySessionsVC")
        present(nextVC)

    }
    
    @IBAction func practicalSessionTapped(_ sender: UIButton) {
        let nextVC =  self.storyBoardLearnDriving!.instantiateViewController(withIdentifier: "PLPracticalSessionsVC")
        present(nextVC)

    }
    
    @IBAction func courcesTapped(_ sender: UIButton) {
        let nextVC =  self.storyBoardLearnDriving!.instantiateViewController(withIdentifier: "PLCoursesVC")
        present(nextVC)

    }
    
    @IBAction func contactUsSubmitTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn {
            let nextVC =  self.storyBoardPopUps?.instantiateViewController(withIdentifier: "PLPopUpEnquiryVC") as! PLPopUpEnquiryVC
            nextVC.enquiryType = .learnDrivingContactUs
            present(nextVC)

        }else {
            showLoginPopUp()
        }

    }
    
    // called from login popup
    @objc private func userLogged(){
        BaseThread.asyncMain {
            self.contactUsSubmitTapped(UIButton())
        }
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != contactUsTV {
            if scrollView.contentOffset.x == 0 {
                UIView.animate(withDuration: 0.2) {
                    self.contactUsBTN.alpha = 0.5
                    self.programsBTN.alpha = 1.0
                    
                }
                
            }else {
                if contactUsSource.isEmpty {
                    getContactUsDataWeb()
                }
                UIView.animate(withDuration: 0.2) {
                    self.contactUsBTN.alpha = 1.0
                    self.programsBTN.alpha = 0.5
                }
                
            }
        }
        
    }
}

//Mark: Table View handling
extension PLLearnDrivingVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactUsSource.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactUsSource[section].branches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLLearnDrivingTVC", for: indexPath) as? PLLearnDrivingTVC else { return UITableViewCell() }
        let source = contactUsSource[indexPath.section].branches[indexPath.row]
        
        if indexPath.row + 1 == contactUsSource[indexPath.section].branches.count {
            cell.cornerView.cornerRadius = 5
        }else {
            cell.cornerView.cornerRadius = 0
        }
        cell.nameLBL.text = source.branch_head
        cell.addressLBL.text = source.address
        cell.mailLBL.text = source.head_email
        cell.phoneLBL.text = source.head_mobile
        cell.addressImage.isHidden = (source.branch_head.isEmpty && source.address.isEmpty)
        cell.contactImage.isHidden = (source.head_email.isEmpty && source.head_mobile.isEmpty)
        return cell
    }

    ///
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension//210//
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 12
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLLearnDrivingHeaderTVC") as? PLLearnDrivingHeaderTVC else { return UIView() }
        cell.titleLBL.text = contactUsSource[section].name
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    // pull refresh
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getContactUsDataWebWithOutActivity()
    }

    

}


// Mark: Handling Webservices
extension PLLearnDrivingVC {
    private func getContactUsDataWeb() {
        let url = "getdrivingBranchlist"
        WebServices.getMethodWith(url: url, parameter: ["":""]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            BaseThread.asyncMain {
                self.contactUsSource = PLLearnDrivingContactModel.getValues(fromArray: dataArray)
                self.contactUsTV.reloadData()
                self.view.layoutIfNeeded()
            }
            
        }
    
    }
    
    private func getContactUsDataWebWithOutActivity() {
        let url = "getdrivingBranchlist"
        WebServices.getMethodWith(url: url, parameter: ["":""]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            self.contactUsSource.removeAll()
            BaseThread.asyncMain {
                self.contactUsSource = PLLearnDrivingContactModel.getValues(fromArray: dataArray)
                self.refreshControl.endRefreshing()
                self.contactUsTV.reloadData()
                //self.view.layoutIfNeeded()
            }
            
        }
        
    }

}




