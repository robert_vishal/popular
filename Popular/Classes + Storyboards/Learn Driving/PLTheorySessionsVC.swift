//
//  PLTheorySessionsVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 06/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLTheorySessionsTVC: UITableViewCell {
    
    @IBOutlet var descriptionLBL: UILabel!
}

class PLTheorySessionsVC: UIViewController {

    @IBOutlet var contentTV: UITableView!
    
    private var contentSource: [String] = []
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white//themeRed
        return refreshControl
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentTV.estimatedRowHeight = UITableViewAutomaticDimension
        contentTV.estimatedRowHeight = 35
        contentTV.addSubview(refreshControl)
        //getDataWeb()
        NotificationCenter.default.addObserver(self, selector: #selector(userLogged), name: .popupLoginDriving, object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        getDataWeb()
    }

    override func viewDidDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self, name: .popupLoginDriving, object: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // called from login popup
    @objc private func userLogged(){
        BaseThread.asyncMain {
            self.sendEnquiryTapped(UIButton())
        }
    }

    @IBAction func sendEnquiryTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn {
            let nextVC =  self.storyBoardPopUps?.instantiateViewController(withIdentifier: "PLPopUpEnquiryVC") as! PLPopUpEnquiryVC
            nextVC.enquiryType = .learnDrivingTheory
            present(nextVC)

        }else {
            showLoginPopUp()
        }

    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }
    //let stringArray = ["Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu"," sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea", "commodo consequat. Duis aute irure dolor in", "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident", "sunt in culpa qui officia deserunt mollit anim id est laborum. ","Nam liber te conscient to", "factor tum poen legum odioque civiuda"]

}

// Mark: Handling TableView
extension PLTheorySessionsVC: UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLTheorySessionsTVC", for: indexPath) as? PLTheorySessionsTVC else { return UITableViewCell()}
        
        cell.descriptionLBL.text = contentSource[indexPath.row]
        return cell
    }
    
    // pull refresh
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //print("refresh")
        getDataWebWithOutActivity()
    }

    
}

// Mark: Handling Webservice
extension PLTheorySessionsVC {
    private func getDataWeb() {
        let url = "getTheorysession"
        WebServices.getMethodWith(url: url, parameter: [:]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            BaseThread.asyncMain {
                dataArray.forEach({ (item) in
                    self.contentSource.append(item["theory_session"] as? String ?? "")
                })

                self.contentTV.reloadData()
            }
            
        }
    }
    
    private func getDataWebWithOutActivity() {
        let url = "getTheorysession"
        WebServices.getMethodWithOutActivity(url: url, parameter: [:]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            var contentSourceNew: [String] = []
            BaseThread.asyncMain {
                dataArray.forEach({ (item) in
                    contentSourceNew.append(item["theory_session"] as? String ?? "")
                })

                if !contentSourceNew.isEmpty {
                    self.contentSource = contentSourceNew
                }
                self.refreshControl.endRefreshing()
                self.contentTV.reloadData()
            }

        }
    }

}


