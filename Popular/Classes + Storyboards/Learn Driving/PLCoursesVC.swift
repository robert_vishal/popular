//
//  PLCoursesVC.swift
//  Popular
//
//  Created by Appzoc-Macmini on 07/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import UIKit

class PLCoursesHeaderTVC: UITableViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    @IBOutlet var sectionBTN: UIButton!
}

class PLCoursesTickTVC: UITableViewCell {
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var isAbsentLBL: UILabel!
    @IBOutlet var isPresentImage: UIImageView!
}

class PLCoursesTextTVC: UITableViewCell {
    
    @IBOutlet var titleLBL: UILabel!
    @IBOutlet var durationLBL: UILabel!
}

class PLCourseFooterTVC: UITableViewCell {
    
}


class PLCoursesVC: UIViewController {

    @IBOutlet var contentTV: UITableView!
    
    private var contentSource: [PLCoursesModel] = []
//    private var deselectedCell:EXOrgAddSlotTVC?
    private var isRotatedArrowIMG: Bool = false
    private var selectedSection: Int?
    private var prevoiusSection: Int?
    private var deselectedCell:PLCoursesHeaderTVC?
    //private var isMultiExpandable: Bool = false
    private var isExpandedCells: [Int] = []
    private var sectionHeaders: [PLCoursesHeaderTVC] = []
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white//themeRed
        return refreshControl
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getDataWeb()
        contentTV.addSubview(refreshControl)
        NotificationCenter.default.addObserver(self, selector: #selector(userLogged), name: .popupLoginDriving, object: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        getDataWeb()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // called from login popup
    @objc private func userLogged(){
        BaseThread.asyncMain {
            self.sendEnquiryTapped(UIButton())
        }
    }

    
    @IBAction func sendEnquiryTapped(_ sender: UIButton) {
        if PLAppState.session.isLoggedIn {
            let nextVC =  self.storyBoardPopUps?.instantiateViewController(withIdentifier: "PLPopUpEnquiryVC") as! PLPopUpEnquiryVC
            nextVC.enquiryType = .learnDrivingCources
            self.present(nextVC)

        }else {
            showLoginPopUp()

        }
        
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss()
    }


    @IBAction func sectionBTNTapped(_ sender: UIButton) {
        prevoiusSection = selectedSection
        selectedSection = sender.tag
        
        if isExpandedCells.contains(selectedSection!) {
            if let index = isExpandedCells.index(of: selectedSection!) {
                isExpandedCells.remove(at: index)
            }else {

            }
        }else{
            if isExpandedCells.isEmpty {
                isExpandedCells.append(selectedSection!)
            }else {
                isExpandedCells.removeAll()
                isExpandedCells.append(selectedSection!)
            }
        }
        
        let selectedCell = sectionHeaders[selectedSection!]
        
        contentTV.beginUpdates()
        rotateArrowIMG(of: selectedCell)
        contentTV.endUpdates()
        
    }
    
}


// Mark: Handling TableView
extension PLCoursesVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return contentSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentSource.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCoursesHeaderTVC") as? PLCoursesHeaderTVC else { return UITableViewCell().contentView }
        cell.sectionBTN.tag = section
        switch section {
        case 0:
            cell.titleLBL.text = contentSource[section].course
        case 1:
            cell.titleLBL.text = contentSource[section].course
        case 2:
            cell.titleLBL.text = contentSource[section].course
        case 3:
            cell.titleLBL.text = contentSource[section].course
        default:
            break
            //print("defalutClause_viewForHeaderInSection")
        }
        sectionHeaders.insert(cell, at: section)
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCoursesTickTVC", for: indexPath) as? PLCoursesTickTVC else { return UITableViewCell()}

            cell.titleLBL.text = "Classroom Training"
            if contentSource[indexPath.section].classroom_training {
                cell.isPresentImage.isHidden = false
                cell.isAbsentLBL.isHidden = true
            }else {
                cell.isPresentImage.isHidden = true
                cell.isAbsentLBL.isHidden = false
            }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCoursesTickTVC", for: indexPath) as? PLCoursesTickTVC else { return UITableViewCell()}
            
            cell.titleLBL.text = "Simulator Training"
            if contentSource[indexPath.section].simulator_training {
                cell.isPresentImage.isHidden = false
                cell.isAbsentLBL.isHidden = true
            }else {
                cell.isPresentImage.isHidden = true
                cell.isAbsentLBL.isHidden = false
            }
            return cell

        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCoursesTickTVC", for: indexPath) as? PLCoursesTickTVC else { return UITableViewCell()}
            
            cell.titleLBL.text = "Practicals (On Road)"
            if contentSource[indexPath.section].practicals {
                cell.isPresentImage.isHidden = false
                cell.isAbsentLBL.isHidden = true
            }else {
                cell.isPresentImage.isHidden = true
                cell.isAbsentLBL.isHidden = false
            }
            return cell

        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCoursesTextTVC", for: indexPath) as? PLCoursesTextTVC else { return UITableViewCell()}
            
            cell.titleLBL.text = "Course Duration"
            cell.durationLBL.text = contentSource[indexPath.section].duration
            return cell

        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PLCourseFooterTVC", for: indexPath) as? PLCourseFooterTVC else { return UITableViewCell()}

            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == contentSource.count {
            if indexPath.section == selectedSection {
                return isExpandedCells.contains(indexPath.section) ? 28 : 0
            }else {
                return 0
            }
        }else {
            if prevoiusSection == nil {
                return selectedSection == indexPath.section ? 56 : 0
            }else if selectedSection == prevoiusSection {
                if selectedSection == indexPath.section {
                    return isExpandedCells.contains(selectedSection!) ? 56 : 0
                }else {
                    return 0
                }
            }else {
                return selectedSection == indexPath.section ? 56 : 0
            }
        }
        
    }
    
    
    // pull refresh
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //print("refresh")
        getDataWebWithOutActivity()
    }
    
    /// Image rotation animation
    private func rotateArrowIMG(of selectedCell: PLCoursesHeaderTVC){
        let towardsDown = CGFloat(Double.pi/2)
        let towardsRight = CGFloat(-Double.pi/2)
        
        BaseThread.asyncMain {
            UIView.animate(withDuration: 0.35, animations: {
                if self.deselectedCell == nil {
                    /// tapping on the cell first time
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    self.isRotatedArrowIMG = true
                }else if self.deselectedCell == selectedCell {
                    /// tapping on already selected cell
                    if self.isRotatedArrowIMG {
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsRight)
                        self.isRotatedArrowIMG = false
                    }else{
                        selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                        self.isRotatedArrowIMG = true
                    }
                }else{
                    /// tapping on other cell not already selected cell
                    selectedCell.arrowImage.transform = selectedCell.arrowImage.transform.rotated(by: towardsDown)
                    guard let dCell = self.deselectedCell else { self.isRotatedArrowIMG = true; return }
                    
                    if self.isRotatedArrowIMG {
                        dCell.arrowImage.transform = dCell.arrowImage.transform.rotated(by: towardsRight)
                    }
                    self.isRotatedArrowIMG = true

                }
                self.deselectedCell = selectedCell
            })
        }
        
    }
}


// Mark: Handling Webservice
extension PLCoursesVC {
    
    private func getDataWeb() {
        let url = "getDrivingCourse"
        WebServices.getMethodWith(url: url, parameter: [:]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            self.contentSource = PLCoursesModel.getValues(fromArray: dataArray)
            BaseThread.asyncMain {
                
                self.contentTV.reloadData()
            }
            
        }
    }
    
    private func getDataWebWithOutActivity() {
        let url = "getDrivingCourse"
        WebServices.getMethodWithOutActivity(url: url, parameter: [:]) { (isSucceeded, response) in
            guard isSucceeded else { return }
            guard let dataArray = response["data"] as? JsonArray else { return }
            self.contentSource.removeAll()
            self.contentSource = PLCoursesModel.getValues(fromArray: dataArray)
            BaseThread.asyncMain {
                self.refreshControl.endRefreshing()
                self.contentTV.reloadData()
            }

        }
    }
    
    
}




