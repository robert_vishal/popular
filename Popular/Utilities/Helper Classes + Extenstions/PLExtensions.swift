//
//  Extensions.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit
import NotificationCenter

// Check whether the optional String is empty or not
protocol StringEmptyable{}
extension String: StringEmptyable{}
extension Optional where Wrapped: StringEmptyable {
    //guard !(string?.trimmingCharacters(in: .whitespaces).isEmpty)!, string != nil else {

    var unwrap: String{
        switch self {
        case .none:
            return ""
        case .some(let value):
            return value as! String
        }
    }
    
    func unwrapBy(string: String = notMentioned) -> String{
        switch self {
        case .none:
            return string
        case .some(let value):
            let currentValue = (value as! String).trimmingCharacters(in: .whitespacesAndNewlines)
            if currentValue == "" || currentValue.isEmpty {
                return string
            }else {
                return value as! String
            }
        }
    }
}

// Check whether the optional Int is empty or not
protocol IntEmptyable{
}
extension Int: IntEmptyable{
}
extension Optional where Wrapped: IntEmptyable {
    var unwrap: Int{
        switch self {
        case .none:
            return 0
        case .some(let value):
            return value as! Int
        }
    }
    
}


extension Int {
    var isZero: Bool {
        return self == 0
    }
    
    func perTotal(count: Int)-> String {
        return self.description + " / " + count.description
    }
}

extension Int {
    
    var leadingZero: String {
        return String(format: "%02d", self)
    }
    func leadingZero(count: Int)-> String {
        return String(format: "%\(count)d", self)
    }

}

// Check whether the optional Double is empty or not
protocol DoubleEmptyable{
}
extension Double: DoubleEmptyable{
}
extension Optional where Wrapped: DoubleEmptyable {
   var unwrap: Double{
        switch self {
        case .none:
            return 0.0
        case .some(let value):
            return value as! Double
        }
    }
}

// Check whether the optional Bool is empty or not
protocol BoolEmptyable{
}
extension Bool: BoolEmptyable{
}
extension Optional where Wrapped: BoolEmptyable {
    var unwrap: Bool{
        switch self {
        case .none:
            return false
        case .some(let value):
            return value as! Bool
        }
    }
}

// String is not empty

extension String {
    public var isNotEmpty: Bool {
        return !self.isEmpty
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func toCGFloat() -> CGFloat{
        if let intNumber = Int(self){
            let cgFloatNumber = CGFloat(intNumber)
            return cgFloatNumber
        }else{
            return -1
        }
    }
}

// present view controller from right and dissmiss

extension UIViewController {
    
    var storyBoardNewCars: UIStoryboard? {
         get { return UIStoryboard(name: "NewCars", bundle: nil)}
    }
    
    var storyBoardUsedCars: UIStoryboard? {
        get { return UIStoryboard(name: "UsedCars", bundle: nil)}
    }
    
    var storyBoardMain: UIStoryboard? {
        get { return UIStoryboard(name: "Main", bundle: nil)}
    }
    var storyBoardService: UIStoryboard? {
        get { return UIStoryboard(name: "Services", bundle: nil)}
    }
    var storyBoardCompare: UIStoryboard? {
        get { return UIStoryboard(name: "Compare", bundle: nil)}
    }
    
    var storyBoardSearch: UIStoryboard? {
        get { return UIStoryboard(name: "Search", bundle: nil)}
    }
    var storyBoardFilter: UIStoryboard? {
        get { return UIStoryboard(name: "Filter", bundle: nil)}
    }
    var storyBoardFavourites: UIStoryboard? {
        get { return UIStoryboard(name: "Favourites", bundle: nil)}
    }
    var storyBoardDateTime: UIStoryboard? {
        get { return UIStoryboard(name: "DateTime", bundle: nil)}
    }
    var storyBoardPopUps: UIStoryboard? {
        get { return UIStoryboard(name: "PopUps", bundle: nil)}
    }
    var storyBoardInsurance: UIStoryboard? {
        get { return UIStoryboard(name: "Insurance", bundle: nil)}
    }
    var storyBoardLearnDriving: UIStoryboard? {
        get { return UIStoryboard(name: "LearnDriving", bundle: nil)}
    }

    var storyBoardReferals: UIStoryboard? {
        get { return UIStoryboard(name: "Referals", bundle: nil)}
    }
    
    var storyBoardLifestyle: UIStoryboard? {
        get { return UIStoryboard(name: "Lifestyle", bundle: nil)}
    }
    
    var storyBoardFindUs: UIStoryboard?{
        get { return UIStoryboard(name:"FindUs", bundle: nil) }
    }
    
    var storyBoardAssist: UIStoryboard?{
        get { return UIStoryboard(name:"Assist", bundle: nil) }
    }

    var storyBoardOffers: UIStoryboard?{
        get { return UIStoryboard(name:"Offers", bundle: nil) }
    }

//    var blackView: UIView {
//        get {
//            let newView = UIView(frame: self.view.frame)
//            return
//
//        }
//    }

    func showLoginPopUp() {
        BaseThread.asyncMain {
            let segueVC = self.storyBoardMain?.instantiateViewController(withIdentifier: "PLLoginPopupVC") as! PLLoginPopupVC
            self.present(segueVC)
        }
    }

    func dismiss(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func present(_ vc: UIViewController){
        present(vc, animated: true, completion: nil)
    }
    
    func emptyBackground(vc: UIViewController, addOrRemove: Bool){
        BaseThread.asyncMain {

            if addOrRemove {
                let emptyView = UIView(frame: vc.view.frame)
                emptyView.backgroundColor = .white
                vc.view.addSubview(emptyView)
            }else {
                if let emptyView = vc.view.subviews.last {
                    emptyView.removeFromSuperview()
                }
            }

        }
    }
    
    func emptyBackground(rectArea: CGRect){
        
    }
    
    func presentRight(_ viewControllerToPresent: UIViewController, duration: CFTimeInterval = 0.25 ) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissRight(duration: CFTimeInterval = 0.25) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}

//(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
extension UIColor {
    convenience init(rgb: Int) {
        self.init(red: CGFloat((rgb >> 16) & 0xFF), green: CGFloat((rgb >> 8) & 0xFF), blue: CGFloat(rgb & 0xFF), alpha: 1.0)
    }
    
    static let themeRed = UIColor(red: 236/255, green: 75/255, blue: 46/255, alpha: 1.0)
    static let themeGreen = UIColor(red: 46/255.0, green: 176/255.0, blue: 181/255.0, alpha: 1.0)
    static let themeGreenDark = UIColor(red: 24/255.0, green: 176/255.0, blue: 181/255.0, alpha: 1.0)
    static let themeGreenDark2 = UIColor(red: 21/255.0, green: 162/255.0, blue: 166/255.0, alpha: 1.0)
    
    static let selectionLightBlue = UIColor(red: 27/255, green: 196/255, blue: 201/255, alpha: 1)
    static let tableBackground = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)

    static func from(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIView {
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
        
    }
}

extension Notification.Name{
    static let refreshData = Notification.Name(rawValue: "refreshData")
    static let documentDeleted = Notification.Name("documentDeleted")
    static let licenseDeleted = Notification.Name("licenseDeleted")
    static let loggedIn = Notification.Name("loggedIn")
    static let usedCarFavourite = Notification.Name(rawValue: "NNUsedCarFavourite")
    static let usedCarFavouriteAll = Notification.Name(rawValue: "NNUsedCarFavouriteAll")
    static let usedCarCompare = Notification.Name(rawValue: "NNUsedCarCompare")
    static let popupLoginDriving = Notification.Name(rawValue: "NNPopupLoginDriving")
    
}

extension UIImageView {
    func setImageColor(_ color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension Double {
    var CGFloatValue: CGFloat {
        return CGFloat(self)
    }
    
    var intValue: Int {
        return Int(self)
    }

}

extension Int {
    var CGFloatValue: CGFloat {
        return CGFloat(self)
    }
    var doubleValue: Double {
        return Double(self)
    }

}
extension Int64 {
    var CGFloatValue: CGFloat {
        return CGFloat(self)
    }
    var doubleValue: Double {
        return Double(self)
    }

}

extension CGFloat {
    var roundedToFiveThousand:CGFloat{
        let modValue = self.truncatingRemainder(dividingBy: 5000)
        let valueToAdd = 5000 - modValue
        return (self + valueToAdd)
    }
    
    var roundedToTenThousand:CGFloat{
        let modValue = self.truncatingRemainder(dividingBy: 10000)
        let valueToAdd = 10000 - modValue
        return (self + valueToAdd)
    }
    
    var doubleValue: Double {
        return Double(self)
    }
    
    var intValue: Int {
        return Int(self)
    }
    
    var int64Value: Int64 {
        return Int64(self)
    }
    
    var half: CGFloat {
        return self / 2
    }
    
    func formatInRuppees() -> String {
        var valueString: String = ""
        let thousand = 1000
        let lakh = 100000
        /// check if it is greater than or equal 1 lakh
        let valueUnformatted = self.int64Value
        if valueUnformatted >= lakh {
            let valueFormatted = (valueUnformatted.doubleValue / lakh.doubleValue).roundToDecimal()
            
            if valueUnformatted == lakh {
                valueString = "₹ " + valueFormatted.description + " Lakh"
            }else {
                valueString = "₹ " + valueFormatted.description + " Lakhs"
            }
        }else {
            let valueFormatted = (valueUnformatted.doubleValue / thousand.doubleValue).roundToDecimal()
            valueString = "₹ " + valueFormatted.description + " K"
        }
        //print("ValueString",valueString)
        return valueString
        
    }
    
    
    func formatInKilometer() -> String {
        let value = self.intValue
        var returnString: String = ""
        
        let answer = value/1000
        let remainder = value.doubleValue.truncatingRemainder(dividingBy: 1000)
        if answer >= 1000 {
            returnString = " " + String(format: "%03d", remainder.intValue)
            let answerString = answer.CGFloatValue.formatInKilometer()
            return answerString + returnString
        }else {
            returnString = answer.description + " " + String(format: "%03d", remainder.intValue)
            return returnString
        }
        
    }

}

extension Date {

    var currentYear: Int {
        return Calendar.current.component(Calendar.Component.year, from: self)
    }

    var nextYear: Int {
        return Calendar.current.component(Calendar.Component.year, from: self) + 1
    }

    
    var currentMonth: Int {
        return Calendar.current.component(Calendar.Component.month, from: self)
    }

    var currentDay: Int {
        return Calendar.current.component(Calendar.Component.day, from: self)
    }
    
    var hour: String {
//        return Calendar.current.component(Calendar.Component.hour, from: self)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh" // "a" prints "pm" or "am"
        let hourString = formatter.string(from: self) // "12 AM"
       // print("hourString",hourString)
        return hourString
    }

    var minute: Int {
        return Calendar.current.component(Calendar.Component.minute, from: self)
    }
    
    var ampm: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "a" // "a" prints "pm" or "am"
        let ampmString = formatter.string(from: self) // "12 AM"
        //print("ampmString",ampmString)
        return ampmString
    }

    
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.height
    }
}

extension String {
    
    var intValue: Int {
        return Int(self)!
    }

    
    func formatedDateString(inputFormat: String = "yyyy-MM-dd HH:mm:ss", outputFormat: String = "dd MMMM yyyy") -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = inputFormat
        if let convertedDate = formatter.date(from: self) {
            formatter.dateFormat = outputFormat
            let dateString = formatter.string(from: convertedDate)
            return dateString
        }else {
            let info = self.split(separator: " ")
            if !info.isEmpty {
                let dateInfo = info[0]
                let dateInfoSplit = dateInfo.split(separator: "-")
                if !dateInfoSplit.isEmpty {
                    let year = dateInfoSplit[0].description // year
                    
                    if dateInfoSplit.count > 1 {
                        let monthInfo = dateInfoSplit[1].description.intValue
                        if monthInfo > 0 && monthInfo < 13 {
                            let yearMonth = year + "-" + monthInfo.description
                            formatter.dateFormat = "yyyy-MMMM"
                            if let convertedDate = formatter.date(from: yearMonth) {
                                let dateString = formatter.string(from: convertedDate)
                                return dateString
                            }else {
                                return year
                            }
                        }else {
                            return year
                        }
                    }else {
                        return year
                    }
                    
                }else {
                    return ""
                }
            }else {
                return ""
            }
            
        }
        
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    func containSpecialCharacters() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9 ].*", options: [])
        //NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: nil, error: nil)!
        if regex.firstMatch(in: self, options:[], range: NSMakeRange(0, self.count)) != nil {
            //print("could not handle special characters")
            return true
        }
        return false
    }

    func containSpecialCharactersIncludingHyphen() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9 -].*", options: [])
        //NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: nil, error: nil)!
        if regex.firstMatch(in: self, options:[], range: NSMakeRange(0, self.count)) != nil {
            //print("could not handle special characters")
            return true
        }
        return false
    }
    
}

// Image resizing
extension UIImage {
    
    func compressToBelow1MB(sizeInKB: Int = 1000) -> UIImage{
        if let imageData = UIImageJPEGRepresentation(self, 1.0), Double(imageData.count / sizeInKB) <= 1.0 {
            return self
        }else {
            if let currentData = UIImageJPEGRepresentation(self, 0.5), Double(currentData.count / sizeInKB) <= 1.0 {
                let currentImage = UIImage(data: currentData)
                return currentImage!
            }else {
                if let currentData = UIImageJPEGRepresentation(self, 0.1) {
                    let currentImage = UIImage(data: currentData)
                    return currentImage!
                }else {
                    return self
                }

            }

        }

    }
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedtoSizeInKB() -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
        
        while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = UIImagePNGRepresentation(resizedImage)
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
        }
        
        return resizingImage
    }

}

extension UIScrollView {
    // Scroll to left
    func scrollToLeft(animated: Bool) {
        let leftOffset = CGPoint(x: -contentInset.left, y: 0)
        setContentOffset(leftOffset, animated: animated)
    }
    
    // Scroll to right
    func scrollToRight(animated: Bool) {
        let rightOffset = CGPoint(x: contentSize.width - bounds.size.width + contentInset.right, y: 0)
        
        if(rightOffset.x > 0) {
            setContentOffset(rightOffset, animated: animated)
        }
    }
    
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int = 2) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}


