//
//  PLAppState.swift
//  Popular
//
//  Created by Appzoc on 12/07/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

class PLAppState{
    private init(){}
    
    static let session:PLAppState = PLAppState()
    
    var isLoggedIn:Bool{
        if self.userID != 0{
            return true
        }else{
            return false
        }
    }
    
    var homeDelegate:RefreshHomePage?
    var isLaunchByNotification = false
    
    var userID:Int{
        if let id = UserDefaults.standard.object(forKey: "UserID") as? Int{
            return id
        }else{
            return 0
        }
    }
    
    var userMobileNumber:String{
        if let mobile = UserDefaults.standard.object(forKey: "UserMobile") as? String{
            return mobile
        }else{
            return ""
        }
    }
    
    var userName:String{
        if let name = UserDefaults.standard.object(forKey: "UserName") as? String{
            return name.lowercased().capitalized
        }else{
            return ""
        }
    }
    
    var isEmployee:Bool{
        if let isEmployee = UserDefaults.standard.object(forKey: "isEmployee") as? String{
            if isEmployee == "1"{
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    var deviceTocken: String{

        if let tocken = UserDefaults.standard.object(forKey: "DeviceToken") as? String{
            return tocken
        }else{
            return "123456"
        }
    }
    
    func logout(){
        UserDefaults.standard.removeObject(forKey: "UserID")
        UserDefaults.standard.removeObject(forKey: "UserName")
        UserDefaults.standard.set(false, forKey: "UserLoggedIn")
        UserDefaults.standard.removeObject(forKey: "isEmployee")
        UserDefaults.standard.removeObject(forKey: "UserMobile")
    }
    
    func commonShare(vc:UIViewController,view:UIView,carName:String,carModel:String,imgURL:String)
    {
        // text to share
        let text1 = "I was viewing \(carName)-\(carModel) on Popular App. I think you might be intrested in viewing it too."
        let Lnk = "\n\(imgURL)"
        let text2 = "\nDownload Popular App \(AppstoreLink)"
        
        // set up activity view controller
        let textToShare = [ text1,Lnk,text2 ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        vc.present(activityViewController, animated: true, completion: nil)
    }
    
    
}
