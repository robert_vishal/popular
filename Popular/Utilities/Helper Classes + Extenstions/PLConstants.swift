//
//  PLConstants.swift
//  Popular
//
//  Created by Appzoc-Macmini on 18/09/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

typealias Json = [String: Any]
typealias JsonArray = [Json]
typealias Information = [String: String]
typealias JsonImage = [String: [UIImage]]

let noDataAvailable = "No data available"
let notMentioned = "Not Mentioned"
let ruppees = "₹   "
let noDataFound = "No data found"

let emergancyNumber = "9387622222"//"18001238090"
let emergancyNumberFormatted = "9387-622-222" //"1 - 800 123 - 8090"
let allowedPhoneDigits = 13

