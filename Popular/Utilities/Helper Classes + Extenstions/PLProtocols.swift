//
//  PLProtocols.swift
//  Popular
//
//  Created by Appzoc on 21/03/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

public protocol InterfaceSettable{
    func setUpInterface()
}

