//
//  PPCoreDataManager.swift
//  Popular
//
//  Created by Appzoc on 24/10/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class CoreDataManager {
    
    //1
    static let sharedManager = CoreDataManager()
    //2.
    private init() {
        //managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    } // Prevent clients from creating another instance.
    
    //3
    
    let managedContext:NSManagedObjectContext

    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Popular")
        
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    //4
    func saveContext () {
        //let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        if managedContext.hasChanges {
            do {
                try managedContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func inserCarVarient(carVarient:PPCarVarient){
       // let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        
        let entity                        = NSEntityDescription.entity(forEntityName: "CarVarient",
                                                in: managedContext)!
        let coredatacarvarient:CarVarient = NSManagedObject(entity: entity,
                                     insertInto: managedContext) as! CarVarient
        carVarient.updateCoredata(entity: coredatacarvarient)
        self.saveContext()
    }
    
    func getcarEventForId(id:Int64) -> PPCarVarient? {
        //let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        
        var ppcarvarient: PPCarVarient?
        
        do {
            let fetchRequest       = NSFetchRequest<NSManagedObject>(entityName: "CarVarient")
            fetchRequest.predicate = NSPredicate(format: "id == %i", id)
            if let fetchedResults     = try managedContext.fetch(fetchRequest) as? [CarVarient]{
                if let aContact = fetchedResults.first {
                    ppcarvarient = PPCarVarient(entity: aContact)
                }
            }
        }
        catch {
            print ("fetch task failed", error)
        }
        return ppcarvarient
    }
    
}
